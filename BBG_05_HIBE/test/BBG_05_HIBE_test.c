// Copyright [2018] PG 0-RTT

#include <stdio.h>
#include <BBG_05_HIBE_types.h>
#include "relic/relic.h"
#include "relic/relic_test.h"
#include "BBG_05_HIBE.h"

static int setup_output_valid() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  int mk_valid, params_valid;

  TRY {
    TEST_BEGIN("bbg05_setup produces valid output") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);

      bbg05_master_key_is_valid(&mk_valid, mk);
      bbg05_public_params_are_valid(&params_valid, params);

      TEST_ASSERT(mk_valid, end);
      TEST_ASSERT(params_valid, end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);

      free(params);
      free(mk);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int keygen_inner_node_from_master_key_output_valid() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node
  unsigned id_depth = 3;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  bbg05_secret_key_inner_node_t *sk_inner;

  int sk_inner_valid;

  TRY {
    TEST_BEGIN("key generation of inner node from master key produces valid output") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_inner,
                                                                    mk,
                                                                    bbg05id,
                                                                    params) == STS_OK, end);

      bbg05_secret_key_inner_node_is_valid(&sk_inner_valid, sk_inner);

      TEST_ASSERT(sk_inner_valid, end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_secret_key_inner_node_to_zero(sk_inner);

      free(params);
      free(mk);
      free(sk_inner);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int keygen_leaf_node_from_master_key_output_valid() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node
  unsigned id_depth = 6;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  bbg05_secret_key_leaf_node_t *sk_leaf;

  int sk_leaf_valid;

  TRY {
    TEST_BEGIN("key generation of leaf node from master key produces valid output") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_leaf_node_from_master_key(&sk_leaf,
                                                                  mk,
                                                                  bbg05id,
                                                                  params) == STS_OK, end);

      bbg05_secret_key_leaf_node_is_valid(&sk_leaf_valid, sk_leaf);

      TEST_ASSERT(sk_leaf_valid, end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_secret_key_leaf_node_to_zero(sk_leaf);

      free(params);
      free(mk);
      free(sk_leaf);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int keygen_inner_node_from_inner_node_output_valid() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node identity
  unsigned id_depth = 3;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  // leaf node identity
  unsigned lower_inner_id_depth = 4;
  bbg05_identity_t *lower_inner_bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      lower_inner_id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < lower_inner_id_depth; i++) {
    lower_inner_bbg05id->id[i] = i;
  }
  lower_inner_bbg05id->depth = lower_inner_id_depth;

  bbg05_secret_key_inner_node_t *sk_inner;
  bbg05_secret_key_inner_node_t *sk_lower_inner;

  int sk_lower_inner_valid;

  TRY {
    TEST_BEGIN("key generation of inner node from inner node produces valid output") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_inner,
                                                                  mk,
                                                                  bbg05id,
                                                                  params) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_parent_full_identity(&sk_lower_inner,
                                                             sk_inner,
                                                             lower_inner_bbg05id,
                                                             params) == STS_OK, end);

      bbg05_secret_key_inner_node_is_valid(&sk_lower_inner_valid, sk_lower_inner);

      TEST_ASSERT(sk_lower_inner_valid, end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_secret_key_inner_node_to_zero(sk_inner);
      bbg05_set_secret_key_inner_node_to_zero(sk_lower_inner);

      free(params);
      free(mk);
      free(sk_inner);
      free(sk_lower_inner);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  free(lower_inner_bbg05id);

  return code;
}

static int keygen_leaf_node_from_inner_node_output_valid() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node identity
  unsigned id_depth = 4;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  // leaf node identity
  unsigned leaf_id_depth = 5;
  bbg05_identity_t *leaf_bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      leaf_id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < leaf_id_depth; i++) {
    leaf_bbg05id->id[i] = i;
  }
  leaf_bbg05id->depth = leaf_id_depth;

  bbg05_secret_key_inner_node_t *sk_inner;
  bbg05_secret_key_leaf_node_t *sk_leaf;

  int sk_leaf_valid;

  TRY {
    TEST_BEGIN("key generation of leaf node from inner node produces valid output") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_inner,
                                                                  mk,
                                                                  bbg05id,
                                                                  params) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_leaf_node_from_parent_full_identity(&sk_leaf,
                                                               sk_inner,
                                                               leaf_bbg05id,
                                                               params) == STS_OK, end);

      bbg05_secret_key_leaf_node_is_valid(&sk_leaf_valid, sk_leaf);

      TEST_ASSERT(sk_leaf_valid, end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_secret_key_inner_node_to_zero(sk_inner);
      bbg05_set_secret_key_leaf_node_to_zero(sk_leaf);

      free(params);
      free(mk);
      free(sk_inner);
      free(sk_leaf);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  free(leaf_bbg05id);

  return code;
}

static int encapsulation_output_valid() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t* key;
  int ciphertext_valid;

  TRY {
    TEST_BEGIN("encapsulate produces valid output") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_encapsulate(&ciphertext, &key, params, bbg05id) == STS_OK, end);

      bbg05_ciphertext_is_valid(&ciphertext_valid, ciphertext);

      TEST_ASSERT(ciphertext_valid, end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_ciphertext_to_zero(ciphertext);
      bbg05_set_key_to_zero(key);

      free(params);
      free(mk);
      free(ciphertext);
      free(key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int decapsulation_with_correct_secret_key_generated_from_master_key() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  bbg05_secret_key_inner_node_t *sk_leaf;

  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t* key;
  bbg05_key_t* decapsulatedKey;

  TRY {
    TEST_BEGIN("decapsulate with correct secret key generated from master key works") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_leaf,
                                                                 mk,
                                                                 bbg05id,
                                                                 params) == STS_OK, end);
      TEST_ASSERT(bbg05_encapsulate(&ciphertext, &key, params, bbg05id) == STS_OK, end);
      TEST_ASSERT(bbg05_decapsulate(&decapsulatedKey, sk_leaf, ciphertext, params) == STS_OK, end);

      TEST_ASSERT(bbg05_keys_are_equal(key, decapsulatedKey), end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_secret_key_inner_node_to_zero(sk_leaf);
      bbg05_set_ciphertext_to_zero(ciphertext);
      bbg05_set_key_to_zero(key);
      bbg05_set_key_to_zero(decapsulatedKey);

      free(params);
      free(mk);
      free(sk_leaf);
      free(ciphertext);
      free(key);
      free(decapsulatedKey);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int decapsulation_with_correct_secret_key_generated_from_sk_of_inner_node() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node identity
  unsigned id_depth = 4;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  // leaf node identity
  unsigned leaf_id_depth = 5;
  bbg05_identity_t *leaf_bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      leaf_id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < leaf_id_depth; i++) {
    leaf_bbg05id->id[i] = i;
  }
  leaf_bbg05id->depth = leaf_id_depth;

  bbg05_secret_key_inner_node_t *sk_inner;
  bbg05_secret_key_inner_node_t *sk_inner2;

  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t* key;
  bbg05_key_t* decapsulatedKey;

  TRY {
    TEST_BEGIN("decapsulate with correct secret key generated from secret key of inner node works") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_inner,
                                                                  mk,
                                                                  bbg05id,
                                                                  params) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_parent_full_identity(&sk_inner2,
                                                             sk_inner,
                                                             leaf_bbg05id,
                                                             params) == STS_OK, end);
      TEST_ASSERT(bbg05_encapsulate(&ciphertext, &key, params, leaf_bbg05id) == STS_OK, end);
      TEST_ASSERT(bbg05_decapsulate(&decapsulatedKey, sk_inner2, ciphertext, params) == STS_OK, end);

      TEST_ASSERT(bbg05_keys_are_equal(key, decapsulatedKey), end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_secret_key_inner_node_to_zero(sk_inner2);
      bbg05_set_ciphertext_to_zero(ciphertext);
      bbg05_set_key_to_zero(key);
      bbg05_set_key_to_zero(decapsulatedKey);

      free(params);
      free(mk);
      free(sk_inner2);
      free(ciphertext);
      free(key);
      free(decapsulatedKey);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  free(leaf_bbg05id);

  return code;
}

static int decapsulate_with_key_not_associated_to_encapsulating_id() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;

  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  bbg05_identity_t *bbg05id2 = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth - 1; i++) {
    bbg05id2->id[i] = i;
  }

  // identities shall be different
  bbg05id2->id[id_depth - 1] = bbg05id->id[id_depth - 1] + 1;

  bbg05id2->depth = id_depth;

  bbg05_secret_key_inner_node_t *sk_leaf;

  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t* key;
  bbg05_key_t* decapsulatedKey;

  TRY {
    TEST_BEGIN("decapsulate with key not associated to id used for encapsulation does NOT work") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_leaf,
                                                                 mk,
                                                                 bbg05id,
                                                                 params) == STS_OK, end);
      TEST_ASSERT(bbg05_encapsulate(&ciphertext, &key, params, bbg05id2) == STS_OK, end);
      TEST_ASSERT(bbg05_decapsulate(&decapsulatedKey, sk_leaf, ciphertext, params) == STS_OK, end);

      TEST_ASSERT(!bbg05_keys_are_equal(key, decapsulatedKey), end);

      bbg05_set_public_params_to_zero(params);
      bbg05_set_master_key_to_zero(mk);
      bbg05_set_secret_key_inner_node_to_zero(sk_leaf);
      bbg05_set_ciphertext_to_zero(ciphertext);
      bbg05_set_key_to_zero(key);
      bbg05_set_key_to_zero(decapsulatedKey);

      free(params);
      free(mk);
      free(sk_leaf);
      free(ciphertext);
      free(key);
      free(decapsulatedKey);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int public_params_serialization() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;
  bbg05_public_params_t *params_deserialized;
  uint8_t *serialized_params;

  TRY {
    TEST_BEGIN("public params serialization is correct") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);

      bbg05_serialize_public_params(&serialized_params, params);
      bbg05_deserialize_public_params(&params_deserialized,
                                      serialized_params);

      TEST_ASSERT(bbg05_public_params_are_equal(params, params_deserialized), end);

      bbg05_set_master_key_to_zero(mk);
      bbg05_set_public_params_to_zero(params);
      bbg05_set_public_params_to_zero(params_deserialized);

      free(mk);
      free(params);
      free(params_deserialized);
      free(serialized_params);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int master_key_serialization() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_master_key_t *mk_deserialized;
  bbg05_public_params_t* params;
  uint8_t *serialized_mk;

  TRY {
    TEST_BEGIN("master key serialization is correct") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);

      bbg05_serialize_master_key(&serialized_mk, mk);
      bbg05_deserialize_master_key(&mk_deserialized,
                                   serialized_mk);

      TEST_ASSERT(bbg05_master_keys_are_equal(mk, mk_deserialized), end);

      bbg05_set_master_key_to_zero(mk);
      bbg05_set_master_key_to_zero(mk_deserialized);
      bbg05_set_public_params_to_zero(params);

      free(mk);
      free(mk_deserialized);
      free(params);
      free(serialized_mk);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int secret_key_leaf_node_serialization() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;
  bbg05_secret_key_leaf_node_t* sk_leaf;
  bbg05_secret_key_leaf_node_t* sk_leaf_deserialized;
  uint8_t *serialized_sk_leaf;

  // inner node
  unsigned id_depth = 6;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  TRY {
    TEST_BEGIN("secret key of leaf node serialization is correct") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_leaf_node_from_master_key(&sk_leaf,
                                                                 mk,
                                                                 bbg05id,
                                                                 params) == STS_OK, end);

      bbg05_serialize_secret_key_leaf_node(&serialized_sk_leaf, sk_leaf);
      bbg05_deserialize_secret_key_leaf_node(&sk_leaf_deserialized,
                                             serialized_sk_leaf);

      TEST_ASSERT(bbg05_secret_keys_leaf_nodes_are_equal(sk_leaf, sk_leaf_deserialized), end);

      bbg05_set_master_key_to_zero(mk);
      bbg05_set_public_params_to_zero(params);
      bbg05_set_secret_key_leaf_node_to_zero(sk_leaf);
      bbg05_set_secret_key_leaf_node_to_zero(sk_leaf_deserialized);

      free(mk);
      free(params);
      free(sk_leaf);
      free(sk_leaf_deserialized);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int secret_key_inner_node_serialization() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;
  bbg05_secret_key_inner_node_t* sk_inner;
  bbg05_secret_key_inner_node_t* sk_inner_deserialized;
  uint8_t *serialized_sk_inner;

  // inner node
  unsigned id_depth = 4;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  TRY {
    TEST_BEGIN("secret key of inner node serialization is correct") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_inner,
                                                                  mk,
                                                                  bbg05id,
                                                                  params) == STS_OK, end);

      bbg05_serialize_secret_key_inner_node(&serialized_sk_inner, sk_inner);
      bbg05_deserialize_secret_key_inner_node(&sk_inner_deserialized,
                                              serialized_sk_inner);

      TEST_ASSERT(bbg05_secret_keys_inner_nodes_are_equal(sk_inner, sk_inner_deserialized), end);

      bbg05_set_master_key_to_zero(mk);
      bbg05_set_public_params_to_zero(params);
      bbg05_set_secret_key_inner_node_to_zero(sk_inner);
      bbg05_set_secret_key_inner_node_to_zero(sk_inner_deserialized);

      free(mk);
      free(params);
      free(sk_inner);
      free(sk_inner_deserialized);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int identity_serialization() {
  int code = STS_ERR;

  bbg05_identity_t* identity_deserialized;
  uint8_t *serialized_identity;


  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  TRY {
    TEST_BEGIN("identity serialization is correct") {
      bbg05_serialize_identity(&serialized_identity, bbg05id);
      bbg05_deserialize_identity(&identity_deserialized,
                            serialized_identity);

      TEST_ASSERT(bbg05_identities_are_equal(bbg05id, identity_deserialized), end);

      free(serialized_identity);
      free(identity_deserialized);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int ciphertext_serialization() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;
  bbg05_secret_key_inner_node_t* sk_leaf;
  bbg05_ciphertext_t* ciphertext;
  bbg05_ciphertext_t* ciphertext_deserialized;
  uint8_t *serialized_ciphertext;
  bbg05_key_t *key;


  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  TRY {
    TEST_BEGIN("ciphertext serialization is correct") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_leaf,
                                                                 mk,
                                                                 bbg05id,
                                                                 params) == STS_OK, end);
      TEST_ASSERT(bbg05_encapsulate(&ciphertext, &key, params, bbg05id) == STS_OK, end);

      bbg05_serialize_ciphertext(&serialized_ciphertext, ciphertext);
      bbg05_deserialize_ciphertext(&ciphertext_deserialized,
                                             serialized_ciphertext);

      TEST_ASSERT(bbg05_ciphertexts_are_equal(ciphertext, ciphertext_deserialized), end);

      bbg05_set_master_key_to_zero(mk);
      bbg05_set_public_params_to_zero(params);
      bbg05_set_secret_key_inner_node_to_zero(sk_leaf);
      bbg05_set_ciphertext_to_zero(ciphertext);
      bbg05_set_ciphertext_to_zero(ciphertext_deserialized);
      bbg05_set_key_to_zero(key);

      free(mk);
      free(params);
      free(sk_leaf);
      free(ciphertext);
      free(ciphertext_deserialized);
      free(key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}

static int key_serialization() {
  int code = STS_ERR;

  unsigned MAX_DEPTH = 5;

  bbg05_master_key_t* mk;
  bbg05_public_params_t* params;
  bbg05_secret_key_inner_node_t* sk_leaf;
  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t *key;
  bbg05_key_t* key_deserialized;
  uint8_t *serialized_key;


  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  TRY {
    TEST_BEGIN("key serialization is correct") {
      TEST_ASSERT(bbg05_setup(&mk, &params, MAX_DEPTH) == STS_OK, end);
      TEST_ASSERT(bbg05_key_generation_inner_node_from_master_key(&sk_leaf,
                                                                 mk,
                                                                 bbg05id,
                                                                 params) == STS_OK, end);
      TEST_ASSERT(bbg05_encapsulate(&ciphertext, &key, params, bbg05id) == STS_OK, end);

      bbg05_serialize_key(&serialized_key, key);
      bbg05_deserialize_key(&key_deserialized,
                            serialized_key);

      TEST_ASSERT(bbg05_keys_are_equal(key, key_deserialized), end);

      bbg05_set_master_key_to_zero(mk);
      bbg05_set_public_params_to_zero(params);
      bbg05_set_secret_key_inner_node_to_zero(sk_leaf);
      bbg05_set_ciphertext_to_zero(ciphertext);
      bbg05_set_key_to_zero(key_deserialized);
      bbg05_set_key_to_zero(key);

      free(mk);
      free(params);
      free(sk_leaf);
      free(ciphertext);
      free(key_deserialized);
      free(key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free(bbg05id);
  return code;
}


int main(void) {
  if (bbg05_init() != STS_OK) {
    core_clean();
    return 1;
  }

  if (setup_output_valid() != STS_OK) {
    core_clean();
    return 1;
  }

  if (keygen_inner_node_from_master_key_output_valid() != STS_OK) {
    core_clean();
    return 1;
  }

  if (keygen_leaf_node_from_master_key_output_valid() != STS_OK) {
    core_clean();
    return 1;
  }

  if (keygen_inner_node_from_inner_node_output_valid() != STS_OK) {
    core_clean();
    return 1;
  }

  if (keygen_leaf_node_from_inner_node_output_valid() != STS_OK) {
    core_clean();
    return 1;
  }

  if (encapsulation_output_valid() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulation_with_correct_secret_key_generated_from_master_key() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulation_with_correct_secret_key_generated_from_sk_of_inner_node() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_key_not_associated_to_encapsulating_id() != STS_OK) {
    core_clean();
    return 1;
  }

  if (public_params_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (master_key_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (secret_key_leaf_node_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (secret_key_inner_node_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (identity_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (ciphertext_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (key_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  core_clean();
  return 0;
}
