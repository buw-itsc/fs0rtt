// Copyright [2018] PG 0-RTT

#ifndef BBG_05_HIBE_INCLUDE_BBG_05_HIBE_UTIL_H_
#define BBG_05_HIBE_INCLUDE_BBG_05_HIBE_UTIL_H_

#include <stdio.h>
#include <relic/relic.h>
#include "BBG_05_HIBE_types.h"

// BN curves work with pairings of type 3, i.e. G1 x G2 -> GT, G1 !=
// G2 and there is no efficient homomorphism between G1 and G2. The paper
// "Hierarchical Identity Based Encryption with Constant Size Ciphertext" by
// Boneh, Boyen and Goh assumes a pairing of type 1, i.e. G1 = G2.

// G_1 and G_2 of the BN curves are additive groups. The paper "Hierarchical
// Identity Based Encryption with Constant Size Ciphertext" by
// Boneh, Boyen and Goh which defines this scheme assumes that G_1 and G_2 are
// multiplicative groups. In order to not call e.g. the function g1_mul(pk->w,
// msk->g, msk->gamma) when the paper states pk.w = msk.g^(msk.gamma), we
// instead use these macros.
#define g1_exponentiate(R, P, Q) g1_mul(R, P, Q)
#define g2_exponentiate(R, P, Q) g2_mul(R, P, Q)
#define g1_multiply(R, P, Q) g1_add(R, P, Q)
#define g2_multiply(R, P, Q) g2_add(R, P, Q)

#define G1_SIZE_COMPRESSED (1 + FP_BYTES)
#define G2_SIZE_COMPRESSED (1 + 2 * FP_BYTES)
#define GT_SIZE_COMPRESSED (8 * FP_BYTES)

#define BBG_05_KEY_SIZE GT_SIZE_COMPRESSED
#define BBG_05_CIPHERTEXT_SIZE (2 * G1_SIZE_COMPRESSED + 2 * G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED)
#define BBG_05_MASTER_KEY_SIZE G1_SIZE_COMPRESSED
#define BBG_05_SECRET_KEY_LEAF_NODE_SIZE (G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED)


/**
 * Prints the given master key on the console.
 *
 * @param[in] master_key       - the master key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_print_master_key(const bbg05_master_key_t *master_key);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given secret key of a leafe node on the console.
 *
 * @param[in] sk       - the secret key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_print_secret_key_leaf_node(const bbg05_secret_key_leaf_node_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given secret key of an inner node on the console.
 *
 * @param[in] sk       - the secret key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_print_secret_key_inner_node(const bbg05_secret_key_inner_node_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given public parameters on the console.
 *
 * @param[in] public_params        - the public parameters that are printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_print_public_params(const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given ciphertext on the console.
 *
 * @param[in] ciphertext        - the ciphertext that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_print_ciphertext(const bbg05_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given key on the console.
 *
 * @param[in] key        - the key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_print_key(const bbg05_key_t *key);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given identity on the console.
 *
 * @param[in] key        - the key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_print_identity(const bbg05_identity_t *identity);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given master key.
 *
 * @param[in] master_key       - the master key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_set_master_key_to_zero(bbg05_master_key_t *master_key);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given secret key of an inner node.
 *
 * @param[in] sk       - the secret key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_set_secret_key_inner_node_to_zero(bbg05_secret_key_inner_node_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given secret key of a leaf node.
 *
 * @param[in] sk       - the secret key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_set_secret_key_leaf_node_to_zero(bbg05_secret_key_leaf_node_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given public parameters.
 *
 * @param[in] public_params        - the public parameters that are freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_set_public_params_to_zero(bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given ciphertext.
 *
 * @param[in] ciphertext        - the ciphertext that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_set_ciphertext_to_zero(bbg05_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given key.
 *
 * @param[in] key        - the key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_set_key_to_zero(bbg05_key_t *key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given master key is valid.
 *
 * @param[in] master_key       - the master key that is checked
 *
 * @return true if the given master key is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_master_key_is_valid(int *valid, const bbg05_master_key_t *master_key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given secret key associated to an inner node is valid.
 *
 * @param[in] sk       - the secret key that is checked
 *
 * @return true if the given secret key is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_secret_key_inner_node_is_valid(int *valid,
                                         const bbg05_secret_key_inner_node_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given secret key associated to a leaf node is valid.
 *
 * @param[in] sk       - the secret key that is checked
 *
 * @return true if the given secret key is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_secret_key_leaf_node_is_valid(int *valid,
                                        const bbg05_secret_key_leaf_node_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given public parameters are valid.
 *
 * @param[in] public_params        - the public parameters that are checked
 *
 * @return true if the given public parameters are valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_public_params_are_valid(int *valid,
                                  const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given ciphertext is valid.
 *
 * @param[in] ciphertext        - the ciphertext that is checked
 *
 * @return true if the given ciphertext is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_ciphertext_is_valid(int *valid, const bbg05_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given identity under the given public parameters is valid.
 *
 * @param[in] identity        - the identity that is checked
 *
 * @return true if the given identity is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_identity_is_valid(int *valid, const bbg05_identity_t *identity,
                            const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Converts a given key to a bit string of length of the security parameter
 * using the KDF2 key derivation function.
 *
 * @param[out] bit_string       - the key represented as bit string
 * @param[in] key               - the key that is converted
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_convert_key_to_bit_string(uint8_t **bit_string,
                                    const bbg05_key_t *key);
#ifdef __cplusplus
}
#endif

/**
 * Writes the given G_1 element in the given byte array using point compression.
 *
 * @param[out] bin          - The byte array in which the G_1 element is
 *                            written.
 * @param[in] element       - The G_1 element that is written in the byte array.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int bbg05_g1_compress(uint8_t **bin, g1_t element);

/**
 * Writes the given G_2 element in the given byte array using point compression.
 *
 * @param[out] bin          - The byte array in which the G_2 element is
 *                            written.
 * @param[in] element       - The G_2 element that is written in the byte array.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int bbg05_g2_compress(uint8_t **bin, g2_t element);

/**
 * Writes the given G_T element in the given byte array using point compression.
 *
 * @param[out] bin          - The byte array in which the G_T element is
 * written.
 * @param[in] element       - The G_T element that is written in the byte array.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int bbg05_gt_compress(uint8_t **bin, gt_t element);

/**
 * Reads in a G_1 element from the given byte array.
 *
 * @param[out] element      - The G_1 element to which the byte array is read
 *                            in.
 * @param[in] bin           - The byte array that is read.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int bbg05_g1_decompress(g1_t element, uint8_t *bin);

/**
 * Reads in a G_2 element from the given byte array.
 *
 * @param[out] element      - The G_2 element to which the byte array is read
 *                            in.
 * @param[in] bin           - The byte array that is read.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int bbg05_g2_decompress(g2_t element, uint8_t *bin);

/**
 * Reads in a G_T element from the given byte array.
 *
 * @param[out] element      - The G_T element to which the byte array is read
 *                            in.
 * @param[in] bin           - The byte array that is read.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int bbg05_gt_decompress(gt_t element, uint8_t *bin);

/**
 * Prints the given binary G_1 element on the console.
 *
 * @param[in] bin        - the element that is printed
 */
int bbg05_g1_print_bin(uint8_t *bin);

/**
 * Prints the given binary G_2 element on the console.
 *
 * @param[in] bin        - the element that is printed
 */
int bbg05_g2_print_bin(uint8_t *bin);

/**
 * Prints the given binary G_T element on the console.
 *
 * @param[in] bin        - the element that is printed
 */
int bbg05_gt_print_bin(uint8_t *bin);

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_serialize_public_params(uint8_t **serialized,
                                   const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_deserialize_public_params(bbg05_public_params_t **public_params,
                                     const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_serialize_master_key(uint8_t **serialized,
                                const bbg05_master_key_t *master_key);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_deserialize_master_key(bbg05_master_key_t **master_key,
                                  const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_serialize_secret_key_inner_node(uint8_t **serialized,
                                           const bbg05_secret_key_inner_node_t *secret_key);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_deserialize_secret_key_inner_node(bbg05_secret_key_inner_node_t **secret_key,
                                             const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_serialize_secret_key_leaf_node(uint8_t **serialized,
                                          const bbg05_secret_key_leaf_node_t *secret_key);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_deserialize_secret_key_leaf_node(bbg05_secret_key_leaf_node_t **secret_key,
                                            const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_serialize_ciphertext(uint8_t **serialized,
                                const bbg05_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_deserialize_ciphertext(bbg05_ciphertext_t **ciphertext,
                                  const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_serialize_key(uint8_t **serialized, const bbg05_key_t *key);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_deserialize_key(bbg05_key_t **key, const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_serialize_identity(uint8_t **serialized,
                              const bbg05_identity_t *identity);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
void bbg05_deserialize_identity(bbg05_identity_t **identity,
                                const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given master keys are equal.
 *
 * @param[in] l     - the first master key
 * @param[in] r     - the second master key
 *
 * @return True if the master keys are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_master_keys_are_equal(const bbg05_master_key_t *l,
                                const bbg05_master_key_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given public parameters are equal.
 *
 * @param[in] l     - the first public params
 * @param[in] r     - the second public params
 *
 * @return True if the public params are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_public_params_are_equal(const bbg05_public_params_t *l,
                                  const bbg05_public_params_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given secret keys of inner nodes are equal.
 *
 * @param[in] l     - the first secret key of an inner node
 * @param[in] r     - the second secret key of an inner node
 *
 * @return True if the secret keys of inner nodes are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_secret_keys_inner_nodes_are_equal(const bbg05_secret_key_inner_node_t *l,
                                            const bbg05_secret_key_inner_node_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given secret keys of leaf nodes are equal.
 *
 * @param[in] l     - the first secret key of a leaf node
 * @param[in] r     - the second secret key of a leaf node
 *
 * @return True if the secret keys of leaf nodes are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_secret_keys_leaf_nodes_are_equal(const bbg05_secret_key_leaf_node_t *l,
                                           const bbg05_secret_key_leaf_node_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given ciphertexts are equal.
 *
 * @param[in] l     - the first ciphertext
 * @param[in] r     - the second ciphertext
 *
 * @return True if the ciphertexts are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_ciphertexts_are_equal(const bbg05_ciphertext_t *l,
                                const bbg05_ciphertext_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given keys are equal.
 *
 * @param[in] l     - the first key
 * @param[in] r     - the second key
 *
 * @return True if the keys are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_keys_are_equal(const bbg05_key_t *l, const bbg05_key_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given identities are equal.
 *
 * @param[in] l     - the first identity
 * @param[in] r     - the second identity
 *
 * @return True if the identity are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_identities_are_equal(const bbg05_identity_t *l,
                               const bbg05_identity_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Returns the size of the given public params.
 *
 * @param[in] public_params     - the public params of which the size is
 *                                returned
 *
 * @return the size of the given public params.
 */
#ifdef __cplusplus
extern "C" {
#endif
unsigned bbg05_get_public_params_size(const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

#endif  // BBG_05_HIBE_INCLUDE_BBG_05_HIBE_UTIL_H_
