// Copyright [2019] PG 0-RTT

#ifndef BBG_05_HIBE_BBG05_HIBE_GLOBAL_H
#define BBG_05_HIBE_BBG05_HIBE_GLOBAL_H

/**
 * Global pointer to the RELIC context.
 */
extern ctx_t *bbg05_context;

/**
 * Attribute to make threads joinable.
 */
extern pthread_attr_t bbg05_attr_joinable;

#endif //BBG_05_HIBE_BBG05_HIBE_GLOBAL_H
