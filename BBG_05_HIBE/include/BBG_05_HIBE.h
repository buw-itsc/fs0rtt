// Copyright [2018] PG 0-RTT

#ifndef BBG_05_HIBE_INCLUDE_BBG_05_HIBE_H_
#define BBG_05_HIBE_INCLUDE_BBG_05_HIBE_H_

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <inttypes.h>
#include <relic/relic.h>
#include "BBG_05_HIBE_types.h"
#include "BBG_05_HIBE_util.h"

#define SECURITY_PARAMETER MD_LEN_SH256

/**
 * Initializes the library.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_init();
#ifdef __cplusplus
}
#endif


/**
 * Executes the setup algorithm and generates the master key and the
 * public parameters.
 *
 * @param[out] master_key           - the resulting master key
 * @param[out] public_params        - the resulting public key
 * @param[in] max_depth             - the maximum depth of the HIBE system
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_setup(bbg05_master_key_t **master_key,
                bbg05_public_params_t **public_params,
                unsigned max_depth);
#ifdef __cplusplus
}
#endif


/**
 * Executes the key generation algorithm and generates a secret key for the
 * given ID of an inner node from the master key.
 *
 * @param[out] secret_key           - the resulting secret key
 * @param[in] master_key            - the master key from which the
 *                                    secret key is derived
 * @param[in] identity              - the ID for which the secret key is
 *                                    generated
 * @param[in] public_params         - the public parameters
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_key_generation_inner_node_from_master_key(
    bbg05_secret_key_inner_node_t **secret_key,
    const bbg05_master_key_t *master_key,
    const bbg05_identity_t *identity,
    const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Executes the key generation algorithm and generates a secret key for the
 * given ID of a leaf node from the master key.
 *
 * @param[out] secret_key           - the resulting secret key
 * @param[in] master_key            - the master key from which the
 *                                    secret key is derived
 * @param[in] identity              - the ID for which the secret key is
 *                                    generated
 * @param[in] public_params         - the public parameters
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_key_generation_leaf_node_from_master_key(bbg05_secret_key_leaf_node_t **secret_key,
                                                   const bbg05_master_key_t *master_key,
                                                   const bbg05_identity_t *identity,
                                                   const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif


/**
 * Executes the key generation algorithm and generates a secret key for the
 * given ID of an inner node from the parent secret key.
 *
 * @param[out] secret_key           - the resulting secret key
 * @param[in] parent_secret_key     - the parent secret key from which a new
 *                                    one is derived
 * @param[in] identity      - the ID for which the secret key is
 *                                    generated
 * @param[in] public_params         - the public parameters
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_key_generation_inner_node_from_parent_full_identity(
    bbg05_secret_key_inner_node_t **secret_key,
    const bbg05_secret_key_inner_node_t *parent_secret_key,
    const bbg05_identity_t *identity,
    const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif


/**
 * Executes the key generation algorithm and generates a secret key for the
 * given ID of an inner node from the parent secret key.
 *
 * @param[out] secret_key           - the resulting secret key
 * @param[in] parent_secret_key     - the parent secret key from which a new
 *                                    one is derived
 * @param[in] encoded_identity      - the ID for which the secret key is
 *                                    generated
 * @param[in] public_params         - the public parameters
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_key_generation_inner_node_from_parent(bbg05_secret_key_inner_node_t **secret_key,
                                                const bbg05_secret_key_inner_node_t *parent_secret_key,
                                                const bn_t encoded_identity,
                                                const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif


/**
 * Executes the key generation algorithm and generates a secret key for the
 * given ID of a leaf node from the parent secret key.
 *
 * @param[out] secret_key           - the resulting secret key
 * @param[in] parent_secret_key     - the parent secret key from which a new
 *                                    one is derived
 * @param[in] encoded_identity      - the ID for which the secret key is
 *                                    generated
 * @param[in] public_params         - the public parameters
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_key_generation_leaf_node_from_parent_full_identity(
    bbg05_secret_key_leaf_node_t **secret_key,
    const bbg05_secret_key_inner_node_t *parent_secret_key,
    const bbg05_identity_t *identity,
    const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Executes the key generation algorithm and generates a secret key for the
 * given ID of a leaf node from the parent secret key.
 *
 * @param[out] secret_key           - the resulting secret key
 * @param[in] parent_secret_key     - the parent secret key from which a new
 *                                    one is derived
 * @param[in] encoded_identity      - the ID for which the secret key is
 *                                    generated
 * @param[in] public_params         - the public parameters
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_key_generation_leaf_node_from_parent(bbg05_secret_key_leaf_node_t **secret_key,
                                               const bbg05_secret_key_inner_node_t *parent_secret_key,
                                               const bn_t encoded_identity,
                                               const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Executes the encapsulate algorithm and generates a random key and a
 * ciphertext encapsulating the key for the identity.
 *
 * @param[out] ciphertext       - the resulting ciphertext
 * @param[out] key              - the resulting random key
 * @param[in] public_params     - the public parameters
 * @param[in] identity          - the identity for which the key is encapsulated
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_encapsulate(bbg05_ciphertext_t **ciphertext,
                      bbg05_key_t **key,
                      const bbg05_public_params_t *public_params,
                      const bbg05_identity_t *identity);
#ifdef __cplusplus
}
#endif

/**
 * Executes the CPA secure encapsulate algorithm and generates a random key and a
 * ciphertext encapsulating the key for the identity.
 *
 * @param[out] ciphertext       - the resulting ciphertext
 * @param[out] key              - the resulting random key
 * @param[in] public_params     - the public parameters
 * @param[in] identity          - the identity for which the key is encapsulated
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_cpa_encapsulate(bbg05_ciphertext_t **ciphertext,
                          bbg05_key_t **key,
                          const bbg05_public_params_t *public_params,
                          const bbg05_identity_t *identity,
                          const bn_t encoded_verfication_key);
#ifdef __cplusplus
}
#endif

/**
 * Encrypts the given message under the given identity.
 *
 * @param[out] ciphertext       - the resulting ciphertext
 * @param[in] message              - the message to encrypt
 * @param[in] public_params     - the public parameters
 * @param[in] identity          - the identity for which the key is encapsulated
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_encrypt(bbg05_ciphertext_t **ciphertext,
                  gt_t message,
                  const bbg05_public_params_t *public_params,
                  const bbg05_identity_t *identity,
                  const bn_t encoded_verfication_key);
#ifdef __cplusplus
}
#endif


/**
 * Executes the CHK transformation and the decapsulate algorithm and computes
 * the key encapsulated in the given ciphertext.
 *
 * @param[out] key              - the resulting key
 * @param[in] secret_key        - the secret key corresponding to
 *                                the ID of a leaf nod for which the ciphertext
 *                                was encapsulated
 * @param[in] ciphertext        - the ciphertext that is decapsulated
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_decapsulate(bbg05_key_t **key,
                      const bbg05_secret_key_inner_node_t *secret_key,
                      const bbg05_ciphertext_t *ciphertext,
                      const bbg05_public_params_t *public_params);
#ifdef __cplusplus
}
#endif

/**
 * Executes the CPA secure decapsulate algorithm and computes the key
 * encapsulated in the given ciphertext.
 *
 * @param[out] key              - the resulting key
 * @param[in] secret_key        - the secret key corresponding to
 *                                the ID of a leaf nod for which the ciphertext
 *                                was encapsulated
 * @param[in] ciphertext        - the ciphertext that is decapsulated
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_cpa_decapsulate(bbg05_key_t **key,
                          const bbg05_secret_key_leaf_node_t *secret_key,
                          const bbg05_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Hashes the given identity to Z_p^* by using sha256 and interpreting the hash
 * value as binary coded number which is then reduced by the group order.
 *
 * @param[out] hashed_id            - the resulting hash value from Z_p^*
 * @param[in] id                    - the identity that is hashed
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_hash_id(bn_t hashed_id, const unsigned id);
#ifdef __cplusplus
}
#endif

/**
 * Converts the given bbg05_identity_t to a vector (Z_p^*)^k by hashing each
 * component of the identity.
 *
 * @param[out] identity_zp_vector       - the resulting hash value from Z_p^*
 * @param[in] identity                 - the identity that is hashed
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int bbg05_convert_identity_to_zp_vector(bn_t *identity_zp_vector,
                                        const bbg05_identity_t *identity);
#ifdef __cplusplus
}
#endif

/**
 * Encodes the identity, i.e. prepending it with a zero. The resulting identity
 * has a depth of identity->depth + 1.
 *
 * @param[out] encoded_identity      - The resulting encoded identity.
 * @param[in] identity              - The identity that will be encoded.
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_encode(bbg05_identity_t **encoded_identity,
                  const bbg05_identity_t *identity);
#ifdef __cplusplus
}
#endif

/**
 * Encodes and hashes the given verfication key.
 *
 * @param[out] encoded_verification_key     - The resulting encoded and hashed
 *                                            verification key
 * @param[in] verification_key              - The verification key that is
 *                                            encoded.
 */
#ifdef __cplusplus
extern "C" {
#endif
void bbg05_encode_verification_key(bn_t encoded_verification_key,
                                   g2_t verification_key);
#ifdef __cplusplus
}
#endif

#endif  // BBG_05_HIBE_INCLUDE_BBG_05_HIBE_H_
