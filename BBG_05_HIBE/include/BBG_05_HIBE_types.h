// Copyright [2018] PG 0-RTT

#ifndef BBG_05_HIBE_INCLUDE_BBG_05_HIBE_TYPES_H_
#define BBG_05_HIBE_INCLUDE_BBG_05_HIBE_TYPES_H_

#include <inttypes.h>

typedef struct {
  uint8_t *mk;  // g1_t
} bbg05_master_key_t;

typedef struct {
  uint8_t *g;  // g2_t
  uint8_t *g1;  // g2_t

  // e(g1,g2)
  uint8_t *pairing_g1_g2;  // gt_t

  uint8_t *g3;  // g1_t
  unsigned max_depth;
  uint8_t *h[];  // g1_t
} bbg05_public_params_t;

typedef struct {
  uint8_t *a0;  // g1_t
  uint8_t *a1;  // g2_t

  /* The ID associated to this secret key represented as an element in g1_t.
 * Stored for faster key delegation.
 * Corresponds to: public_params.g3 * prod_{i=1}^{id_depth} h_{i}^{I_{i}}
 * where id_depth = public_params.max_depth - num_delegatable_levels
 * */
  uint8_t *associated_id;  // g1_t

  unsigned num_delegatable_levels;

  uint8_t *b[];  // g1_t
} bbg05_secret_key_inner_node_t;

typedef struct {
  uint8_t *a0;  // g1_t
  uint8_t *a1;  // g2_t
} bbg05_secret_key_leaf_node_t;

typedef struct {
  uint8_t *a;  // gt_t
  uint8_t *b;  // g2_t
  uint8_t *c;  // g1_t
  uint8_t *verification_key;  // g2_t
  uint8_t *signature;  // g1_t
} bbg05_ciphertext_t;

typedef struct {
  uint8_t *k;  // gt_t
} bbg05_key_t;

typedef struct {
  unsigned depth;
  unsigned id[];
} bbg05_identity_t;

#endif  // BBG_05_HIBE_INCLUDE_BBG_05_HIBE_TYPES_H_
