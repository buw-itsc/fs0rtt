#include "BBG_05_HIBE_types.h"
#include "BBG_05_HIBE.h"
#include "BBG_05_HIBE_global.h"

#define IDENTITY_PREFIX 0
#define VERIFICATION_KEY_PREFIX 1

ctx_t *bbg05_context;

int bbg05_init() {
  if (core_init() != STS_OK) {
    core_clean();
    return STS_ERR;
  }

  //initializes BN-256 curve
  if (ep_param_set_any_pairf() == STS_ERR) {
    THROW(ERR_NO_CURVE);
    core_clean();
    return STS_ERR;
  }

  // Set the global pointer to the context
  bbg05_context = core_get();

  return STS_OK;
}

int bbg05_setup(bbg05_master_key_t **master_key,
                bbg05_public_params_t **public_params,
                unsigned max_depth) {
  *master_key = malloc(sizeof(bbg05_master_key_t));

  *public_params = malloc(offsetof(bbg05_public_params_t, h)
                              + (max_depth + 1) * sizeof(uint8_t *));

  if (!*master_key || !*public_params) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t master_key_mk;
  g2_t public_params_g;
  g2_t public_params_g1;
  g1_t public_params_g2;
  gt_t public_params_pairing_g1_g2;
  g1_t public_params_g3;
  g1_t public_params_h[max_depth + 1];

  bn_t alpha;
  bn_t group_order;

  g1_null(master_key_mk);
  g2_null(public_params_g);
  g2_null(public_params_g1);
  g1_null(public_params_g2);
  gt_null(public_params_pairing_g1_g2);
  g1_null(public_params_g3);

  for (unsigned i = 0; i < max_depth + 1; i++) {
    g1_null(public_params_h[i]);
  }

  bn_null(alpha);
  bn_null(group_order);

  TRY {
    g1_new(master_key_mk);
    g2_new(public_params_g);
    g2_new(public_params_g1);
    g1_new(public_params_g2);
    g2_new(public_params_pairing_g1_g2);
    g1_new(public_params_g3);

    for (unsigned i = 0; i < max_depth + 1; i++) {
      g1_new(public_params_h[i]);
    }

    bn_new(alpha);
    bn_new(group_order);

    g2_rand(public_params_g);
    g1_rand(public_params_g2);
    g1_rand(public_params_g3);

    for (unsigned i = 0; i < max_depth + 1; i++) {
      g1_rand(public_params_h[i]);
    }

    g2_get_ord(group_order);

    // choose random gamma from Z_p^*
    bn_rand_mod(alpha, group_order);

    g2_exponentiate(public_params_g1, public_params_g, alpha);
    g1_exponentiate(master_key_mk, public_params_g2, alpha);

    pc_map(public_params_pairing_g1_g2, public_params_g2, public_params_g1);

    int compress_status =
        bbg05_g1_compress(&(*master_key)->mk, master_key_mk);
    compress_status |=
        bbg05_g2_compress(&(*public_params)->g, public_params_g);
    compress_status |=
        bbg05_g2_compress(&(*public_params)->g1, public_params_g1);
    compress_status |=
        bbg05_gt_compress(&(*public_params)->pairing_g1_g2, public_params_pairing_g1_g2);
    compress_status |=
        bbg05_g1_compress(&(*public_params)->g3, public_params_g3);
    for (unsigned i = 0; i < max_depth + 1; i++) {
      compress_status |=
          bbg05_g1_compress(&((*public_params)->h[i]),
                            public_params_h[i]);
    }
    if (compress_status) {
      return STS_ERR;
    }
    (*public_params)->max_depth = max_depth + 1;

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_free(master_key_mk);
    g2_free(public_params_g);
    g2_free(public_params_g1);
    g1_free(public_params_g2);
    gt_free(public_params_pairing_g1_g2);
    g1_free(public_params_g3);

    for (unsigned i = 0; i < max_depth + 1; i++) {
      g1_free(public_params_h[i]);
    }

    bn_free(alpha);
    bn_free(group_order);
  }
  return result_status;
}

int bbg05_key_generation_inner_node_from_master_key(
    bbg05_secret_key_inner_node_t **secret_key,
    const bbg05_master_key_t *master_key,
    const bbg05_identity_t *identity,
    const bbg05_public_params_t *public_params) {
  int master_key_is_valid;
  int identity_is_valid;
  int public_params_are_valid;
  int is_valid_status =
      bbg05_master_key_is_valid(&master_key_is_valid, master_key);
  is_valid_status |= bbg05_identity_is_valid(&identity_is_valid,
                                             identity,
                                             public_params);
  is_valid_status |= bbg05_public_params_are_valid(&public_params_are_valid,
                                                   public_params);
  if (!master_key_is_valid || !identity_is_valid || !public_params_are_valid
      || is_valid_status) {
    return STS_ERR;
  }

  unsigned num_delegatable_levels = public_params->max_depth - identity->depth;

  *secret_key = malloc(offsetof(bbg05_secret_key_inner_node_t, b) +
      num_delegatable_levels * sizeof(uint8_t *));

  if (!*secret_key) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t secret_key_a0;
  g2_t secret_key_a1;
  g1_t secret_key_associated_id;
  g1_t secret_key_b[num_delegatable_levels];

  bn_t r;
  g1_t product_h_to_the_identity_to_the_r;
  g1_t h_i_to_the_identity_i;
  bn_t identity_zp_vector[identity->depth];
  bn_t group_order;

  g1_t public_params_h[public_params->max_depth];
  g2_t public_params_g;
  g1_t public_params_g3;
  g1_t master_key_mk;

  g1_null(secret_key_a0);
  g2_null(secret_key_a1);
  g1_null(secret_key_associated_id);

  for (unsigned i = 0; i < num_delegatable_levels; i++) {
    g2_null(secret_key_b[i]);
  }

  bn_null(r);
  g1_null(product_h_to_the_identity_to_the_r);
  g1_null(h_i_to_the_identity_i);
  bn_null(group_order);

  TRY {
    g1_new(secret_key_a0);
    g2_new(secret_key_a1);
    g1_new(secret_key_associated_id);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g2_new(secret_key_b[i]);
    }

    bn_new(r);
    g1_new(product_h_to_the_identity_to_the_r);
    g1_new(h_i_to_the_identity_i);
    bn_new(group_order);

    int decompress_status =
        bbg05_g1_decompress(public_params_g3, public_params->g3);
    decompress_status |=
        bbg05_g2_decompress(public_params_g, public_params->g);
    decompress_status |=
        bbg05_g1_decompress(master_key_mk, master_key->mk);

    for (unsigned i = 0; i < public_params->max_depth; i++) {
      decompress_status |= bbg05_g1_decompress(public_params_h[i],
                                               public_params->h[i]);
    }

    if (decompress_status) {
      return STS_ERR;
    }

    // Encoding of identity is done in this function.
    bbg05_convert_identity_to_zp_vector(identity_zp_vector, identity);

    g2_get_ord(group_order);

    // choose random r from Z_p^*
    bn_rand_mod(r, group_order);

    // computation of a_0 = mk * (prod_{i=1 to k} h_i^{I_i} * g3)^r
    g1_copy(secret_key_associated_id, public_params_g3);

    for (unsigned i = 0; i < identity->depth; i++) {
      g1_exponentiate(h_i_to_the_identity_i, public_params_h[i],
                      identity_zp_vector[i]);
      g1_multiply(secret_key_associated_id, secret_key_associated_id,
                  h_i_to_the_identity_i);
    }

    g1_exponentiate(product_h_to_the_identity_to_the_r,
                    secret_key_associated_id, r);
    g1_multiply(secret_key_a0, master_key_mk,
                product_h_to_the_identity_to_the_r);

    g2_exponentiate(secret_key_a1, public_params_g, r);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g1_exponentiate(secret_key_b[i],
                      public_params_h[identity->depth + i],
                      r);
    }

    int compress_status =
        bbg05_g1_compress(&(*secret_key)->a0, secret_key_a0);
    compress_status |=
        bbg05_g2_compress(&(*secret_key)->a1, secret_key_a1);
    compress_status |= bbg05_g1_compress(&(*secret_key)->associated_id,
                          secret_key_associated_id);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      compress_status |=
          bbg05_g1_compress(&(*secret_key)->b[i], secret_key_b[i]);
    }

    if (compress_status) {
      return STS_ERR;
    }

    (*secret_key)->num_delegatable_levels = num_delegatable_levels;

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_free(secret_key_a0);
    g2_free(secret_key_a1);
    g1_free(secret_key_associated_id);
    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g1_free(secret_key_b[i]);
    }

    bn_free(r);
    g1_free(product_h_to_the_identity_to_the_r);
    g1_free(h_i_to_the_identity_i);

    for (unsigned i = 0; i < identity->depth; i++) {
      bn_free(identity_zp_vector[i]);
    }

    g2_free(public_params_g);
    g1_free(public_params_g3);
    g1_free(master_key_mk);
    for (unsigned i = 0; i < public_params->max_depth; i++) {
      g1_free(public_params_h[i]);
    }
    bn_free(group_order);
  }
  return result_status;
}

int bbg05_key_generation_inner_node_from_parent(bbg05_secret_key_inner_node_t **secret_key,
                                                const bbg05_secret_key_inner_node_t *parent_secret_key,
                                                const bn_t encoded_identity,
                                                const bbg05_public_params_t *public_params) {
  unsigned parent_depth = public_params->max_depth -
      parent_secret_key->num_delegatable_levels;

  int secret_key_is_valid;
  int public_params_are_valid;

  int is_valid_status =
      bbg05_secret_key_inner_node_is_valid(&secret_key_is_valid,
                                           parent_secret_key);

  is_valid_status &= bbg05_public_params_are_valid(&public_params_are_valid,
                                           public_params);


  if (!secret_key_is_valid || !public_params_are_valid || is_valid_status) {
    return STS_ERR;
  }

  unsigned num_delegatable_levels = public_params->max_depth - parent_depth - 1;

  *secret_key = malloc(offsetof(bbg05_secret_key_inner_node_t, b) +
      num_delegatable_levels * sizeof(uint8_t *));

  if (!*secret_key) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t secret_key_a0;
  g2_t secret_key_a1;
  g1_t secret_key_associated_id;
  g1_t secret_key_b[num_delegatable_levels];

  g1_t public_params_h[public_params->max_depth];
  g2_t public_params_g;
  g1_t public_params_g3;
  g1_t parent_secret_key_a0;
  g2_t parent_secret_key_a1;
  g1_t parent_secret_key_associated_id;
  g1_t parent_secret_key_b[parent_secret_key->num_delegatable_levels];

  bn_t t;

  g1_t b_k_to_the_identity_k;
  g1_t product_h_to_the_identity_times_g3_to_the_t;
  g1_t product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k;
  g1_t h_k_to_the_identity_k;
  g2_t g_to_the_t;
  g1_t h_i_to_the_t[num_delegatable_levels];
  bn_t group_order;


  g1_null(secret_key_a0);
  g2_null(secret_key_a1);
  g1_null(secret_key_associated_id);

  for (unsigned i = 0; i < num_delegatable_levels; i++) {
    g1_null(secret_key_b[i]);
  }

  bn_null(t);
  g1_null(b_k_to_the_identity_k);
  g1_null(product_h_to_the_identity_times_g3_to_the_t);
  g1_null(product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);
  g1_null(h_k_to_the_identity_k;);
  g2_null(g_to_the_t);

  for (unsigned i = 0; i < num_delegatable_levels; i++) {
    g1_null(h_i_to_the_t[i]);
  }
  bn_null(group_order);

  TRY {
    g1_new(secret_key_a0);
    g2_new(secret_key_a1);
    g1_new(secret_key_associated_id);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g1_new(secret_key_b[i]);
    }

    bn_new(t);
    g1_new(b_k_to_the_identity_k);
    g1_new(product_h_to_the_identity_times_g3_to_the_t;);
    g1_new(
        product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);
    g1_new(h_k_to_the_identity_k;);
    g2_new(g_to_the_t);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g1_new(h_i_to_the_t[i]);
    }
    bn_new(group_order);

    int decompress_status =
        bbg05_g1_decompress(public_params_g3, public_params->g3);
    decompress_status |=
        bbg05_g2_decompress(public_params_g, public_params->g);
    decompress_status |=
        bbg05_g1_decompress(parent_secret_key_a0,
                            parent_secret_key->a0);
    decompress_status |=
        bbg05_g2_decompress(parent_secret_key_a1,
                            parent_secret_key->a1);
    decompress_status |=
        bbg05_g1_decompress(parent_secret_key_associated_id,
                            parent_secret_key->associated_id);

    for (unsigned i = 0; i < parent_secret_key->num_delegatable_levels;
         i++) {
      decompress_status |=
          bbg05_g1_decompress(parent_secret_key_b[i],
                              parent_secret_key->b[i]);
    }

    for (unsigned i = 0; i < public_params->max_depth; i++) {
      decompress_status |= bbg05_g1_decompress(public_params_h[i],
                                               public_params->h[i]);
    }

    if (decompress_status) {
      return STS_ERR;
    }

    g1_exponentiate(h_k_to_the_identity_k, public_params_h[parent_depth],
                      encoded_identity);

    g1_multiply(secret_key_associated_id, parent_secret_key_associated_id,
                h_k_to_the_identity_k);

    g2_get_ord(group_order);

    // choose random t from Z_p^*
    bn_rand_mod(t, group_order);

    g1_exponentiate(product_h_to_the_identity_times_g3_to_the_t,
                    secret_key_associated_id, t);

    g1_exponentiate(b_k_to_the_identity_k, parent_secret_key_b[0],
                    encoded_identity);

    g1_multiply(
        product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k,
        b_k_to_the_identity_k,
        product_h_to_the_identity_times_g3_to_the_t);

    g1_multiply(secret_key_a0,
                parent_secret_key_a0,
                product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);

    g2_exponentiate(g_to_the_t, public_params_g, t);

    g2_multiply(secret_key_a1, parent_secret_key_a1, g_to_the_t);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g1_exponentiate(h_i_to_the_t[i],
                      public_params_h[parent_depth + 1 + i],
                      t);
      g1_multiply(secret_key_b[i],
                  parent_secret_key_b[1 + i],
                  h_i_to_the_t[i]);
    }

    int compress_status =
        bbg05_g1_compress(&(*secret_key)->a0, secret_key_a0);
    compress_status |=
        bbg05_g2_compress(&(*secret_key)->a1, secret_key_a1);
    compress_status |= bbg05_g1_compress(&(*secret_key)->associated_id,
                          secret_key_associated_id);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      compress_status |=
          bbg05_g1_compress(&(*secret_key)->b[i], secret_key_b[i]);
    }

    if (compress_status) {
      return STS_ERR;
    }

    (*secret_key)->num_delegatable_levels = num_delegatable_levels;

  } CATCH_ANY {
    result_status = STS_ERR;

  } FINALLY {
    g1_free(secret_key_a0);
    g2_free(secret_key_a1);
    g1_free(secret_key_associated_id);

    bn_free(t);
    g1_free(b_k_to_the_identity_k);
    g1_free(product_h_to_the_identity_times_g3_to_the_t);
    g1_free(product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);
    g2_free(g_to_the_t);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g1_free(h_i_to_the_t[i]);
    }

    for (unsigned i = 0; i < parent_depth + 1; i++) {
      g1_free(public_params_h[i]);
    }

    g2_free(public_params_g);
    g1_free(public_params_g3);
    g1_free(parent_secret_key_a0);
    g2_free(parent_secret_key_a1);
    g1_free(parent_secret_key_associated_id);

    for (unsigned i = 0; i < num_delegatable_levels; i++) {
      g1_free(secret_key_b[i]);
    }
    bn_free(group_order);
  }
  return result_status;
}

int bbg05_key_generation_inner_node_from_parent_full_identity(
    bbg05_secret_key_inner_node_t **secret_key,
    const bbg05_secret_key_inner_node_t *parent_secret_key,
    const bbg05_identity_t *identity,
    const bbg05_public_params_t *public_params) {
  unsigned parent_depth = public_params->max_depth -
      parent_secret_key->num_delegatable_levels;

  int identity_is_valid;
  int is_valid_status = bbg05_identity_is_valid(&identity_is_valid, identity, public_params);
  if (is_valid_status || !identity_is_valid || parent_depth != (identity->depth - 1)) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  bn_t identity_last_component;

  bn_null(identity_last_component);

  TRY {
    // Encode and hash the last component of the identity.
    bn_new(identity_last_component);
    int hash_status = bbg05_hash_id(identity_last_component, identity->id[identity->depth - 1]);
    if (hash_status) {
      return STS_ERR;
    }

    int key_gen_status = bbg05_key_generation_inner_node_from_parent(
        secret_key,
        parent_secret_key,
        identity_last_component,
        public_params);
    if (key_gen_status) {
      return STS_ERR;
    }
  } CATCH_ANY {
    result_status = STS_OK;
  } FINALLY {
    bn_free(identity_last_component);
  }

  return result_status;
}

int bbg05_key_generation_leaf_node_from_parent(bbg05_secret_key_leaf_node_t **secret_key,
                                               const bbg05_secret_key_inner_node_t *parent_secret_key,
                                               const bn_t encoded_identity,
                                               const bbg05_public_params_t *public_params) {
  unsigned parent_depth = public_params->max_depth -
      parent_secret_key->num_delegatable_levels;

  int secret_key_is_valid;
  int public_params_are_valid;

  int is_valid_status =
      bbg05_secret_key_inner_node_is_valid(&secret_key_is_valid,
                                           parent_secret_key);

  is_valid_status &= bbg05_public_params_are_valid(&public_params_are_valid,
                                                   public_params);


  if (!secret_key_is_valid || !public_params_are_valid || is_valid_status) {
    return STS_ERR;
  }

  unsigned num_delegatable_levels = public_params->max_depth - parent_depth - 1;

  *secret_key = malloc(offsetof(bbg05_secret_key_inner_node_t, b) +
      num_delegatable_levels * sizeof(uint8_t *));

  if (!*secret_key) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t secret_key_a0;
  g2_t secret_key_a1;

  g1_t public_params_h_max_depth;
  g2_t public_params_g;
  g1_t public_params_g3;
  g1_t parent_secret_key_a0;
  g2_t parent_secret_key_a1;
  g1_t parent_secret_key_associated_id;
  g1_t parent_secret_key_bk;

  bn_t t;
  bn_t group_order;

  g1_t b_k_to_the_identity_k;
  g1_t product_h_to_the_identity_times_g3;
  g1_t product_h_to_the_identity_times_g3_to_the_t;
  g1_t product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k;
  g1_t h_k_to_the_identity_k;
  g2_t g_to_the_t;


  g1_null(secret_key_a0);
  g2_null(secret_key_a1);

  bn_null(t);
  g1_null(b_k_to_the_identity_k);
  g1_null(product_h_to_the_identity_times_g3);
  g1_null(product_h_to_the_identity_times_g3_to_the_t);
  g1_null(product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);
  g1_null(h_k_to_the_identity_k;);
  g2_null(g_to_the_t);
  bn_null(group_order);

  TRY {
    g1_new(secret_key_a0);
    g2_new(secret_key_a1);

    bn_new(t);
    g1_new(b_k_to_the_identity_k);
    g1_new(product_h_to_the_identity_times_g3);
    g1_new(product_h_to_the_identity_times_g3_to_the_t;);
    g1_new(
        product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);
    g1_new(h_k_to_the_identity_k;);
    g2_new(g_to_the_t);
    bn_new(group_order);

    int decompress_status =
        bbg05_g1_decompress(public_params_g3, public_params->g3);
    decompress_status |=
        bbg05_g2_decompress(public_params_g, public_params->g);
    decompress_status |=
        bbg05_g1_decompress(parent_secret_key_a0,
                            parent_secret_key->a0);
    decompress_status |=
        bbg05_g2_decompress(parent_secret_key_a1,
                            parent_secret_key->a1);
    decompress_status |=
        bbg05_g1_decompress(parent_secret_key_associated_id,
                            parent_secret_key->associated_id);


    bbg05_g1_decompress(parent_secret_key_bk,
                              parent_secret_key->b[0]);



    decompress_status |= bbg05_g1_decompress(public_params_h_max_depth,
        public_params->h[parent_depth]);

    if (decompress_status) {
      return STS_ERR;
    }

    g1_exponentiate(h_k_to_the_identity_k, public_params_h_max_depth,
                    encoded_identity);

    g1_multiply(product_h_to_the_identity_times_g3, parent_secret_key_associated_id,
                h_k_to_the_identity_k);

    g2_get_ord(group_order);

    // choose random t from Z_p^*
    bn_rand_mod(t, group_order);

    g1_exponentiate(product_h_to_the_identity_times_g3_to_the_t,
                    product_h_to_the_identity_times_g3, t);

    g1_exponentiate(b_k_to_the_identity_k, parent_secret_key_bk,
                    encoded_identity);

    g1_multiply(
        product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k,
        b_k_to_the_identity_k,
        product_h_to_the_identity_times_g3_to_the_t);

    g1_multiply(secret_key_a0,
                parent_secret_key_a0,
                product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);

    g2_exponentiate(g_to_the_t, public_params_g, t);

    g2_multiply(secret_key_a1, parent_secret_key_a1, g_to_the_t);

    int compress_status =
        bbg05_g1_compress(&(*secret_key)->a0, secret_key_a0);
    compress_status |=
        bbg05_g2_compress(&(*secret_key)->a1, secret_key_a1);

    if (compress_status) {
      return STS_ERR;
    }

  } CATCH_ANY {
    result_status = STS_ERR;

  } FINALLY {
    g1_free(secret_key_a0);
    g2_free(secret_key_a1);

    bn_free(t);
    g1_free(b_k_to_the_identity_k);
    g1_free(product_h_to_the_identity_times_g3);
    g1_free(product_h_to_the_identity_times_g3_to_the_t);
    g1_free(product_h_to_the_identity_times_g3_to_the_t_times_bk_to_the_identity_k);
    g2_free(g_to_the_t);

    g1_free(public_params_h_max_depth);

    g2_free(public_params_g);
    g1_free(public_params_g3);
    g1_free(parent_secret_key_a0);
    g2_free(parent_secret_key_a1);
    g1_free(parent_secret_key_associated_id);
    g1_free(parent_secret_key_bk);
    bn_free(group_order);
  }
  return result_status;
}

int bbg05_key_generation_leaf_node_from_parent_full_identity(
    bbg05_secret_key_leaf_node_t **secret_key,
    const bbg05_secret_key_inner_node_t *parent_secret_key,
    const bbg05_identity_t *identity,
    const bbg05_public_params_t *public_params) {
  unsigned parent_depth = public_params->max_depth -
      parent_secret_key->num_delegatable_levels;

  int identity_is_valid;
  int is_valid_status = bbg05_identity_is_valid(&identity_is_valid, identity, public_params);
  if (is_valid_status || !identity_is_valid || parent_depth != (identity->depth - 1)) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  bn_t identity_last_component;

  bn_null(identity_last_component);

  TRY {
    // Encode and hash the last component of the identity.
    bn_new(identity_last_component);
    int hash_status = bbg05_hash_id(identity_last_component, identity->id[identity->depth - 1]);
    if (hash_status) {
      return STS_ERR;
    }

    int key_gen_status = bbg05_key_generation_leaf_node_from_parent(
        secret_key,
        parent_secret_key,
        identity_last_component,
        public_params);
    if (key_gen_status) {
      return STS_ERR;
    }
  } CATCH_ANY {
    result_status = STS_OK;
  } FINALLY {
    bn_free(identity_last_component);
  }

  return result_status;
}

int bbg05_key_generation_leaf_node_from_master_key(bbg05_secret_key_leaf_node_t **secret_key,
                                                   const bbg05_master_key_t *master_key,
                                                   const bbg05_identity_t *identity,
                                                   const bbg05_public_params_t *public_params) {

  int master_key_is_valid;
  int identity_is_valid;
  int public_params_are_valid;
  int is_valid_status =
      bbg05_master_key_is_valid(&master_key_is_valid, master_key);
  is_valid_status |=
      bbg05_identity_is_valid(&identity_is_valid, identity, public_params);
  is_valid_status |=
      bbg05_public_params_are_valid(&public_params_are_valid, public_params);
  if (!master_key_is_valid || !identity_is_valid || !public_params_are_valid
      || is_valid_status || identity->depth != public_params->max_depth) {
    return STS_ERR;
  }

  *secret_key = malloc(sizeof(bbg05_secret_key_leaf_node_t));

  if (!*secret_key) {
    return STS_ERR;
  }

  bbg05_secret_key_inner_node_t *tmp_sk_inner_node;

  int keygen_status =
      bbg05_key_generation_inner_node_from_master_key(&tmp_sk_inner_node,
                                                      master_key,
                                                      identity,
                                                      public_params);

  if (keygen_status) {
    return keygen_status;
  }

  (*secret_key)->a0 = tmp_sk_inner_node->a0;
  (*secret_key)->a1 = tmp_sk_inner_node->a1;

  free(tmp_sk_inner_node);

  return STS_OK;
}

int bbg05_encapsulate(bbg05_ciphertext_t **ciphertext,
                      bbg05_key_t **key,
                      const bbg05_public_params_t *public_params,
                      const bbg05_identity_t *identity) {
  int result_status = STS_OK;
  bn_t signing_key;
  g2_t verification_key;
  bn_t encoded_verification_key;
  int ciphertext_bin_size = G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED;
  uint8_t *ciphertext_bin = malloc((size_t) ciphertext_bin_size);
  g1_t signature;
  bn_t group_order;

  bn_null(signing_key);
  g2_null(verification_key);
  bn_null(encoded_verification_key);
  g1_null(signature);
  bn_null(group_order);

  TRY {
    bn_new(signing_key);
    g2_new(verification_key);
    g2_new(encoded_verification_key);
    g1_new(signature);
    bn_new(group_order);
    g1_get_ord(group_order);

    // Generate BLS signature key pair.
    int gen_status = cp_bls_gen(signing_key, verification_key);
    if (gen_status) {
      return STS_ERR;
    }

    // Encode the verification key.
    bbg05_encode_verification_key(encoded_verification_key, verification_key);

    // Encapsulate under the given identity together with the encoded
    // verification key. The encoding of the identity is done within this
    // function.
    int encapsulate_status = bbg05_cpa_encapsulate(ciphertext,
                                                   key,
                                                   public_params,
                                                   identity,
                                                   encoded_verification_key);
    if (encapsulate_status) {
      return STS_ERR;
    }

    // Sign the ciphertext.
    memcpy(ciphertext_bin, (*ciphertext)->a, GT_SIZE_COMPRESSED);
    memcpy(ciphertext_bin + GT_SIZE_COMPRESSED, (*ciphertext)->b, G2_SIZE_COMPRESSED);
    memcpy(ciphertext_bin + GT_SIZE_COMPRESSED + G2_SIZE_COMPRESSED, (*ciphertext)->c, G1_SIZE_COMPRESSED);
    cp_bls_sig(signature, ciphertext_bin, ciphertext_bin_size, signing_key);

    // Set the verification key and the signature.
    bbg05_g2_compress(&((*ciphertext)->verification_key), verification_key);
    bbg05_g1_compress(&((*ciphertext)->signature), signature);
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    bn_free(signing_key);
    g2_free(verification_key);
    bn_free(encoded_verification_key);
    free(ciphertext_bin);
    g1_free(signature);
    bn_free(group_order);
  }

  return result_status;
}

int bbg05_cpa_encapsulate(bbg05_ciphertext_t **ciphertext,
                      bbg05_key_t **key,
                      const bbg05_public_params_t *public_params,
                      const bbg05_identity_t *identity,
                      const bn_t encoded_verification_key) {
  int result_status = STS_OK;

  *key = malloc(sizeof(bbg05_key_t));

  gt_t key_k;
  gt_null(key_k);

  TRY {
    gt_new(key_k);
    gt_rand(key_k);

    bbg05_encrypt(ciphertext, key_k, public_params, identity, encoded_verification_key);

    int compress_status = bbg05_gt_compress(&(*key)->k, key_k);

    if (compress_status) {
      return STS_ERR;
    }

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    gt_free(key);
  }
  return result_status;
}

int bbg05_encrypt(bbg05_ciphertext_t **ciphertext,
                  gt_t message,
                  const bbg05_public_params_t *public_params,
                  const bbg05_identity_t *identity,
                  const bn_t encoded_verification_key) {

  int identity_is_valid;
  int public_params_are_valid;
  int is_valid_status =
      bbg05_identity_is_valid(&identity_is_valid, identity, public_params);
  is_valid_status |=
      bbg05_public_params_are_valid(&public_params_are_valid, public_params);
  if (!identity_is_valid || !public_params_are_valid || is_valid_status) {
    return STS_ERR;
  }

  *ciphertext = malloc(sizeof(bbg05_ciphertext_t));

  if (!*ciphertext) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  gt_t ciphertext_a;
  g2_t ciphertext_b;
  g1_t ciphertext_c;

  g1_t public_params_h[identity->depth + 1];
  g2_t public_params_g;
  g2_t public_params_g1;
  gt_t public_params_pairing_g1_g2;
  g1_t public_params_g3;

  bn_t identity_zp_vector[identity->depth];

  bn_t s;
  gt_t pairing_of_g1_g2_to_the_s;
  g1_t h_i_to_the_identity_i;
  g1_t product_h_to_the_identity;
  g1_t product_h_to_the_identity_times_g3;
  bn_t group_order;

  gt_null(ciphertext_a);
  g2_null(ciphertext_b);
  g1_null(ciphertext_c);

  bn_null(s);
  gt_null(pairing_of_g1_g2_to_the_s);
  g1_null(h_i_to_the_identity_i);
  g1_null(product_h_to_the_identity);
  g1_null(product_h_to_the_identity_times_g3);
  bn_null(group_order);

  TRY {
    gt_new(ciphertext_a);
    g2_new(ciphertext_b);
    g1_new(ciphertext_c);

    bn_new(s);
    gt_new(pairing_of_g1_g2_to_the_s);
    g1_new(h_i_to_the_identity_i);
    g1_new(product_h_to_the_identity);
    g1_new(product_h_to_the_identity_times_g3);
    bn_new(group_order);

    int decompress_status =
        bbg05_g2_decompress(public_params_g, public_params->g);
    decompress_status |=
        bbg05_g2_decompress(public_params_g1, public_params->g1);
    decompress_status |=
        bbg05_gt_decompress(public_params_pairing_g1_g2, public_params->pairing_g1_g2);
    decompress_status |=
        bbg05_g1_decompress(public_params_g3, public_params->g3);

    for (unsigned i = 0; i < identity->depth + 1; i++) {
      decompress_status |= bbg05_g1_decompress(public_params_h[i],
                                               public_params->h[i]);
    }

    if (decompress_status) {
      return STS_ERR;
    }

    // The identity is encoded in this function.
    bbg05_convert_identity_to_zp_vector(identity_zp_vector, identity);

    g2_get_ord(group_order);

    // choose random t from Z_p^*
    bn_rand_mod(s, group_order);

    gt_exp(pairing_of_g1_g2_to_the_s, public_params_pairing_g1_g2, s);

    gt_mul(ciphertext_a, pairing_of_g1_g2_to_the_s, message);

    g2_exponentiate(ciphertext_b, public_params_g, s);

    g1_set_infty(product_h_to_the_identity);
    for (unsigned i = 0; i < identity->depth + 1; i++) {
      if (i == identity->depth) {
        g1_exponentiate(h_i_to_the_identity_i, public_params_h[i], encoded_verification_key);
      } else {
        g1_exponentiate(h_i_to_the_identity_i, public_params_h[i],
                        identity_zp_vector[i]);
      }
      g1_multiply(product_h_to_the_identity, product_h_to_the_identity,
                  h_i_to_the_identity_i);
    }

    g1_multiply(product_h_to_the_identity_times_g3,
                product_h_to_the_identity,
                public_params_g3);

    g1_exponentiate(ciphertext_c,
                    product_h_to_the_identity_times_g3,
                    s);

    int compress_status =
        bbg05_gt_compress(&(*ciphertext)->a, ciphertext_a);
    compress_status |=
        bbg05_g2_compress(&(*ciphertext)->b, ciphertext_b);
    compress_status |=
        bbg05_g1_compress(&(*ciphertext)->c, ciphertext_c);

    if (compress_status) {
      return STS_ERR;
    }
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY { gt_free(ciphertext_a);
    g2_free(ciphertext_b);
    g1_free(ciphertext_c);

    for (unsigned i = 0; i < identity->depth; i++) {
      bn_free(identity_zp_vector[i]);
    }

    bn_free(s);
    gt_free(pairing_of_g1_g2);
    gt_free(pairing_of_g1_g2_to_the_s);
    g1_free(h_i_to_the_identity_i);
    g1_free(product_h_to_the_identity);
    g1_free(product_h_to_the_identity_times_g3);

    for (unsigned i = 0; i < identity->depth + 1; i++) {
      g1_free(public_params_h[i]);
    }

    g2_free(public_params_g);
    g2_free(public_params_g1);
    g1_free(public_params_pairing_g1_g2);
    g1_free(public_params_g3);
    bn_free(group_order);
  }
  return result_status;
}

int bbg05_decapsulate(bbg05_key_t **key,
                      const bbg05_secret_key_inner_node_t *secret_key,
                      const bbg05_ciphertext_t *ciphertext,
                      const bbg05_public_params_t *public_params) {
  int secret_key_is_valid;
  int ciphertext_is_valid;
  int is_valid_status = bbg05_secret_key_inner_node_is_valid(&secret_key_is_valid, secret_key);
  is_valid_status |= bbg05_ciphertext_is_valid(&ciphertext_is_valid, ciphertext);
  if (!secret_key_is_valid || !ciphertext_is_valid || is_valid_status) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t signature;
  g2_t verification_key;
  int ciphertext_bin_size = G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED;
  uint8_t *ciphertext_bin = malloc((size_t) ciphertext_bin_size);
  bn_t encoded_verification_key;
  bbg05_secret_key_leaf_node_t *verification_key_secret_key = malloc(sizeof(bbg05_secret_key_leaf_node_t));

  g1_null(signature);
  g2_null(verification_key);
  bn_null(encoded_verification_key);

  TRY {
    g1_new(signature);
    g2_new(verification_key);
    bn_new(encoded_verification_key);

    int compress_status =
        bbg05_g1_decompress(signature, (uint8_t *) ciphertext->signature);
    compress_status |= bbg05_g2_decompress(verification_key,
                                           (uint8_t *) ciphertext->verification_key);
    if (compress_status) {
      return STS_ERR;
    }
    memcpy(ciphertext_bin, ciphertext->a, GT_SIZE_COMPRESSED);
    memcpy(ciphertext_bin + GT_SIZE_COMPRESSED, ciphertext->b, G2_SIZE_COMPRESSED);
    memcpy(ciphertext_bin + GT_SIZE_COMPRESSED + G2_SIZE_COMPRESSED, ciphertext->c, G1_SIZE_COMPRESSED);

    int signature_valid = cp_bls_ver(signature, ciphertext_bin, ciphertext_bin_size, verification_key);
    if (!signature_valid) {
      return STS_ERR;
    }

    // Derive secret key for verification key.
    bbg05_encode_verification_key(encoded_verification_key, verification_key);
    bbg05_key_generation_leaf_node_from_parent(&verification_key_secret_key, secret_key, encoded_verification_key, public_params);

    int decapsulate_status = bbg05_cpa_decapsulate(key, verification_key_secret_key, ciphertext);
    if (decapsulate_status) {
      return STS_ERR;
    }
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_free(signature);
    g2_free(verification_key);
    free(ciphertext_bin);
    bn_free(encoded_verification_key);
    bbg05_set_secret_key_leaf_node_to_zero(verification_key_secret_key);
    free(verification_key_secret_key);
  }

  return result_status;
}

int bbg05_cpa_decapsulate(bbg05_key_t **key,
                      const bbg05_secret_key_leaf_node_t *secret_key,
                      const bbg05_ciphertext_t *ciphertext) {
  *key = malloc(sizeof(bbg05_key_t));

  if (!*key) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  gt_t key_k;

  g1_t secret_key_a0;
  g2_t secret_key_a1;
  gt_t ciphertext_A;
  g2_t ciphertext_B;
  g1_t ciphertext_C;

  gt_t pairing_a1_C;
  gt_t pairing_B_a0;
  gt_t pairing_B_a0_inv;
  gt_t A_times_pairing_a1_C;

  gt_null(key_k);
  gt_null(pairing_a1_C);
  gt_null(pairing_B_a0);
  gt_null(pairing_B_a0_inv);
  gt_null(A_times_pairing_a1_C);

  TRY {
    gt_new(key_k);
    gt_new(pairing_a1_C);
    gt_new(pairing_B_a0);
    gt_new(pairing_B_a0_inv);
    gt_new(A_times_pairing_a1_C);

    int decompress_status =
        bbg05_g1_decompress(secret_key_a0, secret_key->a0);
    decompress_status |=
        bbg05_g2_decompress(secret_key_a1, secret_key->a1);
    decompress_status |=
        bbg05_gt_decompress(ciphertext_A, ciphertext->a);
    decompress_status |=
        bbg05_g2_decompress(ciphertext_B, ciphertext->b);
    decompress_status |=
        bbg05_g1_decompress(ciphertext_C, ciphertext->c);

    if (decompress_status) {
      return STS_ERR;
    }

    pc_map(pairing_a1_C, ciphertext_C, secret_key_a1);

    gt_mul(A_times_pairing_a1_C, ciphertext_A, pairing_a1_C);

    pc_map(pairing_B_a0, secret_key_a0, ciphertext_B);

    gt_inv(pairing_B_a0_inv, pairing_B_a0);

    gt_mul(key_k, A_times_pairing_a1_C, pairing_B_a0_inv);

    int compress_status = bbg05_gt_compress(&(*key)->k, key_k);

    if (compress_status) {
      return STS_ERR;
    }
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY { gt_free(key_k);

    g1_free(secret_key_a0);
    g2_free(secret_key_a1);
    gt_free(ciphertext_A);
    g2_free(ciphertext_B);
    g1_free(ciphertext_C);

    gt_free(pairing_a1_C);
    gt_free(pairing_B_a0);
    gt_free(pairing_B_a0_inv);
    gt_free(A_times_pairing_a1_C);
  }

  return result_status;
}


int bbg05_hash_id(bn_t hashed_id, const unsigned id) {
  int result_status = STS_OK;

  bn_t group_order;
  uint8_t *id_byte_array = malloc(sizeof(id) + 1);
  uint8_t hash[SECURITY_PARAMETER];

  bn_null(group_order);

  TRY {
    bn_new(group_order);
    bn_new(hashed_id);

    g1_get_ord(group_order);

    // Encode identity.
    id_byte_array[0] = IDENTITY_PREFIX;
    memcpy(id_byte_array + 1, &id, sizeof(id));

    md_map_sh256(hash, id_byte_array, sizeof(id) + 1);
    bn_read_bin(hashed_id, hash, SECURITY_PARAMETER);
    bn_mod(hashed_id, hashed_id, group_order);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
    bn_free(group_order);
    free(id_byte_array);
  }

  return result_status;
}

int bbg05_convert_identity_to_zp_vector(bn_t *identity_zp_vector,
                                        const bbg05_identity_t *identity) {
  int result_status = STS_OK;

  for (unsigned i = 0; i < identity->depth; i++) {
    bn_null(identity_zp_vector[i]);
  }

  TRY {
    for (unsigned i = 0; i < identity->depth; i++) {
      bn_new(identity_zp_vector[i]);
      // Encoding is done in this function.
      bbg05_hash_id(identity_zp_vector[i], identity->id[i]);
    }
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {}
  return result_status;
}

void bbg05_encode(bbg05_identity_t **encoded_identity, const bbg05_identity_t *identity) {
  *encoded_identity = malloc(offsetof(bbg05_identity_t, id) + (identity->depth + 1) * sizeof(unsigned));
  (*encoded_identity)->depth = identity->depth + 1;
  (*encoded_identity)->id[0] = 0;
  memcpy((*encoded_identity)->id + 1, identity->id, identity->depth);
}

void bbg05_encode_verification_key(bn_t encoded_verification_key, g2_t verification_key) {
  bn_t group_order;

  bn_null(encoded_verification_key);
  bn_null(group_order);

  bn_new(encoded_verification_key);
  bn_new(group_order);

  g1_get_ord(group_order);

  unsigned verification_key_bin_size = (unsigned) G2_SIZE_COMPRESSED + 1;
  uint8_t *verification_key_bin = malloc(verification_key_bin_size);
  verification_key_bin[0] = VERIFICATION_KEY_PREFIX;
  g2_write_bin(&(verification_key_bin[1]), verification_key_bin_size - 1, verification_key, 1);
  uint8_t hash[SECURITY_PARAMETER];
  md_map_sh256(hash, verification_key_bin, verification_key_bin_size);
  bn_read_bin(encoded_verification_key, hash, SECURITY_PARAMETER);
  bn_mod(encoded_verification_key, encoded_verification_key, group_order);

  bn_free(group_order);
  free(verification_key_bin);
}
