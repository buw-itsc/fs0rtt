#include <stddef.h>
#include "BBG_05_HIBE.h"
#include <BBG_05_HIBE_types.h>
#include "BBG_05_HIBE_util.h"

void bbg05_print_master_key(const bbg05_master_key_t *master_key) {
  printf("\nMaster key:\n");
  bbg05_g1_print_bin(master_key->mk);
}

void bbg05_print_public_params(const bbg05_public_params_t *public_params) {
  printf("\nPublic Parameters:\n");

  printf("\nMaximum depth:\n");
  printf("%d\n", public_params->max_depth);

  printf("\ng:\n");
  bbg05_g2_print_bin(public_params->g);

  printf("\ng1:\n");
  bbg05_g2_print_bin(public_params->g1);

  printf("\ng2:\n");
  bbg05_gt_print_bin(public_params->pairing_g1_g2);

  printf("\ng3:\n");
  bbg05_g1_print_bin(public_params->g3);

  for (unsigned i = 0; i < public_params->max_depth; i++) {
    printf("\nh_%d):\n", i + 1);
    bbg05_g1_print_bin(public_params->h[i]);
  }
}

void bbg05_print_identity(const bbg05_identity_t *identity) {
  printf("\nIdentity:\n");
  printf("depth: %d\n", identity->depth);

  for(unsigned i = 0; i < identity->depth; i++) {
    printf("I_%d: %d\n", i + 1, identity->id[i]);
  }
}

void bbg05_print_secret_key_inner_node(const bbg05_secret_key_inner_node_t *secret_key) {
  printf("Secret key of inner node\n");

  printf("a0:\n");
  bbg05_g1_print_bin(secret_key->a0);

  printf("\na1:\n");
  bbg05_g2_print_bin(secret_key->a1);

  printf("\nassociated ID:\n");
  bbg05_g1_print_bin(secret_key->associated_id);

  printf("\nNumber of delegations possible:\n");
  printf("%d\n", secret_key->num_delegatable_levels);

  for(unsigned i = 0; i < secret_key->num_delegatable_levels; i++) {
    printf("\nb%d:\n", i);
    bbg05_g1_print_bin(secret_key->b[i]);
  }

}

void bbg05_print_secret_key_leaf_node(const bbg05_secret_key_leaf_node_t *secret_key) {
  printf("Secret key of leaf node\n");

  printf("a0:\n");
  bbg05_g1_print_bin(secret_key->a0);

  printf("\na1:\n");
  bbg05_g2_print_bin(secret_key->a1);
}

void bbg05_print_ciphertext(const bbg05_ciphertext_t *ciphertext) {
  printf("Ciphertext:\n");

  printf("a:\n");
  bbg05_gt_print_bin(ciphertext->a);

  printf("\nb:\n");
  bbg05_g2_print_bin(ciphertext->b);

  printf("\nc:\n");
  bbg05_g1_print_bin(ciphertext->c);
}

void bbg05_print_key(const bbg05_key_t *key) {
  printf("Key:\n");
  bbg05_gt_print_bin(key->k);
}

int bbg05_identity_is_valid(int *valid, const bbg05_identity_t *identity,
                            const bbg05_public_params_t *public_params) {

  *valid = identity->depth <= public_params->max_depth;
  return STS_OK;
}

int bbg05_master_key_is_valid(int *valid, const bbg05_master_key_t *master_key) {
  g1_t master_key_mk;
  int compress_status =
      bbg05_g1_decompress(master_key_mk, master_key->mk);
  if (compress_status) {
    *valid = 0;
    return STS_ERR;
  }

  *valid = g1_is_valid(master_key_mk);

  g1_free(master_secret_key_g);

  return STS_OK;
}

int bbg05_public_params_are_valid(int *valid, const bbg05_public_params_t *public_params) {
  g2_t public_params_g;
  g2_t public_params_g1;
  gt_t public_params_pairing_g1_g2;
  g1_t public_params_g3;
  g1_t public_params_h[public_params->max_depth];

  int decompress_status = bbg05_g2_decompress(public_params_g, public_params->g);
  decompress_status |= bbg05_g2_decompress(public_params_g1, public_params->g1);
  decompress_status |= bbg05_gt_decompress(public_params_pairing_g1_g2, public_params->pairing_g1_g2);
  decompress_status |= bbg05_g1_decompress(public_params_g3, public_params->g3);
  for (unsigned i = 0; i < public_params->max_depth; i++) {
    decompress_status |=
        bbg05_g1_decompress(public_params_h[i], public_params->h[i]);
  }
  if (decompress_status) {
    *valid = 0;
    return STS_ERR;
  }

  *valid = g2_is_valid(public_params_g);
  *valid &= g2_is_valid(public_params_g1);
  *valid &= gt_is_valid(public_params_pairing_g1_g2);
  *valid &= g1_is_valid(public_params_g3);

  for (unsigned i = 0; i < public_params->max_depth; i++) {
    *valid &= g1_is_valid(public_params_h[i]);
  }

  g2_free(public_params_g);
  g2_free(public_params_g1);
  g1_free(public_params_pairing_g1_g2);
  g1_free(public_params_g3);

  for (unsigned i = 0; i < public_params->max_depth; i++) {
    g1_free(public_params_h[i]);
  }

  return STS_OK;
}

int bbg05_secret_key_inner_node_is_valid(int *valid, const bbg05_secret_key_inner_node_t *secret_key) {

  g1_t secret_key_a0;
  g2_t secret_key_a1;
  g1_t secret_key_associated_id;
  g1_t secret_key_b[secret_key->num_delegatable_levels];

  int decompress_status = bbg05_g1_decompress(secret_key_a0, secret_key->a0);
  decompress_status |= bbg05_g2_decompress(secret_key_a1, secret_key->a1);
  decompress_status |= bbg05_g1_decompress(secret_key_associated_id,
      secret_key->associated_id);

  for (unsigned i = 0; i < secret_key->num_delegatable_levels; i++) {
    decompress_status |=
        bbg05_g1_decompress(secret_key_b[i], secret_key->b[i]);
  }

  if (decompress_status) {
    *valid = 0;
    return STS_ERR;
  }

  *valid = g1_is_valid(secret_key_a0);
  *valid &= g2_is_valid(secret_key_a1);
  *valid &= g1_is_valid(secret_key_associated_id);

  for (unsigned i = 0; i < secret_key->num_delegatable_levels; i++) {
    *valid &= g1_is_valid(secret_key_b[i]);
  }

  g1_free(secret_key_a0);
  g2_free(secret_key_a1);
  g1_free(secret_key_associated_id);

  for (unsigned i = 0; i < secret_key->num_delegatable_levels; i++) {
    g1_free(secret_key_b[i]);
  }

  return STS_OK;
}

int bbg05_secret_key_leaf_node_is_valid(int *valid, const bbg05_secret_key_leaf_node_t *secret_key) {

  g1_t secret_key_a0;
  g2_t secret_key_a1;

  int decompress_status = bbg05_g1_decompress(secret_key_a0, secret_key->a0);
  decompress_status |= bbg05_g2_decompress(secret_key_a1, secret_key->a1);

  if (decompress_status) {
    *valid = 0;
    return STS_ERR;
  }

  *valid = g1_is_valid(secret_key_a0);
  *valid &= g2_is_valid(secret_key_a1);

  g1_free(secret_key_a0);
  g2_free(secret_key_a1);

  return STS_OK;
}


int bbg05_ciphertext_is_valid(int *valid, const bbg05_ciphertext_t *ciphertext) {
  g2_t ciphertext_b;
  g1_t ciphertext_c;

  int decompress_status = bbg05_g2_decompress(ciphertext_b, ciphertext->b);
  decompress_status |= bbg05_g1_decompress(ciphertext_c, ciphertext->c);

  if (decompress_status) {
    *valid = 0;
    return STS_ERR;
  }

  *valid = g2_is_valid(ciphertext_b);
  *valid &= g1_is_valid(ciphertext_c);

  g2_free(ciphertext_b);
  g1_free(ciphertext_c);

  return STS_OK;
}

void bbg05_set_master_key_to_zero(bbg05_master_key_t *master_key) {
  memset(master_key->mk, 0, G1_SIZE_COMPRESSED);
  free(master_key->mk);
}

void bbg05_set_secret_key_inner_node_to_zero(bbg05_secret_key_inner_node_t *secret_key) {
  memset(secret_key->a0, 0, G1_SIZE_COMPRESSED);
  free(secret_key->a0);
  memset(secret_key->a1, 0, G2_SIZE_COMPRESSED);
  free(secret_key->a1);
  memset(secret_key->associated_id, 0, G1_SIZE_COMPRESSED);
  free(secret_key->associated_id);

  for (unsigned i = 0; i < secret_key->num_delegatable_levels; i++) {
    memset(secret_key->b[i], 0, G1_SIZE_COMPRESSED);
    free(secret_key->b[i]);
  }
}

void bbg05_set_secret_key_leaf_node_to_zero(bbg05_secret_key_leaf_node_t *secret_key) {
  memset(secret_key->a0, 0, G1_SIZE_COMPRESSED);
  free(secret_key->a0);
  memset(secret_key->a1, 0, G2_SIZE_COMPRESSED);
  free(secret_key->a1);
}

void bbg05_set_public_params_to_zero(bbg05_public_params_t *public_params) {
  memset(public_params->g, 0, G2_SIZE_COMPRESSED);
  free(public_params->g);
  memset(public_params->g1, 0, G2_SIZE_COMPRESSED);
  free(public_params->g1);
  memset(public_params->pairing_g1_g2, 0, GT_SIZE_COMPRESSED);
  free(public_params->pairing_g1_g2);
  memset(public_params->g3, 0, G1_SIZE_COMPRESSED);
  free(public_params->g3);

  for (unsigned i = 0; i < public_params->max_depth; i++) {
    memset(public_params->h[i], 0, G1_SIZE_COMPRESSED);
    free(public_params->h[i]);
  }
}

void bbg05_set_ciphertext_to_zero(bbg05_ciphertext_t *ciphertext) {
  memset(ciphertext->a, 0, GT_SIZE_COMPRESSED);
  free(ciphertext->a);
  memset(ciphertext->b, 0, G2_SIZE_COMPRESSED);
  free(ciphertext->b);
  memset(ciphertext->c, 0, G1_SIZE_COMPRESSED);
  free(ciphertext->c);
}

void bbg05_set_key_to_zero(bbg05_key_t *key) {
  memset(key->k, 0, GT_SIZE_COMPRESSED);
  free(key->k);
}

int bbg05_g1_compress(uint8_t **bin, g1_t element) {
  int result_status = STS_OK;
  TRY {
    *bin = malloc((size_t) G1_SIZE_COMPRESSED);
    g1_write_bin(*bin, G1_SIZE_COMPRESSED, element, 1);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {}

  return result_status;
}

int bbg05_g2_compress(uint8_t **bin, g2_t element) {
  int result_status = STS_OK;
  TRY {
    *bin = malloc((size_t) G2_SIZE_COMPRESSED);
    g2_write_bin(*bin, G2_SIZE_COMPRESSED, element, 1);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

int bbg05_gt_compress(uint8_t **bin, gt_t element) {
  int result_status = STS_OK;
  TRY {
    *bin = malloc((size_t) GT_SIZE_COMPRESSED);
    gt_write_bin(*bin, GT_SIZE_COMPRESSED, element, 1);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

int bbg05_g1_decompress(g1_t element, uint8_t *bin) {
  int result_status = STS_OK;
  g1_null(element);
  TRY {
    g1_new(element);
    g1_read_bin(element, bin, G1_SIZE_COMPRESSED);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {}

  return result_status;
}

int bbg05_g2_decompress(g2_t element, uint8_t *bin) {
  int result_status = STS_OK;
  g2_null(element);
  TRY {
    g2_new(element);
    g2_read_bin(element, bin, G2_SIZE_COMPRESSED);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {}

  return result_status;
}

int bbg05_gt_decompress(gt_t element, uint8_t *bin) {
  int result_status = STS_OK;
  gt_null(element);
  TRY {
    gt_new(element);
    gt_read_bin(element, bin, GT_SIZE_COMPRESSED);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {}

  return result_status;
}

int bbg05_g1_print_bin(uint8_t *bin) {
  int result_status = STS_OK;
  g1_t element;

  TRY {
    bbg05_g1_decompress(element, bin);
    g1_print(element);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
    g1_free(element);
  }

  return result_status;
}

int bbg05_g2_print_bin(uint8_t *bin) {
  int result_status = STS_OK;
  g2_t element;

  TRY {
    bbg05_g2_decompress(element, bin);
    g2_print(element);
  } CATCH_ANY {
  return STS_ERR;
  } FINALLY {
    g2_free(element);
  }

  return result_status;
}

int bbg05_gt_print_bin(uint8_t *bin) {
  int result_status = STS_OK;
  gt_t element;

  TRY {
    bbg05_gt_decompress(element, bin);
    gt_print(element);
  } CATCH_ANY {
  return STS_ERR;
  } FINALLY {
    gt_free(element);
  }

  return result_status;
}

int bbg05_convert_key_to_bit_string(uint8_t **bit_string, const bbg05_key_t *key) {
  int result_status = STS_OK;

  *bit_string = malloc(SECURITY_PARAMETER);
  TRY {
    // Hash binary represented bit string
    md_kdf2(*bit_string, SECURITY_PARAMETER, key->k, GT_SIZE_COMPRESSED);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

void bbg05_serialize_public_params(uint8_t **serialized,
                                   const bbg05_public_params_t *public_params) {
  size_t size = bbg05_get_public_params_size(public_params);
  *serialized = malloc(size);

  memcpy(*serialized, public_params->g, G2_SIZE_COMPRESSED);
  memcpy(*serialized + G2_SIZE_COMPRESSED, public_params->g1, G2_SIZE_COMPRESSED);
  memcpy(*serialized + 2 * G2_SIZE_COMPRESSED,
         public_params->pairing_g1_g2, GT_SIZE_COMPRESSED);
  memcpy(*serialized + 2 * G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED,
         public_params->g3, G1_SIZE_COMPRESSED);
  memcpy(*serialized + 2 * G2_SIZE_COMPRESSED  + GT_SIZE_COMPRESSED + G1_SIZE_COMPRESSED,
         &public_params->max_depth, sizeof(unsigned));

  for (unsigned i = 0; i < public_params->max_depth; i++) {
    memcpy(*serialized + 2 * G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED +
    G1_SIZE_COMPRESSED + sizeof(unsigned) + i * G1_SIZE_COMPRESSED,
           public_params->h[i],
           G1_SIZE_COMPRESSED);
  }
}

void bbg05_deserialize_public_params(bbg05_public_params_t **public_params,
                                     const uint8_t *serialized) {
  unsigned max_depth;
  memcpy(&max_depth, serialized + 2 * G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED + G1_SIZE_COMPRESSED,
         sizeof(unsigned));

  *public_params = malloc((2 * G2_SIZE_COMPRESSED
      + GT_SIZE_COMPRESSED
      + G1_SIZE_COMPRESSED
      + sizeof(unsigned)
      + max_depth * G1_SIZE_COMPRESSED));

  (*public_params)->g = malloc((size_t) G2_SIZE_COMPRESSED);
  (*public_params)->g1 = malloc((size_t) G2_SIZE_COMPRESSED);
  (*public_params)->pairing_g1_g2 = malloc((size_t) GT_SIZE_COMPRESSED);
  (*public_params)->g3 = malloc((size_t) G1_SIZE_COMPRESSED);

  for (unsigned i = 0; i < max_depth; i++) {
    (*public_params)->h[i] = malloc((size_t) G1_SIZE_COMPRESSED);
  }

  memcpy((*public_params)->g, serialized, G2_SIZE_COMPRESSED);
  memcpy((*public_params)->g1, serialized + G2_SIZE_COMPRESSED, G2_SIZE_COMPRESSED);
  memcpy((*public_params)->pairing_g1_g2, serialized + 2 * G2_SIZE_COMPRESSED,
         GT_SIZE_COMPRESSED);
  memcpy((*public_params)->g3, serialized + 2 * G2_SIZE_COMPRESSED +
      GT_SIZE_COMPRESSED, G1_SIZE_COMPRESSED);

  (*public_params)->max_depth = max_depth;

  for (unsigned i = 0; i < max_depth; i++) {
    memcpy((*public_params)->h[i], serialized + 2 * G2_SIZE_COMPRESSED
        + GT_SIZE_COMPRESSED + G1_SIZE_COMPRESSED +
        sizeof(unsigned) + i * G1_SIZE_COMPRESSED, G1_SIZE_COMPRESSED);
  }
}

void bbg05_serialize_master_key(uint8_t **serialized,
                                const bbg05_master_key_t *master_key) {
  *serialized = malloc((size_t) G1_SIZE_COMPRESSED);
  memcpy(*serialized, master_key->mk, G1_SIZE_COMPRESSED);
}

void bbg05_deserialize_master_key(bbg05_master_key_t **master_key,
                                  const uint8_t *serialized) {
  *master_key = malloc(sizeof(bbg05_master_key_t));
  (*master_key)->mk = malloc((size_t) G1_SIZE_COMPRESSED);
  memcpy((*master_key)->mk, serialized, G1_SIZE_COMPRESSED);
}

void bbg05_serialize_secret_key_inner_node(uint8_t **serialized,
                                           const bbg05_secret_key_inner_node_t *secret_key) {
  size_t size = (size_t) (G1_SIZE_COMPRESSED
      + G2_SIZE_COMPRESSED
      + G1_SIZE_COMPRESSED
      + sizeof(unsigned)
      + secret_key->num_delegatable_levels * G1_SIZE_COMPRESSED);
  *serialized = malloc(size);

  memcpy(*serialized, secret_key->a0, G1_SIZE_COMPRESSED);
  memcpy(*serialized + G1_SIZE_COMPRESSED, secret_key->a1, G2_SIZE_COMPRESSED);
  memcpy(*serialized + G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED,
         secret_key->associated_id, G1_SIZE_COMPRESSED);
  memcpy(*serialized + 2 * G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED,
         &secret_key->num_delegatable_levels, sizeof(unsigned));

  for(unsigned i = 0; i < secret_key->num_delegatable_levels; i++) {
    memcpy(*serialized + 2 * G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED + sizeof(unsigned)
               + i * G1_SIZE_COMPRESSED,
           secret_key->b[i],
           G1_SIZE_COMPRESSED);
  }
}

void bbg05_deserialize_secret_key_inner_node(bbg05_secret_key_inner_node_t **secret_key,
                                             const uint8_t *serialized) {
  unsigned num_delegatable_levels;
  memcpy(&num_delegatable_levels, serialized + 2 * G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED,
         sizeof(unsigned));

  *secret_key = malloc(G1_SIZE_COMPRESSED
      + G2_SIZE_COMPRESSED
      + G1_SIZE_COMPRESSED
      + sizeof(unsigned)
      + num_delegatable_levels * G1_SIZE_COMPRESSED);

  (*secret_key)->a0 = malloc((size_t) G1_SIZE_COMPRESSED);
  (*secret_key)->a1 = malloc((size_t) G2_SIZE_COMPRESSED);
  (*secret_key)->associated_id = malloc((size_t) G1_SIZE_COMPRESSED);
  for (unsigned i = 0; i < num_delegatable_levels; i++) {
    (*secret_key)->b[i] = malloc((size_t) G1_SIZE_COMPRESSED);
  }

  memcpy((*secret_key)->a0, serialized, G1_SIZE_COMPRESSED);
  memcpy((*secret_key)->a1, serialized + G1_SIZE_COMPRESSED, G2_SIZE_COMPRESSED);
  memcpy((*secret_key)->associated_id, serialized + G1_SIZE_COMPRESSED +
      G2_SIZE_COMPRESSED, G1_SIZE_COMPRESSED);

  (*secret_key)->num_delegatable_levels = num_delegatable_levels;

  for (unsigned i = 0; i < num_delegatable_levels; i++) {
    memcpy((*secret_key)->b[i], serialized + 2 * G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED + sizeof(unsigned)
        + i * G1_SIZE_COMPRESSED, G1_SIZE_COMPRESSED);
  }
}

void bbg05_serialize_secret_key_leaf_node(uint8_t **serialized,
                                          const bbg05_secret_key_leaf_node_t *secret_key) {
  *serialized = malloc((size_t) (G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED));
  memcpy(*serialized, secret_key->a0, G1_SIZE_COMPRESSED);
  memcpy(*serialized + G1_SIZE_COMPRESSED, secret_key->a1, G2_SIZE_COMPRESSED);
}

void bbg05_deserialize_secret_key_leaf_node(bbg05_secret_key_leaf_node_t **secret_key,
                                            const uint8_t *serialized) {
  *secret_key = malloc(sizeof(bbg05_secret_key_leaf_node_t));
  (*secret_key)->a0 = malloc((size_t) G1_SIZE_COMPRESSED);
  (*secret_key)->a1 = malloc((size_t) G2_SIZE_COMPRESSED);
  memcpy((*secret_key)->a0, serialized, G1_SIZE_COMPRESSED);
  memcpy((*secret_key)->a1, serialized + G1_SIZE_COMPRESSED, G2_SIZE_COMPRESSED);
}

void bbg05_serialize_ciphertext(uint8_t **serialized,
                                const bbg05_ciphertext_t *ciphertext) {
  *serialized = malloc((size_t) (GT_SIZE_COMPRESSED + 2 * G2_SIZE_COMPRESSED +
      2 * G1_SIZE_COMPRESSED));
  memcpy(*serialized, ciphertext->a, GT_SIZE_COMPRESSED);
  memcpy(*serialized + GT_SIZE_COMPRESSED, ciphertext->b, G2_SIZE_COMPRESSED);
  memcpy(*serialized + GT_SIZE_COMPRESSED + G2_SIZE_COMPRESSED, ciphertext->c,
         G1_SIZE_COMPRESSED);
  memcpy(*serialized + GT_SIZE_COMPRESSED + G2_SIZE_COMPRESSED + G1_SIZE_COMPRESSED, ciphertext->verification_key, G2_SIZE_COMPRESSED);
  memcpy(*serialized + GT_SIZE_COMPRESSED + 2 * G2_SIZE_COMPRESSED + G1_SIZE_COMPRESSED, ciphertext->signature, G1_SIZE_COMPRESSED);
}

void bbg05_deserialize_ciphertext(bbg05_ciphertext_t **ciphertext,
                                  const uint8_t *serialized) {
  *ciphertext = malloc(sizeof(bbg05_ciphertext_t));
  (*ciphertext)->a = malloc((size_t) GT_SIZE_COMPRESSED);
  (*ciphertext)->b = malloc((size_t) G2_SIZE_COMPRESSED);
  (*ciphertext)->c = malloc((size_t) G1_SIZE_COMPRESSED);
  (*ciphertext)->verification_key = malloc((size_t) G2_SIZE_COMPRESSED);
  (*ciphertext)->signature = malloc((size_t) G1_SIZE_COMPRESSED);
  memcpy((*ciphertext)->a, serialized, GT_SIZE_COMPRESSED);
  memcpy((*ciphertext)->b, serialized + GT_SIZE_COMPRESSED, G2_SIZE_COMPRESSED);
  memcpy((*ciphertext)->c, serialized + GT_SIZE_COMPRESSED + G2_SIZE_COMPRESSED,
         G1_SIZE_COMPRESSED);
  memcpy((*ciphertext)->verification_key, serialized + GT_SIZE_COMPRESSED + G2_SIZE_COMPRESSED + G1_SIZE_COMPRESSED, G2_SIZE_COMPRESSED);
  memcpy((*ciphertext)->signature, serialized + GT_SIZE_COMPRESSED + 2 * G2_SIZE_COMPRESSED + G1_SIZE_COMPRESSED, G1_SIZE_COMPRESSED);
}

void bbg05_serialize_key(uint8_t **serialized,
                         const bbg05_key_t *key) {
  *serialized = malloc((size_t) GT_SIZE_COMPRESSED);
  memcpy(*serialized, key->k, GT_SIZE_COMPRESSED);
}

void bbg05_deserialize_key(bbg05_key_t **key, const uint8_t *serialized) {
  *key = malloc(sizeof(bbg05_key_t));
  (*key)->k = malloc((size_t) GT_SIZE_COMPRESSED);
  memcpy((*key)->k, serialized, GT_SIZE_COMPRESSED);
}

void bbg05_serialize_identity(uint8_t **serialized,
                              const bbg05_identity_t *identity) {
  *serialized = malloc((1 + identity->depth) * sizeof(unsigned));
  memcpy(*serialized, &identity->depth, sizeof(unsigned));

  for(unsigned i = 0; i < identity->depth; i++) {
    memcpy(*serialized + (i + 1) * sizeof(unsigned), &identity->id[i], sizeof(unsigned));
  }
}

void bbg05_deserialize_identity(bbg05_identity_t **identity, const uint8_t *serialized) {
  unsigned depth;
  memcpy(&depth, serialized, sizeof(unsigned));

  *identity = malloc((1 + depth) * sizeof(unsigned));

  (*identity)->depth = depth;

  for(unsigned i = 0; i < depth; i++) {
    memcpy(&(*identity)->id[i], serialized + (i + 1) * sizeof(unsigned),
           sizeof(unsigned));
  }
}

int bbg05_master_keys_are_equal(const bbg05_master_key_t *l,
                                const bbg05_master_key_t *r) {
  int equal = !memcmp(l->mk, r->mk, (size_t) G1_SIZE_COMPRESSED);
  return equal;
}

int bbg05_public_params_are_equal(const bbg05_public_params_t *l,
                                  const bbg05_public_params_t *r) {
  int equal = !memcmp(l->g, r->g, (size_t) G2_SIZE_COMPRESSED);
  equal &= !memcmp(l->g1, r->g1, (size_t) G2_SIZE_COMPRESSED);
  equal &= !memcmp(l->pairing_g1_g2, r->pairing_g1_g2, (size_t) GT_SIZE_COMPRESSED);
  equal &= !memcmp(l->g3, r->g3, (size_t) G1_SIZE_COMPRESSED);
  equal &= l->max_depth == r->max_depth;

  for(unsigned i = 0; i < l->max_depth; i++) {
    equal &= !memcmp(l->h[i], r->h[i], (size_t) G1_SIZE_COMPRESSED);
  }

  return equal;
}

int bbg05_secret_keys_inner_nodes_are_equal(const bbg05_secret_key_inner_node_t *l,
                                            const bbg05_secret_key_inner_node_t *r) {
  int equal = !memcmp(l->a0, r->a0, (size_t) G1_SIZE_COMPRESSED);
  equal &= !memcmp(l->a1, r->a1, (size_t) G2_SIZE_COMPRESSED);
  equal &= l->num_delegatable_levels == r->num_delegatable_levels;
  equal &= !memcmp(l->associated_id, r->associated_id, (size_t) G1_SIZE_COMPRESSED);

  for(unsigned i = 0; i < l->num_delegatable_levels; i++) {
    equal &= !memcmp(l->b[i], r->b[i], (size_t) G1_SIZE_COMPRESSED);
  }

  return equal;
}

int bbg05_secret_keys_leaf_nodes_are_equal(const bbg05_secret_key_leaf_node_t *l,
                                           const bbg05_secret_key_leaf_node_t *r) {
  int equal = !memcmp(l->a0, r->a0, (size_t) G1_SIZE_COMPRESSED);
  equal &= !memcmp(l->a1, r->a1, (size_t) G2_SIZE_COMPRESSED);
  return equal;
}

int bbg05_ciphertexts_are_equal(const bbg05_ciphertext_t *l,
                                const bbg05_ciphertext_t *r) {
  int equal = !memcmp(l->a, r->a, (size_t) GT_SIZE_COMPRESSED);
  equal &= !memcmp(l->b, r->b, (size_t) G2_SIZE_COMPRESSED);
  equal &= !memcmp(l->c, r->c, (size_t) G1_SIZE_COMPRESSED);
  return equal;
}

int bbg05_keys_are_equal(const bbg05_key_t *l, const bbg05_key_t *r) {
  int equal = !memcmp(l->k, r->k, (size_t) GT_SIZE_COMPRESSED);
  return equal;
}

int bbg05_identities_are_equal(const bbg05_identity_t *l, const bbg05_identity_t *r) {
  int equal = (l->depth == r->depth);

  for(unsigned i = 0; i < l->depth; i++) {
    equal &= (l->id[i] == r->id[i]);
  }

  return equal;
}

unsigned bbg05_get_public_params_size(const bbg05_public_params_t *public_params) {
  return (1 + public_params->max_depth) * G1_SIZE_COMPRESSED +
  2 * G2_SIZE_COMPRESSED + sizeof(unsigned) + GT_SIZE_COMPRESSED;
}
