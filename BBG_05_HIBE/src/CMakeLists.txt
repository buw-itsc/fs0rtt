set(BBG05_SRCS BBG_05_HIBE.c BBG_05_HIBE_util.c)

find_library(RELIC relic HINTS /usr/local/lib)

add_library(${BBG05} SHARED ${BBG05_SRCS})
target_link_libraries (${BBG05} ${RELIC})
install (TARGETS ${BBG05} DESTINATION lib)

