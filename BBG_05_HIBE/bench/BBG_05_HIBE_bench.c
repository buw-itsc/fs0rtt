// Copyright [2018] PG 0-RTT

#include <stdio.h>
#include <BBG_05_HIBE_types.h>
#include "relic/relic.h"
#include "BBG_05_HIBE.h"

unsigned MAX_DEPTH;

static void setup_bench(void) {
  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;

  BENCH_SMALL("setup_bench", bbg05_setup(&mk, &params, MAX_DEPTH));

  bbg05_set_master_key_to_zero(mk);
  bbg05_set_public_params_to_zero(params);
  free(mk);
  free(params);
}

static void key_generation_leaf_node_from_parent_bench(void) {
  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;

  // identity of inner node
  unsigned id_depth = 4;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  // identity of leaf node
  unsigned leaf_id_depth = 5;
  bbg05_identity_t *leaf_bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      leaf_id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < leaf_id_depth; i++) {
    leaf_bbg05id->id[i] = i;
  }
  leaf_bbg05id->depth = leaf_id_depth;

  bbg05_secret_key_inner_node_t *sk_inner;
  bbg05_secret_key_leaf_node_t *sk_leaf;


  bbg05_setup(&mk, &params, MAX_DEPTH);
  bbg05_key_generation_inner_node_from_master_key(&sk_inner,
                                                  mk,
                                                  bbg05id,
                                                  params);

  BENCH_SMALL("bbg05_key_generation_leaf_node_from_parent", bbg05_key_generation_leaf_node_from_parent_full_identity(&sk_leaf,
                                                           sk_inner,
                                                           leaf_bbg05id,
                                                           params));

  bbg05_set_master_key_to_zero(mk);
  bbg05_set_public_params_to_zero(params);
  bbg05_set_secret_key_inner_node_to_zero(sk_inner);
  free(mk);
  free(params);
  free(sk_inner);
}

static void encapsulate_bench(void) {
  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;

  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  bbg05_secret_key_leaf_node_t *sk_leaf;

  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t* key;

  bbg05_setup(&mk, &params, MAX_DEPTH);
  bbg05_key_generation_leaf_node_from_master_key(&sk_leaf,
                                                 mk,
                                                 bbg05id,
                                                 params);

  BENCH_SMALL("bbg05_encapsulate", bbg05_encapsulate(&ciphertext, &key, params, bbg05id));

  bbg05_set_master_key_to_zero(mk);
  bbg05_set_public_params_to_zero(params);
  free(mk);
  free(params);
}

static void decapsulate_bench(void) {
  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;

  // inner node
  unsigned id_depth = 5;
  bbg05_identity_t *bbg05id = malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    bbg05id->id[i] = i;
  }
  bbg05id->depth = id_depth;

  bbg05_secret_key_inner_node_t *sk_leaf;
  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t* key;
  bbg05_key_t* decapsulatedKey;

  bbg05_setup(&mk, &params, MAX_DEPTH);
  bbg05_key_generation_inner_node_from_master_key(&sk_leaf,
                                                 mk,
                                                 bbg05id,
                                                 params);
  bbg05_encapsulate(&ciphertext, &key, params, bbg05id);

  BENCH_SMALL("bbg05_decapsulate", bbg05_decapsulate(&decapsulatedKey, sk_leaf, ciphertext, params));

  bbg05_set_master_key_to_zero(mk);
  bbg05_set_public_params_to_zero(params);
  bbg05_set_secret_key_inner_node_to_zero(sk_leaf);
  bbg05_set_ciphertext_to_zero(ciphertext);
  bbg05_set_key_to_zero(key);
  free(mk);
  free(params);
  free(sk_leaf);
  free(ciphertext);
  free(key);
}

int main(void) {
  MAX_DEPTH = 5;

  if (bbg05_init() != STS_OK) {
    core_clean();
    return 1;
  }

  util_banner("Benchmarks for the BBG05 HIBE module:", 0);
  setup_bench();
  key_generation_leaf_node_from_parent_bench();
  encapsulate_bench();
  decapsulate_bench();

  core_clean();
  return 0;
}