//
// Created by till on 27.09.18.
//

#include "Encryptor.h"

#include <memory>
#include <cmath>
#include <cstdint>
#include <sstream>
#include <cstring>
#include <iostream>
#include <fstream>

#include <djss18bfe/BloomFilterEncryption.h>
#include <djss18bfe/BFE_IBBE/BloomFilterEncryptionIBBE.h>
#include <djss18bfe/Ciphertext.h>
#include <djss18bfe/BFE_IBBE/CiphertextIBBE.h>
#include <djss18bfe/BFE_IBBE/PublicKeyIBBE.h>
#include <djss18bfe/PublicKey.h>
#include <djss18bfe/BFE_IBBE/KeyIBBE.h>
#include <del07ibbe/Delerablee_07_IBBE.h>
#include <del07ibbe/Delerablee_07_IBBE_util.h>

using namespace bfe;
using namespace std;

/**
 * This constructor is used on the SERVER side to create a bfe - ibbe
 * encryptor object.
 * @param m The size of the Bloom Filter
 * @param k The number of distinct hash functions of the Bloom Filter
 * @param readKeysFromFile If true the key pair is read in from file
 * @throws std::runtime_error if the key can not be read in.
 */
Encryptor::Encryptor(unsigned long m, unsigned int k, bool readKeysFromFile):
        encryptionScheme(bfe_ibbe),
        m(m),
        k(k),
        t(0)
{
    encryption = new BloomFilterEncryptionIBBE(m,k);
    encryption->keyGeneration(readKeysFromFile);
}


/**
 * This constructor is used on the SERVER side to create a bfe - hibe
 * encryptor object. WARNING: THERE IS CURRENTLY NO HIBE IMPLEMENTATION
 * @param m  The size of the Bloom Filter
 * @param k  The number of distinct hash functions of the Bloom Filter
 * @param t  The number of timesteps
 *
 *Encryptor::Encryptor(unsigned long m, unsigned int k, unsigned int t):
 *        encryptionScheme(bfe_hibe),
 *        m(m),
 *        k(k),
 *        t(t)
 *{
 *    //TODO HIBE_Implementation
 *    cerr << "CRITICAL ERROR: HIBE IMPLEMENTATION MISSING" << endl;
 *
 *}
 */

/**
 * This constructor is used on the CLIENT side to create an encryptor
 * object from a public key.
 * @param encryptionScheme  The used encryption scheme
 * @param publicKey         A pointer to the public key
 *
 * @throws std::invalid_argument if `publicKey` can not be deserialized.
 */
Encryptor::Encryptor(EncryptionScheme encryptionScheme, uint8_t* publicKey):
        encryptionScheme(encryptionScheme),
        m(0),
        k(0),
        t(0)
{
    switch (encryptionScheme){
        case bfe_ibbe:{

            //create the encryption object
            encryption = new BloomFilterEncryptionIBBE(publicKey);

            break;
        }
        case bfe_hibe:{
            //TODO HIBE_Implementation
            cerr << "CRITICAL ERROR: HIBE IMPLEMENTATION MISSING" << endl;
            break;
        }
    }
}


/**
 * This method is used on SERVER to extract the public key of this
 * encryptor object.
 *
 * @param publicKey The caller has to provide an array of the size that
 * is returned by getPublicKeySize(). This function copies the public
 * key into the given array. The caller keeps ownership of that array.
 */
void Encryptor::getPublicKey(uint8_t* publicKey){
    //copy the public key into the given array
    std::memcpy(publicKey, encryption->getPublicKey()->getSerialized(), getPublicKeySize() * sizeof(uint8_t));
}

/**
 * This method is used on SERVER side to compute the size of the
 * public key in bytes.
 * @return size of the public key in bytes
 */
unsigned long Encryptor::getPublicKeySize(){
    return encryption->getPublicKey()->getSerializedSize();
}

/**
 * This function is used on Client side. It checks whether the given
 * public key is a valid IBBE public key or not
 * @param publicKey         - the public key that gets checkt
 * @return                  - 1, if the given public key is valid,
 *                            0 otherwise.
 */

const int Encryptor::publicKeyIsValid(EncryptionScheme encryptionScheme,
                                     uint8_t *publicKey) {
    switch (encryptionScheme){
        case bfe_ibbe: {
            try {
                bfe::PublicKeyIBBE *test = new bfe::PublicKeyIBBE(publicKey, false);
                bool rv = test->isValid();
                delete test;
                if (rv == 0) {
                    return 0;
                } else {
                    return 1;
                }
            } catch (const std::invalid_argument &ex) {
                return 0;
            }
        }
        case bfe_hibe: {
            //TODO HIBE_Implementation
            cerr << "CRITICAL ERROR: HIBE IMPLEMENTATION MISSING" << endl;
            return 0;
        }
    }
}

/**
 * This method is used on CLIENT side to create a new session key of the
 * size that is returned by getSessionKeySize(). Currently this size is
 * fixed to 32 byte (256 bit) but this may change in the future.
 *
 * @param sessionKey The caller has to provide an array of the size that
 * is returned by getSessionKeySize(). This function copies the
 * generated session key into the given array. The caller keeps ownership
 * of that array.
 *
 * @param cipherText The caller has to provide an array of the size that
 * is returned by getCipherTextSize(). This function copies the
 * generated cipherText into the given array. The caller keeps ownership
 * of that array.
 */
void Encryptor::encapsulate(uint8_t* sessionKey, uint8_t* cipherText){
    //randomly choose a new session key and encapsulate it
    pair<unique_ptr<Key>, unique_ptr<Ciphertext>> keyMaterial = encryption->encapsulate();

    //copy the session key from stack to heap
    std::memcpy(sessionKey, keyMaterial.first->getKey(), getSessionKeySize() * sizeof(uint8_t));

    //copy the cipherText from the stack to the heap
    std::memcpy(cipherText, keyMaterial.second->getSerialized(), getCipherTextSize() * sizeof(uint8_t));
}


/**
 * This method is used on SERVER side to decapsulate an encrypted
 * session key.
 *
 * @param cipherText A pointer to the cipher text that encapsulates the
 * session key. The caller keeps ownership of the array.
 *
 * @param sessionKey The caller has to provide an array of the size that
 * is returned by getSessionKeySize(). This function copies the
 * decapsulated session key into this array. The caller keeps ownership
 * of this array.
 *
 * @throws std::invalid_argument if `cipherText` can not be deserialized.
 */
void Encryptor::decapsulate(uint8_t* cipherText, uint8_t* sessionKey){
    switch (encryptionScheme){
        case bfe_ibbe: {

            //deserialize cipher text
            auto *deserializedCiphertext = new CiphertextIBBE(cipherText);

            //decapsulate key
            std::unique_ptr<Key> decapsulatedKey = encryption->decapsulate(deserializedCiphertext);
	    if(decapsulatedKey==nullptr){
	    	throw std::invalid_argument("ctxt already used");
	    }
	
            //copy the session key from stack to heap
            memcpy(sessionKey, decapsulatedKey->getKey(), getSessionKeySize() *
                    sizeof(uint8_t));
            break;
        }
        case bfe_hibe: {
            //TODO HIBE_Implementation
            cerr << "CRITICAL ERROR: HIBE IMPLEMENTATION MISSING" << endl;
        }
    }
}


/**
 * This function computes the size of the session key for this encryptor.
 * Currently this size is fixed to 32. But this may change in the future.
 * @return the size of the session key in bytes
 */
unsigned int Encryptor::getSessionKeySize(){
    // The size of the session key is currently fixed to 32 Bytes
    return 32;
}


/**
 * This function computes the size of the cipher text for this encryptor
 * @return the size of the cipher text in bytes
 */
unsigned int Encryptor::getCipherTextSize(){
    switch (encryptionScheme){
        case bfe_ibbe: {
            return (int) bfe::CiphertextIBBE::getSerializedSize();
        }
        case bfe_hibe:{
            //TODO HIBE_Implementation
            cerr << "CRITICAL ERROR: HIBE IMPLEMENTATION MISSING" << endl;
            return 0;
        }
    }
    return 0;
}
