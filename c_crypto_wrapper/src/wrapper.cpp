#include "wrapper.h"
#include "Encryptor.h"
#include <djss18bfe/BFE_IBBE/PublicKeyIBBE.h>

/**
 * This constructor is used on server side to create a new BFE-IBBE
 * encryptor.
 *
 * @param bfeIbbe           - A pointer to the BFE-IBBE encryptor.
 * @param m                 - The size of the Bloom Filter
 * @param k                 - The number of distinct hash functions of the
 *                            Bloom Filter
 * @param readKeysFromFile  - If true the key pair is read in from file.
 *                            This is only allowed for internal debug and
 *                            MUST NOT be used for releases.
 * @return                  - 0 if no error occurred, else 1.
 */
int newBFEIBBE(void **bfeIbbe, unsigned long m, unsigned int k, int readKeysFromFile){
  if (readKeysFromFile){
    printf("WARNING: READING KEYS FROM FILE IS ONLY ALLOWED FOR DEBUG!\n");
  }
  try {
    *bfeIbbe = new Encryptor(m,k,(bool) readKeysFromFile);
  } catch (std::exception& e) {
    return 1;
  }

  return 0;
}


/**
 * This constructor is used on server side to create a new BFE-HIBE
 * encryptor. WARNING:
 * THERE IS CURRENTLY NO HIBE IMPLEMENTATION
 * @param bfeHibe[out]      - A pointer to the BFE-HIBE encryptor
 * @param m                 - The size of the Bloom Filter
 * @param k                 - The number of distinct hash functions of the
 *                            Bloom Filter
 * @param numberTimesteps   - The number of timesteps.
 * @param readKeysFromFile  - If true the key pair is read in from file.
 *                            This is only allowed for internal debug and
 *                            MUST NOT be used for releases.
 * @return                  - 0 if no error occurred, else 1.
 */
//void* newBFEHIBE(void** bfeHibe, unsigned long m, unsigned int k,
//        unsigned int numberTimesteps, int readKeysFromFile);


/**
     * This constructor is used on client side to create a new bfe-ibbe scheme
     * from a given public key.
     * @param bfeIbbe[out]      - A pointer to the BFE-IBBE encryptor.
     * @param publickey[in]     - A pointer to the uint8_t array that contains
     *                            the public key. The caller keeps ownership of
     *                            that array.
     * @return                  - 0 if no error occurred, else 1.
     */
int BFEIBBEFromPublicKey(void **bfeIbbe, uint8_t* publicKey){
  try {
    *bfeIbbe = new Encryptor(bfe_ibbe, publicKey);
  } catch (std::exception& e) {
    return 1;
  }

  return 0;
}


/**
 * This constructor is used on client side to create a new bfe-hibe scheme
 * from a given public key.
 * WARNING: THERE IS CURRENTLY NO HIBE IMPLEMENTATION
 * @param bfeIbbe[out]      - A pointer to the BFE-HIBE encryptor.
 * @param publickey[in]     - A pointer to the uint8_t array that contains
 *                            the public key. The caller keeps ownership of
 *                            that array.
 * @return                  - 0 if no error occurred, else 1.
 */
//int BFEHIBEFromPublicKey(void **bfeHibe, uint8_t* publickey);


/**
 * This function can be used on client and server side.
 * It copies the public key into the given array
 *
 * @param encryptor[in]     - A pointer to the encryptor object.
 * @param publicKey[out]    - The caller has to provide an array of
 *                            the size that is returned by
 *                            getPublicKeySize(...)
 *                            This function copies the public key into the
 *                            given array.
 *                            The caller keeps ownership of the given array.
 * @return                  - 0 if no error occurred, else 1.
 */
int getPublicKey(void* encryptor, uint8_t* publicKey){
  try {
    ((Encryptor*) encryptor)->getPublicKey(publicKey);
  } catch (std::exception& e) {
    return 1;
  }

  return 0;
}


/**
 * Computes the size of the public key that belongs to the
 * given encryptor object
 * @param encryptor         - pointer to the encryptor object
 * @return                  - the size of the public key
 */
unsigned long getPublicKeySize(void* encryptor){
    return ((Encryptor*) encryptor)->getPublicKeySize();
}


/**
 * This function is used on Client side. It checks whether the given
 * public key is a valid IBBE public key or not
 * @param publicKey         - the public key that gets checkt
 * @return                  - 1, if the given public key is valid,
 *                            0 otherwise.
 */
int publicIBBEKeyisValid(uint8_t* publicKey){
    return Encryptor::publicKeyIsValid(bfe_ibbe, publicKey);
}


/**
 * Generates a session key of size getSessionKeySize(...) and encrypts it
 * with the public key. Currently the size of the session key is fixed to
 * 32 byte (256 bit) but this may change in the future.This function is
 * usually called from the client.
 *
 * @param encryptor[in]     - A pointer to the encryptor object that
 *                            should be used to decapsulate this ciphertext
 * @param sessionKey[out]   - The caller has to provide an array of the size
 *                            that is returned by getSessionKeySize(...).
 *                            This function copies the session key into the
 *                            given array. The caller keeps ownership of
 *                            this array.
 * @param cipherText[out]   - The caller cas to provide an array of the size
 *                            that is returned by getCipherTextSize(...).
 *                            This function copies the ciphertext into the
 *                            given array. The caller keeps ownership of
 *                            this array.
 * @return                  - 0 if no error occurred, else 1.
 */
int encapsulate(void* encryptor, uint8_t* sessionKey, uint8_t* cipherText){
  try {
    ((Encryptor *) encryptor)->encapsulate(sessionKey, cipherText);
  } catch (std::exception& e) {
    return 1;
  }
  return 0;
}


/**
 * Decapsulates a session key from the given ciphertext.
 * This function is usually called from the server.
 *
 * @param encryptor         - A pointer to the encryptor object that
 *                            should be used to decapsulate this ciphertext.
 * @param cipherText[in]    - A pointer to a uint8_t array of the size that
 *                            is returned by getCipherTextSize(...).
 *                            This array has to contain the encapsulated
 *                            session key. The caller keeps ownership of
 *                            this array.
 * @param sessionKey[out]   - The caller has to provide an array of the size
 *                            that is returned by getSessionKeySize(...).
 *                            This function copies the session key into the
 *                            given array. The caller keeps ownership of
 *                            this array.
 *
 * @return                  - 0 if no error occurred, else 1.
 */
int decapsulate(void* encryptor, uint8_t* cipherText, uint8_t* sessionKey){
  try {
    ((Encryptor *) encryptor)->decapsulate(cipherText, sessionKey);
  } catch (std::exception& e) {
    return 1;
  }
  return 0;
}


/**
 * This function computes the size of the session key for a given
 * encryptor object
 * @param encryptor         - A pointer to the encryptor object that
 * @return                  - The size of the secret key in bytes
 */
unsigned int getSessionKeySize(void* encryptor){
    return ((Encryptor*) encryptor)->getSessionKeySize();
}


/**
 * This function computes the size of the cipher text for a given
 * encryptor object
 * @param encryptor         - A pointer to the encryptor object
 * @return                  - The size of the cipher text in bytes
 */
unsigned int getCipherTextSize(void* encryptor){
    return ((Encryptor*) encryptor)->getCipherTextSize();
}


/**
 * Destroys the given encryptor object and frees the used memory
 * @param encryptor         - A pointer to the encryptor object that
 *                            should be destroyed.
 */
void PE_destroy(void* encryptor){
    delete ((Encryptor*) encryptor);
}