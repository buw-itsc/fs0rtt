# C Crypto Wrapper
This wrapper allows to use the C++ library of BFE-IBBE and BFE-HIBE in C programs.

See "install_and_usage_instructions.pdf" for further information.
