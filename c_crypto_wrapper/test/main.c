#include <stdio.h>
#include <stdlib.h>
#include "wrapper.h"

static void printkey(char* text, uint8_t* key, int size){
    printf("%s: ", text);
    for (int i = 0; i < size; i++){
        if (i % 8 == 0) printf("\n\t");
        printf("%02x ", key[i]);
    }
    printf("\n");
}

int testIBBE(){
    printf("-----------------------------------------------------------\n");
    printf("                         IBBE Test\n");
    printf("-----------------------------------------------------------\n");
    {
        /*
         * SERVER CREATION
         */
        printf("Create server...\n");
        void* server;
        int status = newBFEIBBE(&server, 100, 3, 1);
        if (status) {
          fprintf(stderr, "Unable to create server BFE IBBE!\n");
          return status;
        }
        uint8_t* server_publicKey = malloc(getPublicKeySize(server) * sizeof(uint8_t));
        getPublicKey(server, server_publicKey);

        /*
         * TRANSFER PUBLIC KEY FROM SERVER TO CLIENT
         */
        printf("Transfer public key from server to client...\n");
        uint8_t* client_publicKey = malloc(getPublicKeySize(server) * sizeof(uint8_t));
        for (unsigned int i = 0; i < getPublicKeySize(server); i++){
            *(client_publicKey + i) = *(server_publicKey + i);
        }

        /*
         * CHECK TRANSFERRED PUBLIC KEY
         */
        printf("Check the transferred public key...\n");
        if (publicIBBEKeyisValid(client_publicKey)) {
            printf("Transferred key is valid.\n");
        } else {
            fprintf(stderr, "Transferred key is NOT valid!\n");
            return 1;
        }

        printf("Test negative case of public key check:\n");
        uint8_t* falseKey_correct_size = malloc(getPublicKeySize(server) * sizeof(uint8_t));
        for (int i = 0; i < getPublicKeySize(server); i++){
            *(falseKey_correct_size + 1) = i;
        }
        if (publicIBBEKeyisValid(falseKey_correct_size)) {
            fprintf(stderr, "Error: Transferred key is valid although it is a wrong bitstring\n");
            return 1;
        } else {
            printf("Correctly detected that the wrong bitstring is not a valid key!\n");
        }
        /*
         * CLIENT CREATION
         */
        printf("Create client from transferred public key...\n");
        void* client;
        status = BFEIBBEFromPublicKey(&client, client_publicKey);
        if (status) {
          fprintf(stderr, "Unable to create client BFE IBBE!\n");
          return status;
        }


        /*
         * CLIENT ENCAPSULATE
         */
        printf("Create and encapsulate session key on client side...\n");
        uint8_t* client_sessionKey = malloc(getSessionKeySize(client) * sizeof(uint8_t));
        uint8_t* client_cipherText = malloc(getCipherTextSize(client) * sizeof(uint8_t));
        encapsulate(client, client_sessionKey, client_cipherText);

        printkey("Session Key [CLIENT]", client_sessionKey, getSessionKeySize(client));

        /*
         * TRANSFER CIPHERTEXT FROM SERVER TO CLIENT
         */
        printf("Transfer ciphertext from client to server...\n");
        uint8_t* server_cipherText = malloc(getCipherTextSize(client) * sizeof(uint8_t));
        for (unsigned int i = 0; i < getCipherTextSize(client); i++){
            *(server_cipherText + i) = *(client_cipherText + i);
        }

        /*
         * SERVER DECAPSULATE
         */
        printf("Decapsulate transferred session key on server side...\n");
        uint8_t* server_sessionKey = malloc(getSessionKeySize(server) * sizeof(uint8_t));
        decapsulate(server, server_cipherText, server_sessionKey);
        printkey("Session Key [SERVER]", server_sessionKey, getSessionKeySize(server));

        /*
         * COMPARE KEYS
         */
        printf("Compare keys:... ");
        for (int i = 0; i < getSessionKeySize(client);i++){
            if (client_sessionKey[i] != server_sessionKey[i]){
                printf("\n");
                fprintf(stderr, "Error in Crypto Wrapper IBBE test. Keys are not equal!\n");
                return 1;
            }
        }
        printf("keys are equal!\n");


        /*
         * CLEAN UP
         */
        printf("Cleanup from IBBE test...\n");
        PE_destroy(server);
        server = 0;
        free(server_publicKey);
        free(server_sessionKey);
        free(server_cipherText);

        PE_destroy(client);
        client = 0;
        free(client_publicKey);
        free(client_sessionKey);
        free(client_cipherText);

        //Everything worked as intended
        printf("IBBE test completed successfully!\n");
        return 0;
    }
}

//int testHIBE(){
//    printf("-----------------------------------------------------------\n");
//    printf("                         HIBE Test\n");
//    printf("-----------------------------------------------------------\n");
//    {
//        /*
//         * SERVER CREATION
//         */
//        printf("Create server...\n");
//        void* server = newBFEHIBE(100, 3, 8);
//        uint8_t* server_publicKey = malloc(getPublicKeySize(server) * sizeof(uint8_t));
//        getPublicKey(server, server_publicKey);
//
//        /*
//         * TRANSFER PUBLIC KEY FROM SERVER TO CLIENT
//         */
//        printf("Transfer public key from server to client...\n");
//        uint8_t* client_publicKey = malloc(getPublicKeySize(server) * sizeof(uint8_t));
//        for (unsigned int i = 0; i < getPublicKeySize(server); i++){
//            *(client_publicKey + i) = *(server_publicKey + i);
//        }
//        /*
//         * CLIENT CREATION
//         */
//        printf("Create client from transferred public key...\n");
//        void* client = BFEHIBEFromPublicKey(client_publicKey);
//
//
//        /*
//         * CLIENT ENCAPSULATE
//         */
//        printf("Create and encapsulate session key on client side...\n");
//        uint8_t* client_sessionKey = malloc(getSessionKeySize(client) * sizeof(uint8_t));
//        uint8_t* client_cipherText = malloc(getCipherTextSize(client) * sizeof(uint8_t));
//        encapsulate(client, client_sessionKey, client_cipherText);
//
//        printkey("Session Key [CLIENT]", client_sessionKey, getSessionKeySize(client));
//
//        /*
//         * TRANSFER CIPHERTEXT FROM SERVER TO CLIENT
//         */
//        printf("Transfer ciphertext from client to server...\n");
//        uint8_t* server_cipherText = malloc(getCipherTextSize(client) * sizeof(uint8_t));
//        for (unsigned int i = 0; i < getCipherTextSize(client); i++){
//            *(server_cipherText + i) = *(client_cipherText + i);
//        }
//
//        /*
//         * SERVER DECAPSULATE
//         */
//        printf("Decapsulate transferred session key on server side...\n");
//        uint8_t* server_sessionKey = malloc(getSessionKeySize(server) * sizeof(uint8_t));
//        decapsulate(server, server_cipherText, server_sessionKey);
//        printkey("Session Key [SERVER]", server_sessionKey, getSessionKeySize(server));
//
//        /*
//         * COMPARE KEYS
//         */
//        printf("Compare keys:... ");
//        for (int i = 0; i < getSessionKeySize(client);i++){
//            if (client_sessionKey[i] != server_sessionKey[i]){
//                printf("\n");
//                fprintf(stderr, "Error in Crypto Wrapper HIBE test. Keys are not equal!\n");
//                return 1;
//            }
//        }
//        printf("keys are equal!\n");
//
//
//        /*
//         * CLEAN UP
//         */
//        printf("Cleanup from HIBE test...\n");
//        destroy(server);
//        server = 0;
//        free(server_publicKey);
//        free(server_sessionKey);
//        free(server_cipherText);
//
//        destroy(client);
//        client = 0;
//        free(client_publicKey);
//        free(client_sessionKey);
//        free(client_cipherText);
//
//        //Everything worked as intended
//        printf("HIBE test completed successfully!\n");
//        return 0;
//    }
//}

int main() {
    int rv = 0;
    rv = testIBBE();
    if (rv != 0) return rv;

    //rv = testHIBE();
    //if (rv != 0) return rv;

    return rv;
}