//
// Created by till on 27.09.18.
//

#ifndef C_CRYPTO_WRAPPER_ENCRYPTOR_H
#define C_CRYPTO_WRAPPER_ENCRYPTOR_H

#include <cstdint>

#include <djss18bfe/BloomFilterEncryption.h>


/**
 * Distinguish between bfe_ibbe and bfe_hibe
 */
enum EncryptionScheme{
    bfe_hibe,
    bfe_ibbe
};

/**
 * Objects of this class represent an initialized encryption scheme.
 */
class Encryptor{
private:

    const EncryptionScheme encryptionScheme; // Type of this encryptor
    const unsigned long m;      // The size of the Bloom Filter
    const unsigned int k;      // The number of distinct hash functions of
                                // the Bloom Filter
    const unsigned int t;       // The number of timesteps
    bfe::BloomFilterEncryption* encryption;

public:

    /**
     * This constructor is used on the SERVER side to create a bfe - ibbe
     * encryptor object.
     * @param m The size of the Bloom Filter
     * @param k The number of distinct hash functions of the Bloom Filter
     * @param readKeysFromFile If true the key pair is read in from file
     * @throws std::runtime_error if the key can not be read in.
     */
    Encryptor(unsigned long m, unsigned int k, bool readKeysFromFile);

    /**
     * This constructor is used on the SERVER side to create a bfe - hibe
     * encryptor object. WARNING: THERE IS CURRENTLY NO HIBE IMPLEMENTATION
     * @param m  The size of the Bloom Filter
     * @param k  The number of distinct hash functions of the Bloom Filter
     * @param t  The number of timesteps
     *
     *    Encryptor(unsigned long m, unsigned int k, unsigned int t);
     */

    /**
     * This constructor is used on the CLIENT side to create an encryptor
     * object from a public key.
     * @param encryptionScheme  The used encryption scheme
     * @param publicKey         A pointer to the public key
     *
     * @throws std::invalid_argument if `publicKey` can not be deserialized.
     */
    Encryptor(EncryptionScheme encryptionScheme, uint8_t* publicKey);

    /**
     * This method is used on SERVER to extract the public key of this
     * encryptor object.
     *
     * @param publicKey The caller has to provide an array of the size that
     * is returned by getPublicKeySize(). This function copies the public
     * key into the given array. The caller keeps ownership of that array.
     */
    void getPublicKey(uint8_t* publicKey);

    /**
     * This method is used on SERVER side to compute the size of the
     * public key in bytes.
     * @return size of the public key in bytes
     */
    unsigned long getPublicKeySize();

    /**
     * This function is used on Client side. It checks whether the given
     * public key is a valid IBBE public key or not
     * @param publicKey         - the public key that gets checkt
     * @return                  - 1, if the given public key is valid,
     *                            0 otherwise.
     */
     static const int publicKeyIsValid(EncryptionScheme encryptionScheme, uint8_t* publicKey);

    /**
     * This method is used on CLIENT side to create a new session key of the
     * size that is returned by getSessionKeySize(). Currently this size is
     * fixed to 32 byte (256 bit) but this may change in the future.
     *
     * @param sessionKey The caller has to provide an array of the size that
     * is returned by getSessionKeySize(). This function copies the
     * generated session key into the given array. The caller keeps ownership
     * of that array.
     *
     * @param cipherText The caller has to provide an array of the size that
     * is returned by getCipherTextSize(). This function copies the
     * generated cipherText into the given array. The caller keeps ownership
     * of that array.
     */
    void encapsulate(uint8_t* sessionKey, uint8_t* cipherText);

    /**
     * This method is used on SERVER side to decapsulate an encrypted
     * session key.
     *
     * @param cipherText A pointer to the cipher text that encapsulates the
     * session key. The caller keeps ownership of the array.
     *
     * @param sessionKey The caller has to provide an array of the size that
     * is returned by getSessionKeySize(). This function copies the
     * decapsulated session key into this array. The caller keeps ownership
     * of this array.
     *
     * @throws std::invalid_argument if `cipherText` can not be deserialized.
     */
    void decapsulate(uint8_t* cipherText, uint8_t* sessionKey);

    /**
     * This function computes the size of the session key for this encryptor
     * @return the size of the session key in bytes
     */
    unsigned int getSessionKeySize();


    /**
     * This function computes the size of the cipher text for this encryptor
     * @return the size of the cipher text in bytes
     */
    unsigned int getCipherTextSize();
};
#endif //C_CRYPTO_WRAPPER_ENCRYPTOR_H
