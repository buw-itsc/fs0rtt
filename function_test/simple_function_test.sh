#!/bin/bash

NUMBER_CLIENTS=1
NUMBER_REQUESTS=1
BFE_M=1000
BFE_K=3
INTER_SLEEP_TIME=0
ALT_INTER_SLEEP_TIME=0
CLIENT_RUNTIME=10

#Parse command line options
while getopts "d:c:n:r:m:k:t:a:l:" option; do
	case "${option}" in
		d) DIRECTORY=${OPTARG};;
		c) CRYPTO=${OPTARG};;
		n) NUMBER_CLIENTS=${OPTARG};;
		r) NUMBER_REQUESTS=${OPTARG};;
		m) BFE_M=${OPTARG};;
		k) BFE_K=${OPTARG};;
		t) INTER_SLEEP_TIME=${OPTARG};;
		a) ALT_INTER_SLEEP_TIME=${OPTARG};;
		l) CLIENT_RUNTIME=${OPTARG};;
	esac
done
shift $((OPTIND-1))

QUIC_CRYPTO='quic'
ZERO_CRYPTO='zero'

srcdir=$DIRECTORY/src
datadir=$DIRECTORY/function_test


#Generate certificates
cd $srcdir/net/tools/quic/certs
./generate-certs.sh

#Import certificate
rm -rf $HOME/.pki
mkdir -p $HOME/.pki/nssdb
certutil -d sql:$HOME/.pki/nssdb -N --empty-password
certutil -d sql:$HOME/.pki/nssdb -A -t "C,," -n QUIC_server_root_CA -i $srcdir/net/tools/quic/certs/out/2048-sha256-root.pem

cd $srcdir

#Start quic_server
# if you want to specify bloom filter size and number of hash functions simply append --bfe_m=<number> and --bfe_k=<number>. Change the BFE_M and BFE_K variables at the top of this script.
./out/Debug/quic_server --quic_response_cache_dir=$datadir/www.example.org --certificate_file=net/tools/quic/certs/out/leaf_cert.pem --key_file=net/tools/quic/certs/out/leaf_cert.pkcs8 --v=1 --bfe_m=$BFE_M --bfe_k=$BFE_K&> $datadir/quic_server_output.txt &
sleep 2s

#pids=""


if [ "$CRYPTO" == "$ZERO_CRYPTO" ];
then
    #Start quic_client with zero crypto
	time=$(($BFE_M/5000))	
	sleep ${time}s  	
	echo "ausgabe crypto $CRYPTO"
	for counter in $(seq 1 $(expr $NUMBER_CLIENTS - 1))
    	do
   		./out/Debug/quic_client --host=127.0.0.1 --port=6121 --v=1 --protocol=zero --runtime=$CLIENT_RUNTIME --incl-sleep-time=$ALT_INTER_SLEEP_TIME https://www.example.org/index.html 2>&1 | tee $datadir/quic_client${counter}_output.txt &
		#pids+=" $!"
    	done
	./out/Debug/quic_client --host=127.0.0.1 --port=6121 --v=1 --protocol=zero --runtime=$CLIENT_RUNTIME --incl-sleep-time=$ALT_INTER_SLEEP_TIME https://www.example.org/index.html 2>&1 | tee $datadir/quic_client0_output.txt
	#pids+=" $!"
	#./out/Debug/quic_client --host=127.0.0.1 --port=6121 https://www.example.org/index.html --disable-certificate-verification 2>&1 | tee $datadir/quic_client_output.txt
    #./out/Debug/quic_client --host=127.0.0.1 --port=6121 --v=1 --protocol=zero https://www.example.org/index.html 2>&1 | tee $datadir/quic_client_output.txt
fi
if [ "$CRYPTO" == "$QUIC_CRYPTO" ];
then
	echo "ausgabe crypto $CRYPTO"
    #Start quic_client with quic crypto
	for counter in $(seq 1 $(expr $NUMBER_CLIENTS - 1))
    	do
    		./out/Debug/quic_client --host=127.0.0.1 --port=6121 --v=1 --request-number=$NUMBER_REQUESTS https://www.example.org/index.html 2>&1 | tee $datadir/quic_client${counter}_output.txt &
		#pids+=" $!"
	done
	./out/Debug/quic_client --host=127.0.0.1 --port=6121 --v=1 --request-number=$NUMBER_REQUESTS https://www.example.org/index.html 2>&1 | tee $datadir/quic_client0_output.txt
	#pids+=" $!"
fi

CLIENTS=$(pidof quic_client)

if [ -n "$CLIENTS" ];
then
	echo "I AM WAITING FOR $CLIENTS"
	wait $CLIENTS
fi

#Exit
kill -SIGINT $(pidof quic_server)
if cat $datadir/quic_client*_output.txt | grep "Request succeeded (200)" -q;
then
    exit 0
fi
exit 1
