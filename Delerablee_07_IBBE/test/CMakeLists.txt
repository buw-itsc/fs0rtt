add_executable(Delerablee_07_IBBE_test Delerablee_07_IBBE_test.c)
target_link_libraries(Delerablee_07_IBBE_test ${DEL07})
add_test(Delerablee_07_IBBE_test ${SIMUL} ${SIMAR} ${EXECUTABLE_OUTPUT_PATH}/Delerablee_07_IBBE_test)