/**
 *
 * Tests for implementation of IBBE scheme.
 *
 */

#include "relic/relic.h"
#include "relic/relic_test.h"
#include "Delerablee_07_IBBE.h"
#include "Delerablee_07_IBBE_util.h"

unsigned MAX_NUMBER_RECIPIENTS;

// Generate random identities of random length (max 2^17 Byte)
void random_identities(unsigned** identity_lengths, uint8_t ***identities,
                       unsigned number_of_identities_to_generate) {
  *identity_lengths = malloc(number_of_identities_to_generate *
      sizeof(unsigned));

  unsigned total_size = 0;
  for (unsigned j = 0; j < number_of_identities_to_generate; j++) {
    (*identity_lengths)[j] = (unsigned) rand() % (1 << 17);
    total_size += (*identity_lengths)[j];
  }

  *identities = malloc(number_of_identities_to_generate * sizeof(uint8_t * ));
  for (unsigned j = 0; j < number_of_identities_to_generate; j++) {
    (*identities)[j] = malloc((*identity_lengths)[j] * sizeof(uint8_t));
    for (unsigned l = 0; l < (*identity_lengths)[j]; l++) {
      (*identities)[j][l] = (uint8_t) (rand() % 256);
    }
  }
}

// Free memory of identities generated with random_identities(.)
void free_identities(unsigned *identity_lengths, uint8_t **identities,
    unsigned num_identities) {
  for(unsigned i = 0; i < num_identities; i++) {
    free(identities[i]);
  }
  free(identities);
  free(identity_lengths);
}

static int decapsulate_with_key_of_ID_used_for_encapsulation() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 5;

  TRY {
    TEST_BEGIN("del07_decapsulate with a key associated to an ID that was used for "
               "encapsulation returns encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, MAX_NUMBER_RECIPIENTS) == STS_OK, end);

      TEST_ASSERT(del07_extract(&secret_key,
                                msk,
                                identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);

      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    decapsulate_index,
                                    secret_key,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 1,
          end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(secret_key);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(secret_key);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);

  return code;
}

static int encapsulate_and_decapsulate_with_only_one_key() {
  int code = STS_ERR;

  const unsigned num_identities = 1;
  const char *identities[num_identities];
  identities[0] = "1";

  unsigned identity_lengths[num_identities] = {1};

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 0;

  TRY {
    TEST_BEGIN("del07_encapsulate for set {1}, del07_decapsulate with sk_1 and set {1} "
               "returns encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk,
                                msk,
                                (uint8_t *) identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    decapsulate_index,
                                    sk,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 1,
          end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(sk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(sk);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);


    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int encapsulate_and_decapsulate_with_colliding_identities() {
  int code = STS_ERR;

  const unsigned num_identities = 3;
  const char *identities[num_identities];
  identities[0] = "1";
  identities[1] = "2";
  identities[2] = "2";

  unsigned identity_lengths[num_identities] = {1, 1, 1};

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 1;

  TRY {
    TEST_BEGIN("del07_encapsulate for set {1,2,2}, del07_decapsulate with sk_1 and set "
               "{1,2,2} returns encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk,
                                msk,
                                (uint8_t *) identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    decapsulate_index,
                                    sk,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 1,
                  end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(sk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(sk);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int encapsulate_and_decapsulate_with_colliding_identities2() {
  int code = STS_ERR;

  const unsigned num_identities = 3;
  const char *identities[num_identities];
  identities[0] = "1";
  identities[1] = "2";
  identities[2] = "2";

  unsigned identity_lengths[num_identities] = {1, 1, 1};

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 2;

  TRY {
    TEST_BEGIN("del07_encapsulate for set {1,2,2}, del07_decapsulate with sk_2 and set "
               "{1,2,2} returns encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk,
                                msk,
                                (uint8_t *) identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    decapsulate_index,
                                    sk,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 1,
                  end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_secret_key_to_zero(sk);
      del07_set_ciphertext_to_zero(c);
      del07_set_key_to_zero(encapsulated_key);
      del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(sk);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int decapsulate_with_key_of_ID_not_used_for_encapsulation() {
  int code = STS_ERR;

  const unsigned num_identities = 3;

  const char *identities[num_identities];
  unsigned identities_lengths[num_identities] = {1, 1, 1};

  identities[0] = "1";
  identities[1] = "2";
  identities[2] = "3";

  const unsigned num_wrong_identities = 3;
  const char *identities_wrong[num_wrong_identities];
  unsigned identities_wrong_lengths[num_wrong_identities] =
      {1, 1, 1};

  identities_wrong[0] = "1";
  identities_wrong[1] = "2";
  identities_wrong[2] = "4";

  const unsigned wrong_identity_index = 2;
  const char *wrong_identity = identities_wrong[wrong_identity_index];
  unsigned wrong_identity_length = identities_wrong_lengths[wrong_identity_index];

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key_wrong_identity;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  TRY {
    TEST_BEGIN("del07_decapsulate with a key associated to an ID that was NOT used "
               "for encapsulation DOES NOT return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&secret_key_wrong_identity,
                                msk,
                                (uint8_t *) wrong_identity,
                                wrong_identity_length) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identities_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities_wrong,
                                    num_wrong_identities,
                                    identities_lengths,
                                    wrong_identity_index,
                                    secret_key_wrong_identity,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 0,
                  end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(secret_key_wrong_identity);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(secret_key_wrong_identity);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);


    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int decapsulate_with_random_key() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 5;

  sk = malloc(sizeof(del07_secret_key_t));

  g1_t secret_key_sk;
  g1_null(secret_key_sk);

  TRY {
    g1_new(secret_key_sk);
    g1_rand(secret_key_sk);
#ifdef WITH_POINT_COMPRESSION
    del07_g1_compress(&sk->sk, secret_key_sk);
#else
    g1_copy(sk->sk, secret_key_sk);
#endif

    TEST_BEGIN("del07_decapsulate with a random element as secret key DOES NOT "
               "return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    10,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    10,
                                    identity_lengths,
                                    decapsulate_index,
                                    sk,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 0,
                  end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);
  g1_free(seckret_key_sk);
  del07_set_secret_key_to_zero(sk);
  free(sk);
  return code;
}

static int decapsulate_with_random_verification_key() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 5;

  g2_t c_verification_key;
  g2_null(c_verification_key);

  TRY {
    g2_new(c_verification_key);
    g2_rand(c_verification_key);

    TEST_BEGIN("del07_decapsulate with a random element as verification key "
               "DOES NOT return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&secret_key,
                                msk,
                                identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    10,
                                    identity_lengths,
                                    pk) == STS_OK, end);
#ifdef WITH_POINT_COMPRESSION
      del07_g2_compress(&c->verification_key, c_verification_key);
#else
      g2_copy(c->verification_key, c_verification_key);
#endif
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    10,
                                    identity_lengths,
                                    decapsulate_index,
                                    secret_key,
                                    c,
                                    pk) == STS_ERR, end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_secret_key_to_zero(secret_key);
      del07_set_ciphertext_to_zero(c);
      del07_set_key_to_zero(encapsulated_key);

      free(msk);
      free(pk);
      free(secret_key);
      free(c);
      free(encapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);
  g1_free(c_verification_key);
  return code;
}

static int decapsulate_with_random_signature() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 5;

  g1_t c_signature;
  g1_null(c_signature);

  TRY {
    g1_new(c_signature);
    g1_rand(c_signature);

    TEST_BEGIN("del07_decapsulate with a random element as signature "
               "DOES NOT return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&secret_key,
                                msk,
                                identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    10,
                                    identity_lengths,
                                    pk) == STS_OK, end);
#ifdef WITH_POINT_COMPRESSION
      del07_g1_compress(&c->signature, c_signature);
#else
      g1_copy(c->signature, c_signature);
#endif
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    10,
                                    identity_lengths,
                                    decapsulate_index,
                                    secret_key,
                                    c,
                                    pk) == STS_ERR, end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_secret_key_to_zero(secret_key);
      del07_set_ciphertext_to_zero(c);
      del07_set_key_to_zero(encapsulated_key);

      free(msk);
      free(pk);
      free(secret_key);
      free(c);
      free(encapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);
  g1_free(c_verification_key);
  return code;
}

static int decapsulate_with_different_set_than_used_for_encapsulate_1() {
  int code = STS_ERR;

  const unsigned num_first_identities = 5;

  const char *first_identities[num_first_identities];
  unsigned first_identities_lengths[num_first_identities] = {1, 1, 1, 1, 1};

  first_identities[0] = "1";
  first_identities[1] = "2";
  first_identities[2] = "3";
  first_identities[3] = "4";
  first_identities[4] = "5";

  const unsigned num_second_identities = 5;
  const char *second_identities[num_second_identities];
  unsigned second_identities_lengths[num_second_identities] = {1, 1, 1, 1, 1};

  second_identities[0] = "1";
  second_identities[1] = "2";
  second_identities[2] = "3";
  second_identities[3] = "4";
  second_identities[4] = "6";

  const unsigned decapsulate_identity_index = 0;
  const char
      *decapsulate_identity = second_identities[decapsulate_identity_index];
  unsigned decapsulate_identity_length =
      second_identities_lengths[decapsulate_identity_index];

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  TRY {
    TEST_BEGIN("del07_encapsulate for set {1,2,3,4,5}, del07_decapsulate with sk_1 and set "
               "{1,2,3,4,6} does NOT return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk,
                                msk,
                                (uint8_t *) decapsulate_identity,
                                decapsulate_identity_length) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) first_identities,
                                    num_first_identities,
                                    first_identities_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) second_identities,
                                    num_second_identities,
                                    second_identities_lengths,
                                    decapsulate_identity_index,
                                    sk,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 0,
                  end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(sk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(sk);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int decapsulate_with_different_set_than_used_for_encapsulate_2() {
  int code = STS_ERR;

  const unsigned num_first_identities = 5;

  const char *first_identities[num_first_identities];
  unsigned first_identities_lengths[num_first_identities] = {1, 1, 1, 1, 1};

  first_identities[0] = "1";
  first_identities[1] = "2";
  first_identities[2] = "3";
  first_identities[3] = "4";
  first_identities[4] = "5";

  const unsigned num_second_identities = 4;
  const char *second_identities[num_second_identities];
  unsigned second_identities_lengths[num_second_identities] = {1, 1, 1, 1};

  second_identities[0] = "1";
  second_identities[1] = "2";
  second_identities[2] = "3";
  second_identities[3] = "4";

  const unsigned decapsulate_identity_index = 0;
  const char
      *decapsulate_identity = second_identities[decapsulate_identity_index];
  unsigned decapsulate_identity_length =
      second_identities_lengths[decapsulate_identity_index];

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  TRY {
    TEST_BEGIN("del07_encapsulate for set {1,2,3,4,5}, del07_decapsulate with sk_1 and set "
               "{1,2,3,4} does NOT return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&secret_key,
                                msk,
                                (uint8_t *) decapsulate_identity,
                                decapsulate_identity_length) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) first_identities,
                                    num_first_identities,
                                    first_identities_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) second_identities,
                                    num_second_identities,
                                    second_identities_lengths,
                                    decapsulate_identity_index,
                                    secret_key,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 0,
                  end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(secret_key);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(secret_key);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int decapsulate_with_different_set_than_used_for_encapsulate_3() {
  int code = STS_ERR;

  const unsigned num_first_identities = 5;

  const char *first_identities[num_first_identities];
  unsigned first_identities_lengths[num_first_identities] = {1, 1, 1, 1, 1};

  first_identities[0] = "1";
  first_identities[1] = "2";
  first_identities[2] = "3";
  first_identities[3] = "4";
  first_identities[4] = "5";

  const unsigned num_second_identities = 6;
  const char *second_identities[num_second_identities];
  unsigned second_identities_lengths[num_second_identities] = {1, 1, 1, 1, 1, 1};

  second_identities[0] = "1";
  second_identities[1] = "2";
  second_identities[2] = "3";
  second_identities[3] = "4";
  second_identities[4] = "5";
  second_identities[5] = "6";

  const unsigned decapsulate_identity_index = 0;
  const char
      *decapsulate_identity = second_identities[decapsulate_identity_index];
  unsigned decapsulate_identity_length =
      second_identities_lengths[decapsulate_identity_index];

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  TRY {
    TEST_BEGIN("del07_encapsulate for set {1,2,3,4,5}, del07_decapsulate with sk_1 and set "
               "{1,2,3,4,5,6} does NOT return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&secret_key,
                                msk,
                                (uint8_t *) decapsulate_identity,
                                decapsulate_identity_length) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) first_identities,
                                    num_first_identities,
                                    first_identities_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) second_identities,
                                    num_second_identities,
                                    second_identities_lengths,
                                    decapsulate_identity_index,
                                    secret_key,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 0,
                  end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(secret_key);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(secret_key);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int decapsulate_with_different_set_than_used_for_encapsulate_4() {
  int code = STS_ERR;

  const unsigned num_first_identities = 5;

  const char *first_identities[num_first_identities];
  unsigned first_identities_lengths[num_first_identities] = {1, 1, 1, 1, 1};

  first_identities[0] = "1";
  first_identities[1] = "2";
  first_identities[2] = "3";
  first_identities[3] = "4";
  first_identities[4] = "5";

  const unsigned num_second_identities = 6;
  const char *second_identities[num_second_identities];
  unsigned second_identities_lengths[num_second_identities] = {1, 1, 1, 1, 1, 1};

  second_identities[0] = "1";
  second_identities[1] = "2";
  second_identities[2] = "3";
  second_identities[3] = "4";
  second_identities[4] = "5";
  second_identities[5] = "5";

  const unsigned decapsulate_identity_index = 0;
  const char *decapsulate_identity =
      second_identities[decapsulate_identity_index];
  unsigned decapsulate_identity_length =
      second_identities_lengths[decapsulate_identity_index];

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  TRY {
    TEST_BEGIN("del07_encapsulate for set {1,2,3,4,5}, del07_decapsulate with sk_1 and set "
               "{1,2,3,4,5,5} does NOT return encapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&secret_key,
                                msk,
                                (uint8_t *) decapsulate_identity,
                                decapsulate_identity_length) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) first_identities,
                                    num_first_identities,
                                    first_identities_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) second_identities,
                                    num_second_identities,
                                    second_identities_lengths,
                                    decapsulate_identity_index,
                                    secret_key,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 0,
                  end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(secret_key);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
              del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(secret_key);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int encapsulate_set_larger_than_maximum_number_recipients() {
  int code = STS_ERR;

  unsigned num_identities = MAX_NUMBER_RECIPIENTS + 1;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;

  unsigned decapsulate_index = 5;

  TRY {
    TEST_BEGIN("number_identities > MAX_NUMBER_RECIPIENTS in del07_encapsulate "
               "results in STS_ERR") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk,
                                msk,
                                identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_ERR, end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(sk);
      // don't free c and encapsulated key because we expect them to be null

      free(msk);
      free(pk);
      free(sk);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);
  return code;
}

static int decapsulate_set_larger_than_maximum_number_recipients() {
  int code = STS_ERR;

  const unsigned num_first_identities = 5;

  const char *first_identities[num_first_identities];
  unsigned first_identities_lengths[num_first_identities] = {1, 1, 1, 1, 1};

  first_identities[0] = "1";
  first_identities[1] = "2";
  first_identities[2] = "3";
  first_identities[3] = "4";
  first_identities[4] = "5";

  unsigned num_wrong_identities = MAX_NUMBER_RECIPIENTS + 1;
  unsigned *identities_wrong_lengths;
  uint8_t **identities_wrong;
  random_identities(&identities_wrong_lengths, &identities_wrong, num_wrong_identities);

  unsigned decapsulate_identity_index = 0;

  uint8_t *decapsulate_identity = identities_wrong[decapsulate_identity_index];
  unsigned decapsulate_identity_length =
      identities_wrong_lengths[decapsulate_identity_index];

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  TRY {
    TEST_BEGIN("number_identities > MAX_NUMBER_RECIPIENTS in del07_decapsulate "
               "results in STS_ERR") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk,
                                msk,
                                decapsulate_identity,
                                decapsulate_identity_length) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) first_identities,
                                    num_first_identities,
                                    first_identities_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities_wrong,
                                    num_wrong_identities,
                                    identities_wrong_lengths,
                                    decapsulate_identity_index,
                                    sk,
                                    c,
                                    pk) == STS_ERR, end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(sk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);
      // dont free decapsulated key

      free(msk);
      free(pk);
      free(sk);
      free(c);
      free(encapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identities_wrong_lengths, identities_wrong, num_wrong_identities);

  return code;
}

static int decapsulation_identity_index_greater_than_num_identities() {
  int code = STS_ERR;

  const unsigned num_identities = 3;
  char *identities[num_identities];
  identities[0] = "1";
  identities[1] = "2";
  identities[2] = "3";

  unsigned identity_lengths[num_identities] = {1, 1, 1};

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 3;

  TRY {
    TEST_BEGIN("del07_decapsulate index greater than number of encapsulating "
               "identities results in STS_ERR") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk,
                                msk,
                                (uint8_t *) identities[0],
                                identity_lengths[0]) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    decapsulate_index,
                                    sk,
                                    c,
                                    pk) == STS_ERR, end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_secret_key_to_zero(sk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(encapsulated_key);

      free(msk);
      free(pk);
      free(sk);
      free(c);
      free(encapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int convert_key_to_bit_string_throws_no_error() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;

  uint8_t *key_as_bit_string;

  TRY {
    TEST_BEGIN("converting key to bit string throws no error") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(
          del07_convert_key_to_bit_string(&key_as_bit_string, encapsulated_key)
              == STS_OK, end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_ciphertext_to_zero(c);
      del07_set_key_to_zero(encapsulated_key);

      free(msk);
      free(pk);
      free(c);
      free(encapsulated_key);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);

  return code;
}

static int master_secret_key_serialization() {
  int code = STS_ERR;

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_master_secret_key_t *msk_deserialized;
  uint8_t *serialized_msk;
  size_t size_serialized_msk;

  TRY {
    TEST_BEGIN("master secret key serialization is correct") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);

      del07_serialize_master_secret_key(&serialized_msk,
                                        &size_serialized_msk,
                                        msk);
      del07_deserialize_master_secret_key(&msk_deserialized,
                                          serialized_msk,
                                          size_serialized_msk);

      TEST_ASSERT(
          del07_master_secret_keys_are_equal(msk, msk_deserialized) == 1,
                  end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_master_secret_key_to_zero(msk_deserialized);

      free(msk);
      free(pk);
      free(msk_deserialized);
      free(serialized_msk);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int secret_key_serialization() {
  int code = STS_ERR;

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_secret_key_t *sk_deserialized;
  del07_secret_key_t *sk_deserialized_compression;

  uint8_t identity = (uint8_t) "1";

  TRY {
    TEST_BEGIN("secret key serialization is correct") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_extract(&sk, msk, &identity, 1) == STS_OK, end);

      uint8_t *serialized_sk_uncompressed;
      TEST_ASSERT(del07_serialize_secret_key(&serialized_sk_uncompressed, sk, 0) == STS_OK, end);
      TEST_ASSERT(del07_deserialize_secret_key(&sk_deserialized, serialized_sk_uncompressed, 0) == STS_OK, end);
      TEST_ASSERT(del07_secret_keys_are_equal(sk, sk_deserialized), end);

      uint8_t *serialized_sk_compressed;
      TEST_ASSERT(del07_serialize_secret_key(&serialized_sk_compressed, sk, 0) == STS_OK, end);
      TEST_ASSERT(del07_deserialize_secret_key(&sk_deserialized_compression, serialized_sk_compressed, 0) == STS_OK, end);
      TEST_ASSERT(del07_secret_keys_are_equal(sk, sk_deserialized_compression), end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_secret_key_to_zero(sk);
      del07_set_secret_key_to_zero(sk_deserialized);
      del07_set_secret_key_to_zero(sk_deserialized_compression);

      free(msk);
      free(pk);
      free(sk);
      free(sk_deserialized);
      free(sk_deserialized_compression);
      free(serialized_sk_uncompressed);
      free(serialized_sk_compressed);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int public_key_serialization() {
  int code = STS_ERR;

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_public_key_t *pk_deserialized;

  TRY {
    TEST_BEGIN("public key serialization is correct") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);

      uint8_t *serialized_pk;
      del07_serialize_public_key(&serialized_pk, pk);
      TEST_ASSERT(!del07_deserialize_public_key(&pk_deserialized,
                                   serialized_pk, 1), end);
      TEST_ASSERT(del07_public_keys_are_equal(pk, pk_deserialized), end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_public_key_to_zero(pk_deserialized);

      free(msk);
      free(pk);
      free(pk_deserialized);
      free(serialized_pk);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  return code;
}

static int ciphertext_serialization() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_ciphertext_t *c;
  del07_key_t *key;
  del07_ciphertext_t *c_deserialized;

  TRY {
    TEST_BEGIN("ciphertext serialization is correct") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);

      uint8_t *serialized_c;
      del07_serialize_ciphertext(&serialized_c, c);
      TEST_ASSERT(! del07_deserialize_ciphertext(&c_deserialized,
                                   serialized_c), end);

      TEST_ASSERT(del07_ciphertexts_are_equal(c, c_deserialized), end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(key);
              del07_set_ciphertext_to_zero(c_deserialized);

      free(msk);
      free(pk);
      free(c);
      free(key);
      free(c_deserialized);
      free(serialized_c);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);

  return code;
}

static int key_serialization() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_ciphertext_t *c;
  del07_key_t *key;
  del07_key_t *key_deserialized;

  TRY {
    TEST_BEGIN("key serialization is correct") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);
      TEST_ASSERT(del07_encapsulate(&c,
                                    &key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);

      uint8_t *serialized_key;
      del07_serialize_key(&serialized_key, key);
      del07_deserialize_key(&key_deserialized,
                            serialized_key);

      TEST_ASSERT(del07_keys_are_equal(key, key_deserialized), end);

              del07_set_master_secret_key_to_zero(msk);
              del07_set_public_key_to_zero(pk);
              del07_set_ciphertext_to_zero(c);
              del07_set_key_to_zero(key);
              del07_set_key_to_zero(key_deserialized);

      free(msk);
      free(pk);
      free(c);
      free(key);
      free(key_deserialized);
      free(serialized_key);
    }
    TEST_END;
  } CATCH_ANY {
      ERROR(end);
    }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);

  return code;
}

static int bitstring_of_encapsulated_and_decapsulated_key_equal() {
  int code = STS_ERR;

  const unsigned num_identities = 10;
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, num_identities);

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *c;
  del07_key_t *encapsulated_key;
  del07_key_t *decapsulated_key;

  unsigned decapsulate_index = 5;

  uint8_t *enc_key_as_bitstring;
  uint8_t *dec_key_as_bitstring;

  TRY {
    TEST_BEGIN("del07_convert_key_to_bit_string returns same bitstring for encapsulated and decapsulated key") {
      TEST_ASSERT(del07_setup(&msk, &pk, 10) == STS_OK, end);

      TEST_ASSERT(del07_extract(&secret_key,
                                msk,
                                (uint8_t *) identities[decapsulate_index],
                                identity_lengths[decapsulate_index]) == STS_OK, end);

      TEST_ASSERT(del07_encapsulate(&c,
                                    &encapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_decapsulate(&decapsulated_key,
                                    (const uint8_t **) identities,
                                    num_identities,
                                    identity_lengths,
                                    decapsulate_index,
                                    secret_key,
                                    c,
                                    pk) == STS_OK, end);
      TEST_ASSERT(del07_keys_are_equal(encapsulated_key, decapsulated_key) == 1,
                  end);

      TEST_ASSERT(
          del07_convert_key_to_bit_string(&enc_key_as_bitstring, encapsulated_key)
              == STS_OK, end);

      TEST_ASSERT(
          del07_convert_key_to_bit_string(&dec_key_as_bitstring, decapsulated_key)
              == STS_OK, end);

      TEST_ASSERT(!memcmp(enc_key_as_bitstring, dec_key_as_bitstring, SECURITY_PARAMETER), end);

      del07_set_master_secret_key_to_zero(msk);
      del07_set_public_key_to_zero(pk);
      del07_set_secret_key_to_zero(secret_key);
      del07_set_ciphertext_to_zero(c);
      del07_set_key_to_zero(encapsulated_key);
      del07_set_key_to_zero(decapsulated_key);

      free(msk);
      free(pk);
      free(secret_key);
      free(c);
      free(encapsulated_key);
      free(decapsulated_key);
      free(enc_key_as_bitstring);
      free(dec_key_as_bitstring);
    }
    TEST_END;
  } CATCH_ANY {
    ERROR(end);
  }
  code = STS_OK;

  end:
  free_identities(identity_lengths, identities, num_identities);

  return code;
}

int main(void) {
  MAX_NUMBER_RECIPIENTS = 10;

  if (del07_init() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_key_of_ID_used_for_encapsulation() != STS_OK) {
    core_clean();
    return 1;
  }

  if (encapsulate_and_decapsulate_with_only_one_key() != STS_OK) {
    core_clean();
    return 1;
  }

  if (encapsulate_and_decapsulate_with_colliding_identities() != STS_OK) {
    core_clean();
    return 1;
  }

  if (encapsulate_and_decapsulate_with_colliding_identities2() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_key_of_ID_not_used_for_encapsulation() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_random_key() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_random_verification_key() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_random_signature() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_different_set_than_used_for_encapsulate_1() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_different_set_than_used_for_encapsulate_2() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_different_set_than_used_for_encapsulate_3() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_with_different_set_than_used_for_encapsulate_4() != STS_OK) {
    core_clean();
    return 1;
  }

  if (encapsulate_set_larger_than_maximum_number_recipients() != STS_OK) {
    core_clean();
    return 1;
  }

  if (decapsulate_set_larger_than_maximum_number_recipients() != STS_OK) {
    core_clean();
    return 1;
  }


  if (decapsulation_identity_index_greater_than_num_identities() != STS_OK) {
    core_clean();
    return 1;
  }



  if (convert_key_to_bit_string_throws_no_error() != STS_OK) {
    core_clean();
    return 1;
  }



  if (master_secret_key_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (secret_key_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (public_key_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (ciphertext_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if (key_serialization() != STS_OK) {
    core_clean();
    return 1;
  }

  if(bitstring_of_encapsulated_and_decapsulated_key_equal() != STS_OK) {
    core_clean();
    return 1;
  }

  del07_clean();

  return 0;
}
