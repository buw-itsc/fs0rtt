#include "relic/relic.h"
#include "Delerablee_07_IBBE.h"
#include "Delerablee_07_IBBE_util.h"

int MAX_NUMBER_RECIPIENTS;

static void setup_bench(void) {
  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;

  BENCH_BEGIN("del07_setup") {
    BENCH_ADD(del07_setup(&msk, &pk, (unsigned) MAX_NUMBER_RECIPIENTS));
  }
  BENCH_END;

  del07_set_master_secret_key_to_zero(msk);
  del07_set_public_key_to_zero(pk);
  free(msk);
  free(pk);
}

static void extract_bench(void) {
  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;

  BENCH_BEGIN("del07_extract") {
    if (del07_setup(&msk, &pk, (unsigned) MAX_NUMBER_RECIPIENTS) != STS_OK) {
      return;
    }
    //generate random identities (with length at most 2^20 bit by creating
    // an array of at most 2^17 byte)
      unsigned identity_length;
    identity_length = (unsigned) rand() % (1 << 17);
    uint8_t identity[identity_length];
    for (unsigned j = 0; j < identity_length; j++) {
      identity[j] = (uint8_t) (rand() % 256);
    }

    BENCH_ADD(del07_extract(&sk, msk, identity, identity_length));
  }
  BENCH_END;

  del07_set_master_secret_key_to_zero(msk);
  del07_set_public_key_to_zero(pk);
  del07_set_secret_key_to_zero(sk);
  free(msk);
  free(pk);
  free(sk);
}

static void encapsulate_bench() {
  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *k;

  BENCH_BEGIN("del07_encapsulate") {
    if (del07_setup(&msk, &pk, (unsigned) MAX_NUMBER_RECIPIENTS) != STS_OK) {
      return;
    }

    //generate random identities (with length at most 2^20 bit by creating an array of at most 2^17 byte)
    unsigned identity_lengths[MAX_NUMBER_RECIPIENTS];
    for (int j = 0; j < MAX_NUMBER_RECIPIENTS; j++) {
      identity_lengths[j] = (unsigned) rand() % (1 << 17);
    }
    uint8_t *identities[MAX_NUMBER_RECIPIENTS];
    for (int j = 0; j < MAX_NUMBER_RECIPIENTS; j++) {
      uint8_t identity[identity_lengths[j]];
      for (unsigned l = 0; l < identity_lengths[j]; l++) {
        identity[l] = (uint8_t) (rand() % 256);
      }
      identities[j] = identity;
    }

    int encapsulation_identity_index = rand() % MAX_NUMBER_RECIPIENTS;
    if (del07_extract(&sk, msk, identities[encapsulation_identity_index],
                      identity_lengths[encapsulation_identity_index]) != STS_OK) {
      return;
    }

    BENCH_ADD(del07_encapsulate(&c,
                                &k,
                                (const uint8_t **) identities,
                                (unsigned) MAX_NUMBER_RECIPIENTS,
                                identity_lengths,
                                pk));
  }
  BENCH_END;

  del07_set_master_secret_key_to_zero(msk);
  del07_set_public_key_to_zero(pk);
  del07_set_secret_key_to_zero(sk);
  del07_set_ciphertext_to_zero(c);
  del07_set_key_to_zero(k);
  free(msk);
  free(pk);
  free(sk);
  free(c);
  free(k);
}

static void decapsulate_bench() {
  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;
  del07_ciphertext_t *c;
  del07_key_t *k;
  del07_key_t *decapsulated_k;

  BENCH_BEGIN("del07_decapsulate") {
    if (del07_setup(&msk, &pk, (unsigned) MAX_NUMBER_RECIPIENTS) != STS_OK) {
      return;
    }
    
    //generate random identities (with length at most 2^20 bit by creating an array of at most 2^17 byte)
    unsigned identity_lengths[MAX_NUMBER_RECIPIENTS];
    for (int j = 0; j < MAX_NUMBER_RECIPIENTS; j++) {
      identity_lengths[j] = (unsigned) rand() % (1 << 17);
    }
    uint8_t *identities[MAX_NUMBER_RECIPIENTS];
    for (int j = 0; j < MAX_NUMBER_RECIPIENTS; j++) {
      uint8_t identity[identity_lengths[j]];
      for (unsigned l = 0; l < identity_lengths[j]; l++) {
        identity[l] = (uint8_t) (rand() % 256);
      }
      identities[j] = identity;
    }

    unsigned encapsulation_identity_index = (unsigned) rand() % MAX_NUMBER_RECIPIENTS;
    if (del07_extract(&sk, msk, identities[encapsulation_identity_index],
                      identity_lengths[encapsulation_identity_index]) != STS_OK) {
      return;
    }

    if (del07_encapsulate(&c,
                          &k,
                          (const uint8_t **) identities,
                          (unsigned) MAX_NUMBER_RECIPIENTS,
                          identity_lengths,
                          pk) != STS_OK) {
      return;
    }

    BENCH_ADD(del07_decapsulate(&decapsulated_k,
                                (const uint8_t **) identities,
                                (unsigned) MAX_NUMBER_RECIPIENTS,
                                identity_lengths,
                                encapsulation_identity_index,
                                sk,
                                c,
                                pk));
  }
  BENCH_END;

  del07_set_master_secret_key_to_zero(msk);
  del07_set_public_key_to_zero(pk);
  del07_set_secret_key_to_zero(sk);
  del07_set_ciphertext_to_zero(c);
  del07_set_key_to_zero(k);
  del07_set_key_to_zero(decapsulated_k);
  free(msk);
  free(pk);
  free(sk);
  free(c);
  free(k);
  free(decapsulated_k);
}

int main(void) {
  MAX_NUMBER_RECIPIENTS = 20;

  if (del07_init() != STS_OK) {
    core_clean();
    return 1;
  }

  util_banner("Benchmarks for the Delerablee 07 IBBE module:", 0);
  setup_bench();
  extract_bench();
  encapsulate_bench();
  decapsulate_bench();

  del07_clean();
  return 0;
}
