How to include this library in another C or C++ project:

Two possibilities to build and install library:
1. Run cmake, make and make install in the top level directory
2. Import the project into CLion, launch target del07ibbe with build and install

In the other projects CMakeList.txt add the following line:
target_link_libraries (<target> del07ibbe)
