cmake_minimum_required(VERSION 3.7)
project(Delerablee_07_IBBE C)

set(CMAKE_C_STANDARD 11)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra")

# add extra include directories
set(INCLUDE /usr/local/include ${CMAKE_CURRENT_SOURCE_DIR}/include ${CMAKE_CURRENT_BINARY_DIR}/include)
include_directories(${INCLUDE})

set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/lib)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/bin)

set(DEL07 "del07ibbe")

set(SIMUL "$ENV{SIMUL}" CACHE STRING "Path to call a simulator of the target platform.")
set(SIMAR "$ENV{SIMAR}" CACHE STRING "Arguments to call a simulator of the target platform.")
string(REPLACE " " ";" SIMAR "${SIMAR}")

if (POINT_COMPRESSION MATCHES true)
    set(WITH_POINT_COMPRESSION 1)
endif()

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/include/Delerablee_07_IBBE_conf.h.in
        ${CMAKE_CURRENT_BINARY_DIR}/include/Delerablee_07_IBBE_conf.h)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/src)
file(GLOB includes "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")
install(FILES ${includes} DESTINATION include/${DEL07})
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/include/Delerablee_07_IBBE_conf.h DESTINATION include/${DEL07})

enable_testing()
add_subdirectory(test)

add_subdirectory(bench)