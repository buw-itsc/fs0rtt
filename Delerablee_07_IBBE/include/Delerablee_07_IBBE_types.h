// Copyright [2018] PG 0-RTT

#ifndef DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_TYPES_H_
#define DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_TYPES_H_

#include <relic/relic.h>
#include "Delerablee_07_IBBE_conf.h"

typedef struct {
  bn_t gamma;
  g1_t g;
  g1_t g_precomputation_table [RELIC_EP_TABLE_NAFWI];
} del07_master_secret_key_t;

typedef struct {
#ifdef WITH_POINT_COMPRESSION
  uint8_t *sk;  // g1_t
#else
  g1_t sk;
#endif
} del07_secret_key_t;

typedef struct {
  g1_t w;
  gt_t v;
  g2_t h;

  g2_t* h_precomputation_table;
  g2_t* h_to_the_gamma_powers_precomputation_table;

  unsigned max_number_recipients;

  g2_t h_to_the_gamma_powers[];
} del07_public_key_t;

typedef struct {
#ifdef WITH_POINT_COMPRESSION
  uint8_t *c_1;  // g1_t
  uint8_t *c_2;  // g2_t
  uint8_t *verification_key;  // g2_t
  uint8_t *signature;  // g1_t
#else
  g1_t c_1;
  g2_t c_2;
  g2_t verification_key;
  g1_t signature;
#endif
} del07_ciphertext_t;

typedef struct {
#ifdef WITH_POINT_COMPRESSION
  uint8_t *k;  // gt_t
#else
  gt_t k;
#endif
} del07_key_t;

typedef struct {
  g2_t result;
  g2_t base_precomputation_table[RELIC_EPX_TABLE_YAOWI];
  bn_t exponent;
} del07_g2_exponentiate_thread_data_with_precomputation_t;

typedef struct {
  g2_t result;
  g2_t base;
  bn_t exponent;
} del07_g2_exponentiate_thread_data_t;

typedef struct {
  gt_t result;
  g1_t g1_element;
  g2_t g2_element;
} del07_pairing_thread_data_t;

typedef struct {
  bn_t result;
  bn_t number_to_invert;
} del07_bn_inversion_thread_data_t;

typedef struct {
  gt_t result;
  gt_t base;
  bn_t exponent;
} del07_gt_exponentiate_thread_data_t;

typedef struct {
  del07_key_t **key;
  uint8_t **identities;
  unsigned number_identities;
  unsigned *identity_lengths;
  unsigned decapsulation_identity_index;
  del07_secret_key_t *secret_key;
  del07_ciphertext_t *ciphertext;
  del07_public_key_t *public_key;
} del07_cpa_decapsulate_thread_data;

#endif  // DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_TYPES_H_
