// Copyright [2018] PG 0-RTT

#ifndef DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_UTIL_H_
#define DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_UTIL_H_

#include <stddef.h>
#include "Delerablee_07_IBBE_types.h"

// G_1 and G_2 of the BN curves are additive groups. The paper "Identity-Based
// Broadcast encapsulation with Constant Size Ciphertexts and Private Keys" by
// Cecile Delerablee which defines this scheme assumes that G_1 and G_2 are
// multiplicative groups. In order to not call e.g. the function g1_mul(pk->w,
// msk->g, msk->gamma) when the paper states pk.w = msk.g^(msk.gamma), we
// instead use these macros.
#define g1_exponentiate(R, P, Q) g1_mul(R, P, Q)
#define g2_exponentiate(R, P, Q) g2_mul(R, P, Q)
#define g1_multiply(R, P, Q) g1_add(R, P, Q)
#define g2_multiply(R, P, Q) g2_add(R, P, Q)

#define G1_SIZE_COMPRESSED (1 + FP_BYTES)
#define G2_SIZE_COMPRESSED (1 + 2 * FP_BYTES)
#define GT_SIZE_COMPRESSED (8 * FP_BYTES)

#define G1_SIZE_UNCOMPRESSED (1 + 2 * FP_BYTES)

#define DEL_07_CIPHERTEXT_SIZE (2 * G1_SIZE_COMPRESSED + 2 * G2_SIZE_COMPRESSED)
#define DEL_07_KEY_SIZE GT_SIZE_COMPRESSED


/**
 * Prints the given master secret key on the console.
 *
 * @param[in] master_secret_key       - the master secret key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_print_master_secret_key(
    const del07_master_secret_key_t *master_secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given secret key on the console.
 *
 * @param[in] secret_key       - the secret key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_print_secret_key(const del07_secret_key_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given public key on the console.
 *
 * @param[in] public_key        - the public key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_print_public_key(const del07_public_key_t *public_key);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given ciphertext on the console.
 *
 * @param[in] ciphertext        - the ciphertext that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_print_ciphertext(const del07_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Prints the given key on the console.
 *
 * @param[in] key        - the key that is printed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_print_key(const del07_key_t *key);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given master secret key.
 *
 * @param[in] master_secret_key       - the master secret key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_set_master_secret_key_to_zero(
    del07_master_secret_key_t *master_secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given secret key.
 *
 * @param[in] secret_key       - the secret key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_set_secret_key_to_zero(del07_secret_key_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given public key.
 *
 * @param[in] public_key        - the public key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_set_public_key_to_zero(del07_public_key_t *public_key);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given ciphertext.
 *
 * @param[in] ciphertext        - the ciphertext that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_set_ciphertext_to_zero(del07_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Frees the given key.
 *
 * @param[in] key        - the key that is freed
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_set_key_to_zero(del07_key_t *key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given master secret key is valid.
 *
 * @param[in] master_secret_key       - the master secret key that is checked
 *
 * @return true if the given master secret key is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_master_secret_key_is_valid(int *valid,
    const del07_master_secret_key_t *master_secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given secret key is valid.
 *
 * @param[in] secret_key       - the secret key that is checked
 *
 * @return true if the given secret key is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_secret_key_is_valid(int *valid, const del07_secret_key_t *secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given public key is valid.
 *
 * @param[in] public_key        - the public key that is checked
 *
 * @return true if the given public key is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_public_key_is_valid(int *valid, const del07_public_key_t *public_key);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given ciphertext is valid.
 *
 * @param[in] ciphertext        - the ciphertext that is checked
 *
 * @return true if the given ciphertext is valid, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_ciphertext_is_valid(int *valid, const del07_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Converts a given key to a bit string of length of the security parameter
 * using the KDF2 key derivation function.
 *
 * @param[out] bit_string       - the key represented as bit string
 * @param[in] key        - the key that is converted
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_convert_key_to_bit_string(uint8_t **bit_string,
                                    const del07_key_t *key);
#ifdef __cplusplus
}
#endif

/**
 * Writes the given G_1 element in the given byte array using point compression.
 *
 * @param[out] bin          - The byte array in which the G_1 element is
 *                            written.
 * @param[in] element       - The G_1 element that is written in the byte array.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_g1_compress(uint8_t **bin, g1_t element);

/**
 * Writes the given G_2 element in the given byte array using point compression.
 *
 * @param[out] bin          - The byte array in which the G_2 element is
 *                            written.
 * @param[in] element       - The G_2 element that is written in the byte array.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_g2_compress(uint8_t **bin, g2_t element);

/**
 * Writes the given G_T element in the given byte array using point compression.
 *
 * @param[out] bin          - The byte array in which the G_T element is
 * written.
 * @param[in] element       - The G_T element that is written in the byte array.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_gt_compress(uint8_t **bin, gt_t element);

/**
 * Reads in a G_1 element from the given byte array.
 *
 * @param[out] element      - The G_1 element to which the byte array is read
 *                            in.
 * @param[in] bin           - The byte array that is read.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_g1_decompress(g1_t element, uint8_t **bin);

/**
 * Reads in a G_2 element from the given byte array.
 *
 * @param[out] element      - The G_2 element to which the byte array is read
 *                            in.
 * @param[in] bin           - The byte array that is read.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_g2_decompress(g2_t element, uint8_t **bin);

/**
 * Reads in a G_T element from the given byte array.
 *
 * @param[out] element      - The G_T element to which the byte array is read
 *                            in.
 * @param[in] bin           - The byte array that is read.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_gt_decompress(gt_t element, uint8_t **bin);

/**
 * Serializes the given master secret key.
 *
 * @param[out] serialized           - the serialized master secret key
 * @param[out] size                 - the size of the serialized array
 * @param[in] master_secret_key     - the master secret key that is serialized
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_serialize_master_secret_key(uint8_t **serialized, size_t *size,
    const del07_master_secret_key_t *master_secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Serializes the given secret key.
 *
 * @param[out] serialized       - the serialized secret key
 * @param[in] secret_key        - the secret key that is serialized
 * @param[in] pack              - if true the point is serialized in compressed
 *                                form
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_serialize_secret_key(uint8_t **serialized,
                                const del07_secret_key_t *secret_key,
                                int compression);
#ifdef __cplusplus
}
#endif

/**
 * Serializes the given public key.
 *
 * @param[out] serialized       - the serialized public key
 * @param[in] public_key        - the public key that is serialized
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_serialize_public_key(uint8_t **serialized,
                                const del07_public_key_t *public_key);
#ifdef __cplusplus
}
#endif

/**
 * Serializes the given ciphertext.
 *
 * @param[out] serialized       - the serialized ciphertext
 * @param[in] ciphertext        - the ciphertext that is serialized
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_serialize_ciphertext(uint8_t **serialized,
                                const del07_ciphertext_t *ciphertext);
#ifdef __cplusplus
}
#endif

/**
 * Serializes the given key.
 *
 * @param[out] serialized       - the serialized key
 * @param[in] key               - the key that is serialized
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_serialize_key(uint8_t **serialized, const del07_key_t *key);
#ifdef __cplusplus
}
#endif

/**
 * Deserializes the given master secret key.
 *
 * @param[out] master_secret_key        - the resulting deserialized master
 *                                        secret key
 * @param[in] serialized                - the serialized master secret key
 * @param[in] size                      - the size of the serialized array
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_deserialize_master_secret_key(
    del07_master_secret_key_t **master_secret_key, const uint8_t *serialized,
    const size_t size);
#ifdef __cplusplus
}
#endif

/**
 * Deserializes the given secret key.
 *
 * @param[out] secret_key       - the resulting deserialized secret key
 * @param[in] serialized        - the serialized secret key
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_deserialize_secret_key(del07_secret_key_t **secret_key,
                                 const uint8_t *serialized,
                                 int compression);
#ifdef __cplusplus
}
#endif

/**
 * Deserializes the given public key.
 *
 * @param[out] public_key       - the resulting deserialized public key
 * @param[in] serialized        - the serialized public key
 * @param[in] compute_precomputation_tables
 *                              - if true the precomputation tables are
 *                                deserialized as well.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_deserialize_public_key(del07_public_key_t **public_key,
                                 const uint8_t *serialized,
                                 int compute_precomputation_tables);
#ifdef __cplusplus
}
#endif

/**
 * Deserializes the given ciphertext.
 *
 * @param[out] ciphertext       - the resulting deserialized ciphertext
 * @param[in] serialized        - the serialized ciphertext
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_deserialize_ciphertext(del07_ciphertext_t **ciphertext,
                                  const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

/**
 * Deserializes the given key.
 *
 * @param[out] key              - the resulting deserialized key
 * @param[in] serialized        - the serialized key
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_deserialize_key(del07_key_t **key, const uint8_t *serialized);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given master secret keys are equal.
 *
 * @param[in] l     - the first master secret key
 * @param[in] r     - the second master secret key
 *
 * @return True if the master secret keys are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_master_secret_keys_are_equal(const del07_master_secret_key_t *l,
                                       const del07_master_secret_key_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given secret keys are equal.
 *
 * @param[in] l     - the first secret key
 * @param[in] r     - the second secret key
 *
 * @return True if the secret keys are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_secret_keys_are_equal(const del07_secret_key_t *l,
                                const del07_secret_key_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given public keys are equal.
 *
 * @param[in] l     - the first public key
 * @param[in] r     - the second public key
 *
 * @return True if the public keys are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_public_keys_are_equal(const del07_public_key_t *l,
                                const del07_public_key_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given ciphertexts are equal.
 *
 * @param[in] l     - the first ciphertext
 * @param[in] r     - the second ciphertext
 *
 * @return True if the ciphertexts are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_ciphertexts_are_equal(const del07_ciphertext_t *l,
                                const del07_ciphertext_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Checks if the given keys are equal.
 *
 * @param[in] l     - the first key
 * @param[in] r     - the second key
 *
 * @return True if the keys are equal, false otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_keys_are_equal(const del07_key_t *l, const del07_key_t *r);
#ifdef __cplusplus
}
#endif

/**
 * Returns the size of the given public key.
 *
 * @param[in] public_key        - the public key of which the size is returned.
 *
 * @return the size of the given public key.
 */
#ifdef __cplusplus
extern "C" {
#endif
unsigned del07_get_public_key_size(const del07_public_key_t *public_key);
#ifdef __cplusplus
}
#endif

/**
 * Returns the size of the given master secret key.
 *
 * @param[in] master_secret_key        - the master secret key of which the size is returned
 *
 * @return the size of the given master secret key.
 */
#ifdef __cplusplus
extern "C" {
#endif
unsigned del07_get_master_secret_key_size(
    const del07_master_secret_key_t *master_secret_key);
#ifdef __cplusplus
}
#endif

/**
 * Returns the size of a secret key.
 *
 * @param[in] compression        - if true the size is returned for compression
 *
 * @return the size of a secret key.
 */
#ifdef __cplusplus
extern "C" {
#endif
unsigned del07_get_secret_key_size(int compression);
#ifdef __cplusplus
}
#endif

/**
 * Function of a helper thread which is responsible for only computing a
 * exponentiation in G_2.
 *
 * @param[in, out] data     - The del07_g2_exponentiate_thread_data containing
 *                            the data for this thread
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
void *del07_g2_exponentiate_thread(void *data);
#ifdef __cplusplus
}
#endif

/**
 * Function of a helper thread which is responsible for only computing a
 * exponentiation in G_2 using a pre-computation table.
 *
 * @param[in, out] data     - The del07_g2_exponentiate_thread_data containing
 *                            the data for this thread
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
void *del07_g2_exponentiate_thread_with_precomputation(void *arg);
#ifdef __cplusplus
}
#endif

/**
 * Function of a helper thread which is responsible for only computing a
 * exponentiation in G_T.
 *
 * @param[in, out] data     - The del07_gt_exponentiate_thread_data containing
 *                            the data for this thread
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
void *del07_gt_exponentiate_thread(void *data);
#ifdef __cplusplus
}
#endif

/**
 * Function of a helper thread which is responsible for only computing a
 * paring.
 *
 * @param[in, out] data     - The del07_pairing_thread_data containing
 *                            the data for this thread
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
void *del07_pairing_thread(void *data);
#ifdef __cplusplus
}
#endif

/**
 * Function of a helper thread which is responsible for only computing the
 * inversion of a bn.
 *
 * @param[in, out] data     - The del07_bn_inversion_thread_data containing
 *                            the data for this thread
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
void *del07_bn_inversion_thread(void *data);
#ifdef __cplusplus
}
#endif

/**
 * Function of a thread executing the CPA secure decapsulation.
 *
 * @param[in, out] data     - The del07_cpa_decapsulate_thread_data containing
 *                            the data for this thread
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
void * del07_cpa_decapsulate_thread(void *data);

/**
 * Sets memory at the given location to zero.
 *
 * @param[in] ptr     - pointer to location which is to be zeroed
 * @param[in] len     - number of bytes to zero
 *
 * Credits: https://github.com/etairi/memzero
 */
void memzero(void *ptr, size_t len);

#endif  // DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_UTIL_H_
