// Copyright [2018] PG 0-RTT

#ifndef DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_H_
#define DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_H_

#include "Delerablee_07_IBBE_types.h"

#define SECURITY_PARAMETER MD_LEN_SH256

/**
 * Initializes the library.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_init();
#ifdef __cplusplus
}
#endif


/**
 * Executes the setup algorithm and generates the master secret key and the
 * public key.
 *
 * @param[out] master_secret_key        - the resulting master secret key
 * @param[out] public_key               - the resulting public key
 * @param[in] max_number_recipients     - the maximum number recipients of the
 *                                        IBBE system
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_setup(del07_master_secret_key_t **master_secret_key,
                del07_public_key_t **public_key,
                const unsigned max_number_recipients);
#ifdef __cplusplus
}
#endif


/**
 * Executes the extract algorithm and generates a secret key for the given ID.
 *
 * @param[out] secret_key           - the resulting secret key
 * @param[in] master_secret_key     - the master secret key from which the
 *                                    secret key is derived
 * @param[in] identity              - the ID for which the secret key is generated
 * @param[in] identity_length       - the length of the ID in byte
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_extract(del07_secret_key_t **secret_key,
                  const del07_master_secret_key_t *master_secret_key,
                  const uint8_t *identity,
                  const unsigned identity_length);
#ifdef __cplusplus
}
#endif


/**
 * Executes the CHK transformation and the encapsulate algorithm and generates a
 * random key and a ciphertext encapsulating the key for the given set of
 * identities.
 *
 * @param[out] ciphertext           - the resulting ciphertext
 * @param[out] key                  - the resulting random key
 * @param[in] identities            - an array containing the IDs for which the
 *                                    key is encapsulated
 * @param[in] number_identities     - the number of IDs in identities
 * @param[in] identity_lengths      - an array containing the lengths of the
 *                                    IDs in identities
 * @param[in] public_key            - the public key
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_encapsulate(del07_ciphertext_t **ciphertext,
                      del07_key_t **key,
                      const uint8_t **identities,
                      const unsigned number_identities,
                      const unsigned *identity_lengths,
                      const del07_public_key_t *public_key);
#ifdef __cplusplus
}
#endif

/**
 * Executes the CPA secure encapsulate algorithm and generates a random key and
 * a ciphertext encapsulating the key for the given set of identities.
 *
 * @param[out] ciphertext           - the resulting ciphertext
 * @param[out] key                  - the resulting random key
 * @param[in] identities            - an array containing the IDs for which the
 *                                    key is encapsulated
 * @param[in] number_identities     - the number of IDs in identities
 * @param[in] identity_lengths      - an array containing the lengths of the
 *                                    IDs in identities
 * @param[in] public_key            - the public key
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_cpa_encapsulate(del07_ciphertext_t **ciphertext,
                          del07_key_t **key,
                          const uint8_t **identities,
                          const unsigned number_identities,
                          const unsigned *identity_lengths,
                          const del07_public_key_t *public_key);


/**
 * Executes the CHK transformation and decapsulate algorithm and computes the
 * key encapsulated in the given ciphertext.
 *
 * @param[out] key                          - the resulting key
 * @param[in] identities                    - an array containing the IDs for
 *                                            which the key has been encapsulated
 * @param[in] number_identities             - the number of IDs in identities
 * @param[in] identity_lengths              - an array containing the lengths
 *                                          of the IDs in identities
 * @param[in] decapsulation_identity_index     - the index of the ID for which
 *                                               the ciphertext is decapsulated in
 *                                               the array
 *                                            identities
 * @param[in] secret_key                    - the secret key corresponding to
 *                                            the ID for which the ciphertext is
 *                                            decapsulated
 * @param[in] ciphertext                    - the ciphertext that is decapsulated
 * @param[in] public_key                    - the public key
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_decapsulate(del07_key_t **key,
                      const uint8_t **identities,
                      const unsigned number_identities,
                      const unsigned *identity_lengths,
                      const unsigned decapsulation_identity_index,
                      const del07_secret_key_t *secret_key,
                      const del07_ciphertext_t *ciphertext,
                      const del07_public_key_t *public_key);
#ifdef __cplusplus
}
#endif


/**
 * Executes the CPA secure decapsulate algorithm and computes the key
 * encapsulated in the given ciphertext.
 *
 * @param[out] key                          - the resulting key
 * @param[in] identities                    - an array containing the IDs for
 *                                            which the key has been encapsulated
 * @param[in] number_identities             - the number of IDs in identities
 * @param[in] identity_lengths              - an array containing the lengths
 *                                          of the IDs in identities
 * @param[in] decapsulation_identity_index     - the index of the ID for which
 *                                               the ciphertext is decapsulated in
 *                                               the array
 *                                            identities
 * @param[in] secret_key                    - the secret key corresponding to
 *                                            the ID for which the ciphertext is
 *                                            decapsulated
 * @param[in] ciphertext                    - the ciphertext that is decapsulated
 * @param[in] public_key                    - the public key
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
int del07_cpa_decapsulate(del07_key_t **key,
                          const uint8_t **identities,
                          const unsigned number_identities,
                          const unsigned *identity_lengths,
                          const unsigned decapsulation_identity_index,
                          const del07_secret_key_t *secret_key,
                          const del07_ciphertext_t *ciphertext,
                          const del07_public_key_t *public_key);


/**
 * Hashes the given identity to Z_p^* by using sha256 and interpreting the hash
 * value as binary coded number which is then reduced by the group order.
 *
 * @param[out] hashed_id            - the resulting hash value from Z_p^*
 * @param[in] id                    - the identity that is hashed
 * @param[in] identity_length       - the length of the identity that is hashed
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_hash_id(bn_t hashed_id,
                  const uint8_t *id,
                  const unsigned identity_length);
#ifdef __cplusplus
}
#endif

/**
 * Computes coefficients of the polynomial with the given roots modulo a modulus.
 *
 * @param[out] coefficients         - the resulting coefficients
 * @param[in] roots                 - the roots of the polynomial
 * @param[in] number_roots          - number of roots
 * @param[in] modulus               - group order
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_polynomial_coefficients_from_roots(bn_t *coefficients,
                                             const bn_t *roots,
                                             const unsigned number_roots,
                                             const bn_t modulus);
#ifdef __cplusplus
}
#endif


/**
 * Encodes the identity, i.e. prepending it with a zero. The resulting identity
 * has a length of identity_length + 1.
 *
 * @param[out] encoded_identity     - The resulting encoded identity.
 * @param[in] identity              - The identity that will be encoded.
 * @param[in] identity_length       - The length of the identity to encode.
 */
#ifdef __cplusplus
extern "C" {
#endif
void del07_encode(uint8_t **encoded_identity,
                 const uint8_t *identity,
                 const unsigned identity_length);
#ifdef __cplusplus
}
#endif


/**
 * Encodes the given set of identities and verification key.
 *
 * @param[out] encoded_identities
 * @param[out] encoded_identity_lengths
 * @param[in] identities
 * @param[in] number_identities
 * @param[in] identity_lengths
 * @param[in] verification_key
 */
void del07_encode_identity_set(uint8_t **encoded_identities,
                               unsigned int *encoded_identity_lengths,
                               const uint8_t **identities,
                               const unsigned int number_identities,
                               const unsigned int *identity_lengths,
                               g2_t verification_key);


/**
 * Finalizes the library.
 *
 * @return STS_OK if no error occurs, STS_ERR otherwise.
 */
#ifdef __cplusplus
extern "C" {
#endif
int del07_clean();
#ifdef __cplusplus
}
#endif

#endif  // DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_H_
