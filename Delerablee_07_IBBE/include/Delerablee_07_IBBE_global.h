// Copyright [2018] PG 0-RTT

#ifndef DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_GLOBAL_H_
#define DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_GLOBAL_H_

/**
 * Global pointer to the RELIC context.
 */
extern ctx_t *del07_context;

/**
 * Attribute to make threads joinable.
 */
extern pthread_attr_t del07_attr_joinable;

#endif  // DELERABLEE_07_IBBE_INCLUDE_DELERABLEE_07_IBBE_GLOBAL_H_
