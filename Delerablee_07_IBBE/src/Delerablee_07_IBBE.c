#include <pthread.h>
#include <stddef.h>
#include <relic/relic.h>
#include "Delerablee_07_IBBE.h"
#include "Delerablee_07_IBBE_conf.h"
#include "Delerablee_07_IBBE_global.h"
#include "Delerablee_07_IBBE_util.h"

#define IDENTITY_PREFIX 0
#define VERIFICATION_KEY_PREFIX 1

ctx_t *del07_context;
pthread_attr_t del07_attr_joinable;

int del07_init() {
  if (core_init() != STS_OK) {
    core_clean();
    return STS_ERR;
  }

  //initializes BN-256 curve
  if (ep_param_set_any_pairf() == STS_ERR) {
    THROW(ERR_NO_CURVE);
    core_clean();
    return STS_ERR;
  }

  // Set the global pointer to the context
  del07_context = core_get();

  pthread_attr_init(&del07_attr_joinable);
  pthread_attr_setdetachstate(&del07_attr_joinable, PTHREAD_CREATE_JOINABLE);

  return STS_OK;
}

int del07_setup(del07_master_secret_key_t **master_secret_key,
                del07_public_key_t **public_key,
                const unsigned max_number_recipients) {

  *master_secret_key = malloc(sizeof(del07_master_secret_key_t));

  *public_key = malloc(offsetof(del07_public_key_t, h_to_the_gamma_powers)
                           + (max_number_recipients + 1) * sizeof(g2_t));

  if (!*master_secret_key || !*public_key) {
    return STS_ERR;
  }

  (*public_key)->h_precomputation_table =
      malloc(RELIC_EPX_TABLE_YAOWI * sizeof(g2_t));

  (*public_key)->h_to_the_gamma_powers_precomputation_table =
      malloc((max_number_recipients - 1) * RELIC_EPX_TABLE_YAOWI * sizeof(g2_t));

  if (!(*public_key)->h_to_the_gamma_powers_precomputation_table
      || !(*public_key)->h_precomputation_table) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t public_key_w;
  gt_t public_key_v;
  g2_t public_key_h;
  g2_t public_key_h_powers[max_number_recipients + 1];

  bn_t group_order;
  bn_t i_bn;
  bn_t exponent;

  g1_null((*master_secret_key)->g);
  bn_null(master_secret_key->gamma);
  g1_null(public_key_w);
  gt_null(public_key_v);
  g2_null(public_key_h);

  for (size_t i = 0; i < max_number_recipients + 1; i++) {
    g2_null(public_key_h_powers[i]);
  }

  bn_null(group_order);
  bn_null(i_bn);
  bn_null(exponent);

  TRY {
    bn_new((*master_secret_key)->gamma);
    g1_new((*master_secret_key)->g);
    g1_new(public_key_w);
    gt_new(public_key_v);
    g2_new(public_key_h);

    for (size_t i = 0; i < max_number_recipients + 1; i++) {
      g2_new(public_key_h_powers[i]);
    }

    bn_new(group_order);
    bn_new(i_bn);
    bn_new(exponent);

    g1_rand((*master_secret_key)->g);
    g2_rand(public_key_h);

    g2_get_ord(group_order);

    // choose random gamma from Z_p^*
    bn_rand_mod((*master_secret_key)->gamma, group_order);

    g1_exponentiate(public_key_w,
                    (*master_secret_key)->g,
                    (*master_secret_key)->gamma);

    pc_map(public_key_v, (*master_secret_key)->g, public_key_h);

    // compute h^(gamma^i)
    for (size_t i = 1; i <= max_number_recipients + 1; i++) {
      bn_null(i_bn);
      bn_new(i_bn);

      // convert i to bn_t
      bn_set_dig(i_bn, (dig_t) i);

      bn_null(exponent);
      bn_new(exponent);

      bn_mxp(exponent, (*master_secret_key)->gamma, i_bn, group_order);

      g2_exponentiate(public_key_h_powers[i - 1], public_key_h, exponent);
    }

    g1_copy((*public_key)->w, public_key_w);
    gt_copy((*public_key)->v, public_key_v);
    g2_copy((*public_key)->h, public_key_h);

    for (size_t i = 0; i < max_number_recipients + 1; i++) {
      g2_copy((*public_key)->h_to_the_gamma_powers[i], public_key_h_powers[i]);
    }

    (*public_key)->max_number_recipients = max_number_recipients + 1;

    // Compute pre-computation tables
    ep_mul_pre_nafwi((*master_secret_key)->g_precomputation_table,
                     (*master_secret_key)->g);
    ep2_mul_pre_yaowi((*public_key)->h_precomputation_table, (*public_key)->h);
    for(unsigned i = 0; i < max_number_recipients - 1; i++) {
      ep2_mul_pre_yaowi(
          (*public_key)->h_to_the_gamma_powers_precomputation_table
              + i * RELIC_EPX_TABLE_YAOWI, (*public_key)->h_to_the_gamma_powers[i]);
    }

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_free(public_key_w);
    gt_free(public_key_v);
    g2_free(public_key_h);
    bn_free(group_order);
    bn_free(i_bn);
    bn_free(exponent);
  }

  return result_status;
}

int del07_extract(del07_secret_key_t **secret_key,
                  const del07_master_secret_key_t *master_secret_key,
                  const uint8_t *identity,
                  const unsigned identity_length) {
  int master_secret_key_is_valid;
  int is_valid_status =
      del07_master_secret_key_is_valid(&master_secret_key_is_valid,
                                       master_secret_key);
  if (is_valid_status || !master_secret_key_is_valid) {
    return STS_ERR;
  }

  *secret_key = malloc(sizeof(del07_secret_key_t));

  if (!*secret_key) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t secret_key_sk;
  bn_t hashed_id;
  bn_t gamma_plus_hashed_id;
  bn_t group_order;
  bn_t r;
  bn_t exponent;

  g1_null(secret_key_sk);
  bn_null(hashed_id);
  bn_null(gamma_plus_hashed_id);
  bn_null(group_order);
  bn_null(r);
  bn_null(exponent);

  TRY {
    g1_new(secret_key_sk);
    bn_new(hashed_id);
    bn_new(gamma_plus_hashed_id);
    bn_new(group_order);
    bn_new(r);
    bn_new(exponent);

    // CHK Transformation: encode identity.
    uint8_t *encoded_identity;
    del07_encode(&encoded_identity, identity, identity_length);

    int hash_status = del07_hash_id(hashed_id, encoded_identity, identity_length + 1);
    free(encoded_identity);
    if (hash_status) {
      return hash_status;
    }

    bn_add(gamma_plus_hashed_id, master_secret_key->gamma, hashed_id);

    g1_get_ord(group_order);

    bn_gcd_ext(r, exponent, NULL, gamma_plus_hashed_id, group_order);
    if (bn_sign(exponent) == BN_NEG) {
      bn_add(exponent, exponent, group_order);
    }

    ep_mul_fix_nafwi(secret_key_sk,
                     master_secret_key->g_precomputation_table,
                     exponent);

#ifdef  WITH_POINT_COMPRESSION
    int compress_status =
        del07_g1_compress(&(*secret_key)->sk, secret_key_sk);
    if (compress_status) {
      return STS_ERR;
    }
#else
    g1_copy((*secret_key)->sk, secret_key_sk);
#endif

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_set_infty(secret_key_sk);
    g1_free(secret_key_sk);
    bn_free(hashed_id);
    bn_free(gamma_plus_hashed_id);
    bn_free(group_order);
    bn_free(r);
    bn_free(exponent);
  }

  return result_status;
}

int del07_cpa_encapsulate(del07_ciphertext_t **ciphertext,
                      del07_key_t **key,
                      const uint8_t **identities,
                      const unsigned number_identities,
                      const unsigned *identity_lengths,
                      const del07_public_key_t *public_key) {
  *ciphertext = malloc(sizeof(del07_ciphertext_t));
  *key = malloc(sizeof(del07_key_t));

  if (!*ciphertext || !*key) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t ciphertext_c_1;
  g2_t ciphertext_c_2;
  g1_t public_key_w;
  gt_t public_key_v;
  g2_t public_key_h;
  g2_t public_key_h_powers[public_key->max_number_recipients];
  bn_t group_order;
  bn_t k;
  bn_t negative_k;
  bn_t hashed_identities[number_identities];
  bn_t negative_hashed_identities[number_identities];
  bn_t coefficients[number_identities + 1];
  g2_t h_to_the_gamma_polynomial;

  // Helper threads to compute g2 exponentiations in parallel.
  pthread_t g2_exponentiate_threads[number_identities + 1];
  del07_g2_exponentiate_thread_data_t
      g2_exponentiate_thread_data_array[number_identities + 1];
  // Helper thread to compute gt exponentiations in parallel.
  pthread_t gt_exponentiate_thread;
  del07_gt_exponentiate_thread_data_t gt_exponentiate_thread_data;

  g1_null(ciphertext_c_1);
  g2_null(ciphertext_c_2);
  bn_null(group_order);
  bn_null(k);
  bn_null(negative_k);
  gt_null(gt_exponentiate_thread_data.base);
  bn_null(gt_exponentiate_thread_data.exponent);

  for (size_t i = 0; i < number_identities; i++) {
    bn_null(hashed_identities[i]);
    bn_null(negative_hashed_identities[i]);
  }

  for (size_t i = 0; i < number_identities + 1; i++) {
    bn_null(coefficients[i]);
    g2_null(g2_exponentiate_thread_data_array[i].base);
    bn_null(g2_exponentiate_thread_data_array[i].exponent);
  }

  g2_null(h_to_the_gamma_polynomial);

  TRY {
    g1_new(ciphertext_c_1);
    g2_new(ciphertext_c_2);
    bn_new(group_order);
    bn_new(k);
    bn_new(negative_k);
    gt_new(gt_exponentiate_thread_data.base);
    bn_new(gt_exponentiate_thread_data.exponent);

    for (size_t i = 0; i < number_identities; i++) {
      bn_new(hashed_identities[i]);
      bn_new(negative_hashed_identities[i]);
    }

    for (size_t i = 0; i < number_identities + 1; i++) {
      bn_new(coefficients[i]);
      g2_new(g2_exponentiate_thread_data_array[i].base);
      bn_new(g2_exponentiate_thread_data_array[i].exponent);
    }

    g2_new(h_to_the_gamma_polynomial);

    g1_copy(public_key_w, public_key->w);
    gt_copy(public_key_v, ((del07_public_key_t *) public_key)->v);
    g2_copy(public_key_h, ((del07_public_key_t *) public_key)->h);
    for (size_t i = 0; i < public_key->max_number_recipients; i++) {
      g2_copy(public_key_h_powers[i], ((del07_public_key_t *) public_key)->h_to_the_gamma_powers[i]);
    }

    g2_get_ord(group_order);

    // compute c_1

    // choose random k from Z_p^*
    bn_rand_mod(k, group_order);

    // compute key
    // Set the data for the gt exponentiate helper thread.
    gt_copy(gt_exponentiate_thread_data.base, public_key_v);
    bn_copy(gt_exponentiate_thread_data.exponent, k);

    // Start the helper thread.
    pthread_create(&gt_exponentiate_thread,
                   &del07_attr_joinable,
                   del07_gt_exponentiate_thread,
                   &gt_exponentiate_thread_data);

    bn_neg(negative_k, k);

    g1_exponentiate(ciphertext_c_1, public_key_w, negative_k);

    // compute c_2

    // hash all input IDs
    for (size_t i = 0; i < number_identities; i++) {
      int hash_status = del07_hash_id(hashed_identities[i],
                                      identities[i],
                                      identity_lengths[i]);
      if (hash_status) {
        return hash_status;
      }

      bn_neg(negative_hashed_identities[i], hashed_identities[i]);
      bn_mod(negative_hashed_identities[i],
             negative_hashed_identities[i],
             group_order);
    }

    // Let p = prod (gamma + H(ID_i).
    // First compute the coefficients a_0, ..., a_n such that p = a_n * gamma^n
    // + ... + a_1 * gamma^1 + a_0
    int coeff_from_roots_status = del07_polynomial_coefficients_from_roots(
        coefficients,
        negative_hashed_identities,
        number_identities,
        group_order);
    if (coeff_from_roots_status) {
      return coeff_from_roots_status;
    }

    // Use h and h_to_the_gamma_powers and the coefficients of p to compute for
    // random k:
    // c_2 = (prod_{i=0}^{number_identities} h^{gamma^i}^{a_i} )^k
    // Note that h^{gamma^i} is given in the public key by
    // h_to_the_gamma_powers[i - 1]
    // Each helper thread computes one exponentiation h^{gamma^i}^{a_i}
    for (size_t i = 0; i < number_identities + 1; i++) {

      // Set the base and the exponent for exponentiate helper thread i.
      if(i == 0) {
        g2_copy(g2_exponentiate_thread_data_array[i].base, public_key_h);
      } else {
        g2_copy(g2_exponentiate_thread_data_array[i].base, public_key_h_powers[i - 1]);
      }

      bn_copy(g2_exponentiate_thread_data_array[i].exponent, coefficients[i]);

      // Start the helper thread
      pthread_create(&g2_exponentiate_threads[i],
                     &del07_attr_joinable,
                     del07_g2_exponentiate_thread,
                     (void *) &g2_exponentiate_thread_data_array[i]);
    }

    for (size_t i = 0; i < number_identities + 1; i++) {
      // Join the helper thread.
      pthread_join(g2_exponentiate_threads[i], NULL);

      if (i == 0) {
        g2_copy(h_to_the_gamma_polynomial, g2_exponentiate_thread_data_array->result);
      } else {
        g2_multiply(h_to_the_gamma_polynomial,
                    h_to_the_gamma_polynomial,
                    g2_exponentiate_thread_data_array[i].result);
      }
    }

    g2_exponentiate(ciphertext_c_2, h_to_the_gamma_polynomial, k);

    // Join the gt exponentiate helper thread.
    pthread_join(gt_exponentiate_thread, NULL);

#ifdef WITH_POINT_COMPRESSION
    int compress_status =
        del07_g1_compress(&(*ciphertext)->c_1, ciphertext_c_1);
    compress_status |=
        del07_g2_compress(&(*ciphertext)->c_2, ciphertext_c_2);
    compress_status |= del07_gt_compress(&(*key)->k,
                                         gt_exponentiate_thread_data.result);

    if (compress_status) {
      return STS_ERR;
    }
#else
    g1_copy((*ciphertext)->c_1, ciphertext_c_1);
    g2_copy((*ciphertext)->c_2, ciphertext_c_2);
    gt_copy((*key)->k, gt_exponentiate_thread_data.result);
#endif

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_free(ciphertext_c_1);
    g2_free(ciphertext_c_2);
    g1_free(public_key_w);
    gt_free(public_key_v);
    g2_free(public_key_h);
    for (size_t i = 0; i < public_key->max_number_recipients; i++) {
      g2_free(public_key_h_powers[i]);
    }
    bn_free(k);
    bn_free(negative_k);
    bn_free(group_order);
    gt_free(gt_exponentiate_thread_data.result);
    gt_free(gt_exponentiate_thread_data.base);
    bn_free(gt_exponentiate_thread_data.exponent);

    for (size_t i = 0; i < number_identities; i++) {
      bn_free(hashed_identities[i]);
      bn_free(negative_hashed_identities[i])
    }

    for (size_t i = 0; i < number_identities + 1; i++) {
      bn_free(coefficients[i]);
      g2_free(g2_exponentiate_thread_data_array[i].result);
      g2_free(g2_exponentiate_thread_data_array[i].base);
      bn_free(g2_exponentiate_thread_data_array[i].exponent);
    }

    g2_free(h_to_the_gamma_polynomial);
  }

  return result_status;
}

int del07_encapsulate(del07_ciphertext_t **ciphertext,
                      del07_key_t **key,
                      const uint8_t **identities,
                      const unsigned number_identities,
                      const unsigned *identity_lengths,
                      const del07_public_key_t *public_key) {
  if (number_identities > public_key->max_number_recipients - 1) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  bn_t signing_key;
  g2_t verification_key;
  uint8_t **encoded_identities = malloc((number_identities + 1) * sizeof(uint8_t *));
  unsigned *encoded_identity_lengths = malloc((number_identities + 1) * sizeof(unsigned));
  int ciphertext_bin_size = G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED;
  uint8_t *ciphertext_bin = malloc((size_t) ciphertext_bin_size);
  g1_t signature;

  bn_null(signing_key);
  g2_null(verification_key);
  g1_null(signature);

  TRY {
    bn_new(signing_key);
    g2_new(verification_key);
    g1_new(signature);

    // Generate BLS signature key pair.
    int gen_status = cp_bls_gen(signing_key, verification_key);
    if (gen_status) {
      return STS_ERR;
    }

    // Encode the identities and verification key.
    del07_encode_identity_set(encoded_identities,
                              encoded_identity_lengths,
                              identities,
                              number_identities,
                              identity_lengths,
                              verification_key);

    // Encapsulate under the encoded identities together with the verification
    // key.
    int encapsulate_status = del07_cpa_encapsulate(ciphertext, key, (const uint8_t **) encoded_identities, number_identities + 1, encoded_identity_lengths, public_key);
    if (encapsulate_status) {
      return STS_ERR;
    }

    // Sign the ciphertext.
#ifdef WITH_POINT_COMPRESSION
    memcpy(ciphertext_bin, (*ciphertext)->c_1, G1_SIZE_COMPRESSED);
    memcpy(ciphertext_bin + G1_SIZE_COMPRESSED, (*ciphertext)->c_2, G2_SIZE_COMPRESSED);
#else
    g1_write_bin(ciphertext_bin, ciphertext_bin_size, (*ciphertext)->c_1, 1);
    g2_write_bin(ciphertext_bin + G1_SIZE_COMPRESSED, ciphertext_bin_size - G1_SIZE_COMPRESSED, (*ciphertext)->c_2, 1);
#endif
    cp_bls_sig(signature, ciphertext_bin, ciphertext_bin_size, signing_key);

    // Set the verification key and the signature.
#ifdef WITH_POINT_COMPRESSION
    del07_g2_compress(&((*ciphertext)->verification_key), verification_key);
    del07_g1_compress(&((*ciphertext)->signature), signature);
#else
    g2_copy((*ciphertext)->verification_key, verification_key);
    g1_copy((*ciphertext)->signature, signature);
#endif

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    bn_free(signing_key);
    g2_free(verification_key);
    for (size_t i = 0; i < number_identities + 1; i++) {
      free(encoded_identities[i]);
    }
    free(encoded_identities);
    free(encoded_identity_lengths);
    free(ciphertext_bin);
    g1_free(signature);
  }

  return result_status;
}

int del07_cpa_decapsulate(del07_key_t **key,
                      const uint8_t **identities,
                      const unsigned number_identities,
                      const unsigned *identity_lengths,
                      const unsigned decapsulation_identity_index,
                      const del07_secret_key_t *secret_key,
                      const del07_ciphertext_t *ciphertext,
                      const del07_public_key_t *public_key) {
  *key = malloc(sizeof(del07_key_t));

  if (!*key) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  gt_t key_k;
  g1_t secret_key_sk;
  g1_t ciphertext_c_1;
  g2_t ciphertext_c_2;
  g2_t public_key_h;
  g2_t public_key_h_powers[public_key->max_number_recipients];
  bn_t group_order;
  g2_t h_to_the_gamma_polynomial;
  gt_t mapping_sk_c_2;
  gt_t product_mappings;
  bn_t non_decapsulation_identities_product;
  bn_t hashed_identities[number_identities];
  bn_t negative_non_decapsulation_hashed_identities[number_identities - 1];
  bn_t coefficients[number_identities];

  // Helper thread to compute bn inversion in parallel.
  pthread_t bn_inversion_thread;
  del07_bn_inversion_thread_data_t bn_inversion_thread_data;

  // Helper threads to compute g2 exponentiations in parallel.
  pthread_t g2_exponentiate_threads[number_identities - 1];
  del07_g2_exponentiate_thread_data_with_precomputation_t
      g2_exponentiate_thread_data_with_precomputation_array[number_identities];
  // Helper thread to compute one pairing in parallel.
  del07_pairing_thread_data_t pairing_thread_data;
  pthread_t pairing_thread;

  gt_null(key_k);
  bn_null(group_order);
  g2_null(h_to_the_gamma_polynomial);
  gt_null(mapping_sk_c_2);
  gt_null(product_mappings);
  bn_null(non_decapsulation_identities_product);
  g1_null(pairing_thread_data.g1_element);
  g2_null(pairing_thread_data.g2_element);

  for (size_t i = 0; i < number_identities; i++) {
    bn_null(hashed_identities[i]);
    bn_null(coefficients[i]);
  }

  for (size_t i = 0; i < number_identities - 1; i++) {
    bn_null(negative_non_decapsulation_hashed_identities[i]);
    for(int l = 0; l < RELIC_EPX_TABLE_YAOWI; l++) {
      g2_null(g2_exponentiate_thread_data_with_precomputation_array[i].base_precomputation_table[l]);
    }
    bn_null(g2_exponentiate_thread_data_with_precomputation_array[i].exponent);
  }

  bn_null(bn_inversion_thread_data.number_to_invert);

  TRY {
    gt_new(key_k);
    bn_new(group_order);
    g2_new(h_to_the_gamma_polynomial);
    gt_new(mapping_sk_c_2);
    gt_new(product_mappings);
    bn_new(non_decapsulation_identities_product);
    g1_new(pairing_thread_data.g1_element);
    g2_new(pairing_thread_data.g2_element);

    for (size_t i = 0; i < number_identities; i++) {
      bn_new(hashed_identities[i]);
      bn_new(coefficients[i]);
    }

    for (size_t i = 0; i < number_identities - 1; i++) {
      bn_new(negative_non_decapsulation_hashed_identities[i]);
      for(int l = 0; l < RELIC_EPX_TABLE_YAOWI; l++) {
        g2_new(g2_exponentiate_thread_data_with_precomputation_array[i].base_precomputation_table[l]);
      }
      bn_new(g2_exponentiate_thread_data_with_precomputation_array[i].exponent);
    }

    bn_new(bn_inversion_thread_data.number_to_invert);

#ifdef WITH_POINT_COMPRESSION
    int compress_status =
        del07_g1_decompress(secret_key_sk, (uint8_t **) &secret_key->sk);
    compress_status |=
        del07_g1_decompress(ciphertext_c_1, (uint8_t **) &ciphertext->c_1);
    compress_status |=
        del07_g2_decompress(ciphertext_c_2, (uint8_t **) &ciphertext->c_2);

    if (compress_status) {
      return STS_ERR;
    }
#else
    g1_copy(secret_key_sk, secret_key->sk);
    g1_copy(ciphertext_c_1, ciphertext->c_1);
    g2_copy(ciphertext_c_2, ((del07_ciphertext_t *) ciphertext)->c_2);
#endif

    g2_copy(public_key_h, ((del07_public_key_t *) public_key)->h);
    for (size_t i = 0; i < public_key->max_number_recipients; i++) {
      g2_copy(public_key_h_powers[i],
              ((del07_public_key_t *) public_key)->h_to_the_gamma_powers[i]);
    }

    gt_get_ord(group_order);

    if (number_identities == 1) {
      pc_map(key_k, secret_key_sk, ciphertext_c_2);
    } else {

      // hash IDs and compute negative non-decapsulation hashed identities
      unsigned j = 0;
      for (size_t i = 0; i < number_identities; i++) {
        int hash_status = del07_hash_id(hashed_identities[i],
                                        identities[i],
                                        identity_lengths[i]);
        if (hash_status) {
          return hash_status;
        }

        if (i != decapsulation_identity_index) {
          bn_neg(negative_non_decapsulation_hashed_identities[j],
                 hashed_identities[i]);
          bn_mod(negative_non_decapsulation_hashed_identities[j],
                 negative_non_decapsulation_hashed_identities[j],
                 group_order);
          j++;
        }
      }

      bn_set_dig(non_decapsulation_identities_product, 1);

      // compute prod_{i=0, i != decapsulation_identity_index}^{
      // number_identities} H(ID_i)
      for (size_t i = 0; i < number_identities; i++) {
        if (i != decapsulation_identity_index) {
          bn_mul(non_decapsulation_identities_product,
                 non_decapsulation_identities_product,
                 hashed_identities[i]);
          bn_mod(non_decapsulation_identities_product,
                 non_decapsulation_identities_product,
                 group_order);
        }
      }

      bn_copy(bn_inversion_thread_data.number_to_invert, non_decapsulation_identities_product);

      pthread_create(&bn_inversion_thread, &del07_attr_joinable, del07_bn_inversion_thread, (void *) &bn_inversion_thread_data);


      // Let p = prod (gamma + H(ID_i) - prod (H(ID_i)) for i !=
      // decapsulation_identity_index.
      // First compute the coefficients a_0, ..., a_n such that
      // p = a_n * gamma^n + ... + a_1 * gamma^1 + a_0
      int coeff_from_roots_status = del07_polynomial_coefficients_from_roots(
          coefficients,
          negative_non_decapsulation_hashed_identities,
          number_identities - 1,
          group_order);

      if (coeff_from_roots_status) {
        return coeff_from_roots_status;
      }

      // compute p / gamma, that is shift coefficients to the left by 1
      for (size_t i = 0; i < number_identities - 1; i++) {
        bn_copy(coefficients[i], coefficients[i + 1]);
      }

      // Use h and h_to_the_gamma_powers and the coefficients of p / gamma to
      // compute:
      // (prod_{i=0, i != decapsulation_identity_index}^{number_identities}
      // h^{gamma^i}^{a_i} )
      // Note that h^{gamma^i} is given in the public key by
      // h_to_the_gamma_powers[i - 1]
      // Each helper thread computes one exponentiation h^{gamma^i}^{a_i}
      g2_copy(h_to_the_gamma_polynomial, public_key_h);

      for (size_t i = 0; i < number_identities - 1; i++) {

        // Set the base and the exponent for exponentiate helper thread i.
        if(i == 0) {
          for(int l = 0; l < RELIC_EPX_TABLE_YAOWI; l++) {
            g2_copy(g2_exponentiate_thread_data_with_precomputation_array[i].base_precomputation_table[l],
                    public_key->h_precomputation_table[l]);
          }
        } else {
          for(int l = 0; l < RELIC_EPX_TABLE_YAOWI; l++) {
            g2_copy(g2_exponentiate_thread_data_with_precomputation_array[i].base_precomputation_table[l],
                    public_key->h_to_the_gamma_powers_precomputation_table[(i - 1) * RELIC_EPX_TABLE_YAOWI + l]);
          }
        }
        bn_copy(g2_exponentiate_thread_data_with_precomputation_array[i].exponent, coefficients[i]);

        // Start the helper thread
        pthread_create(&g2_exponentiate_threads[i], &del07_attr_joinable,
                       del07_g2_exponentiate_thread_with_precomputation, (void *) &g2_exponentiate_thread_data_with_precomputation_array[i]);
      }

      for (size_t i = 0; i < number_identities - 1; i++) {
        // Join the helper thread.
        pthread_join(g2_exponentiate_threads[i], NULL);

        if (i == 0) {
          g2_copy(h_to_the_gamma_polynomial, g2_exponentiate_thread_data_with_precomputation_array->result);
        } else {
          g2_multiply(h_to_the_gamma_polynomial,
                      h_to_the_gamma_polynomial,
                      g2_exponentiate_thread_data_with_precomputation_array[i].result);
        }
      }

      // Set the two elements for the pairing helper thread.
      g1_copy(pairing_thread_data.g1_element, ciphertext_c_1);
      g2_copy(pairing_thread_data.g2_element, h_to_the_gamma_polynomial);

      pthread_create(&pairing_thread, &del07_attr_joinable, del07_pairing_thread, (void *) &pairing_thread_data);

      pc_map(mapping_sk_c_2, secret_key_sk, ciphertext_c_2);

      pthread_join(pairing_thread, NULL);

      gt_mul(product_mappings,
             pairing_thread_data.result,
             mapping_sk_c_2);

      pthread_join(bn_inversion_thread, NULL);

      gt_exp(key_k,
             product_mappings,
             bn_inversion_thread_data.result);
    }

#ifdef WITH_POINT_COMPRESSION
    compress_status = del07_gt_compress(&(*key)->k, key_k);
    if (compress_status) {
      return STS_ERR;
    }
#else
    gt_copy((*key)->k, key_k);
#endif

  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    gt_zero(key_k);
    gt_free(key_k);
    g1_set_infty(secret_key_sk);
    g1_free(secret_key_sk);
    g1_free(ciphertext_c_1);
    g2_free(ciphertext_c_2);
    g2_free(public_key_h);
    for (size_t i = 0; i < public_key->max_number_recipients; i++) {
      g2_free(public_key_h_powers[i]);
    }
    bn_free(group_order);
    g2_free(h_to_the_gamma_polynomial);
    gt_free(mapping_c_1_h_polynom);
    gt_zero(mapping_sk_c_2);
    gt_free(mapping_sk_c_2);
    gt_zero(product_mappings);
    gt_free(product_mappings);
    bn_free(non_decapsulation_identities_product);
    bn_free(hashed_id);
    g1_free(pairing_thread_data.g1_element);
    g2_free(pairing_thread_data.g2_element);
    gt_free(pairing_thread_data.result);
    bn_free(bn_inversion_thread_data.result);
    bn_free(bn_inversion_thread_data.number_to_invert);

    for (size_t i = 0; i < number_identities; i++) {
      bn_free(hashed_identities[i]);
      bn_free(coefficients[i]);
    }

    for (size_t i = 0; i < number_identities - 1; i++) {
      bn_free(negative_non_decapsulation_hashed_identities[i]);
      g2_free(g2_exponentiate_thread_data_with_precomputation_array[i].result);

      for(int l = 0; l < RELIC_EPX_TABLE_YAOWI; l++) {
        g2_free(g2_exponentiate_thread_data_with_precomputation_array[i].base_precomputation_table[l]);
      }

      bn_free(g2_exponentiate_thread_data_with_precomputation_array[i].exponent);
    }
  }

  return result_status;
}

int del07_decapsulate(del07_key_t **key,
                      const uint8_t **identities,
                      const unsigned number_identities,
                      const unsigned *identity_lengths,
                      const unsigned decapsulation_identity_index,
                      const del07_secret_key_t *secret_key,
                      const del07_ciphertext_t *ciphertext,
                      const del07_public_key_t *public_key) {
  int secret_key_is_valid;
  int ciphertext_is_valid;
  int is_valid_status =
      del07_secret_key_is_valid(&secret_key_is_valid, secret_key);
  is_valid_status |=
      del07_ciphertext_is_valid(&ciphertext_is_valid, ciphertext);
  if (number_identities > public_key->max_number_recipients - 1
      || decapsulation_identity_index >= number_identities
      || !secret_key_is_valid || !ciphertext_is_valid
      || is_valid_status) {
    return STS_ERR;
  }

  int result_status = STS_OK;

  g1_t signature;
  g2_t verification_key;
  int ciphertext_bin_size = G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED;
  uint8_t *ciphertext_bin = malloc((size_t) ciphertext_bin_size);
  uint8_t **encoded_identities = malloc((number_identities + 1) * sizeof(uint8_t *));
  unsigned *encoded_identity_lengths = malloc((number_identities + 1) * sizeof(unsigned));

  pthread_t decapsulate_thread;
  del07_cpa_decapsulate_thread_data decapsulate_thread_data;

  g1_null(signature);
  g2_null(verification_key);

  TRY {
    g1_new(signature);
    g2_new(verification_key);

#ifdef WITH_POINT_COMPRESSION
    int compress_status =
        del07_g1_decompress(signature, (uint8_t **) &ciphertext->signature);
    compress_status |= del07_g2_decompress(verification_key,
        (uint8_t **) &ciphertext->verification_key);
    if (compress_status) {
      return STS_ERR;
    }
    memcpy(ciphertext_bin, ciphertext->c_1, G1_SIZE_COMPRESSED);
    memcpy(ciphertext_bin + G1_SIZE_COMPRESSED, ciphertext->c_2, G2_SIZE_COMPRESSED);
#else
    g1_copy(signature, ciphertext->signature);
    g2_copy(verification_key, ((del07_ciphertext_t *) ciphertext)->verification_key);
    g1_write_bin(ciphertext_bin, ciphertext_bin_size, ciphertext->c_1, 1);
    g2_write_bin(ciphertext_bin + G1_SIZE_COMPRESSED, ciphertext_bin_size - G1_SIZE_COMPRESSED, ((del07_ciphertext_t *) ciphertext)->c_2, 1);
#endif

    // Encode the identities and verification key.
    del07_encode_identity_set(encoded_identities,
                              encoded_identity_lengths,
                              identities,
                              number_identities,
                              identity_lengths,
                              verification_key);

    decapsulate_thread_data.key = key;
    decapsulate_thread_data.identities = encoded_identities;
    decapsulate_thread_data.number_identities = number_identities + 1;
    decapsulate_thread_data.identity_lengths = encoded_identity_lengths;
    decapsulate_thread_data.decapsulation_identity_index = decapsulation_identity_index;
    decapsulate_thread_data.secret_key = (del07_secret_key_t *) secret_key;
    decapsulate_thread_data.ciphertext = (del07_ciphertext_t *) ciphertext;
    decapsulate_thread_data.public_key = (del07_public_key_t *) public_key;

    // Run decapsulate in parallel to verification in seperate thread.
    pthread_create(&decapsulate_thread, &del07_attr_joinable, del07_cpa_decapsulate_thread, (void *) &decapsulate_thread_data);

    // Verify the signature.
    int signature_valid = cp_bls_ver(signature, ciphertext_bin, ciphertext_bin_size, verification_key);

    // Wait for decapsulate thread to finish.
    void *decapsulate_status;
    pthread_join(decapsulate_thread, &decapsulate_status);

    // If error occured in decapsulate or signature not valid return failure.
    if ((int) decapsulate_status || !signature_valid) {
      return STS_ERR;
    }
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_free(signature);
    g2_free(verification_key);
    free(ciphertext_bin);
    for (size_t i = 0; i < number_identities + 1; i++) {
      free(encoded_identities[i]);
    }
    free(encoded_identities);
    free(encoded_identity_lengths);
  }

  return result_status;
}

int del07_hash_id(bn_t hashed_id,
                  const uint8_t *id,
                  const unsigned identity_length) {
  int result_status = STS_OK;

  bn_t group_order;
  uint8_t hash[SECURITY_PARAMETER];

  bn_null(group_order);

  TRY {
    bn_new(group_order);
    bn_new(hashed_id);

    g1_get_ord(group_order);

    md_map_sh256(hash, id, identity_length);
    bn_read_bin(hashed_id, hash, SECURITY_PARAMETER);
    bn_mod(hashed_id, hashed_id, group_order);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
    bn_free(group_order);
  }

  return result_status;
}

int del07_polynomial_coefficients_from_roots(bn_t *coefficients,
                                             const bn_t *roots,
                                             const unsigned number_roots,
                                             const bn_t modulus) {
  int result_status = STS_OK;

  unsigned number_coefficients = number_roots + 1;

  bn_t p_1[number_coefficients];
  bn_t p_2[number_coefficients];

  for (size_t i = 0; i < number_coefficients; i++) {
    bn_null(coefficients[i]);
    bn_null(p_1[i]);
    bn_null(p_2[i]);
  }

  TRY {
    for (size_t i = 0; i < number_coefficients; i++) {
      bn_new(coefficients[i]);
      bn_new(p_1[i]);
      bn_new(p_2[i]);
    }
    // Initialize resulting polyomial with 1
    bn_set_dig(coefficients[0], 1);
    for (size_t i = 1; i < number_coefficients; i++) {
      bn_zero(coefficients[i]);
    }

    // Iteratively compute the coefficients of the product p = prod(x - r_k).
    // We do this by iteratively computing the coefficients of the product of
    // the first i roots. We start with p_{0} = 1. Then, p_{1} is just p_{0}
    // multiplied by the first root. That is, p_{1} = p_{0} * (x - r_1) =
    // 1 (x - r_i) = x - r_i.
    // Let p_{i-1} be current polynomial, r_i the root to add.
    // Then p_i = p_{i-1} * (x - r_i) = p_{i-1} * x - p_{i-1} * r_i.
    for (size_t i = 0; i < number_roots; i++) {

      // p_1 = p * x
      bn_zero(p_1[0]);
      for (size_t k = 0; k <= i; k++) {
        bn_copy(p_1[k + 1], coefficients[k]);
      }

      // p_2 = p * h_i
      for (size_t k = 0; k <= i; k++) {
        bn_mul(p_2[k], coefficients[k], roots[i]);
        bn_mod(coefficients[k], coefficients[k], modulus);
      }

      // p = p_1 - p_2
      for (size_t k = 0; k <= i + 1; k++) {
        if (bn_is_zero(p_2[k])) {
          bn_copy(coefficients[k], p_1[k]);
        } else {
          bn_sub(coefficients[k], p_1[k], p_2[k]);
        }
        bn_mod(coefficients[k], coefficients[k], modulus);
      }
    }
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
    for (size_t i = 0; i < number_coefficients; i++) {
      bn_free(p_1[i]);
      bn_free(p_2[i]);
    }
  }

  return result_status;
}

void del07_encode(uint8_t **encoded_identity,
                  const uint8_t *identity,
                  const unsigned identity_length) {
  *encoded_identity = malloc((identity_length + 1) * sizeof(uint8_t));
  (*encoded_identity)[0] = IDENTITY_PREFIX;
  memcpy(*encoded_identity + 1, identity, identity_length);
}

void del07_encode_identity_set(uint8_t **encoded_identities,
                               unsigned int *encoded_identity_lengths,
                               const uint8_t **identities,
                               const unsigned int number_identities,
                               const unsigned int *identity_lengths,
                               g2_t verification_key) {
  // Encode the set of encapsulating identities.
  for (size_t i = 0; i < number_identities; i++) {
    del07_encode(&(encoded_identities[i]), identities[i], identity_lengths[i]);
    encoded_identity_lengths[i] = identity_lengths[i] + 1;
  }
  // Add the encoded verification key.
  unsigned verification_key_size = (unsigned) g2_size_bin(verification_key, 1);
  encoded_identities[number_identities] = malloc(verification_key_size + 1);
  encoded_identities[number_identities][0] = VERIFICATION_KEY_PREFIX;
  g2_write_bin(&(encoded_identities[number_identities][1]),
               verification_key_size,
               verification_key,
               1);
  encoded_identity_lengths[number_identities] = verification_key_size + 1;
}

int del07_clean() {
  core_clean();
  pthread_attr_destroy(&del07_attr_joinable);

  return STS_OK;
}
