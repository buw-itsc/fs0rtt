#include <relic/relic.h>
#include <string.h>
#include "Delerablee_07_IBBE.h"
#include "Delerablee_07_IBBE_global.h"
#include "Delerablee_07_IBBE_util.h"

void del07_print_master_secret_key(
    const del07_master_secret_key_t *master_secret_key) {
  printf("g:\n");
  g1_print(master_secret_key->g);

  printf("\ngamma:\n");
  bn_print(master_secret_key->gamma);
}

void del07_print_secret_key(const del07_secret_key_t *secret_key) {

  g1_t secret_key_sk;
#ifdef WITH_POINT_COMPRESSION
  int compress_status =
      del07_g1_decompress(secret_key_sk, (uint8_t **) &secret_key->sk);
  if (compress_status) {
    printf("\nError decompressing the secret key\n");
  }
#else
  g1_copy(secret_key_sk, secret_key->sk);
#endif

  g1_print(secret_key_sk);

  g1_free(secret_key_sk);
}

void del07_print_public_key(const del07_public_key_t *public_key) {
  g1_t public_key_w;
  gt_t public_key_v;
  g2_t public_key_h;
  g2_t public_key_h_powers[public_key->max_number_recipients];

  g1_copy(public_key_w, public_key->w);
  gt_copy(public_key_v, ((del07_public_key_t *) public_key)->v);
  g2_copy(public_key_h, ((del07_public_key_t *) public_key)->h);
  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    g2_copy(public_key_h_powers[i], ((del07_public_key_t *) public_key)->h_to_the_gamma_powers[i]);
  }

  printf("w:\n");
  g1_print(public_key_w);

  printf("\nv:\n");
  gt_print(public_key_v);

  printf("\nh:\n");
  g2_print(public_key_h);

  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    printf("\nh^(gamma^%zu):\n", i + 1);
    g2_print(public_key_h_powers[i]);
  }

  g1_free(public_key_w);
  gt_free(public_key_v);
  g2_free(public_key_h);
  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    g2_free(public_key_h_powers[i]);
  }
}

void del07_print_ciphertext(const del07_ciphertext_t *ciphertext) {
  g1_t ciphertext_c_1;
  g2_t ciphertext_c_2;
  g2_t verification_key;
  g1_t signature;

#ifdef WITH_POINT_COMPRESSION
  int compress_status =
      del07_g1_decompress(ciphertext_c_1, (uint8_t **) &ciphertext->c_1);
  compress_status |=
      del07_g2_decompress(ciphertext_c_2, (uint8_t **) &ciphertext->c_2);
  compress_status |=
      del07_g2_decompress(verification_key, (uint8_t **) &ciphertext->verification_key);
  compress_status |=
      del07_g1_decompress(signature, (uint8_t **) &ciphertext->signature);
  if (compress_status) {
    printf("\nError decompressing the ciphertext\n");
  }
#else
  g1_copy(ciphertext_c_1, ciphertext->c_1);
  g2_copy(ciphertext_c_2, ((del07_ciphertext_t *) ciphertext)->c_2);
  g2_copy(verification_key, ((del07_ciphertext_t *) ciphertext)->verification_key);
  g1_copy(signature, ciphertext->signature);
#endif

  printf("C_1:\n");
  g1_print(ciphertext_c_1);

  printf("\nC_2:\n");
  g2_print(ciphertext_c_2);

  printf("\nVerification key:\n");
  g2_print(verification_key);

  printf("\nSignature:\n");
  g1_print(signature);

  g1_free(ciphertext_c_1);
  g2_free(ciphertext_c_2);
  g2_free(verification_key);
  g1_free(signature);
}

void del07_print_key(const del07_key_t *key) {
  gt_t key_k;
#ifdef WITH_POINT_COMPRESSION
  int compress_status = del07_gt_decompress(key_k, (uint8_t **) &key->k);
  if (compress_status) {
    printf("\nError decompressing the key\n");
  }
#else
  gt_copy(key_k, ((del07_key_t *) key)->k);
#endif

  printf("\nThe key is:\n");
  gt_print(key_k);

  gt_free(key_k);
}

void del07_set_master_secret_key_to_zero(del07_master_secret_key_t *master_secret_key) {
  g1_set_infty(master_secret_key->g);
  g1_free(master_secret_key->g);

  bn_zero(master_secret_key->gamma);
  bn_free(master_secret_key->gamma);
}

void del07_set_secret_key_to_zero(del07_secret_key_t *secret_key) {
#ifdef WITH_POINT_COMPRESSION
  memzero(secret_key->sk, (size_t) G1_SIZE_COMPRESSED);
  free(secret_key->sk);
#else
  g1_set_infty(secret_key->sk);
  g1_free(secret_key->sk);
#endif
}

void del07_set_public_key_to_zero(del07_public_key_t *public_key) {
  g1_set_infty(public_key->w);
  g1_free(public_key->w);
  gt_zero(public_key->v);
  gt_free(public_key->v);
  g2_set_infty(public_key->h);
  g2_free(public_key->h);
  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    g2_set_infty(public_key->h_to_the_gamma_powers[i]);
    g2_free(public_key->h_to_the_gamma_powers[i]);
  }

  // TODO set also precomputation tables to zero? must work on client side also
}

void del07_set_ciphertext_to_zero(del07_ciphertext_t *ciphertext) {
#ifdef WITH_POINT_COMPRESSION
  memzero(ciphertext->c_1, (size_t) G1_SIZE_COMPRESSED);
  free(ciphertext->c_1);
  memzero(ciphertext->c_2, (size_t) G2_SIZE_COMPRESSED);
  free(ciphertext->c_2);
  memzero(ciphertext->verification_key, (size_t) G2_SIZE_COMPRESSED);
  free(ciphertext->verification_key);
  memzero(ciphertext->signature, (size_t) G1_SIZE_COMPRESSED);
  free(ciphertext->signature);
#else
  g1_set_infty(ciphertext->c_1);
  g1_free(ciphertext->c_1);
  g2_set_infty(ciphertext->c_2);
  g2_free(ciphertext->c_2);
  g2_set_infty(ciphertext->verification_key);
  g2_free(ciphertext->verification_key);
  g1_set_infty(ciphertext->signature);
  g1_free(ciphertext->signature);
#endif
}

void del07_set_key_to_zero(del07_key_t *key) {
#ifdef WITH_POINT_COMPRESSION
  memzero(key->k, (size_t) GT_SIZE_COMPRESSED);
  free(key->k);
#else
  gt_zero(key->k);
  gt_free(key->k);
#endif
}

int del07_master_secret_key_is_valid(int *valid,
                                     const del07_master_secret_key_t *master_secret_key) {
  // TODO: check whole pre-computation table
  *valid = g1_is_valid(((del07_master_secret_key_t *)master_secret_key)->g);

  return STS_OK;
}

int del07_secret_key_is_valid(int *valid,
                              const del07_secret_key_t *secret_key) {
  g1_t secret_key_sk;
#ifdef WITH_POINT_COMPRESSION
  int compress_status =
      del07_g1_decompress(secret_key_sk, (uint8_t **) &secret_key->sk);
  if (compress_status) {
    *valid = 0;
    return STS_ERR;
  }
#else
  g1_copy(secret_key_sk, secret_key->sk);
#endif

  *valid = g1_is_valid(secret_key_sk);

  g1_set_infty(secret_key_sk);
  g1_free(secret_key_sk);
  return STS_OK;
}

int del07_convert_key_to_bit_string(uint8_t **bit_string,
                                    const del07_key_t *key) {
  int result_status = STS_OK;

  *bit_string = malloc(SECURITY_PARAMETER);

  TRY {
    // Hash binary represented bit string
#ifdef WITH_POINT_COMPRESSION
    md_kdf2(*bit_string, SECURITY_PARAMETER, key->k, GT_SIZE_COMPRESSED);
#else
    uint8_t* key_k;
    del07_gt_compress(&key_k, ((del07_key_t *) key)->k);
    md_kdf2(*bit_string, SECURITY_PARAMETER, key_k, GT_SIZE_COMPRESSED);
    free(key_k);
#endif
    } CATCH_ANY {
      return STS_ERR;
    } FINALLY {
    }

  return result_status;
}

int del07_public_key_is_valid(int *valid,
                              const del07_public_key_t *public_key) {
  g1_t public_key_w;
  g2_t public_key_h;
  g2_t public_key_h_powers[public_key->max_number_recipients];

  g1_copy(public_key_w, public_key->w);
  g2_copy(public_key_h, ((del07_public_key_t *) public_key)->h);
  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    g2_copy(public_key_h_powers[i], ((del07_public_key_t *) public_key)->h_to_the_gamma_powers[i]);
  }

  *valid = g1_is_valid(public_key_w);
  *valid &= g2_is_valid(public_key_h);
  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    *valid &= g2_is_valid(public_key_h_powers[i]);
  }

  g1_free(public_key_w);
  g2_free(public_key_h);
  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    g2_free(public_key_h_powers[i]);
  }

  return STS_OK;
}

int del07_ciphertext_is_valid(int *valid,
                              const del07_ciphertext_t *ciphertext) {
  g1_t ciphertext_c_1;
  g2_t ciphertext_c_2;
  g2_t verification_key;
  g1_t signature;

#ifdef WITH_POINT_COMPRESSION
  int compress_status =
      del07_g1_decompress(ciphertext_c_1, (uint8_t **) &ciphertext->c_1);
  compress_status |=
      del07_g2_decompress(ciphertext_c_2, (uint8_t **) &ciphertext->c_2);
  compress_status |=
      del07_g2_decompress(verification_key, (uint8_t **) &ciphertext->verification_key);
  compress_status |=
      del07_g1_decompress(signature, (uint8_t **) &ciphertext->signature);
  if (compress_status) {
    *valid = 0;
    return STS_ERR;
  }
#else
  g1_copy(ciphertext_c_1, ciphertext->c_1);
  g2_copy(ciphertext_c_2, ((del07_ciphertext_t *) ciphertext)->c_2);
  g2_copy(verification_key, ((del07_ciphertext_t *) ciphertext)->verification_key);
  g1_copy(signature, ciphertext->signature);
#endif

  *valid = g1_is_valid(ciphertext_c_1) && g2_is_valid(ciphertext_c_2);

  g1_free(ciphertext_c_1);
  g2_free(ciphertext_c_2);

  return STS_OK;
}

int del07_g1_compress(uint8_t **bin, g1_t element) {
  int result_status = STS_OK;
  TRY {
    *bin = malloc((size_t) G1_SIZE_COMPRESSED);
    g1_write_bin(*bin, G1_SIZE_COMPRESSED, element, 1);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

int del07_g2_compress(uint8_t **bin, g2_t element) {
  int result_status = STS_OK;
  TRY {
    *bin = malloc((size_t) G2_SIZE_COMPRESSED);
    g2_write_bin(*bin, G2_SIZE_COMPRESSED, element, 1);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

int del07_gt_compress(uint8_t **bin, gt_t element) {
  int result_status = STS_OK;
  TRY {
    *bin = malloc((size_t) GT_SIZE_COMPRESSED);
    gt_write_bin(*bin, GT_SIZE_COMPRESSED, element, 1);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

int del07_g1_decompress(g1_t element, uint8_t **bin) {
  int result_status = STS_OK;
  g1_null(element);
  TRY {
    g1_new(element);
    g1_read_bin(element, *bin, G1_SIZE_COMPRESSED);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

int del07_g2_decompress(g2_t element, uint8_t **bin) {
  int result_status = STS_OK;
  g2_null(element);
  TRY {
    g2_new(element);
    g2_read_bin(element, *bin, G2_SIZE_COMPRESSED);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

int del07_gt_decompress(gt_t element, uint8_t **bin) {
  int result_status = STS_OK;
  gt_null(element);
  TRY { 
    gt_new(element);
    gt_read_bin(element, *bin, GT_SIZE_COMPRESSED);
  } CATCH_ANY {
    return STS_ERR;
  } FINALLY {
  }

  return result_status;
}

void del07_serialize_master_secret_key(uint8_t **serialized,
                                       size_t *size,
                                       const del07_master_secret_key_t *master_secret_key) {
  int size_gamma = bn_size_bin(master_secret_key->gamma);
  *size = (size_t) (G1_SIZE_COMPRESSED + size_gamma);
  *serialized = malloc(*size);
  g1_write_bin(*serialized, G1_SIZE_COMPRESSED, master_secret_key->g, 1);
  bn_write_bin(*serialized + G1_SIZE_COMPRESSED,
               size_gamma,
               master_secret_key->gamma);
}

int del07_serialize_secret_key(uint8_t **serialized,
                                const del07_secret_key_t *secret_key,
                                int compression) {
  int result_status = STS_OK;
  if (compression) {
    *serialized = malloc((size_t) G1_SIZE_COMPRESSED);
  } else {
    *serialized = malloc((size_t) G1_SIZE_UNCOMPRESSED);
  }
#ifdef WITH_POINT_COMPRESSION
  g1_t tmp;
  TRY {
    if (compression) {
      memcpy(*serialized, secret_key->sk, G1_SIZE_COMPRESSED);
    } else {
      g1_null(tmp);
      g1_new(tmp);
      g1_read_bin(tmp, secret_key->sk, G1_SIZE_COMPRESSED);
      g1_write_bin(*serialized, G1_SIZE_UNCOMPRESSED, tmp, 0);
    }
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
    g1_set_infty(tmp);
    g1_free(tmp);
  };
#else
  if (compression) {
    g1_write_bin(*serialized, G1_SIZE_COMPRESSED, secret_key->sk, compression);
  } else {
    g1_write_bin(*serialized, G1_SIZE_UNCOMPRESSED, secret_key->sk, compression);
  }
#endif

  return result_status;
}

void del07_serialize_public_key(uint8_t **serialized,
                                const del07_public_key_t *public_key) {
  size_t size = (size_t) del07_get_public_key_size(public_key);
  *serialized = malloc(size);

  g1_write_bin(*serialized, G1_SIZE_COMPRESSED, public_key->w, 1);
  gt_write_bin(*serialized + G1_SIZE_COMPRESSED, GT_SIZE_COMPRESSED, ((del07_public_key_t *) public_key)->v, 1);
  g2_write_bin(*serialized + G1_SIZE_COMPRESSED + GT_SIZE_COMPRESSED,
         G2_SIZE_COMPRESSED,
               ((del07_public_key_t *) public_key)->h,
         1);

  memcpy(*serialized + G1_SIZE_COMPRESSED + GT_SIZE_COMPRESSED
             + G2_SIZE_COMPRESSED,
         &public_key->max_number_recipients,
         sizeof(unsigned));

  for (size_t i = 0; i < public_key->max_number_recipients; i++) {
    g2_write_bin(*serialized + G1_SIZE_COMPRESSED + GT_SIZE_COMPRESSED
               + (i + 1) * G2_SIZE_COMPRESSED + sizeof(unsigned),
           G2_SIZE_COMPRESSED,
                 ((del07_public_key_t *) public_key)->h_to_the_gamma_powers[i],
           1);
  }
}

void del07_serialize_ciphertext(uint8_t **serialized,
                                const del07_ciphertext_t *ciphertext) {
  *serialized = malloc((size_t) 2 * G1_SIZE_COMPRESSED +  2 * G2_SIZE_COMPRESSED);

#ifdef WITH_POINT_COMPRESSION
  memcpy(*serialized, ciphertext->c_1, G1_SIZE_COMPRESSED);
  memcpy(*serialized + G1_SIZE_COMPRESSED, ciphertext->c_2, G2_SIZE_COMPRESSED);
  memcpy(*serialized + G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED, ciphertext->verification_key, G2_SIZE_COMPRESSED);
  memcpy(*serialized + G1_SIZE_COMPRESSED + 2 * G2_SIZE_COMPRESSED, ciphertext->signature, G1_SIZE_COMPRESSED);
#else
  g1_write_bin(*serialized, G1_SIZE_COMPRESSED, ciphertext->c_1, 1);
  g2_write_bin(*serialized + G1_SIZE_COMPRESSED,
               G2_SIZE_COMPRESSED,
               ((del07_ciphertext_t *) ciphertext)->c_2,
               1);
  g2_write_bin(*serialized + G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED,
               G2_SIZE_COMPRESSED,
               ((del07_ciphertext_t *) ciphertext)->verification_key,
               1);
  g1_write_bin(*serialized + G1_SIZE_COMPRESSED + 2 *G2_SIZE_COMPRESSED, G1_SIZE_COMPRESSED, ciphertext->signature, 1);
#endif
}

void del07_serialize_key(uint8_t **serialized, const del07_key_t *key) {
  *serialized = malloc((size_t) GT_SIZE_COMPRESSED);
#ifdef WITH_POINT_COMPRESSION
  memcpy(*serialized, key->k, GT_SIZE_COMPRESSED);
#else
  gt_write_bin(*serialized, GT_SIZE_COMPRESSED, ((del07_key_t *) key)->k, 1);
#endif
}

void del07_deserialize_master_secret_key(del07_master_secret_key_t **master_secret_key,
                                         const uint8_t *serialized,
                                         const size_t size) {
  *master_secret_key = malloc(sizeof(del07_master_secret_key_t));

  g1_null((*master_secret_key)->g);
  g1_new((*master_secret_key)->g);
  g1_read_bin((*master_secret_key)->g, serialized, G1_SIZE_COMPRESSED);

  // Compute pre-computation table for g
  ep_mul_pre_nafwi((*master_secret_key)->g_precomputation_table,
                   (*master_secret_key)->g);

  bn_null((*master_secret_key)->gamma);
  bn_new((*master_secret_key)->gamma);
  bn_read_bin((*master_secret_key)->gamma,
              serialized + G1_SIZE_COMPRESSED,
              (int) (size - G1_SIZE_COMPRESSED));
}

int del07_deserialize_secret_key(del07_secret_key_t **secret_key,
                                 const uint8_t *serialized,
                                 int compression) {
  int result_status = STS_OK;
  *secret_key = malloc(sizeof(del07_secret_key_t));

  TRY {
#ifdef WITH_POINT_COMPRESSION
    g1_t secret_key_sk;
    g1_null(secret_key_sk);
    g1_new(secret_key_sk);
    (*secret_key)->sk = malloc((size_t) G1_SIZE_COMPRESSED);
    if (compression) {
      memcpy((*secret_key)->sk, serialized, G1_SIZE_COMPRESSED);
    } else {
      g1_read_bin(secret_key_sk, (uint8_t *) serialized, G1_SIZE_UNCOMPRESSED);
      g1_write_bin((*secret_key)->sk, G1_SIZE_COMPRESSED, secret_key_sk, 1);
    }
#else
    g1_null((*secret_key)->sk);
    g1_new((*secret_key)->sk);
    if (compression) {
      g1_read_bin((*secret_key)->sk, (uint8_t *) serialized, G1_SIZE_COMPRESSED);
    } else {
      g1_read_bin((*secret_key)->sk,
                  (uint8_t *) serialized,
                  G1_SIZE_UNCOMPRESSED);
    }
#endif
  } CATCH_ANY {
    result_status = STS_ERR;
  } FINALLY {
#ifdef WITH_POINT_COMPRESSION
    g1_free(secret_key_sk);
#endif
  }

  return result_status;
}

int del07_deserialize_public_key(del07_public_key_t **public_key,
                                 const uint8_t *serialized,
                                 int compute_precomputation_tables) {
  int result_status = STS_OK;

  unsigned max_number_recipients;

  // already includes slot for CHK identity
  memcpy(&max_number_recipients,
         serialized + G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED
             + GT_SIZE_COMPRESSED,
         sizeof(unsigned));

  g1_t public_key_w;
  gt_t public_key_v;
  g2_t public_key_h;
  g2_t public_key_h_to_the_gamma_powers [max_number_recipients];

  g1_null(public_key_w);
  g1_new(public_key_w);
  gt_null(public_key_v);
  gt_new(public_key_v);
  g2_null(public_key_h);
  g2_new(public_key_h);
  for (size_t i = 0; i < max_number_recipients; i++) {
    g2_null(public_key_h_to_the_gamma_powers[i]);
    g2_new(public_key_h_to_the_gamma_powers[i]);
  }

  TRY {
    g1_read_bin(public_key_w, serialized, G1_SIZE_COMPRESSED);
    gt_read_bin(public_key_v, (uint8_t *) serialized + G1_SIZE_COMPRESSED, GT_SIZE_COMPRESSED);
    g2_read_bin(public_key_h,
                (uint8_t *) serialized + G1_SIZE_COMPRESSED + GT_SIZE_COMPRESSED,
                G2_SIZE_COMPRESSED);

    for (size_t i = 0; i < max_number_recipients; i++) {
      g2_read_bin(public_key_h_to_the_gamma_powers[i],
                  (uint8_t *) serialized + G1_SIZE_COMPRESSED + (1 + i) *
                      G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED
                      + sizeof(unsigned),
                  G2_SIZE_COMPRESSED);
    }

    *public_key = malloc(offsetof(del07_public_key_t, h_to_the_gamma_powers)
                           + (max_number_recipients + 1) * sizeof(g2_t));

    // TODO think about whether we want to compute pre-computation tables here
    // probably not

    g1_null((*public_key)->w);
    g1_new((*public_key)->w);
    gt_null((*public_key)->v);
    gt_new((*public_key)->v);
    g2_null((*public_key)->h);
    g2_new((*public_key)->h);
    for (size_t i = 0; i < max_number_recipients; i++) {
      g2_null(((*public_key)->h_to_the_gamma_powers)[i]);
      g2_new(((*public_key)->h_to_the_gamma_powers)[i]);
    }

    g1_copy((*public_key)->w, public_key_w);
    gt_copy((*public_key)->v, public_key_v);
    g2_copy((*public_key)->h, public_key_h);
    for (size_t i = 0; i < max_number_recipients; i++) {
      g2_copy(((*public_key)->h_to_the_gamma_powers)[i], public_key_h_to_the_gamma_powers[i]);
    }

    (*public_key)->max_number_recipients = max_number_recipients;

    if (compute_precomputation_tables) {
      (*public_key)->h_precomputation_table =
          malloc(RELIC_EPX_TABLE_YAOWI * sizeof(g2_t));
      (*public_key)->h_to_the_gamma_powers_precomputation_table =
          malloc((max_number_recipients - 1) * RELIC_EPX_TABLE_YAOWI * sizeof(g2_t));

      ep2_mul_pre_yaowi((*public_key)->h_precomputation_table, (*public_key)->h);
      for(unsigned i = 0; i < max_number_recipients - 1; i++) {
        ep2_mul_pre_yaowi(
            (*public_key)->h_to_the_gamma_powers_precomputation_table
                + i * RELIC_EPX_TABLE_YAOWI, (*public_key)->h_to_the_gamma_powers[i]);
      }
    }

  } CATCH_ANY {
    // read_bin fails
    result_status = STS_ERR;
  } FINALLY {
    g1_free(public_key_w);
    gt_free(public_key_v);
    g2_free(public_key_h);
    for (size_t i = 0; i < max_number_recipients; i++) {
      g2_free(public_key_h_to_the_gamma_powers[i]);
    }
  }

  return result_status;
}

int del07_deserialize_ciphertext(del07_ciphertext_t **ciphertext,
                                  const uint8_t *serialized) {
  int result_status = STS_OK;

  g1_t ciphertext_c_1;
  g2_t ciphertext_c_2;
  g2_t ciphertext_verification_key;
  g1_t ciphertext_signature;

  *ciphertext = malloc(sizeof(del07_ciphertext_t));

  TRY {
    g1_null(ciphertext_c_1);
    g2_null(ciphertext_c_2);
    g2_null(ciphertext_verification_key);
    g1_null(ciphertext_signature);
    g1_new(ciphertext_c_1);
    g2_new(ciphertext_c_2);
    g2_new(ciphertext_verification_key);
    g2_new(ciphertext_signature);

    g1_read_bin(ciphertext_c_1, serialized, G1_SIZE_COMPRESSED);
    g2_read_bin(ciphertext_c_2,
                (uint8_t *) serialized + G1_SIZE_COMPRESSED,
                G2_SIZE_COMPRESSED);
    g2_read_bin(ciphertext_verification_key,
                (uint8_t *) serialized + G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED,
                G2_SIZE_COMPRESSED);
    g1_read_bin(ciphertext_signature,
                (uint8_t *) serialized + G1_SIZE_COMPRESSED + 2 * G2_SIZE_COMPRESSED,
                G1_SIZE_COMPRESSED);

#ifdef WITH_POINT_COMPRESSION
    int compress_status =
        del07_g1_compress(&(*ciphertext)->c_1, ciphertext_c_1);
    compress_status |=
        del07_g2_compress(&(*ciphertext)->c_2, ciphertext_c_2);
    compress_status |=
        del07_g2_compress(&(*ciphertext)->verification_key, ciphertext_verification_key);
    compress_status |=
        del07_g1_compress(&(*ciphertext)->signature, ciphertext_signature);
    if (compress_status) {
      return STS_ERR;
    }
#else
    g1_null((*ciphertext)->c_1);
    g2_null((*ciphertext)->c_2);
    g2_null((*ciphertext)->verification_key);
    g1_null((*ciphertext)->signature);
    g1_new((*ciphertext)->c_1);
    g2_new((*ciphertext)->c_2);
    g2_new((*ciphertext)->verification_key);
    g2_new((*ciphertext)->signature);
    g1_copy((*ciphertext)->c_1, ciphertext_c_1);
    g2_copy((*ciphertext)->c_2, ciphertext_c_2);
    g2_copy((*ciphertext)->verification_key, ciphertext_verification_key);
    g1_copy((*ciphertext)->signature, ciphertext_signature);
#endif

  } CATCH_ANY {
      // read_bin fails
      result_status = STS_ERR;
    } FINALLY {
      g1_free(ciphertext_c_1);
      g2_free(ciphertext_c_2);
      g2_free(ciphertext_verification_key);
      g1_free(ciphertext_signature);
    }

  return result_status;
}

void del07_deserialize_key(del07_key_t **key, const uint8_t *serialized) {
  *key = malloc(sizeof(del07_key_t));

#ifdef WITH_POINT_COMPRESSION
  (*key)->k = malloc((size_t) GT_SIZE_COMPRESSED);
  memcpy((*key)->k, serialized, GT_SIZE_COMPRESSED);
#else
  gt_null((*key)->k);
  gt_new((*key)->k);
  gt_read_bin((*key)->k, (uint8_t *) serialized, GT_SIZE_COMPRESSED);
#endif
}

int del07_master_secret_keys_are_equal(const del07_master_secret_key_t *l,
                                       const del07_master_secret_key_t *r) {
  int equal = !bn_cmp(l->gamma, r->gamma);
  equal &= !g1_cmp(l->g, r->g);

  return equal;
}

int del07_secret_keys_are_equal(const del07_secret_key_t *l,
                                const del07_secret_key_t *r) {
#ifdef WITH_POINT_COMPRESSION
  int equal = !memcmp(l->sk, r->sk, (size_t) G1_SIZE_COMPRESSED);
#else
  int equal = !g1_cmp(l->sk, r->sk);
#endif
  return equal;
}

int del07_public_keys_are_equal(const del07_public_key_t *l,
                                const del07_public_key_t *r) {
  int equal = !g1_cmp(l->w, r->w);
  equal &= !gt_cmp(((del07_public_key_t *) l)->v, ((del07_public_key_t *) r)->v);
  equal &= !g2_cmp(((del07_public_key_t *) l)->h, ((del07_public_key_t *) r)->h);
  equal &= l->max_number_recipients == r->max_number_recipients;
  for (size_t j = 0; j < l->max_number_recipients; j++) {
    equal &= !g2_cmp(((del07_public_key_t *) l)->h_to_the_gamma_powers[j], ((del07_public_key_t *) r)->h_to_the_gamma_powers[j]);
  }

  return equal;
}

int del07_ciphertexts_are_equal(const del07_ciphertext_t *l,
                                const del07_ciphertext_t *r) {
#ifdef WITH_POINT_COMPRESSION
  int equal = !memcmp(l->c_1, r->c_1, (size_t) G1_SIZE_COMPRESSED);
  equal &= !memcmp(l->c_2, r->c_2, (size_t) G2_SIZE_COMPRESSED);
  equal &= !memcmp(l->verification_key, r->verification_key, (size_t) G2_SIZE_COMPRESSED);
  equal &= !memcmp(l->signature, r->signature, (size_t) G1_SIZE_COMPRESSED);
#else
  int equal = !g1_cmp(l->c_1, r->c_1);
  equal &= !g2_cmp(((del07_ciphertext_t *) l)->c_2, ((del07_ciphertext_t *) r)->c_2);
  equal &= !g2_cmp(((del07_ciphertext_t *) l)->verification_key, ((del07_ciphertext_t *) r)->verification_key);
  equal &= !g1_cmp(l->signature, r->signature);
#endif
  return equal;
}

int del07_keys_are_equal(const del07_key_t *l, const del07_key_t *r) {
#ifdef WITH_POINT_COMPRESSION
  int equal = !memcmp(l->k, r->k, (size_t) GT_SIZE_COMPRESSED);
#else
  int equal = !gt_cmp(((del07_key_t *) l)->k, ((del07_key_t *) r)->k);
#endif
  return equal;
}

unsigned del07_get_public_key_size(const del07_public_key_t *public_key) {
  unsigned size = G1_SIZE_COMPRESSED + G2_SIZE_COMPRESSED + GT_SIZE_COMPRESSED
      + sizeof(unsigned);
  size += public_key->max_number_recipients * G2_SIZE_COMPRESSED;
  return size;
}

unsigned del07_get_master_secret_key_size(const del07_master_secret_key_t *master_secret_key) {
  unsigned size = G1_SIZE_COMPRESSED + (unsigned) bn_size_bin(master_secret_key->gamma);
  return size;
}

unsigned del07_get_secret_key_size(int compression) {
  return (unsigned) (compression ? G1_SIZE_COMPRESSED : G1_SIZE_UNCOMPRESSED);
}

void *del07_g2_exponentiate_thread_with_precomputation(void *arg) {

  del07_g2_exponentiate_thread_data_with_precomputation_t
      *data = (del07_g2_exponentiate_thread_data_with_precomputation_t *) arg;

  // Set the RELIC context.
  core_set(del07_context);

  // Compute the exponentiation.
  g2_null(data->result);
  g2_new(data->result);
  ep2_mul_fix_yaowi(data->result,
                    data->base_precomputation_table,
                    data->exponent);

  pthread_exit(NULL);
}

void *del07_g2_exponentiate_thread(void *arg) {

  del07_g2_exponentiate_thread_data_t *data = (del07_g2_exponentiate_thread_data_t *) arg;

  // Set the RELIC context.
  core_set(del07_context);

  // Compute the exponentiation.
  g2_null(data->result);
  g2_new(data->result);
  g2_exponentiate(data->result, data->base, data->exponent);

  pthread_exit(NULL);
}

void *del07_gt_exponentiate_thread(void *arg) {

  del07_gt_exponentiate_thread_data_t *data = (del07_gt_exponentiate_thread_data_t *) arg;

  // Set the RELIC context.
  core_set(del07_context);

  // Compute the exponentiation.
  gt_null(data->result);
  gt_new(data->result);
  gt_exp(data->result, data->base, data->exponent);

  pthread_exit(NULL);
}

void *del07_pairing_thread(void *arg) {
  del07_pairing_thread_data_t *data = (del07_pairing_thread_data_t *) arg;

  core_set(del07_context);

  // Compute the pairing
  gt_null(data->result);
  gt_new(data->result);
  pc_map(data->result, data->g1_element, data->g2_element);

  pthread_exit(NULL);
}

void *del07_bn_inversion_thread(void *arg) {
  del07_bn_inversion_thread_data_t *data = (del07_bn_inversion_thread_data_t *) arg;

  core_set(del07_context);

  bn_t group_order;
  bn_t r;
  bn_null(group_order);
  bn_null(r);
  bn_new(group_order);
  bn_new(r);

  g1_get_ord(group_order);

  bn_null(data->result);
  bn_new(data->result);
  bn_gcd_ext(r, data->result, NULL, data->number_to_invert, group_order);
  if (bn_sign(data->result) == BN_NEG) {
    bn_add(data->result, data->result, group_order);
  }

  // Clean up everything used by this thread.
  bn_free(group_order);
  bn_free(r);

  pthread_exit(NULL);
}

void *del07_cpa_decapsulate_thread(void *arg) {
  del07_cpa_decapsulate_thread_data
      *data = (del07_cpa_decapsulate_thread_data *) arg;

  core_set(del07_context);

  int decapsulate_status = del07_cpa_decapsulate(data->key,
                                                 (const uint8_t **) data->identities,
                                                 data->number_identities,
                                                 data->identity_lengths,
                                                 data->decapsulation_identity_index,
                                                 data->secret_key,
                                                 data->ciphertext,
                                                 data->public_key);
  if (decapsulate_status) {
    return (void *) STS_ERR;
  }

  return (void *) STS_OK;
}

void memzero(void *ptr, size_t len)
{
  typedef void *(*memset_t)(void *, int, size_t);
  static volatile memset_t memset_func = memset;
  memset_func(ptr, 0, len);
}