// Copyright [2018] PG 0-RTT

#include <gtest/gtest.h>
#include <cstdio>
#include <del07ibbe/Delerablee_07_IBBE_util.h>
#include "BFE_IBBE/BloomFilterEncryptionIBBE.h"
#include "BFE_IBBE/CiphertextIBBE.h"
#include "BFE_IBBE/KeyIBBE.h"
#include "BFE_IBBE/PublicKeyIBBE.h"

using bfe::BloomFilterEncryptionIBBE;
using bfe::Key;
using bfe::Ciphertext;
using bfe::CiphertextIBBE;
using bfe::PublicKeyIBBE;
using bfe::BloomFilterEncryption;
using bfe::PublicKey;

class BFEIBBEKeysGenerated : public testing::Test {
 protected:
  static const long m = 1000;
  static const int k = 10;

  void SetUp() override {
    bfeIBBE = new BloomFilterEncryptionIBBE(m, k);
    bfeIBBE->keyGeneration(false);
  }

  void TearDown() override {
    delete bfeIBBE;
  }

  BloomFilterEncryptionIBBE *bfeIBBE;
};

/**
 * If encapsulate outputs key-ciphertext pair (k, c) then decapsulate(c)
 * returns the same key k.
 */
TEST_F(BFEIBBEKeysGenerated, sameKeyReturnedByEncapsulateAndDecapsulate) {
  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      bfeIBBE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);

  std::unique_ptr<Key> decapsulatedKey = bfeIBBE->decapsulate(ciphertext.get());

  ASSERT_TRUE(*decapsulatedKey == *encapsulatedKey);
}

/**
 * A ciphertext can only be decapsulated once.
 */
TEST_F(BFEIBBEKeysGenerated, puncturingDeletesSecretKeys) {
  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      bfeIBBE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);

  std::unique_ptr<Key> decapsulatedKey = bfeIBBE->decapsulate(ciphertext.get());
  ASSERT_NE(decapsulatedKey, nullptr);

  std::unique_ptr<Key> decapsulatedKeyAfterPuncture =
      bfeIBBE->decapsulate(ciphertext.get());

  ASSERT_EQ(decapsulatedKeyAfterPuncture, nullptr);
}

/**
 * Consecutive calls to encapsulate return (with overwhelming probability)
 * different keys and ciphertexts.
 */
TEST_F(BFEIBBEKeysGenerated, encapsulateReturnsDifferentKeyOnEachCall) {
  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      bfeIBBE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey1 = move(tmp.first);
  std::unique_ptr<Ciphertext> ciphertext1 = move(tmp.second);

  tmp = bfeIBBE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey2 = move(tmp.first);
  std::unique_ptr<Ciphertext> ciphertext2 = move(tmp.second);

  ASSERT_TRUE(*encapsulatedKey1 != *encapsulatedKey2);
}

/**
 * Decapsulating a ciphertext that was encapsulated under a different public
 * key must not return the same key that was encapsulated.
 */
TEST_F(BFEIBBEKeysGenerated, differentPKForEncapsulationAndDecapsulation) {
  BloomFilterEncryptionIBBE bfeIBBE2(m, k);
  bfeIBBE2.keyGeneration(false);

  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      bfeIBBE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);

  std::unique_ptr<Key> decapsulatedKey = bfeIBBE2.decapsulate(ciphertext.get());

  /* encapsulatedKey must not equal decapsulatedKey since bfeIBBE2 does not
   * know the secret key associated to the public key that was used for
   * encapsulating. */
  ASSERT_TRUE(*encapsulatedKey != *decapsulatedKey);
}

/**
 * Serializing and consecutively deserializing a ciphertext yields the same
 * ciphertext that was initially serialized.
 */
TEST_F(BFEIBBEKeysGenerated, serializationCiphertext) {
  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      bfeIBBE->encapsulate();
  auto* ciphertext = dynamic_cast<CiphertextIBBE*>(tmp.second.get());

  uint8_t *serialized = ciphertext->getSerialized();
  CiphertextIBBE *deserializedCiphertext = new CiphertextIBBE(serialized);

  ASSERT_TRUE(*ciphertext == *deserializedCiphertext);

  delete[] serialized;
  delete deserializedCiphertext;
}

/**
 * Serializing and consecutively deserializing a public key yields the same
 * public key that was initially serialized.
 */
TEST_F(BFEIBBEKeysGenerated, serializationPublicKey) {
  auto *pk = dynamic_cast<const PublicKeyIBBE *>(bfeIBBE->getPublicKey());

  uint8_t *serialized = pk->getSerialized();
  PublicKeyIBBE *deserializedPK = new PublicKeyIBBE(serialized, false);

  ASSERT_TRUE(*pk == *deserializedPK);

  delete[] serialized;
  delete deserializedPK;
}

/**
 * Tests the scenario where the client constructs the public key from a
 * serialized public key that was sent by a server. The client encpapsulates a
 * key and the server is able to decapsulate it accordingly.
 */
TEST_F(BFEIBBEKeysGenerated, encapsulateAfterSerialization) {
  auto *pk = dynamic_cast<const PublicKeyIBBE *>(bfeIBBE->getPublicKey());

  uint8_t *serialized = pk->getSerialized();

  BloomFilterEncryption
      *clientBFE = new BloomFilterEncryptionIBBE(serialized);

  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      clientBFE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);

  std::unique_ptr<Key> decapsulatedKey = bfeIBBE->decapsulate(ciphertext.get());

  ASSERT_TRUE(*decapsulatedKey == *encapsulatedKey);

  delete clientBFE;
}

/**
 * Tests the scenario where the client constructs the public key from a
 * serialized public key that was sent by a server. The client the encpapsulates
 * a key and serializes it. Then the server deserializes this serialized
 * ciphertext and decapsulates the key.
 */
TEST_F(BFEIBBEKeysGenerated, realWorldScenario) {
  auto *pk = dynamic_cast<const PublicKeyIBBE *>(bfeIBBE->getPublicKey());

  // serialize public key on server side
  uint8_t *serializedPublicKey = pk->getSerialized();

  // construct BloomFilterEncryptionIBBE object on client side from serialized
  // public key
  BloomFilterEncryption
      *clientBFE = new BloomFilterEncryptionIBBE(serializedPublicKey);

  // encapsulate key on client side
  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      clientBFE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
  auto* ciphertext = dynamic_cast<CiphertextIBBE*>(tmp.second.get());

  // serialize ciphertext on client side
  uint8_t *serializedCiphertext = ciphertext->getSerialized();

  // deserialize ciphertext on server side
  CiphertextIBBE
      *deserializedCiphertext = new CiphertextIBBE(serializedCiphertext);

  ASSERT_TRUE(*ciphertext == *deserializedCiphertext);

  // decapsulate ciphertext on server side
  std::unique_ptr<Key>
      decapsulatedKey = bfeIBBE->decapsulate(deserializedCiphertext);

  ASSERT_TRUE(*decapsulatedKey == *encapsulatedKey);

  delete clientBFE;
  delete deserializedCiphertext;
}

/**
 * Multiple punctures of secret key.
 */
TEST_F(BFEIBBEKeysGenerated, multiplePuncturings) {
  int failures = 0;
  for (int i = 0; i < 100; i++) {
    std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
        bfeIBBE->encapsulate();
    std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
    std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);

    std::unique_ptr<Key>
        decapsulatedKey = bfeIBBE->decapsulate(ciphertext.get());
    if (decapsulatedKey == nullptr) {
      failures++;
    }
  }
  std::cout << "Failures: " << failures << std::endl;
}

/**
 * Wrong initialization of client bfe.
 */
TEST_F(BFEIBBEKeysGenerated, wrongClientSideInitialization) {
  auto *pk = dynamic_cast<const PublicKeyIBBE *>(bfeIBBE->getPublicKey());

  // serialize public key on server side
  uint8_t *serializedPublicKey = pk->getSerialized();

  unsigned bfek = bfeIBBE->getK();
  memcpy(serializedPublicKey + sizeof(unsigned long long) + G1_SIZE_COMPRESSED
             + GT_SIZE_COMPRESSED + G2_SIZE_COMPRESSED,
         &bfek,
         sizeof(unsigned));

  // construct BloomFilterEncryptionIBBE object on client side from serialized
  // public key
  BloomFilterEncryption
      *clientBFE = new BloomFilterEncryptionIBBE(serializedPublicKey);

  // encapsulate key on client side
  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      clientBFE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
  auto* ciphertext = dynamic_cast<CiphertextIBBE*>(tmp.second.get());

  // serialize ciphertext on client side
  uint8_t *serializedCiphertext = ciphertext->getSerialized();

  // deserialize ciphertext on server side
  CiphertextIBBE
      *deserializedCiphertext = new CiphertextIBBE(serializedCiphertext);

  ASSERT_TRUE(*ciphertext == *deserializedCiphertext);

  // decapsulate ciphertext on server side
  std::unique_ptr<Key>
      decapsulatedKey = bfeIBBE->decapsulate(deserializedCiphertext);

  ASSERT_TRUE(*decapsulatedKey != *encapsulatedKey);

  delete clientBFE;
  delete deserializedCiphertext;
}

/**
 * Generates public key and secret key and writes them to a file.
 */
TEST(GeneralBFEIBBETests, WriteAndReadInKey) {
  unsigned long long m = 100;
  unsigned int k = 3;

  BloomFilterEncryptionIBBE bfeIBBE(m, k);
  BloomFilterEncryptionIBBE bfeIBBE2(m, k);

  // Remove key files to ensure that writing them is also tested.
  std::string secretKeyFileName = bfeIBBE.KEY_FILE_PREFIX + "_" + std::to_string(m)
      + "_" + std::to_string(k) + "." + bfeIBBE.SECRET_KEY_FILE_EXTENSION;
  std::string publicKeyFileName = bfeIBBE.KEY_FILE_PREFIX + "_" + std::to_string(m)
      + "_" + std::to_string(k) + "." + bfeIBBE.PUBLIC_KEY_FILE_EXTENSION;
  std::remove(secretKeyFileName.c_str());
  std::remove(publicKeyFileName.c_str());
  // Generate new keys and write them to files.
  bfeIBBE.keyGeneration(true);
  // Second bfe object reads in the keys instead of generating new ones.
  bfeIBBE2.keyGeneration(true);

  // Check if encapsulating and decapsulating works after reading in the keys.
  auto *pk = dynamic_cast<const PublicKeyIBBE *>(bfeIBBE2.getPublicKey());

  // serialize public key on server side
  uint8_t *serializedPublicKey = pk->getSerialized();

  // construct BloomFilterEncryptionIBBE object on client side from serialized
  // public key
  BloomFilterEncryption
      *clientBFE = new BloomFilterEncryptionIBBE(serializedPublicKey);

  // encapsulate key on client side
  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
      clientBFE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
  auto* ciphertext = dynamic_cast<CiphertextIBBE*>(tmp.second.get());

  // serialize ciphertext on client side
  uint8_t *serializedCiphertext = ciphertext->getSerialized();

  // deserialize ciphertext on server side
  CiphertextIBBE
      *deserializedCiphertext = new CiphertextIBBE(serializedCiphertext);

  ASSERT_TRUE(*ciphertext == *deserializedCiphertext);

  // decapsulate ciphertext on server side
  std::unique_ptr<Key>
      decapsulatedKey = bfeIBBE2.decapsulate(deserializedCiphertext);

  ASSERT_TRUE(*decapsulatedKey == *encapsulatedKey);

  delete clientBFE;
  delete deserializedCiphertext;
}

/**
 * Generates public key and secret key for a BFE with more practically relevant
 * parameters.
 */
TEST(GeneralBFEIBBETests, LargeBloomFilter) {
  unsigned long long m = 131072;  // 2^17
  unsigned int k = 10;

  BloomFilterEncryptionIBBE bfeIBBE(m, k);
  bfeIBBE.keyGeneration(false);
}
