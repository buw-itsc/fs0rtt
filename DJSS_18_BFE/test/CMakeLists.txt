add_executable(DJSS_18_BFE_test ${CMAKE_CURRENT_SOURCE_DIR}/DJSS_18_BFE_test.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/DJSS_18_BFE_IBBE_tests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/DJSS_18_BFE_HIBE_tests.cpp)

target_link_libraries(DJSS_18_BFE_test ${DJSS18} gtest)


add_executable(DJSS_18_BFE_demo ${CMAKE_CURRENT_SOURCE_DIR}/DJSS_18_BFE_demo.cpp)
target_link_libraries(DJSS_18_BFE_demo ${DJSS18})