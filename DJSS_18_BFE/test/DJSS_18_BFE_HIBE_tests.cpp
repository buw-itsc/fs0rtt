//#include <gtest/gtest.h>
//#include "BFE_HIBE/BloomFilterEncryptionHIBE.h"
//#include "BFE_HIBE/CiphertextHIBE.h"
//#include "BFE_HIBE/KeyHIBE.h"
//#include "BFE_HIBE/PublicKeyHIBE.h"
//
//using namespace bfe;
//
//class BFEHIBEKeysGenerated : public testing::Test {
// protected:
//  static const long m = 100;
//  static const int k = 3;
//  static const int timesteps = 8;
//
//  void SetUp() override {
//    bfeHIBE = new BloomFilterEncryptionHIBE(m, k, timesteps);
//    bfeHIBE->keyGeneration();
//  }
//
//  void TearDown() override {
//    delete bfeHIBE;
//  }
//
//  BloomFilterEncryptionHIBE *bfeHIBE;
//};
//
///**
// * If encapsulate outputs key-ciphertext pair (k, c) then decapsulate(c)
// * returns the same key k.
// */
//TEST_F(BFEHIBEKeysGenerated, sameKeyReturnedByEncapsulateAndDecapsulate) {
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//      bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
//  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);
//
//  std::unique_ptr<Key> decapsulatedKey = bfeHIBE->decapsulate(ciphertext.get());
//
//  ASSERT_TRUE(*decapsulatedKey == *encapsulatedKey);
//}
//
///**
// * Encapsulation and decapsulation is correct even after advancing to the next
// * timestep.
// */
//TEST_F(BFEHIBEKeysGenerated,
//       sameKeyReturnedByEncapsulateAndDecapsulateAfterPunctureTimestep) {
//  bfeHIBE->punctureTimestep();
//
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//      bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
//  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);
//
//  std::unique_ptr<Key> decapsulatedKey = bfeHIBE->decapsulate(ciphertext.get());
//
//  ASSERT_TRUE(*decapsulatedKey == *encapsulatedKey);
//}
//
///**
// * A ciphertext can only be decapsulated once.
// */
//TEST_F(BFEHIBEKeysGenerated, puncturingDeletesSecretKeys) {
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//      bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
//  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);
//
//  std::unique_ptr<Key> decapsulatedKey = bfeHIBE->decapsulate(ciphertext.get());
//  ASSERT_NE(decapsulatedKey, nullptr);
//
//  std::unique_ptr<Key> decapsulatedKeyAfterPuncture =
//      bfeHIBE->decapsulate(ciphertext.get());
//
//  ASSERT_EQ(decapsulatedKeyAfterPuncture, nullptr);
//}
//
///**
// * A ciphertext can not be decapsulated after advancing to the next timestep.
// */
//TEST_F(BFEHIBEKeysGenerated, puntureTimestepDeletesSecretKeys) {
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//      bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
//  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);
//
//  bfeHIBE->punctureTimestep();
//
//  EXPECT_THROW(bfeHIBE->decapsulate(ciphertext.get()), std::invalid_argument);
//}
//
///**
// * Consecutive calls to encapsulate return (with overwhelming probability)
// * different keys and ciphertexts.
// */
//TEST_F(BFEHIBEKeysGenerated, encapsulateReturnsDifferentKeyOnEachCall) {
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//      bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey1 = move(tmp.first);
//  std::unique_ptr<Ciphertext> ciphertext1 = move(tmp.second);
//
//  tmp = bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey2 = move(tmp.first);
//  std::unique_ptr<Ciphertext> ciphertext2 = move(tmp.second);
//
//  ASSERT_TRUE(*encapsulatedKey1 != *encapsulatedKey2);
//}
//
///**
// * Decapsulating a ciphertext that was encapsulated under a different public
// * key must not return the same key that was encapsulated.
// */
//TEST_F(BFEHIBEKeysGenerated, differentPKForEncapsulationAndDecapsulation) {
//  BloomFilterEncryptionHIBE bfeHIBE2(m, k, timesteps);
//  bfeHIBE2.keyGeneration();
//
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//      bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
//  std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);
//
//  std::unique_ptr<Key> decapsulatedKey = bfeHIBE2.decapsulate(ciphertext.get());
//
//  /* encapsulatedKey must not equal decapsulatedKey since bfeHIBE2 does not
//   * know the secret key associated to the public key that was used for
//   * encapsulating. */
//  ASSERT_TRUE(*encapsulatedKey != *decapsulatedKey);
//}
//
//TEST_F(BFEHIBEKeysGenerated, serializationCiphertext) {
//
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//      bfeHIBE->encapsulate();
//  auto *ciphertext = dynamic_cast<CiphertextHIBE *>(tmp.second.get());
//
//  uint8_t *serialized = ciphertext->getSerialized();
//  auto *deserializedCiphertext = new CiphertextHIBE(serialized, k, timesteps);
//
//  ASSERT_TRUE(*ciphertext == *deserializedCiphertext);
//
//  delete deserializedCiphertext;
//}
//
//TEST_F(BFEHIBEKeysGenerated, serializationPublicKey) {
//
//  auto *pk = dynamic_cast<const PublicKeyHIBE *>(bfeHIBE->getPublicKey());
//
//  uint8_t *serialized = pk->getSerialized();
//  PublicKeyHIBE *deserializedPK = new PublicKeyHIBE(serialized);
//
//  ASSERT_TRUE(*pk == *deserializedPK);
//
//  delete deserializedPK;
//}
//
///**
// * Tests the scenario where the client constructs the public key from a
// * serialized public key that was sent by a server. The client the encpapsulates
// * a key and serializes it. Then the server deserializes this serialized
// * ciphertext and decapsulates the key.
// */
//TEST_F(BFEHIBEKeysGenerated, realWorldScenario) {
//
//  auto *pk = dynamic_cast<const PublicKeyHIBE *>(bfeHIBE->getPublicKey());
//
//  // serialize public key on server side
//  uint8_t *serializedPublicKey = pk->getSerialized();
//
//  // construct BloomFilterEncryptionHIBE object on client side from serialized
//  // public key
//  std::unique_ptr<PublicKey>
//      deserializedPK = std::make_unique<PublicKeyHIBE>(serializedPublicKey);
//  BloomFilterEncryption
//      *clientBFE = new BloomFilterEncryptionHIBE(move(deserializedPK));
//
//  // encapsulate key on client side
//  std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp2 =
//      clientBFE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey = move(tmp2.first);
//  auto *ciphertext = dynamic_cast<CiphertextHIBE *>(tmp2.second.get());
//
//  // serialize ciphertext on client side
//  uint8_t *serializedCiphertext = ciphertext->getSerialized();
//
//  // deserialize ciphertext on server side
//  auto *deserializedCiphertext =
//      new CiphertextHIBE(serializedCiphertext, k, timesteps);
//
//  ASSERT_TRUE(*ciphertext == *deserializedCiphertext);
//
//  // decapsulate ciphertext on server side
//  std::unique_ptr<Key>
//      decapsulatedKey = bfeHIBE->decapsulate(deserializedCiphertext);
//
//  ASSERT_TRUE(*decapsulatedKey == *encapsulatedKey);
//
//  delete clientBFE;
//  delete deserializedCiphertext;
//}
//
///**
// * Multiple punctures of secret key.
// */
//TEST_F(BFEHIBEKeysGenerated, multiplePuncturings) {
//  for (int i = 0; i < 100; i++) {
//    std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>> tmp =
//        bfeHIBE->encapsulate();
//    std::unique_ptr<Key> encapsulatedKey = move(tmp.first);
//    std::unique_ptr<Ciphertext> ciphertext = move(tmp.second);
//
//    std::unique_ptr<Key>
//        decapsulatedKey = bfeHIBE->decapsulate(ciphertext.get());
//  }
//}
//
///**
// * Generates public key and secret key for a BFE with more practically relevant
// * parameters.
// */
//TEST(GeneralBFEHIBETests, LargeBloomFilter) {
//  unsigned long long m = 131072; // 2^17
//  unsigned int k = 10;
//  unsigned int timesteps = 8;
//
//  BloomFilterEncryptionHIBE bfeHIBE(m, k, timesteps);
//  bfeHIBE.keyGeneration();
//}
