// Copyright [2018] PG 0-RTT

#include "gtest/gtest.h"

/**
 * Initializes googletest framework and runs all unit tests.
 *
 * @param argc
 * @param argv
 * @return 0 if all tests succeed, 1 otherwise.
 */
int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
