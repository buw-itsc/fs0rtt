// Copyright [2018] PG 0-RTT

#include <iostream>
#include "BFE_IBBE/BloomFilterEncryptionIBBE.h"
#include "BFE_IBBE/CiphertextIBBE.h"
#include "BFE_IBBE/KeyIBBE.h"
//#include "BFE_HIBE/BloomFilterEncryptionHIBE.h"
//#include "BFE_HIBE/CiphertextHIBE.h"
//#include "BFE_HIBE/KeyHIBE.h"

using bfe::BloomFilterEncryptionIBBE;
//using bfe::BloomFilterEncryptionHIBE;
using bfe::Key;
using bfe::Ciphertext;

void bfeIBBEDemo() {
  std::cout << std::endl;

  size_t bloomFilterSize = 500;
  unsigned numberHashFunctions = 4;

  // Create the bloom filter encryption object and call the key gen function
  auto bfeIBBE =
      new BloomFilterEncryptionIBBE(bloomFilterSize, numberHashFunctions);
  bfeIBBE->keyGeneration(false);


  // Encapsulate returns a key and the corresponding ciphertext
  auto tmp = bfeIBBE->encapsulate();
  std::unique_ptr<Key> encapsulatedKey = std::move(tmp.first);

  // Print the key
  std::cout << "Encapsulated key: ";
  std::cout << *encapsulatedKey << std::endl;


  // Decapsulate the ciphertext
  Ciphertext *ciphertext = tmp.second.get();
  std::unique_ptr<Key> decapsulatedKey = bfeIBBE->decapsulate(ciphertext);

  // Print the decapsulated key
  std::cout << "Decapsulated key: ";
  std::cout << *decapsulatedKey << std::endl;


  // If decapsulate is called again with the same ciphertext it returns a null
  // pointer
  std::unique_ptr<Key>
      decapsulatedKeyAfterPuncture = bfeIBBE->decapsulate(ciphertext);
  if (decapsulatedKeyAfterPuncture == nullptr) {
    std::cout << "A ciphertext can only be decapuslated once" << std::endl;
  }
}


//void bfeHIBEDemo() {
//  std::cout << std::endl;
//
//  size_t bloomFilterSize = 200;
//  unsigned numberHashFunctions = 3;
//  unsigned numberTimesteps = 8;
//
//  // Create the bloom filter encryption object and call the key gen function
//  auto bfeHIBE = new BloomFilterEncryptionHIBE(bloomFilterSize,
//                                               numberHashFunctions,
//                                               numberTimesteps);
//  bfeHIBE->keyGeneration();
//
//  // Encapsulate returns a key and the corresponding ciphertext
//  auto tmp = bfeHIBE->encapsulate();
//  std::unique_ptr<Key> encapsulatedKey = std::move(tmp.first);
//
//  // Print the key
//  std::cout << "Encapsulated key: ";
//  std::cout << *encapsulatedKey << std::endl;
//
//  // Decapsulate the ciphertext
//  Ciphertext *ciphertext = tmp.second.get();
//  std::unique_ptr<Key> decapsulatedKey = bfeHIBE->decapsulate(ciphertext);
//
//  // Print the decapsulated key
//  std::cout << "Decapsulated key: ";
//  std::cout << *decapsulatedKey << std::endl;
//
//  // If decapsulate is called again with the same ciphertext it returns a null
//  // pointer
//  std::unique_ptr<Key>
//      decapsulatedKeyAfterPuncture = bfeHIBE->decapsulate(ciphertext);
//
//  if (decapsulatedKeyAfterPuncture == nullptr) {
//    std::cout << "A ciphertext can only be decapsulated once" << std::endl;
//  }
//
//  std::cout << std::endl << "Second timestep" << std::endl;
//
//  // Advance to the next timestep
//  bfeHIBE->punctureTimestep();
//
//  // Encapsulate a ciphertext
//  tmp = bfeHIBE->encapsulate();
//  encapsulatedKey = std::move(tmp.first);
//
//  // Print the key
//  std::cout << "Encapsulated key: ";
//  std::cout << *encapsulatedKey << std::endl;
//
//  // Decapsulate the ciphertext
//  ciphertext = tmp.second.get();
//  decapsulatedKey = bfeHIBE->decapsulate(ciphertext);
//
//  // Print the decapsulated key
//  std::cout << "Decapsulated key: ";
//  std::cout << *decapsulatedKey << std::endl;
//}

int main() {
  bfeIBBEDemo();
  //bfeHIBEDemo();
}
