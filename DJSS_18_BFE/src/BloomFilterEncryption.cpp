// Copyright [2018] PG 0-RTT

#include <del07ibbe/Delerablee_07_IBBE.h>
#include "BloomFilterEncryption.h"
#include "Ciphertext.h"
#include "UtilityBFE.h"

using bfe::BloomFilterEncryption;
using bfe::PublicKey;

BloomFilterEncryption::BloomFilterEncryption(const unsigned long long m,
                                             const unsigned int k)
    : m(m), k(k) {

  // Create the Bloom Filter.
  countingBloomFilter =
      std::make_unique<bf::counting_bloom_filter>(bf::make_hasher(k), m, COUNTING_BF_BITS_PER_CELL, false);
}

unsigned int BloomFilterEncryption::getK() const {
  return k;
}

BloomFilterEncryption::BloomFilterEncryption() {
  secretKey = nullptr;
}

const PublicKey *BloomFilterEncryption::getPublicKey() const {
  return publicKey.get();
}

uint8_t* BloomFilterEncryption::random256Bits() {
  auto randomBytes = new uint8_t[32];

  size_t randomNumberByteSize;
  randomNumberByteSize = sizeof(std::random_device::result_type);

  if (0 != 32 % randomNumberByteSize) {
    throw std::runtime_error("32 not evenly divisible by output"
                             "size of randomness source.");
  }

  size_t randomNumbersRequired = 32 / randomNumberByteSize;

  for (unsigned i = 0; i < randomNumbersRequired; i++) {
      std::random_device::result_type randomNumber = randomDevice();
      memcpy(&randomBytes[randomNumberByteSize * i], &randomNumber,
             randomNumberByteSize);
  }

  return randomBytes;
}


void BloomFilterEncryption::puncture(const bfe::Ciphertext *ciphertext) {
  const uint8_t *randomness = ciphertext->getR();

  // Generate the identities from the randomness for which the secret keys
  // have to be deleted.
  std::vector<bf::digest>
      identitiesToDelete = generateIdentitiesAsVector(randomness);

  // Delete the identity secret keys.
  secretKey->deleteIdentitySecretKeys(identitiesToDelete);

  // Convert array to vector so that in can be put into add
  std::vector<uint8_t>
      randomnessAsVector(randomness, randomness + SECURITY_PARAMETER);
}

std::vector<bf::digest> BloomFilterEncryption::generateIdentitiesAsVector(
    const uint8_t *randomness) {

  // Hash the randomness.
  bf::object randomnessAsObject(randomness, SECURITY_PARAMETER);
  std::vector<bf::digest>
      identities = countingBloomFilter->find_indices(randomnessAsObject);

  return identities;
}
