// Copyright [2018] PG 0-RTT

#include "Key.h"
#include <del07ibbe/Delerablee_07_IBBE.h>
#include <iomanip>
#include "BloomFilterEncryption.h"

using bfe::Key;

Key::~Key() {
  delete[] key;
}

const uint8_t* Key::getKey() const {
  return key;
}

bool Key::operator==(const Key &other) const {
  bool equal =
      (bool) !memcmp(this->getKey(), other.getKey(), SECURITY_PARAMETER);
  return equal;
}

bool Key::operator!=(const Key &other) const {
  return !(*this == other);
}

std::ostream &bfe::operator<<(std::ostream &os, const Key &key) {
  const uint8_t *keyBytes = key.getKey();
  for (size_t i = 0; i < SECURITY_PARAMETER; i++) {
    os << std::hex << std::setfill('0') << std::setw(2)
       << (unsigned) keyBytes[i] << " ";
  }
  return os;
}
