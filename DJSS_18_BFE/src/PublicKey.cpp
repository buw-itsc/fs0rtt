// Copyright [2018] PG 0-RTT

#include <cstring>
#include "PublicKey.h"

using bfe::PublicKey;

PublicKey::PublicKey(unsigned long long bloomFilterSize) : bloomFilterSize(
    bloomFilterSize) {}

PublicKey::PublicKey(const uint8_t *serialized) : bloomFilterSize(
    deserializeBloomFilterSize(serialized)) {}

const unsigned long long PublicKey::getBloomFilterSize() const {
  return bloomFilterSize;
}

const unsigned long long PublicKey::deserializeBloomFilterSize(
    const uint8_t *serialized) {
  unsigned long long bloomFilterSize;
  memcpy(&bloomFilterSize, serialized, sizeof(unsigned long long));
  return bloomFilterSize;
}
