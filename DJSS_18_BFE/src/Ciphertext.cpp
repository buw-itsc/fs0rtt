// Copyright [2018] PG 0-RTT

#include "Ciphertext.h"
#include <del07ibbe/Delerablee_07_IBBE.h>
#include <algorithm>

using bfe::Ciphertext;

Ciphertext::Ciphertext(const uint8_t *serialized) : r(deserializeCiphertextR(
    serialized)) {}

const uint8_t *Ciphertext::getR() const {
  return r;
}

const uint8_t* Ciphertext::deserializeCiphertextR(const uint8_t *serialized) {
  auto *r = new uint8_t[SECURITY_PARAMETER];
  std::copy(serialized, serialized + SECURITY_PARAMETER, r);
  return r;
}
