// Copyright [2018] PG 0-RTT

#include <del07ibbe/Delerablee_07_IBBE.h>
#include <del07ibbe/Delerablee_07_IBBE_util.h>
#include "BFE_IBBE/CiphertextIBBE.h"

using bfe::CiphertextIBBE;

CiphertextIBBE::CiphertextIBBE(const uint8_t *r,
                               std::unique_ptr<del07_ciphertext_t> c)
    : Ciphertext(r), ciphertextIBBE(move(c)) {}

CiphertextIBBE::CiphertextIBBE(const uint8_t *serialized)
    : Ciphertext(serialized + DEL_07_CIPHERTEXT_SIZE), ciphertextIBBE(move(
    deserializeIBBECiphertext(serialized))) {}

CiphertextIBBE::~CiphertextIBBE() {
  // Delete the heap allocated data.
  delete[] r;

  // Set the memory allocated by del07_ciphertext_t to 0 and free it.
  del07_set_ciphertext_to_zero(ciphertextIBBE.get());
}

const del07_ciphertext_t *CiphertextIBBE::getCiphertext() const {
  return ciphertextIBBE.get();
}

bool CiphertextIBBE::isValid() const {
  int valid;
  del07_ciphertext_is_valid(&valid, ciphertextIBBE.get());
  return (bool) valid;
}

uint8_t *CiphertextIBBE::getSerialized() {
  // size of the ibbe ciphertext + size of randomness which is the security
  // parameter
  auto *serialized = new uint8_t[getSerializedSize()];

  uint8_t *serializedDel07Ciphertext;
  del07_serialize_ciphertext(&serializedDel07Ciphertext, ciphertextIBBE.get());
  memcpy(serialized,
         serializedDel07Ciphertext,
         (size_t) DEL_07_CIPHERTEXT_SIZE);

  memcpy(serialized + DEL_07_CIPHERTEXT_SIZE, r, SECURITY_PARAMETER);

  return serialized;
}

std::unique_ptr<del07_ciphertext_t> CiphertextIBBE::deserializeIBBECiphertext(
    const uint8_t *serialized) {

  del07_ciphertext_t *ciphertext;

  int deserialize_status = del07_deserialize_ciphertext(&ciphertext, serialized);

  if(deserialize_status == STS_ERR) {
    throw std::invalid_argument("Deserialization of public key failed.");
  }

  std::unique_ptr<del07_ciphertext_t> ciphertextUniquePtr(ciphertext);

  return move(ciphertextUniquePtr);
}

bool CiphertextIBBE::operator==(const CiphertextIBBE &other) const {
  const del07_ciphertext_t *ctIBBE = getCiphertext();
  const del07_ciphertext_t *otherCtIBBE = other.getCiphertext();
  bool equal = (bool) del07_ciphertexts_are_equal(ctIBBE, otherCtIBBE);

  const uint8_t *lr = getR();
  const uint8_t *rr = other.getR();
  equal &= !memcmp(lr, rr, SECURITY_PARAMETER);

  return equal;
}

bool CiphertextIBBE::operator!=(const CiphertextIBBE &other) const {
  return !(*this == other);
}

const unsigned long CiphertextIBBE::getSerializedSize() {
  return DEL_07_CIPHERTEXT_SIZE + SECURITY_PARAMETER;
}
