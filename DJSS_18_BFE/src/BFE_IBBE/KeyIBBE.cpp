// Copyright [2018] PG 0-RTT

#include <del07ibbe/Delerablee_07_IBBE_util.h>
#include "BFE_IBBE/KeyIBBE.h"

using bfe::KeyIBBE;

KeyIBBE::KeyIBBE(std::unique_ptr<del07_key_t> param) {
  // Convert the given del07_key_t to a byte array.
  int convertStatus = del07_convert_key_to_bit_string(&key, param.get());
  if (convertStatus) {
    throw std::runtime_error(
        "Could not convert the given key into a session key");
  }
}
