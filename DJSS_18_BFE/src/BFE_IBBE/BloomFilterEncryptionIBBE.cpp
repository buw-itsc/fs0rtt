// Copyright [2018] PG 0-RTT

#include <del07ibbe/Delerablee_07_IBBE.h>
#include <del07ibbe/Delerablee_07_IBBE_util.h>
#include <sys/stat.h>
#include "BFE_IBBE/BloomFilterEncryptionIBBE.h"
#include "BFE_IBBE/CiphertextIBBE.h"
#include "BFE_IBBE/IdentitySecretKeyIBBE.h"
#include "BFE_IBBE/KeyIBBE.h"
#include "BFE_IBBE/PublicKeyIBBE.h"
#include "BFE_IBBE/SecretKeyIBBE.h"
#include "BFE_IBBE/UtilityIBBE.h"

#include <iostream>

using bfe::BloomFilterEncryptionIBBE;
using bfe::Key;
using bfe::Ciphertext;

BloomFilterEncryptionIBBE::BloomFilterEncryptionIBBE(const unsigned long long m,
                                                     const unsigned int k)
    : BloomFilterEncryption(m, k) {
  numberOfBFEIBBEObjectsMutex.lock();
  if (numberOfBFEIBBEObjects == 0) {
    // Initialize the del07 library.
    int initStatus = del07_init();
    if (initStatus) {
      throw std::runtime_error("Could not initialize del07");
    }
  }
  numberOfBFEIBBEObjects++;
  numberOfBFEIBBEObjectsMutex.unlock();
}

BloomFilterEncryptionIBBE::BloomFilterEncryptionIBBE(
    const uint8_t *serializedPublicKey) : BloomFilterEncryption() {
  numberOfBFEIBBEObjectsMutex.lock();
  if (numberOfBFEIBBEObjects == 0) {
    // Initialize the del07 library.
    int initStatus = del07_init();
    if (initStatus) {
      throw std::runtime_error("Could not initialize del07");
    }
  }
  numberOfBFEIBBEObjects++;
  numberOfBFEIBBEObjectsMutex.unlock();

  // Deserialize the public key.
  publicKey = std::make_unique<PublicKeyIBBE>(serializedPublicKey, false);
  if (!publicKey->isValid()) {
    throw std::invalid_argument("Public key not valid");
  }

  m = publicKey->getBloomFilterSize();
  auto publicKeyIBBE = dynamic_cast<PublicKeyIBBE *>(publicKey.get());
  k = publicKeyIBBE->getPublicKeyIBBE()->max_number_recipients - 1;

  countingBloomFilter =
      std::make_unique<bf::counting_bloom_filter>(bf::make_hasher(k), m, COUNTING_BF_BITS_PER_CELL, false);
}

BloomFilterEncryptionIBBE::~BloomFilterEncryptionIBBE() {
  numberOfBFEIBBEObjectsMutex.lock();
  numberOfBFEIBBEObjects--;
  if (numberOfBFEIBBEObjects == 0) {
    del07_clean();
  }
  numberOfBFEIBBEObjectsMutex.unlock();
}

void BloomFilterEncryptionIBBE::keyGeneration(const bool loadKeyFromFile) {
  // Generate the master secret key and public key of del07.
  del07_master_secret_key_t *masterSecretKeyIBBE;
  del07_public_key_t *publicKeyIBBE;

  if (loadKeyFromFile) {
    struct stat skBuffer;
    struct stat pkBuffer;
    std::string secretKeyFileName = KEY_FILE_PREFIX + "_" + std::to_string(m)
        + "_" + std::to_string(k) + "." + SECRET_KEY_FILE_EXTENSION;
    std::string publicKeyFileName = KEY_FILE_PREFIX + "_" + std::to_string(m)
        + "_" + std::to_string(k) + "." + PUBLIC_KEY_FILE_EXTENSION;
    // Check if secret key and public files exist.
    if (stat(secretKeyFileName.c_str(), &skBuffer) == 0 && stat(publicKeyFileName.c_str(), &pkBuffer) == 0) {
      // Read in keys from file.
      secretKey = std::make_unique<SecretKeyIBBE>(m, secretKeyFileName);
      publicKey = std::make_unique<PublicKeyIBBE>(m, publicKeyFileName);
    } else {
      generateNewKeyPair();
      secretKey->writeToFile(secretKeyFileName);
      publicKey->writeToFile(publicKeyFileName);
    }
  } else {
    generateNewKeyPair();
  }

  //std::unique_ptr<del07_public_key_t> publicKeyIBBEUniquePtr(publicKeyIBBE);
  //publicKey = std::make_unique<PublicKeyIBBE>(m, move(publicKeyIBBEUniquePtr));

  if (!publicKey->isValid()) {
    throw std::runtime_error("Public key not valid");
  }
}

std::pair<std::unique_ptr<Key>, std::unique_ptr<Ciphertext>>
BloomFilterEncryptionIBBE::encapsulate() {
  uint8_t *randomBytes = random256Bits();

  // Generate identities from the random bits.
  uint8_t **encapsulatingIdentitiesAsArray;
  unsigned numberIdentities;
  unsigned *identitiesLengths;
  std::tie(encapsulatingIdentitiesAsArray,
           numberIdentities,
           identitiesLengths) = generateIdentitiesAsByteArray(randomBytes);

  del07_ciphertext_t *ciphertextIBBE;
  del07_key_t *encapsulatedKeyIBBE;

  auto publicKeyIBBE = dynamic_cast<PublicKeyIBBE *>(publicKey.get());

  // Encapsulate a session key for the given identities
  int encapsulateStatus = del07_encapsulate(&ciphertextIBBE,
      &encapsulatedKeyIBBE, (const uint8_t **) encapsulatingIdentitiesAsArray,
      numberIdentities, identitiesLengths, publicKeyIBBE->getPublicKeyIBBE());
  if (encapsulateStatus) {
    throw std::runtime_error("Could not encapsulate a session key");
  }

  // Delete the arrays containing the identities.
  for (int i = 0; i < numberIdentities; i++) {
    delete[] encapsulatingIdentitiesAsArray[i];
  }
  delete[] encapsulatingIdentitiesAsArray;
  delete[] identitiesLengths;

  // Create the CiphertextIBBE and KeyIBBE
  std::unique_ptr<del07_ciphertext_t> ciphertextIBBEUniquePtr(ciphertextIBBE);
  std::unique_ptr<CiphertextIBBE> ciphertext = std::make_unique<CiphertextIBBE>(
      randomBytes,
      move(ciphertextIBBEUniquePtr));
  std::unique_ptr<del07_key_t>
      encapsulatedKeyIBBEUniquePtr(encapsulatedKeyIBBE);
  std::unique_ptr<KeyIBBE>
      key = std::make_unique<KeyIBBE>(move(encapsulatedKeyIBBEUniquePtr));

  if (!ciphertext->isValid()) {
    throw std::runtime_error("(BloomFilterEncryptionIBBE::encapsulate): "
                             "Ciphertext not valid");
  }

  return std::pair<std::unique_ptr<KeyIBBE>, std::unique_ptr<CiphertextIBBE>>(
      move(key),
      move(ciphertext));
}

std::unique_ptr<Key> BloomFilterEncryptionIBBE::decapsulate(
    const Ciphertext *ciphertext) {
  if (!ciphertext->isValid()) {
    throw std::invalid_argument("(BloomFilterEncryptionIBBE::decapsulate):"
                                " Ciphertext not valid");
  }
  auto ciphertextIBBE = dynamic_cast<const CiphertextIBBE *>(ciphertext);

  const uint8_t *randomness = ciphertextIBBE->getR();

  // Convert array to vector so that in can be put into lookup
  std::vector<uint8_t> randomnessAsVector(
      randomness, randomness + SECURITY_PARAMETER);

  // The ciphertext can not be decapsulated if all secret keys for which the
  // ciphertext was encapsulated are deleted. This is the case if lookup for
  // this randomness returns > 0. If it returns 0 there must be a secret key
  // with which the ciphertext can be decapsulated.
  bloomFilterMutex.lock();
  size_t bloomFilterCheck = countingBloomFilter->lookup(randomnessAsVector);
  if (bloomFilterCheck > 0) {
    bloomFilterMutex.unlock();
    return nullptr;

  } else {
    // Generate the identities as Bloom Filter indices.
    std::vector<bf::digest> encapsulatingIdentitiesAsVector =
        generateIdentitiesAsVector(randomness);

    // Find the first identity (if existing) for which the secret key has not
    // been deleted and save the index in the identities array of it.
    bf::digest decapsulatingIdentity = 0;
    unsigned decapsulatingIdentityIndex = 0;
    for (auto identity : encapsulatingIdentitiesAsVector) {
      if (countingBloomFilter->count(identity) == 0) {
        bloomFilterCheck = 0;
        decapsulatingIdentity = identity;
        break;
      } else {
        decapsulatingIdentityIndex++;
      }
    }

    // Add the randomness to the Bloom Filter to mark the keys as deleted.
    countingBloomFilter->add(randomnessAsVector);
    bloomFilterMutex.unlock();

    // Generate the identities as array from the randomness.
    // TODO: do not call generateIdentitiesAsVector in generateIdentitiesAsByteArray again

    uint8_t **encapsulatingIdentitiesAsArray;
    unsigned numberIdentities;
    unsigned *identitiesLengths;
    std::tie(encapsulatingIdentitiesAsArray,
             numberIdentities,
             identitiesLengths) = generateIdentitiesAsByteArray(randomness);

    // Get the secret key for the identity found in the previous step.
    auto secretKeyIBBE = dynamic_cast<SecretKeyIBBE *>(secretKey.get());
    IdentitySecretKey *decapsulatingIdentitySecretKey =
        secretKeyIBBE->getIdentitySecretKeyForIdentity(decapsulatingIdentity);
    auto decapsulatingIdentitySecretKeyIBBE =
        dynamic_cast<IdentitySecretKeyIBBE *>(decapsulatingIdentitySecretKey);

    auto publicKeyIBBE = dynamic_cast<PublicKeyIBBE *>(publicKey.get());

    // Decapsulate the ciphertext.
    del07_key_t *decapsulatedKeyIBBE;
    int decapsulateStatus = del07_decapsulate(&decapsulatedKeyIBBE,
        (const uint8_t **) encapsulatingIdentitiesAsArray, numberIdentities,
        identitiesLengths, decapsulatingIdentityIndex,
        decapsulatingIdentitySecretKeyIBBE->getSecretKeyIBBE(),
        ciphertextIBBE->getCiphertext(), publicKeyIBBE->getPublicKeyIBBE());

    // Delete the identity arrays.
    for (int i = 0; i < numberIdentities; i++) {
      delete[] encapsulatingIdentitiesAsArray[i];
    }
    delete[] encapsulatingIdentitiesAsArray;
    delete[] identitiesLengths;

    if (decapsulateStatus == STS_ERR) {
      // BLS signature was invalid or decapsulation failed for other reason
      bloomFilterMutex.lock();
      countingBloomFilter->remove(randomnessAsVector);
      bloomFilterMutex.unlock();
      return nullptr;
    }

    std::unique_ptr<del07_key_t>
        decapsulatedKeyIBBEUniquePtr(decapsulatedKeyIBBE);
    std::unique_ptr<Key>
        key = std::make_unique<KeyIBBE>(move(decapsulatedKeyIBBEUniquePtr));

    // Puncture the secret key for the ciphertext, that was just decapsulated.
    puncture(ciphertextIBBE);

    return key;
  }
}

std::tuple<uint8_t **,
           unsigned,
           unsigned *> BloomFilterEncryptionIBBE::generateIdentitiesAsByteArray(
    const uint8_t *randomness) {

  // Generate the identities as vector.
  std::vector<bf::digest>
      encapsulatingIdentitiesAsVector = generateIdentitiesAsVector(randomness);

  // Number of identities is length of the vector.
  auto numberIdentities = (unsigned) encapsulatingIdentitiesAsVector.size();

  auto encapsulatingIdentitiesAsByteArray = new uint8_t *[numberIdentities];
  auto identitiesLengths = new unsigned[numberIdentities];

  // Convert each identity to a byte array.
  uint8_t *identityAsByteArray;
  unsigned identityLength;
  for (size_t i = 0; i < numberIdentities; i++) {
    std::tie(identityAsByteArray, identityLength) =
        identityToByteArray(encapsulatingIdentitiesAsVector.at(i));
    encapsulatingIdentitiesAsByteArray[i] = identityAsByteArray;
    identitiesLengths[i] = identityLength;
  }

  return std::make_tuple<uint8_t **, unsigned, unsigned *>(
      (uint8_t **) encapsulatingIdentitiesAsByteArray,
      (unsigned) numberIdentities,
      (unsigned *) identitiesLengths);
}

void BloomFilterEncryptionIBBE::generateNewKeyPair() {
  del07_master_secret_key_t *masterSecretKeyIBBE;
  del07_public_key_t *publicKeyIBBE;

  int setupStatus = del07_setup(&masterSecretKeyIBBE, &publicKeyIBBE, k);
  if (setupStatus) {
    throw std::runtime_error("Error in setup of del07");
  }
  int mkValid;
  del07_master_secret_key_is_valid(&mkValid, masterSecretKeyIBBE);
  if (!mkValid) {
    throw std::runtime_error("Master secret key not valid");
  }

  std::unique_ptr<del07_master_secret_key_t>
      masterSecretKeyIBBEUniquePtr(masterSecretKeyIBBE);

  // Create secret key and public key member variables.
  secretKey =
      std::make_unique<SecretKeyIBBE>(m, move(masterSecretKeyIBBEUniquePtr));

  std::unique_ptr<del07_public_key_t> publicKeyIBBEUniquePtr(publicKeyIBBE);
  publicKey = std::make_unique<PublicKeyIBBE>(m, move(publicKeyIBBEUniquePtr));
}
