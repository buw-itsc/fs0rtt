// Copyright [2018] PG 0-RTT

#include <del07ibbe/Delerablee_07_IBBE_util.h>
#include <fstream>
#include "BFE_IBBE/PublicKeyIBBE.h"

using bfe::PublicKeyIBBE;

PublicKeyIBBE::PublicKeyIBBE(unsigned long long bloomFilterSize,
                             std::unique_ptr<const del07_public_key_t> pk)
    : PublicKey(bloomFilterSize), publicKeyIBBE(move(pk)) {}

PublicKeyIBBE::PublicKeyIBBE(unsigned long long bloomFilterSize,
    const std::string fileName) : PublicKey(bloomFilterSize), publicKeyIBBE(move(readInFromFile(fileName))) {}

PublicKeyIBBE::PublicKeyIBBE(const uint8_t *serialized, bool computePrecomputationTables)
    : PublicKey(serialized), publicKeyIBBE(move(deserializeIBBEPublicKey(
    serialized + sizeof(unsigned long long), computePrecomputationTables))) {}

PublicKeyIBBE::~PublicKeyIBBE() {
  // Set the memory allocated by the del07_public_key_t to 0 and free it.
  del07_set_public_key_to_zero(
      const_cast<del07_public_key_t *>(publicKeyIBBE.get()));
}

const del07_public_key_t *const PublicKeyIBBE::getPublicKeyIBBE() const {
  return publicKeyIBBE.get();
}

bool PublicKeyIBBE::isValid() const {
  int valid;
  del07_public_key_is_valid(&valid, publicKeyIBBE.get());
  return (bool) valid;
}

uint8_t *PublicKeyIBBE::getSerialized() const {
  unsigned publicKeySize = del07_get_public_key_size(publicKeyIBBE.get());

  // Size of the ibbe public key + size of bloom filter size
  auto *serialized = new uint8_t[getSerializedSize()];

  memcpy(serialized, &bloomFilterSize, sizeof(unsigned long long));

  uint8_t *serializedPK;
  del07_serialize_public_key(&serializedPK, publicKeyIBBE.get());
  memcpy(serialized + sizeof(unsigned long long), serializedPK, publicKeySize);

  return serialized;
}

std::unique_ptr<del07_public_key_t> PublicKeyIBBE::deserializeIBBEPublicKey(
    const uint8_t *serialized, bool computePrecomputationTables) {
  del07_public_key_t *publicKey;
  int deserialize_status = del07_deserialize_public_key(&publicKey, serialized, computePrecomputationTables);

  if(deserialize_status == STS_ERR) {
    throw std::invalid_argument("Deserialization of public key failed.");
  }
  std::unique_ptr<del07_public_key_t> publicKeyUniquePtr(publicKey);

  return move(publicKeyUniquePtr);
}

bool PublicKeyIBBE::operator==(const PublicKeyIBBE &other) const {
  const del07_public_key_t *pkIBBE = getPublicKeyIBBE();
  const del07_public_key_t *otherPkIBBE = other.getPublicKeyIBBE();

  bool equal = (bool) del07_public_keys_are_equal(pkIBBE, otherPkIBBE);
  equal &= this->bloomFilterSize == other.bloomFilterSize;
  return equal;
}

bool PublicKeyIBBE::operator!=(const PublicKeyIBBE &other) const {
  return !(*this == other);
}

const unsigned long PublicKeyIBBE::getSerializedSize() const {
  return sizeof(unsigned long long)
      + del07_get_public_key_size(publicKeyIBBE.get());
}

void PublicKeyIBBE::writeToFile(const std::string fileName) {
  std::ofstream file(fileName);
  if (!file.is_open()) {
    throw std::runtime_error("Unable to write public key file");
  }
  uint8_t *serialized = getSerialized();
  // Do not write the bloomFilterSize therefore skip the first sizeof(unsigned
  // long long) bytes
  for (size_t i = sizeof(unsigned long long); i < getSerializedSize(); i++) {
    file << serialized[i];
  }
  file.close();
}

std::unique_ptr<del07_public_key_t> PublicKeyIBBE::readInFromFile(const std::string fileName) {
  std::ifstream file(fileName, std::ios::binary);
  if (!file.is_open()) {
    throw std::runtime_error("Unable to read public key file");
  }
  // Read in whole file.
  std::string serialized((std::istreambuf_iterator<char>(file)),
                       std::istreambuf_iterator<char>());
  file.close();
  return deserializeIBBEPublicKey((uint8_t *) serialized.data(), true);
}
