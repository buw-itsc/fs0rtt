// Copyright [2018] PG 0-RTT

#include <del07ibbe/Delerablee_07_IBBE.h>
#include <del07ibbe/Delerablee_07_IBBE_util.h>
#include "BFE_IBBE/UtilityIBBE.h"
#include "BFE_IBBE/IdentitySecretKeyIBBE.h"

using bfe::IdentitySecretKeyIBBE;

IdentitySecretKeyIBBE::IdentitySecretKeyIBBE(
    const bf::digest identity, del07_master_secret_key_t *const msk) {
  del07_secret_key_t *skIBBE;

  // Convert the identity to a byte array.
  uint8_t *identityAsByteArray;
  unsigned identityLength;
  std::tie(identityAsByteArray, identityLength) = identityToByteArray(identity);

  // Derive the secret key for the identity.
  int statusCodeExtract =
      del07_extract(&skIBBE, msk, identityAsByteArray, identityLength);
  if (statusCodeExtract) {
    throw std::runtime_error(
        "Unable to extract secret key for identity" + std::to_string(identity));
  }

  // Delete the identity byte array.
  delete[] identityAsByteArray;

  secretKeyIBBE.reset(skIBBE);
}

IdentitySecretKeyIBBE::IdentitySecretKeyIBBE(const bf::digest identity,
    const uint8_t *serialized) : secretKeyIBBE(move(deserializeIdentitySecretKey(serialized))) {}

IdentitySecretKeyIBBE::~IdentitySecretKeyIBBE() {
  // Set the memory allocated by the del07_secret_key_t to 0.
  del07_set_secret_key_to_zero(secretKeyIBBE.get());
}

del07_secret_key_t *IdentitySecretKeyIBBE::getSecretKeyIBBE() {
  return secretKeyIBBE.get();
}

bool IdentitySecretKeyIBBE::isValid() const {
  int valid;
  del07_secret_key_is_valid(&valid, secretKeyIBBE.get());
  return (bool) valid;
}

uint8_t* IdentitySecretKeyIBBE::getSerialized() {
  uint8_t *serialized;
  int result_status = del07_serialize_secret_key(&serialized, getSecretKeyIBBE(), (int) false);
  if (result_status) {
    throw std::runtime_error("Unable to serialize identity secret key");
  }
  return serialized;
}

const unsigned IdentitySecretKeyIBBE::getSerializedSize(bool compression) const {
  return del07_get_secret_key_size((int) compression);
}

std::unique_ptr<del07_secret_key_t> IdentitySecretKeyIBBE::deserializeIdentitySecretKey(
    const uint8_t *serialized) {
  del07_secret_key_t *secretKey;
  int deserialize_status = del07_deserialize_secret_key(&secretKey, serialized, (int) false);

  if(deserialize_status == STS_ERR) {
    throw std::invalid_argument("Deserialization of secret key failed.");
  }
  std::unique_ptr<del07_secret_key_t> secretKeyUniquePtr(secretKey);

  return move(secretKeyUniquePtr);
}
