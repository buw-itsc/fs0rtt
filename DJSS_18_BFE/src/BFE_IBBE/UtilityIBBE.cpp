// Copyright [2018] PG 0-RTT

#include <cmath>
#include "BFE_IBBE/UtilityIBBE.h"

namespace bfe {

std::pair<uint8_t*, int> identityToByteArray(const bf::digest identity) {
  // Compute needed length of byte array
  auto identityLength =
      (unsigned int) std::floor(log(identity) / log(256.0)) + 1;
  auto identityAsByteArray = new uint8_t[identityLength];

  // Extract each byte: shift the long by n-1, ..., 2, 1 byte to the right and
  // get last 8 bit with & 0xFF
  for (int i = 0; i < identityLength; i++) {
    unsigned int shiftValue = ((identityLength - 1 - i) * 8);
    auto byte = (uint8_t) ((identity >> shiftValue) & 0xFF);
    identityAsByteArray[i] = byte;
  }

  return std::make_pair(identityAsByteArray, identityLength);
}

}  // namespace bfe

