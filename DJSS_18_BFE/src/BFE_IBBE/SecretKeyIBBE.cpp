// Copyright [2018] PG 0-RTT

extern "C" {
#include <relic/relic.h>
}

#include "BFE_IBBE/SecretKeyIBBE.h"

#include <del07ibbe/Delerablee_07_IBBE_global.h>
#include <del07ibbe/Delerablee_07_IBBE_util.h>
#include <thread>
#include <fstream>
#include "BFE_IBBE/IdentitySecretKeyIBBE.h"

using bfe::SecretKeyIBBE;
using bfe::IdentitySecretKey;

SecretKeyIBBE::SecretKeyIBBE(const unsigned long long bloomFilterSize,
    const std::unique_ptr<del07_master_secret_key_t> msk)
    : SecretKey(bloomFilterSize) {

  unsigned numberOfThreads = std::thread::hardware_concurrency();
  std::vector<std::thread> threads(numberOfThreads - 1);
  std::mutex mutex;

  // Split up the n identity secret keys that have to be computed between k
  // threads. Each thread computes n/k identity secret keys. For first n/k
  // identities no extra thread is created and they are computed in this master
  // thread.
  for (size_t i = 1; i < numberOfThreads; i++) {
    threads[i - 1] = std::thread(generateIdentitySecretKeys,
                             i * bloomFilterSize / numberOfThreads,
                             (i + 1) * bloomFilterSize / numberOfThreads,
                             &identitySecretKeys,
                             &mutex,
                             msk.get());
  }

  // For each i in [1,bloomFilterSize/numberOfThreads] create a new
  // IdentitySecretKeyIBBE and store it in the map of identitySecretKeys.
  for (bf::digest identity = 0; identity < bloomFilterSize / numberOfThreads;
       ++identity) {
    std::unique_ptr<IdentitySecretKeyIBBE> identitySecretKey =
        std::make_unique<IdentitySecretKeyIBBE>(identity, msk.get());

    if (!identitySecretKey->isValid()) {
      throw std::runtime_error(
          "Identity secret key for identity " + std::to_string(identity)
              + "not valid");
    }

    mutex.lock();
    identitySecretKeys[identity] = move(identitySecretKey);
    mutex.unlock();
  }

  for (size_t i = 0; i < numberOfThreads - 1; i++) {
    threads[i].join();
  }

  del07_set_master_secret_key_to_zero(msk.get());
}

SecretKeyIBBE::SecretKeyIBBE(const unsigned long long bloomFilterSize,
    const std::string fileName) : SecretKey(bloomFilterSize) {
  std::ifstream file(fileName);
  unsigned identitySecretKeySize = del07_get_secret_key_size((int) false);
  for (bf::digest identity = 0; identity < bloomFilterSize; identity++) {
    char *serialized = new char[identitySecretKeySize];
    file.read(serialized, identitySecretKeySize);

    std::unique_ptr<IdentitySecretKeyIBBE> identitySecretKey =
        std::make_unique<IdentitySecretKeyIBBE>(identity, (uint8_t *) serialized);

    if (!identitySecretKey->isValid()) {
      throw std::runtime_error(
          "Read in identity secret key for identity " + std::to_string(identity)
              + "not valid");
    }

    identitySecretKeys[identity] = move(identitySecretKey);

    // Read new line character.
    file.get();
  }
  file.close();
}

IdentitySecretKey *
SecretKeyIBBE::getIdentitySecretKeyForIdentity(bf::digest identity) {
  if (identitySecretKeys.find(identity) != identitySecretKeys.end()) {
    return identitySecretKeys.at(identity).get();
  }
  return nullptr;
}

void SecretKeyIBBE::writeToFile(const std::string fileName) {
  std::ofstream file(fileName);
  if (file.is_open()) {
    for (bf::digest identity = 0; identity < bloomFilterSize; identity++) {
      IdentitySecretKey
          *identitySecretKey = getIdentitySecretKeyForIdentity(identity);
      const uint8_t *serialized = identitySecretKey->getSerialized();
      for (size_t i = 0; i < identitySecretKey->getSerializedSize(false); i++) {
        file << serialized[i];
      }
      file << "\n";
    }
  }
  file.close();
}

void SecretKeyIBBE::generateIdentitySecretKeys(bf::digest startIdentity,
    bf::digest endIdentity, bfe::identitySecretKeyMap *identitySecretKeys,
    std::mutex *mutex, del07_master_secret_key_t *const msk) {
  core_set(del07_context);

  // For each i in [startIdentity, endIdentity-1] create a new
  // IdentitySecretKeyIBBE and store it in the map of identitySecretKeys.
  for (bf::digest identity = startIdentity; identity < endIdentity;
       ++identity) {
    std::unique_ptr<IdentitySecretKeyIBBE> identitySecretKey =
        std::make_unique<IdentitySecretKeyIBBE>(identity, msk);

    if (!identitySecretKey->isValid()) {
      throw std::runtime_error(
          "Identity secret key for identity " + std::to_string(identity)
              + "not valid");
    }

    mutex->lock();
    (*identitySecretKeys)[identity] = move(identitySecretKey);
    mutex->unlock();
  }
}
