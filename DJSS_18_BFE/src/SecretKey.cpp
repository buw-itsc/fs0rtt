// Copyright [2018] PG 0-RTT

#include "SecretKey.h"

using bfe::SecretKey;

SecretKey::SecretKey(const unsigned long long bloomFilterSize)
    : bloomFilterSize(bloomFilterSize) {}

void SecretKey::deleteIdentitySecretKeys(
    const std::vector<bf::digest> &identities) {
  for (auto &identity : identities) {
    // Check if identity was already deleted.
    if (identitySecretKeys.find(identity) != identitySecretKeys.end()) {
      identitySecretKeys.erase(identity);
    }
  }
}
