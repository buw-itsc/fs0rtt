// Copyright [2018] PG 0-RTT

extern "C" {
#include <relic/relic.h>
}

#include "BFE_HIBE/SecretKeyHIBE.h"

#include <bbg05hibe/BBG_05_HIBE_global.h>
#include <bbg05hibe/BBG_05_HIBE_util.h>
#include <thread>
#include <iostream>

#include "BFE_HIBE/IdentitySecretKeyHIBEInnerNode.h"
#include "BFE_HIBE/IdentitySecretKeyHIBELeafNode.h"



using namespace bfe;

SecretKeyHIBE::SecretKeyHIBE(const unsigned long long bloomFilterSize,
                             const std::unique_ptr<bbg05_master_key_t> bbgMasterSecretKey,
                             const PublicKeyHIBE *publicKey) : SecretKey(
    bloomFilterSize) {

  std::unique_ptr<IdentitySecretKeyHIBEInnerNode> parentIdentitySecretKey;

  // The maximum depth of the HIBE - 2 is the binary logarithm of the number of
  // timesteps since the last two levels are needed for the bloom filter keys
  // and the verification keys
  unsigned logNumberTimesteps = publicKey->getPublicKeyHIBE()->max_depth - 2;

  // Go down the left most branch of the tree, i.e. deriving secret keys for
  // identities 0^t for t in [1, logNumberTimesteps]. In the process also derive
  // the secret keys for the identities 0^t|1, i.e. the secret keys which will
  // be needed for future timesteps. After generating both children of a secret
  // key, delete it.
  for (unsigned depth = 1; depth <= logNumberTimesteps; depth++) {
    std::unique_ptr<IdentitySecretKeyHIBEInnerNode> leftChildIdentitySecretKey;

    // Identity for left child: 0^depth.
    auto *leftChildIdentity = (bbg05_identity_t *) malloc(
        offsetof(bbg05_identity_t, id) + sizeof(unsigned) * depth);
    leftChildIdentity->depth = depth;
    for (size_t i = 0; i < depth; i++) {
      leftChildIdentity->id[i] = 0;
    }

    // If depth = 1 secret key has to be derived from master key else from
    // parent secret key.
    if (depth == 1) {
      leftChildIdentitySecretKey =
          std::make_unique<IdentitySecretKeyHIBEInnerNode>(leftChildIdentity,
                                                           bbgMasterSecretKey.get(),
                                                           publicKey);
    } else {
      leftChildIdentitySecretKey =
          std::make_unique<IdentitySecretKeyHIBEInnerNode>(leftChildIdentity,
                                                           parentIdentitySecretKey->getSecretKeyHIBE(),
                                                           publicKey);
    }

    if (!leftChildIdentitySecretKey->isValid()) {
      // Convert the bbg identity to a printable string.
      std::string identityAsString;
      for (size_t i = 0; i < leftChildIdentity->depth; i++) {
        identityAsString += std::to_string(leftChildIdentity->id[i]);
      }
      throw std::runtime_error(
          "Could not extract secret key for identity " + identityAsString);
    }

    // Free memory allocated for leftChildIdentity.
    delete leftChildIdentity;

    std::unique_ptr<IdentitySecretKeyHIBEInnerNode> rightChildIdentitySecretKey;

    // Identity for the right child: 0^(depth-1)|1.
    auto *rightChildIdentity = (bbg05_identity_t *) malloc(
        offsetof(bbg05_identity_t, id) + sizeof(unsigned) * depth);
    rightChildIdentity->depth = depth;
    for (size_t i = 0; i < depth - 1; i++) {
      rightChildIdentity->id[i] = 0;
    }
    rightChildIdentity->id[depth - 1] = 1;

    // If depth = 1 secret key has to be derived from master key else from
    // parent secret key.
    if (depth == 1) {
      rightChildIdentitySecretKey =
          std::make_unique<IdentitySecretKeyHIBEInnerNode>(rightChildIdentity,
                                                           bbgMasterSecretKey.get(),
                                                           publicKey);
    } else {
      rightChildIdentitySecretKey =
          std::make_unique<IdentitySecretKeyHIBEInnerNode>(rightChildIdentity,
                                                           parentIdentitySecretKey->getSecretKeyHIBE(),
                                                           publicKey);
    }

    if (!rightChildIdentitySecretKey->isValid()) {
      // Convert the bbg identity to a printable string.
      std::string identityAsString;
      for (size_t i = 0; i < rightChildIdentity->depth; i++) {
        identityAsString += std::to_string(rightChildIdentity->id[i]);
      }
      throw std::runtime_error(
          "Could not extract secret key for identity " + identityAsString);
    }

    // Save the right child on the stack of secret keys needed for future time
    // steps.
    identitySecretKeysTime.push(std::make_pair(rightChildIdentity,
                                               move(rightChildIdentitySecretKey)));

    // The left child is the new parent.
    parentIdentitySecretKey = move(leftChildIdentitySecretKey);
  }

  // For the first timestep for each index of the Bloom Filter create one
  // secret key.

  // Identity
  auto *bbgIdentity = (bbg05_identity_t *) malloc(
      offsetof(bbg05_identity_t, id) + sizeof(unsigned) * logNumberTimesteps);
  bbgIdentity->depth = logNumberTimesteps;
  for (size_t i = 0; i < logNumberTimesteps; i++) {
    bbgIdentity->id[i] = 0;
  }

  // If number of timesteps is 1 all keys have to be derived from the master
  // key.
  if (logNumberTimesteps == 0) {
    throw std::runtime_error("1 timestep not yet implemented");
  } else {
    generateIdentitySecretKeys(bbgIdentity,
                               parentIdentitySecretKey->getSecretKeyHIBE(),
                               publicKey);
  }

  free(bbgIdentity);

  // Delete the master key.
  bbg05_set_master_key_to_zero(bbgMasterSecretKey.get());
}

IdentitySecretKey *
SecretKeyHIBE::getIdentitySecretKeyForIdentity(bf::digest identity) {
  if (identitySecretKeys.find(identity) != identitySecretKeys.end()) {
    return identitySecretKeys.at(identity).get();
  }

  return nullptr;
}

void SecretKeyHIBE::punctureTimestep(const PublicKeyHIBE *publicKey) {

  // Clear the map storing the secret keys for the previous timestep
  identitySecretKeys.clear();

  // The top element of the stack is the secret key, from which the secret keys
  // for the next timestep can be derived
  auto parentIdentity = identitySecretKeysTime.top().first;
  auto parentIdentitySecretKey = move(identitySecretKeysTime.top().second);
  identitySecretKeysTime.pop();

  auto parentIdentitySecretKeyHIBE =
      dynamic_cast<IdentitySecretKeyHIBEInnerNode *>(parentIdentitySecretKey.get());
  std::unique_ptr<IdentitySecretKeyHIBEInnerNode> leftChildIdentitySecretKey;
  std::unique_ptr<IdentitySecretKeyHIBEInnerNode> rightChildIdentitySecretKey;

  // As long as num_delegatable_levels > 2, we are not at the level at which the
  // secret keys for bloom filter are generated.
  while (parentIdentitySecretKeyHIBE->getSecretKeyHIBE()->num_delegatable_levels
      > 2) {

    unsigned parentIdentityDepth = parentIdentity->depth;

    // Identity for left child: parentIdentity|0.
    auto *leftChildIdentity = (bbg05_identity_t *) malloc(
        offsetof(bbg05_identity_t, id) + sizeof(unsigned) * parentIdentityDepth
            + 1);
    leftChildIdentity->depth = parentIdentityDepth + 1;
    for (size_t i = 0; i < parentIdentityDepth; i++) {
      leftChildIdentity->id[i] = parentIdentity->id[i];
    }
    leftChildIdentity->id[parentIdentityDepth] = 0;

    leftChildIdentitySecretKey =
        std::make_unique<IdentitySecretKeyHIBEInnerNode>(leftChildIdentity,
                                                         parentIdentitySecretKeyHIBE->getSecretKeyHIBE(),
                                                         publicKey);

    if (!leftChildIdentitySecretKey->isValid()) {
      // Convert the bbg identity to a printable string.
      std::string identityAsString;
      for (size_t i = 0; i < leftChildIdentity->depth; i++) {
        identityAsString += std::to_string(leftChildIdentity->id[i]);
      }
      throw std::runtime_error(
          "Could not extract secret key for identity " + identityAsString);
    }

    // Identity for the right child: parentIdentity|1.
    auto *rightChildIdentity = (bbg05_identity_t *) malloc(
        offsetof(bbg05_identity_t, id) + sizeof(unsigned) * parentIdentityDepth
            + 1);
    rightChildIdentity->depth = parentIdentityDepth + 1;
    for (size_t i = 0; i < parentIdentityDepth; i++) {
      rightChildIdentity->id[i] = parentIdentity->id[i];
    }
    rightChildIdentity->id[parentIdentityDepth] = 1;

    rightChildIdentitySecretKey =
        std::make_unique<IdentitySecretKeyHIBEInnerNode>(rightChildIdentity,
                                                         parentIdentitySecretKeyHIBE->getSecretKeyHIBE(),
                                                         publicKey);

    if (!rightChildIdentitySecretKey->isValid()) {
      // Convert the bbg identity to a printable string.
      std::string identityAsString;
      for (size_t i = 0; i < rightChildIdentity->depth; i++) {
        identityAsString += std::to_string(rightChildIdentity->id[i]);
      }
      throw std::runtime_error(
          "Could not extract secret key for identity " + identityAsString);
    }

    // Save the right child on the stack of secret keys needed for future time
    // steps.
    identitySecretKeysTime.push(std::make_pair(rightChildIdentity,
                                               move(rightChildIdentitySecretKey)));

    // Delete the memory allocated for the parent identity.
    delete parentIdentity;

    // The left child is the new parent.
    parentIdentitySecretKey = move(leftChildIdentitySecretKey);
    parentIdentitySecretKeyHIBE =
        dynamic_cast<IdentitySecretKeyHIBEInnerNode *>(parentIdentitySecretKey.get());
    parentIdentity = leftChildIdentity;
  }

  // For the current timestep for each index of the Bloom Filter create one
  // secret key.
  generateIdentitySecretKeys(parentIdentity, parentIdentitySecretKeyHIBE->getSecretKeyHIBE(), publicKey);
}

void SecretKeyHIBE::generateIdentitySecretKeys(bbg05_identity_t *parentIdentity,
                                               bbg05_secret_key_inner_node_t *parentSecretKey,
                                               const PublicKeyHIBE *publicKey) {
  unsigned numberOfThreads = std::thread::hardware_concurrency();
  std::vector<std::thread> threads(numberOfThreads - 1);
  std::mutex mutex;

  // Split up the n identity secret keys that have to be computed between k
  // threads. Each thread computes n/k identity secret keys. For first n/k
  // identities no extra thread is created and they are computed in this master
  // thread.
  for (size_t i = 1; i < numberOfThreads; i++) {
    threads[i - 1] = std::thread(generateRangeOfIdentitySecretKeys,
                                 i * bloomFilterSize / numberOfThreads,
                                 (i + 1) * bloomFilterSize / numberOfThreads,
                                 parentIdentity,
                                 parentSecretKey,
                                 publicKey,
                                 &identitySecretKeys,
                                 &mutex);
  }

  generateRangeOfIdentitySecretKeys(0, bloomFilterSize / numberOfThreads,
      parentIdentity, parentSecretKey, publicKey, &identitySecretKeys, &mutex);

  for (size_t i = 0; i < numberOfThreads - 1; i++) {
    threads[i].join();
  }
}

void SecretKeyHIBE::generateRangeOfIdentitySecretKeys(bf::digest startIdentity,
                                                      bf::digest endIdentity,
                                                      bbg05_identity_t *parentIdentity,
                                                      bbg05_secret_key_inner_node_t *parentSecretKey,
                                                      const PublicKeyHIBE *publicKey,
                                                      bfe::identitySecretKeyMap *identitySecretKeys,
                                                      std::mutex *mutex) {
  core_set(bbg05_context);

  unsigned parentIdentityDepth = parentIdentity->depth;
  unsigned bbgIdentityDepth = parentIdentityDepth + 1;

  // Identity that we want to derive an IdentitySecretKey for
  auto *bbgIdentity = (bbg05_identity_t *) malloc(offsetof(bbg05_identity_t, id)
                                                      + sizeof(unsigned)
                                                          * bbgIdentityDepth);

  // Prefix is parent identity
  for (size_t i = 0; i < parentIdentityDepth; i++) {
    bbgIdentity->id[i] = parentIdentity->id[i];
  }
  bbgIdentity->depth = bbgIdentityDepth;

  // For each identity in [startIdentity, endIdentity-1] create a new
  // IdentitySecretKeyHIBEInnerNode and store it in the map of identitySecretKeys.
  for (bf::digest identity = startIdentity; identity < endIdentity; identity++) {
    // Set last component of identity to bloom filter index
    bbgIdentity->id[bbgIdentityDepth - 1] = (unsigned) identity;

    std::unique_ptr<IdentitySecretKeyHIBEInnerNode> identitySecretKey =
        std::make_unique<IdentitySecretKeyHIBEInnerNode>(bbgIdentity,
                                                         parentSecretKey,
                                                         publicKey);

    if (!identitySecretKey->isValid()) {
      throw std::runtime_error(
          "Identity secret key for identity with last component" +
              std::to_string(identity) + "not valid");
    }

    mutex->lock();
    (*identitySecretKeys)[identity] = move(identitySecretKey);
    mutex->unlock();
  }

  free(bbgIdentity);
}
