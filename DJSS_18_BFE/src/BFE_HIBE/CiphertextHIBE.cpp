#include "BFE_HIBE/CiphertextHIBE.h"
#include <cmath>
using namespace bfe;

CiphertextHIBE::CiphertextHIBE(std::vector<std::unique_ptr<bbg05_ciphertext_t>> bbgCiphertexts,
                               const uint8_t *r,
                               std::vector<uint8_t *> keys,
                               std::vector<bool> timestep)
    : Ciphertext(r),
      ciphertextsHIBE(move(bbgCiphertexts)),
      keys(move(keys)),
      timestep(move(timestep)) {}

CiphertextHIBE::CiphertextHIBE(const uint8_t *serialized,
                               const unsigned long numberHashFunctions,
                               const unsigned numberTimesteps)
    : Ciphertext(serialized
        + numberHashFunctions * (BBG_05_CIPHERTEXT_SIZE + SECURITY_PARAMETER)
        + sizeof(unsigned)),
    ciphertextsHIBE(move(
        deserializeHIBECiphertexts(serialized, numberHashFunctions))),
    keys(deserializeHIBEKeys(
        serialized + numberHashFunctions * BBG_05_CIPHERTEXT_SIZE,
        numberHashFunctions)),
    timestep(deserializeTimestep(
        serialized + numberHashFunctions * (BBG_05_CIPHERTEXT_SIZE +
        SECURITY_PARAMETER), numberTimesteps)) {}

CiphertextHIBE::~CiphertextHIBE() {
  // Delete the heap allocated data.
  delete[] r;

  for (size_t i = 0; i < ciphertextsHIBE.size(); i++) {
    bbg05_set_ciphertext_to_zero(ciphertextsHIBE[i].get());
    delete[] keys[i];
  }
}

const std::vector<bbg05_ciphertext_t *> CiphertextHIBE::getCiphertexts() const {
  std::vector<bbg05_ciphertext_t *> ciphertexts;
  for (size_t i = 0; i < ciphertextsHIBE.size(); i++) {
    ciphertexts.push_back(ciphertextsHIBE[i].get());
  }
  return ciphertexts;
}

const std::vector<uint8_t *> CiphertextHIBE::getKeys() const {
  return keys;
}

const std::vector<bool> CiphertextHIBE::getTimestep() const {
  return timestep;
}

bool CiphertextHIBE::isValid() const {
  int valid;
  for (size_t i = 0; i < ciphertextsHIBE.size(); i++) {
    bbg05_ciphertext_is_valid(&valid, ciphertextsHIBE[i].get());
    if(!valid) {
      return false;
    }
  }
  return true;
}

uint8_t *CiphertextHIBE::getSerialized() {
  // pairs of ciphertextHIBE and corresponding key, randomness, timestep of encapsulation
  auto *serialized =
      new uint8_t[ciphertextsHIBE.size() * (BBG_05_CIPHERTEXT_SIZE + SECURITY_PARAMETER) + SECURITY_PARAMETER
          + sizeof(unsigned)];

  for (size_t i = 0; i < ciphertextsHIBE.size(); i++) {
    uint8_t *serializedBBG05Ciphertext;
    bbg05_serialize_ciphertext(&serializedBBG05Ciphertext,
                               ciphertextsHIBE[i].get());
    memcpy(serialized + i * BBG_05_CIPHERTEXT_SIZE,
           serializedBBG05Ciphertext,
           (size_t) BBG_05_CIPHERTEXT_SIZE);
    free(serializedBBG05Ciphertext);
  }

  for (size_t i = 0; i < keys.size(); i++) {
    memcpy(serialized + ciphertextsHIBE.size() * BBG_05_CIPHERTEXT_SIZE + i * SECURITY_PARAMETER,
           keys[i],
           SECURITY_PARAMETER);
  }

  unsigned timestepAsNumber = 0;
  for (size_t i = 0; i < timestep.size(); i++) {
    if (timestep[i]) {
      timestepAsNumber |= 1 << i;
    }
  }

  memcpy(
      serialized + ciphertextsHIBE.size() * BBG_05_CIPHERTEXT_SIZE + keys.size() * SECURITY_PARAMETER,
      &timestepAsNumber,
      sizeof(unsigned));

  memcpy(
      serialized + ciphertextsHIBE.size() * BBG_05_CIPHERTEXT_SIZE + keys.size() * SECURITY_PARAMETER
          + sizeof(unsigned), r, SECURITY_PARAMETER);

  return serialized;
}

bool CiphertextHIBE::operator==(const bfe::CiphertextHIBE &other) const {
  if (getCiphertexts().size() != other.getCiphertexts().size()) {
    return false;
  }
  for (size_t i = 0; i < getCiphertexts().size(); i++) {
    if (!bbg05_ciphertexts_are_equal(getCiphertexts()[i], other.getCiphertexts()[i])) {
      return false;
    }
  }

  if (getKeys().size() != other.getKeys().size()) {
    return false;
  }
  for (size_t i = 0; i < getKeys().size(); i++) {
    if (memcmp(getKeys()[i], other.getKeys()[i], SECURITY_PARAMETER)) {
      return false;
    }
  }

  if (getTimestep().size() != other.getTimestep().size()) {
    return false;
  }
  for (size_t i = 0; i < getTimestep().size(); i++) {
    if (getTimestep()[i] != other.getTimestep()[i]) {
      return false;
    }
  }

  return memcmp(getR(), other.getR(), SECURITY_PARAMETER) == 0;
}

bool CiphertextHIBE::operator!=(const bfe::CiphertextHIBE &other) const {
  return !(*this == other);
}

const unsigned long CiphertextHIBE::getSerializedSize() const {
  return ciphertextsHIBE.size() * (BBG_05_CIPHERTEXT_SIZE + SECURITY_PARAMETER)
      + SECURITY_PARAMETER + sizeof(unsigned);
}

std::vector<std::unique_ptr<bbg05_ciphertext_t>> CiphertextHIBE::deserializeHIBECiphertexts(
    const uint8_t *serialized, const unsigned long numberHashFunctions) {
  std::vector<std::unique_ptr<bbg05_ciphertext_t>> ciphertexts(numberHashFunctions);
  bbg05_ciphertext_t *ciphertext;
  for (size_t i = 0; i < numberHashFunctions; i++) {
    bbg05_deserialize_ciphertext(&ciphertext, serialized + i * BBG_05_CIPHERTEXT_SIZE);
    std::unique_ptr<bbg05_ciphertext_t> ciphertextUniquePtr(ciphertext);
    ciphertexts[i] = move(ciphertextUniquePtr);
  }
  return ciphertexts;
}

std::vector<uint8_t *> CiphertextHIBE::deserializeHIBEKeys(
    const uint8_t *serialized, const unsigned long numberHashFunctions) {
  std::vector<uint8_t *> keys(numberHashFunctions);
  for (size_t i = 0; i < numberHashFunctions; i++) {
    auto key = new uint8_t[SECURITY_PARAMETER];
    memcpy(key, serialized + i * SECURITY_PARAMETER, SECURITY_PARAMETER);
    keys[i] = key;
  }
  return keys;
}

std::vector<bool> CiphertextHIBE::deserializeTimestep(const uint8_t *serialized, const unsigned numberTimesteps) {
  auto *timestepAsByteArray = new uint8_t[(unsigned) log2(numberTimesteps)];
  memcpy(timestepAsByteArray, serialized, sizeof(unsigned));
  std::vector<bool> timestep;
  for (size_t i = 0; i < (unsigned) log2(numberTimesteps); i++) {
    timestep.push_back((timestepAsByteArray[i/8] & (1 << i % 8)) != 0);
  }
  return timestep;
}
