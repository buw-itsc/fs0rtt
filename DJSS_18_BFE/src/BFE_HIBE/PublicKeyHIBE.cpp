#include "BFE_HIBE/PublicKeyHIBE.h"

using namespace bfe;

PublicKeyHIBE::PublicKeyHIBE(std::unique_ptr<bbg05_public_params_t> pk,
                             unsigned int numberHashFunctions,
                             unsigned long long bloomFilterSize)
    : PublicKey(bloomFilterSize), publicKeyHIBE(move(pk)), numberHashFunctions(numberHashFunctions) {}

PublicKeyHIBE::PublicKeyHIBE(const uint8_t *serialized)
    : PublicKey(serialized),
      numberHashFunctions(deserializeNumberHashFunctions(
          serialized + sizeof(unsigned long long))),
      publicKeyHIBE(deserializeHIBEPublicKey(
          serialized + sizeof(unsigned long long) + sizeof(unsigned))) {}

PublicKeyHIBE::~PublicKeyHIBE() {
  bbg05_set_public_params_to_zero(publicKeyHIBE.get());
}

bbg05_public_params_t *PublicKeyHIBE::getPublicKeyHIBE() const {
  return publicKeyHIBE.get();
}

const unsigned int PublicKeyHIBE::getNumberHashFunctions() const {
  return numberHashFunctions;
}

bool PublicKeyHIBE::isValid() const {
  int valid;
  bbg05_public_params_are_valid(&valid, publicKeyHIBE.get());
  return (bool) valid;
}

uint8_t *PublicKeyHIBE::getSerialized() const {
  auto *serialized = new uint8_t[getSerializedSize()];
  memcpy(serialized, &bloomFilterSize, sizeof(unsigned long long));
  memcpy(serialized + sizeof(unsigned long long), &numberHashFunctions, sizeof(unsigned));
  uint8_t *serializedPK;
  bbg05_serialize_public_params(&serializedPK, publicKeyHIBE.get());
  memcpy(serialized + sizeof(unsigned long long) + sizeof(unsigned), serializedPK, bbg05_get_public_params_size(publicKeyHIBE.get()));

  return serialized;
}

const unsigned long PublicKeyHIBE::getSerializedSize() const {
  return sizeof(unsigned long long) + sizeof(unsigned) + bbg05_get_public_params_size(publicKeyHIBE.get());
}

bool PublicKeyHIBE::operator==(const PublicKeyHIBE &other) const {

  const bbg05_public_params_t *pkIBBE = getPublicKeyHIBE();
  const bbg05_public_params_t *otherPkIBBE = other.getPublicKeyHIBE();

  bool equal = (bool) bbg05_public_params_are_equal(pkIBBE, otherPkIBBE);
  equal &= this->bloomFilterSize == other.bloomFilterSize;
  equal &= this->numberHashFunctions == other.numberHashFunctions;
  return equal;
}

bool PublicKeyHIBE::operator!=(const PublicKeyHIBE &other) const {
  return !(*this == other);
}

const unsigned PublicKeyHIBE::deserializeNumberHashFunctions(const uint8_t *serialized) {
  unsigned int numberHasFunctions;
  memcpy(&numberHasFunctions, serialized, sizeof(unsigned));
  return numberHasFunctions;
}

std::unique_ptr<bbg05_public_params_t> PublicKeyHIBE::deserializeHIBEPublicKey(
    const uint8_t *serialized) {
  bbg05_public_params_t *publicKey;
  bbg05_deserialize_public_params(&publicKey, serialized);
  std::unique_ptr<bbg05_public_params_t> publicKeyUniquePtr(publicKey);

  return move(publicKeyUniquePtr);
}