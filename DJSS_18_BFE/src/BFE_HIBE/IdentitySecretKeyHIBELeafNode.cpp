#include <string>
#include "BFE_HIBE/IdentitySecretKeyHIBELeafNode.h"

using namespace bfe;

IdentitySecretKeyHIBELeafNode::IdentitySecretKeyHIBELeafNode(bbg05_identity_t *const identity,
                                                             bbg05_secret_key_inner_node_t *const parentSecretKey,
                                                             const PublicKeyHIBE *publicKeyHIBE) {

  auto *skHIBE = new bbg05_secret_key_leaf_node_t();

  bbg05_public_params_t *bbg05PublicParams = publicKeyHIBE->getPublicKeyHIBE();

  // Derive the secret key for the identity.
  int statusCodeKeyGeneration =
      bbg05_key_generation_leaf_node_from_parent_full_identity(&skHIBE,
                                            parentSecretKey,
                                            identity,
                                            bbg05PublicParams);
  if (statusCodeKeyGeneration) {
    // Convert the bbg identity to a printable string.
    std::string identityAsString;
    for (size_t i = 0; i < identity->depth; i++) {
      identityAsString += std::to_string(identity->id[i]);
    }
    throw std::runtime_error(
        "Could not extract secret key for identity " + identityAsString);
  }

  secretKeyHIBE.reset(skHIBE);
}

IdentitySecretKeyHIBELeafNode::IdentitySecretKeyHIBELeafNode(bbg05_identity_t *const identity,
                                                             bbg05_master_key_t *const masterSecretKey,
                                                             const PublicKeyHIBE *publicKeyHIBE) {
  auto *skHIBE = new bbg05_secret_key_leaf_node_t();

  bbg05_public_params_t *bbg05PublicParams = publicKeyHIBE->getPublicKeyHIBE();

  // Derive the secret key for the identity.
  int statusCodeKeyGeneration =
      bbg05_key_generation_leaf_node_from_master_key(&skHIBE,
                                               masterSecretKey,
                                               identity,
                                               bbg05PublicParams);
  if (statusCodeKeyGeneration) {
    // Convert the bbg identity to a printable string.
    std::string identityAsString;
    for (size_t i = 0; i < identity->depth; i++) {
      identityAsString += std::to_string(identity->id[i]);
    }
    throw std::runtime_error(
        "Could not extract secret key for identity " + identityAsString);
  }

  secretKeyHIBE.reset(skHIBE);
}

IdentitySecretKeyHIBELeafNode::~IdentitySecretKeyHIBELeafNode() {

  // Delete the bbg05_secret_key which also sets the allocated memory to 0.
  bbg05_set_secret_key_leaf_node_to_zero(secretKeyHIBE.get());
}

bbg05_secret_key_leaf_node_t *IdentitySecretKeyHIBELeafNode::getSecretKeyHIBE() {
  return secretKeyHIBE.get();
}

bool IdentitySecretKeyHIBELeafNode::isValid() const {
  int valid;
  bbg05_secret_key_leaf_node_is_valid(&valid, secretKeyHIBE.get());
  return (bool) valid;
}