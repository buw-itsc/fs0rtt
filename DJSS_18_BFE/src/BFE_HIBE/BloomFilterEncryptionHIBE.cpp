#include <bbg05hibe/BBG_05_HIBE.h>
#include "BFE_HIBE/BloomFilterEncryptionHIBE.h"
#include "BFE_HIBE/CiphertextHIBE.h"
#include "BFE_HIBE/IdentitySecretKeyHIBEInnerNode.h"
#include "BFE_HIBE/KeyHIBE.h"
#include "BFE_HIBE/PublicKeyHIBE.h"
#include "BFE_HIBE/SecretKeyHIBE.h"

using namespace bfe;

BloomFilterEncryptionHIBE::BloomFilterEncryptionHIBE(const unsigned long long m,
                                                     const unsigned int k,
                                                     const unsigned numberTimesteps)
    : BloomFilterEncryption(m, k), numberTimesteps(numberTimesteps) {
  // Initialize the del07 library.
  int initStatus = bbg05_init();
  if (initStatus) {
    throw std::runtime_error("Could not initialize del07");
  }

  // Set the current timestep to 0.
  currentTimestep =
      new std::vector<bool>((unsigned) log2(numberTimesteps), false);
}

BloomFilterEncryptionHIBE::BloomFilterEncryptionHIBE(const uint8_t *serializedPublicKey)
    : BloomFilterEncryption() {
  // Initialize the bbg05 library.
  int initStatus = bbg05_init();
  if (initStatus) {
    throw std::runtime_error("Could not initialize bbg05");
  }

  // Deserialized the public key.
  publicKey = std::make_unique<PublicKeyHIBE>(serializedPublicKey);
  if (!publicKey->isValid()) {
    throw std::invalid_argument("Public key not valid");
  }

  m = publicKey->getBloomFilterSize();
  auto publicKeyHIBE = dynamic_cast<PublicKeyHIBE *>(publicKey.get());
  k = publicKeyHIBE->getNumberHashFunctions();
  numberTimesteps = (unsigned) pow(2, publicKeyHIBE->getPublicKeyHIBE()->max_depth-2);

  // Set the current timestep to 0.
  currentTimestep =
      new std::vector<bool>((unsigned) log2(numberTimesteps), false);

  countingBloomFilter =
      std::make_unique<bf::counting_bloom_filter>(bf::make_hasher(k), m, COUNTING_BF_BITS_PER_CELL, false);
}

const unsigned int BloomFilterEncryptionHIBE::getNumberTimesteps() const {
  return numberTimesteps;
}

void BloomFilterEncryptionHIBE::keyGeneration() {
  // Number of timesteps has to be a power of 2.
  if (numberTimesteps == 0 || (numberTimesteps & (numberTimesteps - 1)) != 0) {
    throw std::invalid_argument(
        "Can not generate keys since the number of timesteps is not a power of 2");
  }

  // Generate the master secret key and public key of bbg05.
  auto *masterSecretKeyHIBE = new bbg05_master_key_t();
  auto *publicParamsHIBE = new bbg05_public_params_t();

  int setupStatus = bbg05_setup(&masterSecretKeyHIBE,
                                &publicParamsHIBE,
                                (unsigned) log2(numberTimesteps) + 1);
  if (setupStatus) {
    throw std::runtime_error("Error in setup of bbg05");
  }
  int mk_valid;
  bbg05_master_key_is_valid(&mk_valid, masterSecretKeyHIBE);
  if (!mk_valid) {
    throw std::runtime_error("Master secret key not valid");
  }

  std::unique_ptr<bbg05_master_key_t>
      masterSecretKeyHIBEUniquePtr(masterSecretKeyHIBE);
  std::unique_ptr<bbg05_public_params_t>
      publicKeyHIBEUniquePtr(publicParamsHIBE);

  // Create secret key and public key member variables.
  publicKey =
      std::make_unique<PublicKeyHIBE>(move(publicKeyHIBEUniquePtr), k, m);
  auto *publicKeyHIBE = dynamic_cast<PublicKeyHIBE *>(publicKey.get());
  secretKey = std::make_unique<SecretKeyHIBE>(m,
                                              move(masterSecretKeyHIBEUniquePtr),
                                              publicKeyHIBE);

  if (!publicKey->isValid()) {
    throw std::runtime_error("Public key not valid");
  }
}

std::pair<std::unique_ptr<Key>,
          std::unique_ptr<Ciphertext>> BloomFilterEncryptionHIBE::encapsulate() {
  uint8_t *keyByteString = random256Bits();
  uint8_t *randomBytes = random256Bits();

  auto publicKeyHIBE = dynamic_cast<PublicKeyHIBE *>(publicKey.get());

  // Generate the bloom filter indices from the randomness.
  std::vector<bf::digest> encapsulatingIdentitiesAsVector =
      generateIdentitiesAsVector(randomBytes);

  std::vector<std::unique_ptr<bbg05_ciphertext_t>> ciphertextsHIBE;
  std::vector<uint8_t *> keysHIBE;

  for (size_t i = 0; i < k; i++) {
    // Generate the identity, i.e. current timestep and bloom filter index.
    auto *identity = (bbg05_identity_t *) malloc(offsetof(bbg05_identity_t, id)
                                                     + sizeof(unsigned)
                                                         * (currentTimestep->size()
                                                             + 1));
    identity->depth = (unsigned) currentTimestep->size() + 1;
    for (size_t j = 0; j < currentTimestep->size(); j++) {
      identity->id[j] = (unsigned) (*currentTimestep)[j];
    }
    identity->id[identity->depth - 1] =
        (unsigned) encapsulatingIdentitiesAsVector[i];

    auto *ciphertextHIBE = new bbg05_ciphertext_t();
    auto *encapsulatedKeyHIBE = new bbg05_key_t();

    // Encapsulate a key for the computed identity.
    int encapsulateStatus = bbg05_encapsulate(&ciphertextHIBE,
                                              &encapsulatedKeyHIBE,
                                              publicKeyHIBE->getPublicKeyHIBE(),
                                              identity);

    if (encapsulateStatus) {
      throw std::runtime_error("Could not encapsulate a session key");
    }

    std::unique_ptr<bbg05_ciphertext_t> ciphertextHIBEUniquePtr(ciphertextHIBE);
    ciphertextsHIBE.push_back(move(ciphertextHIBEUniquePtr));

    // Compute random key xor key returned by HIBE scheme.
    auto *keyXorKeyHIBE = new uint8_t[SECURITY_PARAMETER];
    for (size_t j = 0; j < SECURITY_PARAMETER; j++) {
      keyXorKeyHIBE[j] = keyByteString[j] ^ encapsulatedKeyHIBE->k[j];
    }
    keysHIBE.push_back(keyXorKeyHIBE);
  }

  std::unique_ptr<CiphertextHIBE> ciphertext =
      std::make_unique<CiphertextHIBE>(move(ciphertextsHIBE),
                                       randomBytes,
                                       keysHIBE,
                                       *currentTimestep);

  std::unique_ptr<KeyHIBE> key = std::make_unique<KeyHIBE>(keyByteString);

  if (!ciphertext->isValid()) {
    throw std::runtime_error("(BloomFilterEncryptionHIBE::encapsulate): "
                             "Ciphertext not valid");
  }

  return std::pair<std::unique_ptr<KeyHIBE>, std::unique_ptr<CiphertextHIBE>>(
      move(key),
      move(ciphertext));
}

std::unique_ptr<Key> BloomFilterEncryptionHIBE::decapsulate(const bfe::Ciphertext *ciphertext) {
  if (!ciphertext->isValid()) {
    throw std::invalid_argument("(BloomFilterEncryptionHIBE::decapsulate):"
                                " Ciphertext not valid");
  }

  auto ciphertextHIBE = dynamic_cast<const CiphertextHIBE *>(ciphertext);
  std::vector<bool> timestepOfEncapsulation = ciphertextHIBE->getTimestep();

  // Check if  ciphertext was encapsulated in a different timestep.
  for (size_t i = 0; i < timestepOfEncapsulation.size(); i++) {
    if (timestepOfEncapsulation[i] != (*currentTimestep)[i]) {
      throw std::invalid_argument(
          "Ciphertext was encapsulated in a different timstep");
    }
  }

  const uint8_t *randomness = ciphertextHIBE->getR();

  // Convert array to vector so that in can be put into lookup
  std::vector<uint8_t> randomnessAsVector(
      randomness, randomness + SECURITY_PARAMETER);

  // The ciphertext can not be decapsulated if all secret keys for which the
  // ciphertext was encapsulated are deleted. This is the case if lookup for
  // this randomness returns > 0. If it returns 0 there must be a secret key
  // with which the ciphertext can be decapsulated.
  size_t bloomFilterCheck = countingBloomFilter->lookup(randomnessAsVector);
  if (bloomFilterCheck > 0) {
    return nullptr;
  } else {
    // Generate the identities as Bloom Filter indices.
    std::vector<bf::digest> encapsulatingIdentitiesAsVector =
        generateIdentitiesAsVector(randomness);

    // Find the first identity (if existing) for which the secret key has not
    // been deleted and save the index in the identities array of it.
    bf::digest decapsulatingIdentity = 0;
    unsigned decapsulatingIdentityIndex = 0;


    for (auto identity : encapsulatingIdentitiesAsVector) {
      if (countingBloomFilter->count()[identity] == 0) {
        decapsulatingIdentity = identity;
        break;
      } else {
        decapsulatingIdentityIndex++;
      }
    }


    // Get the secret key for the identity found in the previous step.
    auto secretKeyHIBE = dynamic_cast<SecretKeyHIBE *>(secretKey.get());
    IdentitySecretKey *decapsulatingIdentitySecretKey =
        secretKeyHIBE->getIdentitySecretKeyForIdentity(decapsulatingIdentity);
    auto decapsulatingIdentitySecretKeyHIBE =
        dynamic_cast<IdentitySecretKeyHIBEInnerNode *>(decapsulatingIdentitySecretKey);

    auto *publicKeyHIBE = dynamic_cast<PublicKeyHIBE *>(publicKey.get());

    // Decapsulate the ciphertext.
    auto *decapsulatedKeyHIBE = new bbg05_key_t();
    int decapsulateStatus = bbg05_decapsulate(&decapsulatedKeyHIBE,
                                              decapsulatingIdentitySecretKeyHIBE->getSecretKeyHIBE(),
                                              ciphertextHIBE->getCiphertexts()[decapsulatingIdentityIndex],
                                              publicKeyHIBE->getPublicKeyHIBE());

    if (decapsulateStatus) {
      throw std::runtime_error("Could not decapsulate the given ciphertext");
    }

    // Compute session key = decapsulatedKey xor HIBEKey for the identity found
    // in previous step.
    uint8_t decapsulatedKeyByteString[SECURITY_PARAMETER];
    for (size_t i = 0; i < SECURITY_PARAMETER; i++) {
      decapsulatedKeyByteString[i] = decapsulatedKeyHIBE->k[i]
          ^ ciphertextHIBE->getKeys()[decapsulatingIdentityIndex][i];
    }

    std::unique_ptr<Key>
        key = std::make_unique<KeyHIBE>(decapsulatedKeyByteString);

    // Puncture the secret key for the ciphertext, that was just decapsulated.
    puncture(ciphertext);

    return key;
  }
}

void BloomFilterEncryptionHIBE::punctureTimestep() {
  // Clear the bloom filter.
  countingBloomFilter->clear();

  // Puncture the secret key for the current timestep.
  auto secretKeyHIBE = dynamic_cast<SecretKeyHIBE *>(secretKey.get());
  secretKeyHIBE->punctureTimestep(dynamic_cast<PublicKeyHIBE *>(publicKey.get()));

  // Compute the next timestep.
  for (int i = (int) currentTimestep->size() - 1; i >= 0; i--) {
    (*currentTimestep)[i] = !((*currentTimestep)[i]);
    if ((*currentTimestep)[i]) {
      break;
    }
  }
}
