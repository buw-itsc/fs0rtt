#include "BFE_HIBE/KeyHIBE.h"
#include <bbg05hibe/BBG_05_HIBE.h>

using namespace bfe;

KeyHIBE::KeyHIBE(const uint8_t* key) {
  this->key = new uint8_t[SECURITY_PARAMETER];
  memcpy(this->key, key, SECURITY_PARAMETER);

}
