#ifndef DJSS_18_BFE_CIPHERTEXTHIBE_H
#define DJSS_18_BFE_CIPHERTEXTHIBE_H

#include <vector>
#include <memory>
#include <bbg05hibe/BBG_05_HIBE.h>
#include "../Ciphertext.h"

namespace bfe {

class CiphertextHIBE : public Ciphertext {
 public:
  /**
   * Creates a new CiphertextHIBE which wraps the bbg05_ciphertext and the
   * random bit string which was used to generate the identities for which
   * the bbg05_ciphertext was encrypted.
   *
   * @param c       - The bbg05_ciphertext.
   * @param r       - The random bit string from which the encapsulating
   *                  identities were generated.
   * @param keys    - The bbg
   */
  CiphertextHIBE(std::vector<std::unique_ptr<bbg05_ciphertext_t>> bbgCiphertexts,
                 const uint8_t *r,
                 std::vector<uint8_t *> keys,
                 std::vector<bool> timestep);

  /**
   * Creates a new CiphertextHIBE by deserializing the given serialized
   * ciphertext.
   *
   * @param serialized              - The serialized ciphertext.
   * @param numberHashFunctions     - The number of Hashfunctions.
   * @param numberTimesteps         - The number of timesteps.
   */
  CiphertextHIBE(const uint8_t *serialized,
                 const unsigned long numberHashFunctions,
                 const unsigned numberTimesteps);

  /**
   * Destructor which explicitly deletes the random bit string which is a
   * dynamically allocated byte array.
   */
  ~CiphertextHIBE() override;

  /**
   * Returns a pointer to a vector of the bbg05_ciphertexts.
   *
   * @return The bbg05_ciphertexts.
   */
  const std::vector<bbg05_ciphertext_t *> getCiphertexts() const;

  /**
   * Returns a pointer to a vector of the keys.
   *
   * @return The keys.
   */
  const std::vector<uint8_t *> getKeys() const;

  /**
   * Returns the timestep in which the ciphertext was encapsulated.
   *
   * @return The timestep.
   */
  const std::vector<bool> getTimestep() const;

  /**
   * Checks if the bbg05_ciphertext is valid.
   *
   * @return True if the bbg05_ciphertext is valid, false otherwise.
*/
  bool isValid() const override;

  /**
   * Returns a serialized version of this ciphertext.
   *
   * @return A uint8_t array of length 32 + number of hash functions * 386 bytes
   * containing the serialized ciphertext.
   */
  uint8_t *getSerialized() override;

  /**
   * Overload of the == operator.
   *
   * @param other       - The CiphertextHIBE with which this one is compared.
   *
   * @return True if the objects are equal, false otherwise.
   */
  bool operator==(const CiphertextHIBE &other) const;

  /**
   * Overload of the != operator.
   *
   * @param other       - The CiphertextHIBE with which this one is compared.
   *
   * @return True if the objects are not equal, false otherwise.
   */
  bool operator!=(const CiphertextHIBE &other) const;

  /**
   * Returns the size of a serialized ciphertext.
   *
   * @return the size of a serialized ciphertext.
   */
  const unsigned long getSerializedSize() const override;

 private:

  /**
   * The bbg05_ciphertext.
   */
  const std::vector<std::unique_ptr<bbg05_ciphertext_t>> ciphertextsHIBE;

  /**
   * The bbg keys xor-ed with the random key.
   */
  const std::vector<uint8_t *> keys;

  /**
   * The timestep in which this ciphertext was encapsulated.
   */
  const std::vector<bool> timestep;

  /**
   * Deserializes the given serialized CiphertextHIBEs and returns the
   * deserialized bbg05_ciphertexts.
   *
   * @param serialized      - The serialized CiphertextHIBEs.
   * @param numberHashFunctions     - The number of Hashfunctions.
   */
  std::vector<std::unique_ptr<bbg05_ciphertext_t>> deserializeHIBECiphertexts(
      const uint8_t *serialized, const unsigned long numberHashFunctions);

  /**
   * Deserializes the given serialized KeyHIBEs and returns the
   * deserialized bbg05_keys.
   *
   * @param serialized      - The serialized KeyHIBEs.
   * @param numberHashFunctions     - The number of Hashfunctions.
   */
  std::vector<uint8_t *> deserializeHIBEKeys(
      const uint8_t *serialized, const unsigned long numberHasFunctions);

  /**
   * Deserializes the given serialized timestep and returns the
   * deserialized timestep.
   *
   * @param serialized      - The serialized timestep.
   * @param numberTimesteps         - The number of timesteps.
   */
  std::vector<bool> deserializeTimestep(const uint8_t *serialized, const unsigned numberTimesteps);

};
}

#endif //DJSS_18_BFE_CIPHERTEXTHIBE_H
