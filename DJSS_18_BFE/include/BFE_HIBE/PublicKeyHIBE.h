#ifndef DJSS_18_BFE_PUBLICKEYHIBE_H
#define DJSS_18_BFE_PUBLICKEYHIBE_H

#include <memory>
#include <bbg05hibe/BBG_05_HIBE.h>
#include "../PublicKey.h"

namespace bfe {

class PublicKeyHIBE : public PublicKey {
 public:

  /**
   * Creates a new PublicKeyHIBE with the given bbg05_public_params.
   *
   * @param pk          - The bbg05_public_params.
   */
  PublicKeyHIBE(const std::unique_ptr<bbg05_public_params_t> pk,
                unsigned int numberHashFunctions,
                unsigned long long bloomFilterSize);

  /**
   * Creates a new PublicKeyHIBE by deserializing the given serialized public
   * key.
   *
   * @param serialized      - The serialized public key.
   */
  explicit PublicKeyHIBE(const uint8_t *serialized);

  /**
   * Destructor frees all allocated memory.
   */
  ~PublicKeyHIBE() override;

  /**
   * Returns the bbg05_public_params.
   *
   * @return The bbg05_public_params.
   */
  bbg05_public_params_t *getPublicKeyHIBE() const;

  /**
   * Returns the number of hash functions in the bloom filter.
   *
   * @return The number of hash functions in the bloom filter.
   */
  const unsigned int getNumberHashFunctions() const;

  /**
  * Checks if the bbg05_public_params are valid.
  *
  * @return True if the bbg05_public_params are valid, false otherwise.
  */
  bool isValid() const override;

  /**
   * Returns a serialized version of this public key.
   *
   * @return A uint8_t array of length 196 + 33 * (log2(numberTimesteps) + 1)
   * bytes containing the serialized public key.
   */
  uint8_t *getSerialized() const override;

  /**
   * Returns the size of a serialized public key.
   *
   * @return the size of a serialized public key.
   */
  const unsigned long getSerializedSize() const override;

  /**
   * Overload of the == operator.
   *
   * @param other       - The PublicKeyIBBE with which this one is compared.
   *
   * @return True if the objects are equal, false otherwise.
   */
  bool operator==(const PublicKeyHIBE &other) const;

  /**
   * Overload of the != operator.
   *
   * @param other       - The PublicKeyIBBE with which this one is compared.
   *
   * @return True if the objects are not equal, false otherwise.
   */
  bool operator!=(const PublicKeyHIBE &other) const;

 private:

  const unsigned numberHashFunctions;

  /**
   * The bbg05_public_params.
   */
  const std::unique_ptr<bbg05_public_params_t> publicKeyHIBE;

  /**
   * Deserializes the given serialized number of hash functions.
   *
   * @param serialized      - The serialized number of hash functions.
   *
   * @return The number of hash functions.
   */
  const unsigned deserializeNumberHashFunctions(
      const uint8_t *serialized);

  /**
   * Deserializes the given serialized PublicKeyHIBE and returns the
   * deserialized bbg05_publicKey.
   *
   * @param serialized      - The serialized PublicKeyHIBE.
   */
  std::unique_ptr<bbg05_public_params_t> deserializeHIBEPublicKey(
      const uint8_t *serialized);

};
}
#endif //DJSS_18_BFE_PUBLICKEYHIBE_H
