#ifndef DJSS_18_BFE_DJSS_18_BFE_HIBE_H
#define DJSS_18_BFE_DJSS_18_BFE_HIBE_H

#include "../BloomFilterEncryption.h"

namespace bfe {

class BloomFilterEncryptionHIBE : public BloomFilterEncryption {
 public:
  /**
   * Creates a new BloomFilterEncryptionHIBE. This constructor is used on the
   * server side. Instantiates the Bloom Filter.
   *
   * @param m                   - The size of the Bloom Filter
   * @param k                   - The number of distinct hash functions of the
   *                              Bloom Filter
   * @param numberTimesteps     - The number of timesteps.
   */
  BloomFilterEncryptionHIBE(const unsigned long long m,
                            const unsigned int k,
                            const unsigned numberTimesteps);

  /**
   * Creates a new BloomFilterEncryptionHIBE. This constructor is used on the
   * client side. Instantiates the Bloom Filter.
   *
   * @param serializedPublicKey      - The serialized public key of the server.
   */
  BloomFilterEncryptionHIBE(const uint8_t *serializedPublicKey);

  /**
   * Generates the public and secret key and assigns them to the member
   * variables.
   */
  void keyGeneration() override;

  /**
   * Generates a 256 bit session key and encrypts it with the public key.
   *
   * @return        - The session key and the encapsulation of it.
   */
  std::pair<std::unique_ptr<Key>,
            std::unique_ptr<Ciphertext>> encapsulate() override;

  /**
   * Decapsulates a session key from the given ciphertext.
   *
   * @param c       - A ciphertext resulting from an encapsulation.
   * @return        - The decapsulated 256 bit session key.
   */
  std::unique_ptr<Key> decapsulate(const Ciphertext *ciphertext) override;

  /**
   * Advances to the next timestep.
   */
  void punctureTimestep();

  /**
   * Returns the number of timesteps.
   *
   * @return The number of timesteps.
   */
  const unsigned int getNumberTimesteps() const;

 private:

  /**
   * The current timestep in binary representation.
   */
  std::vector<bool> *currentTimestep;

  /**
   * The number of timesteps.
   */
  unsigned numberTimesteps;

};
}

#endif //DJSS_18_BFE_DJSS_18_BFE_HIBE_H
