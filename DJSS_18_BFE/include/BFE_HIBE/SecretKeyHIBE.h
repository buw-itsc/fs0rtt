#ifndef DJSS_18_BFE_SECRETKEYHIBE_H
#define DJSS_18_BFE_SECRETKEYHIBE_H

#include <memory>
#include <stack>
#include <mutex>
#include <bf/hash.hpp>
#include <bbg05hibe/BBG_05_HIBE.h>
#include "../SecretKey.h"
#include "PublicKeyHIBE.h"


namespace bfe {
class SecretKeyHIBE : public SecretKey {
 public :
  /**
   * Creates a new SecretKeyHIBE with the given Bloom Filter state and a hash
   * map containing an IdentitySecretKeyHIBELeafNode for each i in [1,m] for the
   * first timestep and the secret keys needed to derive secret keys for all
   * future timesteps.
   *
   * @param bloomFilterState        - The Bloom Filter state.
   * @param m                       - The number of IdentitySecretKeys that
   *                                  are created per timestep.
   * @param bbgMasterSecretKey      - The bbg05_master_key_t from which the
   *                                  IdentitySecretKeys are derived.
   * @param publicKey               - The public key.
   */
  SecretKeyHIBE(const unsigned long long bloomFilterSize,
                const std::unique_ptr<bbg05_master_key_t> bbgMasterSecretKey,
                const PublicKeyHIBE *publicKey);

  /**
   * Returns the IdentitySecretKey corresponding to the given identity at the
   * current timestep.
   *
   * @param identity        - The identity for which the IdentitySecretKey is
   *                          returned.
   * @return The IdentitySecretKey corresponding to the given identity.
   */
  IdentitySecretKey *getIdentitySecretKeyForIdentity(const bf::digest identity);

  /**
   * Deletes all identity secret keys for the current timestep and creates all
   * new ones for the next timestep.
   *
   * @param publicKey       - The public key.
   */
  void punctureTimestep(const PublicKeyHIBE *publicKey);

 private:

  /**
   * A stack containing identitySecretKeys that are needed for future timesteps.
   */
  std::stack<std::pair<bbg05_identity_t *, std::unique_ptr<IdentitySecretKey>>> identitySecretKeysTime;

  static void generateRangeOfIdentitySecretKeys(bf::digest startIdentity,
                                                bf::digest endIdentity,
                                                bbg05_identity_t *parentIdentity,
                                                bbg05_secret_key_inner_node_t *parentSecretKey,
                                                const PublicKeyHIBE *publicKey,
                                                bfe::identitySecretKeyMap *identitySecretKeys,
                                                std::mutex *mutex);

  void generateIdentitySecretKeys(bbg05_identity_t *parentIdentity,
                                  bbg05_secret_key_inner_node_t *parentSecretKey,
                                  const PublicKeyHIBE *publicKey);




};
}
#endif //DJSS_18_BFE_SECRETKEYHIBE_H
