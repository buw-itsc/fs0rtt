#ifndef DJSS_18_BFE_IDENTITYSECRETKEYHIBELEAFNODE_H
#define DJSS_18_BFE_IDENTITYSECRETKEYHIBELEAFNODE_H

#include <bbg05hibe/BBG_05_HIBE.h>
#include "../IdentitySecretKey.h"
#include "PublicKeyHIBE.h"

namespace bfe {
class IdentitySecretKeyHIBELeafNode : public IdentitySecretKey {

 public:
  /**
   * Creates a new IdentitySecretKeyHIBELeafNode by deriving a
   * bbg05_secret_key_leaf_node for the given identity from the given parent
   * secret key.
   *
   * @param identity            - The identity for which the identity secret key
   *                              is created.
   * @param parentSecretKey     - The parent secret key from which the identity
   *                              secret key is derived.
   * @param publicKeyHIBE       - The public key.
   */
  IdentitySecretKeyHIBELeafNode(bbg05_identity_t *const identity,
                                bbg05_secret_key_inner_node_t *const parentSecretKey,
                                const PublicKeyHIBE *publicKeyHIBE);

  /**
   * Creates a new IdentitySecretKeyHIBELeafNode by deriving a
   * bbg05_secret_key_inner_node for the given identity from the given master
   * secret key.
   *
   * @param identity            - The identity for which the identity secret key
   *                              is created.
   * @param masterSecretKey     - The parent secret key from which the identity
   *                              secret key is derived.
   * @param publicKeyHIBE       - The public key.
   */
  IdentitySecretKeyHIBELeafNode(bbg05_identity_t *const identity,
                                bbg05_master_key_t *const masterSecretKey,
                                const PublicKeyHIBE *publicKeyHIBE);

  /**
   * Destructor. Sets the memory allocated for the bbg05_secret_key to 0.
   */
  ~IdentitySecretKeyHIBELeafNode() override;

  /**
   * Returns the bbg05_secret_key.
   *
   * @return the bbg05_secret_key.
   */
  bbg05_secret_key_leaf_node_t *getSecretKeyHIBE();

  /**
   * Checks if the bbg05_secret_key is valid.
   *
   * @return True if the bbg05_secret_key is valid, false otherwise.
   */
  bool isValid() const override;

 private:
  /**
   * The bb05_SecretKey.
   */
  std::unique_ptr<bbg05_secret_key_leaf_node_t> secretKeyHIBE;
};
}

#endif //DJSS_18_BFE_IDENTITYSECRETKEYHIBELEAFNODE_H
