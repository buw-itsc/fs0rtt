#ifndef DJSS_18_BFE_UTILITYHIBE_H
#define DJSS_18_BFE_UTILITYHIBE_H

#include <bf/hash.hpp>

namespace bfe {
/**
 * Converts the given identity to a byte array.
 *
 * @param identity        - The identity that is converted to a byte array.
 * @return The resulting byte array and its length.
 */
std::pair<uint8_t *, int> identityToByteArray(const bf::digest identity);

}

#endif //DJSS_18_BFE_UTILITYHIBE_H
