#ifndef DJSS_18_BFE_IDENTITYSECRETKEYHIBEINNERNODE_H
#define DJSS_18_BFE_IDENTITYSECRETKEYHIBEINNERNODE_H

#include <bbg05hibe/BBG_05_HIBE.h>
#include "../IdentitySecretKey.h"
#include "PublicKeyHIBE.h"

namespace bfe {
class IdentitySecretKeyHIBEInnerNode : public IdentitySecretKey {

 public:
  /**
   * Creates a new IdentitySecretKeyHIBEInnerNode by deriving a
   * bbg05_secret_key_inner_node for the given identity from the given parent
   * secret key.
   *
   * @param identity            - The identity for which the identity secret key
   *                              is created.
   * @param parentSecretKey     - The parent secret key from which the identity
   *                              secret key is derived.
   * @param publicKeyHIBE       - The public key.
   */
  IdentitySecretKeyHIBEInnerNode(bbg05_identity_t *const identity,
                                 bbg05_secret_key_inner_node_t *const parentSecretKey,
                                 const PublicKeyHIBE *publicKeyHIBE);

  /**
   * Creates a new IdentitySecretKeyHIBEInnerNode by deriving a
   * bbg05_secret_key_inner_node for the given identity from the given master
   * secret key.
   *
   * @param identity            - The identity for which the identity secret key
   *                              is created.
   * @param masterSecretKey     - The parent secret key from which the identity
   *                              secret key is derived.
   * @param publicKeyHIBE       - The public key.
   */
  IdentitySecretKeyHIBEInnerNode(bbg05_identity_t *const identity,
                                 bbg05_master_key_t *const masterSecretKey,
                                 const PublicKeyHIBE *publicKeyHIBE);

  /**
   * Destructor. Sets the memory allocated for the bbg05_secret_key to 0.
   */
  ~IdentitySecretKeyHIBEInnerNode() override;

  /**
   * Returns the bbg05_secret_key.
   *
   * @return the bbg05_secret_key.
   */
  bbg05_secret_key_inner_node_t *getSecretKeyHIBE();

  /**
   * Checks if the bbg05_secret_key is valid.
   *
   * @return True if the bbg05_secret_key is valid, false otherwise.
   */
  bool isValid() const override;

 private:
  /**
   * The bb05_secret_key.
   */
  std::unique_ptr<bbg05_secret_key_inner_node_t> secretKeyHIBE;

};
}

#endif //DJSS_18_BFE_IDENTITYSECRETKEYHIBEINNERNODE_H
