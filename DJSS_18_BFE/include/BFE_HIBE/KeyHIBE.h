#ifndef DJSS_18_BFE_KEYHIBE_H
#define DJSS_18_BFE_KEYHIBE_H

#include "../Key.h"

namespace bfe {

class KeyHIBE : public Key {
 public:

  /**
   * Creates a new KeyHIBE containing the given bbg05_key.
   * @param key
   */
  KeyHIBE(const uint8_t *key);

};
}

#endif //DJSS_18_BFE_KEYHIBE_H
