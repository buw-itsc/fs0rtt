// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_CIPHERTEXT_H_
#define DJSS_18_BFE_INCLUDE_CIPHERTEXT_H_

#include <inttypes.h>

namespace bfe {

class Ciphertext {
 public:
  /**
   * Creates a new Ciphertext by deserializing the random bit string.
   *
   * @param r       - The serialized random bit string.
   */
  explicit Ciphertext(const uint8_t *serialized);

  /**
  * Default destructor.
  */
  virtual ~Ciphertext() = default;

  /**
   * Checks if the Ciphertext is valid.
   *
   * @return True if the Ciphertext is valid, false otherwise.
   */
  virtual bool isValid() const = 0;

  /**
   * Returns a serialized version of this ciphertext.
   *
   * @return A uint8_t array containing the serialized ciphertext.
   */
  virtual uint8_t* getSerialized() = 0;

  /**
   * Returns the random bit string.
   *
   * @return The random bit string.
   */
  const uint8_t *getR() const;

 protected:
  /**
   * The random bit string used for generating identities.
   */
  const uint8_t *r;

  /**
   * Deserializes the random bit string included in the given serialized
   * ciphertext.
   *
   * @param serialized      - The part of the serialized ciphertext containing
   *                          the random bit stirng
   * @return The random bit string.
   */
  const uint8_t *deserializeCiphertextR(const uint8_t *serialized);
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_CIPHERTEXT_H_
