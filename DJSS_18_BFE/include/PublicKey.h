// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_PUBLICKEY_H_
#define DJSS_18_BFE_INCLUDE_PUBLICKEY_H_

#include <stdint.h>
#include <string>

namespace bfe {

class PublicKey {
 public:
  /**
   * Creates a new PublicKey.
   *
   * @param bloomFilterSize         - The size of the bloom filter.
   */
  explicit PublicKey(unsigned long long bloomFilterSize);

  /**
   * Creates a new PublicKey by deserializing the serialized  bloom filter size.
   *
   * @param serialized      - The serialized bloom filter size.
   */
  explicit PublicKey(const uint8_t *serialized);

  /**
   * Default destructor.
   */
  virtual ~PublicKey() = default;

  /**
   * Checks if the PublicKey is valid.
   *
   * @return True if the PublicKey is valid, false otherwise.
   */
  virtual bool isValid() const = 0;

  /**
   * Returns a serialized version of this public key.
   *
   * @return A uint8_t array containing the serialized public key.
   */
  virtual uint8_t *getSerialized() const = 0;

  /**
   * Returns the bloom filter size.
   *
   * @return the bloom filter size
   */
  const unsigned long long getBloomFilterSize() const;

  /**
   * Returns the size of a serialized public key.
   *
   * @return the size of a serialized public key.
   */
  virtual const unsigned long getSerializedSize() const = 0;

  /**
   * Writes the public key to the given file.
   *
   * @param fileName        - The file the public key is written to.
   */
  virtual void writeToFile(const std::string fileName) = 0;

 protected:
  /**
   * The size of the bitstring in the bloom filter.
   */
  const unsigned long long bloomFilterSize;

  /**
   * Deserializes the given serialized bloom filter size.
   *
   * @param serialized      - The serailized bloom filter size.
   *
   * @return The bloom filter size.
   */
  const unsigned long long deserializeBloomFilterSize(
      const uint8_t *serialized);
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_PUBLICKEY_H_
