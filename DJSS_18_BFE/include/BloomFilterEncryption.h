// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BLOOMFILTERENCRYPTION_H_
#define DJSS_18_BFE_INCLUDE_BLOOMFILTERENCRYPTION_H_

#include <memory>
#include <mutex>
#include <bf/all.hpp>
#include "PublicKey.h"
#include "SecretKey.h"

namespace bfe {

class Key;
class Ciphertext;
class BloomFilterEncryption {
 public:
  /**
   * Creates a new BloomFilterEncryption. This constructor is used on the
   * server side. Instantiates the Bloom Filter.
   *
   * @param m       - The size of the Bloom Filter
   * @param k       - The number of distinct hash functions of the Bloom Filter
   */
  BloomFilterEncryption(const unsigned long long m, const unsigned int k);

  /**
   * Creates a new BloomFilterEncryption. This constructor is used on the
   * client side.
   *
   * @param pk      - The public key of the server.
   */
  BloomFilterEncryption();

  /**
   * Default destructor.
   */
  virtual ~BloomFilterEncryption() = default;

  /**
   * Generates the public and secret key and assigns them to the member
   * variables.
   */
  virtual void keyGeneration(const bool loadKeyFromFile) = 0;

  /**
   * Generates a 256 bit session key and encrypts it with the public key.
   *
   * @return        - The session key and the encapsulation of it.
   */
  virtual std::pair<std::unique_ptr<Key>,
                    std::unique_ptr<Ciphertext>> encapsulate() = 0;

  /**
   * Decapsulates a session key from the given ciphertext.
   *
   * @param c       - A ciphertext resulting from an encapsulation.
   * @return        - The decapsulated 256 bit session key.
   */
  virtual std::unique_ptr<Key> decapsulate(const Ciphertext *c) = 0;

  /**
   * Returns the public key.
   *
   * @return The public key.
   */
  const PublicKey *getPublicKey() const;

  /**
   * Returns the number of hash functions in the bloom filter.
   *
   * @return The number of hash functions in the bloom filter.
   */
  unsigned int getK() const;

 protected:
  /**
   * Number of bits per cell of the counting bloom filter.
   */
  const size_t COUNTING_BF_BITS_PER_CELL = 4;

  /*
   * Mutex for accessing the bloom filter.
   */
  std::mutex bloomFilterMutex;

  /**
   * The size of the Bloom Filter.
   */
  unsigned long long m;

  /**
   * The number of hash functions in the Bloom Filter.
   */
  unsigned int k;

  /**
   * The Bloom Filter.
   */
  std::unique_ptr<bf::counting_bloom_filter> countingBloomFilter;

  /**
   * The secret key.
   */
  std::unique_ptr<SecretKey> secretKey;

  /**
   * The public key.
   */
  std::unique_ptr<PublicKey> publicKey;

  /**
   * Generates random 256 bits. Return value points to array on the heap.
   *
   * @return                - pointer to uint8_t
   */
  uint8_t* random256Bits();

  /**
  *  Punctures the secret key for the given ciphertext, i.e. deleting the
  *  identity secret keys corresponding to the identities that were used for
  *  encapsulation.
  *
  *  @param c       - The ciphertext for which the secret key is punctured.
  */
  virtual void puncture(const Ciphertext *ciphertext);

  /**
    * Generates identities, i.e. indices of the Bloom Filter, from the given
    * random bit string and returns them in a vector.
    *
    * @param randomness      - The random bit string form which the identities
    * are generated.
    * @return                - A vector of indices of the Bloom Filter.
    */
  std::vector<bf::digest> generateIdentitiesAsVector(const uint8_t *randomness);

 private:
  /**
  * The random number generator used for generating random bits.
  */
  std::random_device randomDevice;

};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BLOOMFILTERENCRYPTION_H_
