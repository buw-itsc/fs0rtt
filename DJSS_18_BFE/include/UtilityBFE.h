// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_UTILITYBFE_H_
#define DJSS_18_BFE_INCLUDE_UTILITYBFE_H_

/**
 * This class has the same generate() function that std::seed_seq has and is
 * used to properly initialize a std::mt19937_64.
 *
 * Source:  https://probablydance.com/2016/12/29/random_seed_seq-a-small-utility-to-properly-seed-random-number-generators-in-c/
 */
struct random_seed_seq {
  template<typename It>
  void generate(It begin, It end) {
    for (; begin != end; ++begin) {
      *begin = device();
    }
  }

  static random_seed_seq & get_instance() {
    static thread_local random_seed_seq result;
    return result;
  }

 private:
  std::random_device device;
};

#endif  // DJSS_18_BFE_INCLUDE_UTILITYBFE_H_
