// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BFE_IBBE_SECRETKEYIBBE_H_
#define DJSS_18_BFE_INCLUDE_BFE_IBBE_SECRETKEYIBBE_H_

#include <del07ibbe/Delerablee_07_IBBE_types.h>
#include <mutex>
#include <string>
#include "../SecretKey.h"

namespace bfe {

class SecretKeyIBBE : public SecretKey {
 public:
  /**
   * Creates a new SecretKeyIBBE with the given Bloom Filter size and a hash
   * map containing an IdentitySecretkeyIBBE for each i in [1,m].
   *
   * @param m                       - The number of IdentitySecretKeys that
   *                                  are created.
   * @param msk                     - The del07_master_secret_key_t from which
   *                                  the IdentitySecretKeys are derived.
   */
  SecretKeyIBBE(const unsigned long long bloomFilterSize,
                const std::unique_ptr<del07_master_secret_key_t> msk);

  /**
   * Reads in a SecretKeyIBBE from the given file.
   *
   * @param m               - The number of IdentitySecretKeys that
   *                          are read in.
   * @param filename        - The file from which the key is read in.
   */
  SecretKeyIBBE(const unsigned long long bloomFilterSize,
                const std::string fileName);

  /**
   * Returns the IdentitySecretKey corresponding to the given identity.
   *
   * @param identity        - The identity for which the IdentitySecretKey is
   *                          returned.
   * @return The IdentitySecretKey corresponding to the given identity.
   */
  IdentitySecretKey *getIdentitySecretKeyForIdentity(const bf::digest identity);

  /**
   * Writes the secret key to the given file.
   *
   * @param fileName        - The file the secret key is written to.
   */
  void writeToFile(const std::string fileName) override;

  /**
   * Function for a helper thread generating the IdentitySecretKeys for the
   * interval [startIdentity, endIdentity-1].
   *
   * @param startIdentity           - The last identity for which this thread
   *                                  generates the IdentitySecretKey.
   * @param endIdentity             - The last identity for which this thread
   *                                  generates the IdentitySecretKey.
   * @param identitySecretKeys      - The map containing all IdentitySecretKeys.
   * @param mutex                   - The mutex controlling the access to
   *                                  identitySecretKeys.
   * @param msk                     - The master secret key from which the keys
   *                                  are derived.
   */
  static void generateIdentitySecretKeys(bf::digest startIdentity,
     bf::digest endIdentity,
     identitySecretKeyMap *identitySecretKeys,
     std::mutex *mutex,
     del07_master_secret_key_t *const msk);
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BFE_IBBE_SECRETKEYIBBE_H_
