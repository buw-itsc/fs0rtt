// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BFE_IBBE_IDENTITYSECRETKEYIBBE_H_
#define DJSS_18_BFE_INCLUDE_BFE_IBBE_IDENTITYSECRETKEYIBBE_H_

#include <del07ibbe/Delerablee_07_IBBE_types.h>
#include <memory>
#include <bf/hash.hpp>
#include "../IdentitySecretKey.h"

namespace bfe {

class IdentitySecretKeyIBBE : public IdentitySecretKey {
 public:
  /**
   * Creates a new IdentitySecretKeyIBBE containing a del07_secret_key_t for the
   * given identity and the given del07_master_secret_key_t.
   *
   * @param identity        - The identity for which the identity secret key
   *                          is created.
   * @param msk             - The master secret key from which the identity
   *                          secret key is derived.
   */
  IdentitySecretKeyIBBE(const bf::digest identity,
                        del07_master_secret_key_t *const msk);

  /**
   * Cereates an IdentitySecretKeyIBBE from the given serialized secret key.
   *
   * @param identity        - The identity for which the identity secret key
   *                          is created.
   * @param serialized      - The master secret key from which the identity
   *                          secret key is derived.
   */
  IdentitySecretKeyIBBE(const bf::digest identity, const uint8_t *serialized);

  /**
   * Destructor. Sets the memory allocated for the del07_secret_key_t to 0.
   */
  ~IdentitySecretKeyIBBE() override;

  /**
   * Returns the del07_secret_key_t.
   *
   * @return the del07_secret_key_t.
   */
  del07_secret_key_t *getSecretKeyIBBE();

  /**
    * Checking if the del07_secret_key_t is valid.
    *
    * @return True if the del07_secret_key_t is valid, false otherwise.
    */
  bool isValid() const override;

  /**
   * Returns a serialized version of this identity secret key.
   *
   * @return A uint8_t array containing the serialized identity secret key.
   */
  uint8_t* getSerialized() override;

  /**
   * Returns the size of a serialized identity secret key.
   *
   * @return the size of a serialized identity secret key.
   */
  const unsigned getSerializedSize(bool compression) const override;

 private:
  /**
   * The del07_secret_key_t.
   */
  std::unique_ptr<del07_secret_key_t> secretKeyIBBE;

  /**
   * Deserializes the given serialized IdentitySecretKeyIBBE and returns the
   * deserialized del07_secret_key.
   *
   * @param serialized      - The serialized IdentitySecretKeyIBBE.
   *
   * @throws std::invalid_argument if `serialized` can not be deserialized.
   */
  std::unique_ptr<del07_secret_key_t> deserializeIdentitySecretKey(
      const uint8_t *serialized);
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BFE_IBBE_IDENTITYSECRETKEYIBBE_H_
