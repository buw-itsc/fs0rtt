// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BFE_IBBE_UTILITYIBBE_H_
#define DJSS_18_BFE_INCLUDE_BFE_IBBE_UTILITYIBBE_H_

#include <bf/hash.hpp>

namespace bfe {

/**
 * Converts the given identity to a byte array.
 *
 * @param identity        - The identity that is converted to a byte array.
 * @return The resulting byte array and its length.
 */
std::pair<uint8_t *, int> identityToByteArray(const bf::digest identity);

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BFE_IBBE_UTILITYIBBE_H_
