// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BFE_IBBE_BLOOMFILTERENCRYPTIONIBBE_H_
#define DJSS_18_BFE_INCLUDE_BFE_IBBE_BLOOMFILTERENCRYPTIONIBBE_H_

#include <mutex>
#include "../BloomFilterEncryption.h"

namespace bfe {

class BloomFilterEncryptionIBBE : public BloomFilterEncryption {
 public:
  /**
   * The prefix of a key file to which a key will be written.
   */
  const std::string KEY_FILE_PREFIX = "bfe_ibbe";

  /**
   * The file to which the public key will be written.
   */
  const std::string SECRET_KEY_FILE_EXTENSION = "sk";

  /**
   * The file to which the public key will be written.
   */
  const std::string PUBLIC_KEY_FILE_EXTENSION = "pk";

  /**
   * Counts number of BloomFilterEncryptionIBBE objects created to know when to
   * call del07_clean.
   */
  static int numberOfBFEIBBEObjects;

  /**
   * Mutex protecting the access to the numberOfBFEIBBEObjects variable.
   */
  static std::mutex numberOfBFEIBBEObjectsMutex;

  /**
   * Creates a new BloomFilterEncryptionIBBE. This constructor is used on the
   * server side. Instantiates the Bloom Filter.
   *
   * @param m       - The size of the Bloom Filter
   * @param k       - The number of distinct hash functions of the Bloom Filter
   */
  BloomFilterEncryptionIBBE(const unsigned long long m, const unsigned int k);

  /**
   * Creates a new BloomFilterEncryptionIBBE. This constructor is used on the
   * client side. Instantiates the Bloom Filter.
   *
   * @param serializedPublicKey      - The serialized public key of the server.
   *
   * @throws std::invalid_argument if `serializedPublicKey` can not be
   * deserialized.
   */
  explicit BloomFilterEncryptionIBBE(const uint8_t *serializedPublicKey);

  /**
   * Destructor finalizes del07 library if no BloomFilterEncryptionIBBE object
   * is alive.
   */
  ~BloomFilterEncryptionIBBE();

  /**
   * Generates the public and secret key and assigns them to the member
   * variables.
   *
   * @param loadKeyFromFile     - If true an existing key pair is read in from
   *                              memory. This should only be done for
   *                              benchmarking purposes! Doing this is usually
   *                              not secure!
   */
  void keyGeneration(const bool loadSecretKeyFromFile) override;

  /**
   * Generates a 256 bit session key and encrypts it with the public key.
   *
   * @return        - The session key and the encapsulation of it.
   */
  std::pair<std::unique_ptr<Key>,
            std::unique_ptr<Ciphertext>> encapsulate() override;

  /**
   * Decapsulates a session key from the given ciphertext.
   *
   * @param c       - A ciphertext resulting from an encapsulation.
   * @return        - The decapsulated 256 bit session key.
   */
  std::unique_ptr<Key> decapsulate(const Ciphertext *c) override;

 private:
  /**
   * Generates , i.e. indices of the Bloom Filter, from the given
   * random bit string and returns them in an array.
   *
   * @param randomness      - The random bit string from which the identities
   * are generated.
   * @return                - A two dimensional byte array containing the
   * identities, the number of identities in the array and for each identity
   * its length
   */
  std::tuple<uint8_t **, unsigned, unsigned *> generateIdentitiesAsByteArray(
      const uint8_t *randomness);

  /**
   * Generates a new key pair.
   */
  void generateNewKeyPair();
};

int BloomFilterEncryptionIBBE::numberOfBFEIBBEObjects = 0;
std::mutex BloomFilterEncryptionIBBE::numberOfBFEIBBEObjectsMutex;

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BFE_IBBE_BLOOMFILTERENCRYPTIONIBBE_H_
