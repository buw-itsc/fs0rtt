// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BFE_IBBE_KEYIBBE_H_
#define DJSS_18_BFE_INCLUDE_BFE_IBBE_KEYIBBE_H_

#include <del07ibbe/Delerablee_07_IBBE_types.h>
#include <memory>
#include "../Key.h"

namespace bfe {

class KeyIBBE : public Key {
 public:
  /**
   * Creates a new KeyIBBE containing the given del07_key_t.
   * @param key
   */
  explicit KeyIBBE(std::unique_ptr<del07_key_t> key);
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BFE_IBBE_KEYIBBE_H_
