// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BFE_IBBE_PUBLICKEYIBBE_H_
#define DJSS_18_BFE_INCLUDE_BFE_IBBE_PUBLICKEYIBBE_H_

#include <del07ibbe/Delerablee_07_IBBE_types.h>
#include <memory>
#include "../PublicKey.h"

namespace bfe {

class PublicKeyIBBE : public PublicKey {
 public:
  /**
   * Creates a new PublicKeyIBBE with the given del07_public_key_t.
   *
   * @param bloomFilterSize     - The size of the bloom filter.
   * @param pk                  - The del07_public_key_t.
   */
  PublicKeyIBBE(unsigned long long bloomFilterSize,
                std::unique_ptr<const del07_public_key_t> pk);

  /**
   * Creates a new PublicKeyIBBE by reading it in from the given file.
   *
   * @param bloomFilterSize     - The size of the bloom filter.
   * @param fileName            - The file from which the public key is read in.
   */
  PublicKeyIBBE(unsigned long long bloomFilterSize, const std::string fileName);

  /**
   * Creates a new PublicKeyIBBE by deserializing the given serialized public
   * key.
   *
   * @param serialized      - The serialized public key.
   *
   * @throws std::invalid_argument if `serialized` can not be deserialized.
   */
  PublicKeyIBBE(const uint8_t *serialized, bool computePrecomputationTables);

  /**
   * Destructor frees all allocated memory.
   */
  ~PublicKeyIBBE() override;

  /**
   * Returns the del07_public_key_t.
   *
   * @return The del07_public_key_t.
   */
  const del07_public_key_t *const getPublicKeyIBBE() const;

  /**
    * Checks if the del07_public_key_t is valid.
    *
    * @return True if the del07_public_key_t is valid, false otherwise.
    */
  bool isValid() const override;

  /**
   * Returns a serialized version of this public key.
   *
   * @return A uint8_t array of length 364 + 65 * number of Bloom Filter hash
   * functions bytes containing the serialized public key.
   */
  uint8_t *getSerialized() const override;

  /**
   * Overload of the == operator.
   *
   * @param other       - The PublicKeyIBBE with which this one is compared.
   *
   * @return True if the objects are equal, false otherwise.
   */
  bool operator==(const PublicKeyIBBE &other) const;

  /**
   * Overload of the != operator.
   *
   * @param other       - The PublicKeyIBBE with which this one is compared.
   *
   * @return True if the objects are not equal, false otherwise.
   */
  bool operator!=(const PublicKeyIBBE &other) const;

  /**
   * Returns the size of a serialized public key.
   *
   * @return the size of a serialized public key.
   */
  const unsigned long getSerializedSize() const override;

  /**
   * Writes the public key to the given file.
   *
   * @param fileName        - The file the public key is written to.
   */
  void writeToFile(const std::string fileName) override;

 private:
  /**
   * The del07_public_key_t.
   */
  const std::unique_ptr<const del07_public_key_t> publicKeyIBBE;

  /**
   * Deserializes the given serialized PublicKeyIBBE and returns the
   * deserialized del07_publicKey.
   *
   * @param serialized      - The serialized PublicKeyIBBE.
   * @param computePrecomputationTables
   *                        - If true the precomputation tables are computed.
   *
   * @throws std::invalid_argument if `serialized` can not be deserialized.
   */
  std::unique_ptr<del07_public_key_t> deserializeIBBEPublicKey(
      const uint8_t *serialized, bool computePrecomputationTables);

  /**
   * Reads in the serialized PublicKeyIBBE from the given file and returns the
   * deserialized del07_publicKey.
   *
   * @param fileName      - The file containing the serialized PublicKeyIBBE.
   *
   * @throws std::invalid_argument if the public key can not be read in.
   */
  std::unique_ptr<del07_public_key_t> readInFromFile(
      const std::string fileName);
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BFE_IBBE_PUBLICKEYIBBE_H_
