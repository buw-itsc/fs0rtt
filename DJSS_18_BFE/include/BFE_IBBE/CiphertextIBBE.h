// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_BFE_IBBE_CIPHERTEXTIBBE_H_
#define DJSS_18_BFE_INCLUDE_BFE_IBBE_CIPHERTEXTIBBE_H_

#include <del07ibbe/Delerablee_07_IBBE_types.h>
#include <memory>
#include "../Ciphertext.h"

namespace bfe {
class CiphertextIBBE : public Ciphertext {
 public:
  /**
   * Creates a new CiphertextIBBE which wraps the del07_ciphertext and the
   * random bit string which was used to generate the identities for which
   * the del07_ciphertext was encrypted.
   *
   * @param r       - The random bit string from which the encapsulating
   *                  identities were generated.
   * @param c       - The del07_ciphertext.
   */
  CiphertextIBBE(const uint8_t *r, std::unique_ptr<del07_ciphertext_t> c);

  /**
   * Creates a new CiphertextIBBE by deserializing the given serialized
   * ciphertext.
   *
   * @param serialized      - The serialized ciphertext.
   *
   * @throws std::invalid_argument if `serialized` can not be deserialized.
   */
  explicit CiphertextIBBE(const uint8_t *serialized);

  /**
   * Destructor which explicitly deletes the random bit string which is a
   * dynamically allocated byte array.
   */
  ~CiphertextIBBE() override;

  /**
   * Returns a pointer to the del07_ciphertext_t.
   *
   * @return The del07_ciphertext_t.
   */
  const del07_ciphertext_t *getCiphertext() const;

  /**
   * Checks if the del07_ciphertext_t is valid.
   *
   * @return True if the del07_ciphertext_t is valid, false otherwise.
   */
  bool isValid() const override;

  /**
   * Returns a serialized version of this ciphertext.
   *
   * @return A uint8_t array of length 130 bytes containing the serialized
   * ciphertext.
   */
  uint8_t *getSerialized() override;

  /**
   * Overload of the == operator.
   *
   * @param other       - The CiphertextIBBE with which this one is compared.
   *
   * @return True if the objects are equal, false otherwise.
   */
  bool operator==(const CiphertextIBBE &other) const;

  /**
   * Overload of the != operator.
   *
   * @param other       - The CiphertextIBBE with which this one is compared.
   *
   * @return True if the objects are not equal, false otherwise.
   */
  bool operator!=(const CiphertextIBBE &other) const;

  /**
   * Returns the size of a serialized ciphertext.
   *
   * @return the size of a serialized ciphertext.
   */
  static const unsigned long getSerializedSize();

 private:
  /**
   * The del07_ciphertext_t.
   */
  const std::unique_ptr<del07_ciphertext_t> ciphertextIBBE;

  /**
   * Deserializes the given serialized CiphertextIBBE and returns the
   * deserialized del07_ciphertext.
   *
   * @param serialized      - The serialized CiphertextIBBE.
   *
   * @throws std::invalid_argument if `serialized` can not be deserialized.
   */
  std::unique_ptr<del07_ciphertext_t> deserializeIBBECiphertext(
      const uint8_t *serialized);
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_BFE_IBBE_CIPHERTEXTIBBE_H_
