// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_IDENTITYSECRETKEY_H_
#define DJSS_18_BFE_INCLUDE_IDENTITYSECRETKEY_H_

namespace bfe {

class IdentitySecretKey {
 public:
  /**
   * Default destructor.
   */
  virtual ~IdentitySecretKey() = default;

  /**
   * Checks if the IdentitySecretKey is valid.
   *
   * @return True if the IdentitySecretKey is valid, false otherwise.
   */
  virtual bool isValid() const = 0;

  /**
   * Returns a serialized version of this identity secret key.
   *
   * @return A uint8_t array containing the serialized identity secret key.
   */
  virtual uint8_t* getSerialized() = 0;

  /**
   * Returns the size of a serialized identity secret key.
   *
   * @return the size of a serialized identity secret key.
   */
  virtual const unsigned getSerializedSize(bool compression) const = 0;
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_IDENTITYSECRETKEY_H_
