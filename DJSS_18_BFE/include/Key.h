// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_KEY_H_
#define DJSS_18_BFE_INCLUDE_KEY_H_

#include <stdint.h>
#include <ostream>

namespace bfe {

class Key {
 public:
  /**
   * Destructor frees heap allocated memory.
   */
  virtual ~Key();

  /**
   * Returns the actual session key.
   *
   * @return        - the session key.
   */
  const uint8_t *getKey() const;

  /**
   * Overload of the == operator.
   *
   * @param other       - The Key with which this one is compared.
   *
   * @return True if the objects are equal, false otherwise.
   */
  bool operator==(const Key &other) const;

  /**
   * Overload of the << operator.
   *
   * @param os          - The stream the key is put into.
   * @param key         - The key that is output.
   * @return
   */
  friend std::ostream &operator<<(std::ostream &os, const Key &key);

  /**
   * Overload of the != operator.
   *
   * @param other       - The Key with which this one is compared.
   *
   * @return True if the objects are not equal, false otherwise.
   */
  bool operator!=(const Key &other) const;

 protected:
  /**
   * The session key.
   */
  uint8_t *key;
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_KEY_H_
