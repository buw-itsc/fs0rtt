// Copyright [2018] PG 0-RTT

#ifndef DJSS_18_BFE_INCLUDE_SECRETKEY_H_
#define DJSS_18_BFE_INCLUDE_SECRETKEY_H_

#include <memory>
#include <unordered_map>
#include <bf/hash.hpp>
#include "IdentitySecretKey.h"

namespace bfe {

typedef std::unordered_map<bf::digest, std::unique_ptr<IdentitySecretKey>>
    identitySecretKeyMap;

class SecretKey {
 public:
  /**
   * Creates a new SecretKey given the state of the Bloom Filter.
   *
   * @param bloomFilterState        - The state of the Bloom Filter.
   */
  explicit SecretKey(const unsigned long long bloomFilterSize);

  /**
   * Default destructor.
   */
  virtual ~SecretKey() = default;

  /**
    * Deletes the IdentitySecretKeys corresponding to the given identities.
    *
    * @param identities      - The identities for which the secret keys are
    *                          deleted.
    */
  void deleteIdentitySecretKeys(const std::vector<bf::digest> &identities);

  /**
   * Writes the secret key to the given file.
   *
   * @param fileName        - The file the secret key is written to.
   */
  virtual void writeToFile(const std::string fileName) = 0;

 protected:
  /**
   * The state of the Bloom Filter.
   */
  const unsigned long long bloomFilterSize;

  /**
    * A map containing identities and the corresponding IdentitySecretKey.
    */
  identitySecretKeyMap
      identitySecretKeys;
};

}  // namespace bfe

#endif  // DJSS_18_BFE_INCLUDE_SECRETKEY_H_
