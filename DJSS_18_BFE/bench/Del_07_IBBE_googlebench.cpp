// Copyright [2018] PG 0-RTT


#include <stdio.h>
#include <benchmark/benchmark.h>

extern "C" {
#include <relic/relic.h>
}

#include <del07ibbe/Delerablee_07_IBBE.h>
#include <del07ibbe/Delerablee_07_IBBE_util.h>

#define BENCHMARK_MILLISECONDS(n) BENCHMARK(n)->Unit(benchmark::kMillisecond)
#define BENCHMARK_MICROSECONDS(n) BENCHMARK(n)->Unit(benchmark::kMicrosecond)

// only used for encapsulate and decapsulate
unsigned MAX_NUM_BENCH_ITERATIONS = 30;

unsigned MAX_NUMBER_RECIPIENTS = 10;

/**
 *  Forward declaration of function that generates random identities.
 */
void random_identities(unsigned** identity_lengths, uint8_t ***identities,
                       unsigned number_of_identities_to_generate);

/**
 *  Forward declaration of function that frees memory of random identities.
 */
void free_identities(unsigned *identity_lengths, uint8_t **identities,
                     unsigned num_identities);

/**
 * Benchmarks setup of Delerablee 07 IBBE.
 */
static void benchmark_ibbe_setup(benchmark::State& state) {
  int bench_status = 0;

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;

  for (auto _ : state) {
    bench_status |= del07_setup(&msk, &pk, MAX_NUMBER_RECIPIENTS);

    del07_set_master_secret_key_to_zero(msk);
    free(msk);
    del07_set_public_key_to_zero(pk);
    free(pk);
  }

  if (bench_status == STS_ERR) {
    printf("Error in benchmark_ibbe_setup.\n");
    return;
  }
}

/**
 * Benchmarks extract key of Delerablee 07 IBBE.
 * Uses a different random identity in every iteration of bench loop.
 */
static void benchmark_ibbe_extract(benchmark::State& state) {
  int bench_status = 0;

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_secret_key_t *sk;

  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, MAX_NUM_BENCH_ITERATIONS);

  bench_status |= del07_setup(&msk, &pk, MAX_NUMBER_RECIPIENTS);

  int iteration = 0;
  for (auto _ : state) {
    bench_status |= del07_extract(&sk, msk,
        identities[iteration % MAX_NUM_BENCH_ITERATIONS],
        identity_lengths[iteration % MAX_NUM_BENCH_ITERATIONS]);
    iteration++;

    del07_set_secret_key_to_zero(sk);
    free(sk);
  }

  del07_set_master_secret_key_to_zero(msk);
  free(msk);
  del07_set_public_key_to_zero(pk);
  free(pk);
  free_identities(identity_lengths, identities, MAX_NUM_BENCH_ITERATIONS);

  if (bench_status == STS_ERR) {
    printf("Error in benchmark_ibbe_extract.\n");
    return;
  }
}

/**
 * Benchmarks encapsulate function of Delerablee 07 IBBE.
 * Uses random but fixed set of identites in bench loop.
 */
static void benchmark_ibbe_encapsulate(benchmark::State& state) {
  int bench_status = 0;

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_ciphertext_t *c;
  del07_key_t *k;

  // use the same set of identities for every encapsulation benchmark
  unsigned* identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, MAX_NUMBER_RECIPIENTS);

  bench_status |= del07_setup(&msk, &pk, MAX_NUMBER_RECIPIENTS);

  for (auto _ : state) {
    bench_status |= del07_encapsulate(&c,
                                      &k,
                                      (const uint8_t **) identities,
                                      MAX_NUMBER_RECIPIENTS,
                                      identity_lengths,
                                      pk);

    del07_set_ciphertext_to_zero(c);
    free(c);
    del07_set_key_to_zero(k);
    free(k);
  }

  del07_set_master_secret_key_to_zero(msk);
  free(msk);
  del07_set_public_key_to_zero(pk);
  free(pk);
  free_identities(identity_lengths, identities, MAX_NUMBER_RECIPIENTS);

  if (bench_status == STS_ERR) {
    printf("Error in benchmark_ibbe_encapsulate.\n");
    return;
  }
}

/**
 * Benchmarks decapsulate function of Delerablee 07 IBBE.
 * Uses random but fixed set of identites in bench loop.
 * The decapsulation index is random for each bench loop iteration.
 */
static void benchmark_ibbe_decapsulate(benchmark::State& state) {
  int bench_status = 0;

  del07_master_secret_key_t *msk;
  del07_public_key_t *pk;
  del07_ciphertext_t *ciphertexts[MAX_NUM_BENCH_ITERATIONS];
  del07_key_t *k;
  del07_key_t *decapsulated_key;
  del07_secret_key_t *secret_keys[MAX_NUMBER_RECIPIENTS];
  int encapsulation_identity_index[MAX_NUM_BENCH_ITERATIONS];

  // use the same set of identities for every encapsulation benchmark
  unsigned *identity_lengths;
  uint8_t **identities;
  random_identities(&identity_lengths, &identities, MAX_NUMBER_RECIPIENTS);

  // use a different index for every iteration
  for (int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    encapsulation_identity_index[i] = rand() % MAX_NUMBER_RECIPIENTS;
  }

  del07_setup(&msk, &pk, MAX_NUMBER_RECIPIENTS);

  // extract secret key for every identity
  for (int i = 0; i < MAX_NUMBER_RECIPIENTS; i++) {
    bench_status |= del07_extract(&secret_keys[i], msk,
                                  identities[i], identity_lengths[i]);
  }

  // use different ciphertext in every iteration of bench loop
  for (int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    bench_status |= del07_encapsulate(&ciphertexts[i], &k,
        (const uint8_t **) identities, MAX_NUMBER_RECIPIENTS, identity_lengths,
        pk);

    // we do not need the keys for the benchmark
    del07_set_key_to_zero(k);
    free(k);
  }

  int iteration = 0;
  for (auto _ : state) {
    del07_decapsulate(&decapsulated_key, (const uint8_t **) identities,
                      MAX_NUMBER_RECIPIENTS, identity_lengths,
                      (const unsigned) encapsulation_identity_index[iteration],
                      secret_keys[encapsulation_identity_index[iteration]],
                      ciphertexts[iteration], pk);

    del07_set_key_to_zero(decapsulated_key);
    free(decapsulated_key);

    iteration++;
  }

  for (int i = 0; i < MAX_NUMBER_RECIPIENTS; i++) {
    del07_set_secret_key_to_zero(secret_keys[i]);
    free(secret_keys[i]);
  }

  for (int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    del07_set_ciphertext_to_zero(ciphertexts[i]);
    free(ciphertexts[i]);
  }

  del07_set_master_secret_key_to_zero(msk);
  free(msk);
  del07_set_public_key_to_zero(pk);
  free(pk);
  free_identities(identity_lengths, identities, MAX_NUMBER_RECIPIENTS);

  if (bench_status == STS_ERR) {
    printf("Error in benchmark_ibbe_decapsulate.\n");
    return;
  }
}

/**
 * Benchmarks compute polynomial coefficients from roots of Delerablee 07 IBBE.
 */
static void benchmark_poly_coeff_from_roots(benchmark::State& state) {
  int bench_status = 0;

  del07_init();

  bn_t group_order;

  bn_t coefficients[MAX_NUMBER_RECIPIENTS + 1];
  bn_t roots[MAX_NUMBER_RECIPIENTS];

  bn_null(group_order);
  bn_new(group_order);
  g2_get_ord(group_order);

  for (int i = 0; i < MAX_NUMBER_RECIPIENTS; i++) {
    bn_null(roots[i]);
    bn_new(roots[i]);
    bn_rand_mod(roots[i], group_order);
  }

  for (auto _ : state) {
    bench_status |= del07_polynomial_coefficients_from_roots(coefficients,
        roots, MAX_NUMBER_RECIPIENTS, group_order);
  }

  for (int i = 0; i < MAX_NUMBER_RECIPIENTS; i++) {
    bn_free(roots[i]);
  }

  for (int i = 0; i < MAX_NUMBER_RECIPIENTS + 1; i++) {
    bn_free(coefficients[i]);
  }

  if (bench_status == STS_ERR) {
    printf("Error in benchmark_poly_coeff_from_roots.\n");
    return;
  }
}

BENCHMARK_MILLISECONDS(benchmark_ibbe_setup);
BENCHMARK_MICROSECONDS(benchmark_ibbe_extract);
BENCHMARK_MILLISECONDS(benchmark_ibbe_encapsulate);
BENCHMARK_MILLISECONDS(benchmark_ibbe_decapsulate);
BENCHMARK_MICROSECONDS(benchmark_poly_coeff_from_roots);

int main(int argc, char** argv) {
  ::benchmark::Initialize(&argc, argv);
  if (::benchmark::ReportUnrecognizedArguments(argc, argv))
    return 1;

  if (del07_init() != STS_OK) {
    return 1;
  }

  ::benchmark::RunSpecifiedBenchmarks();

  del07_clean();
}

// Implementation of helper functions

// Generate random identities of random length (max 2^17 Byte)
void random_identities(unsigned** identity_lengths, uint8_t ***identities,
                       unsigned number_of_identities_to_generate) {
  *identity_lengths =
      (unsigned *) malloc(number_of_identities_to_generate * sizeof(unsigned));

  unsigned total_size = 0;
  for (unsigned j = 0; j < number_of_identities_to_generate; j++) {
    (*identity_lengths)[j] = (unsigned) rand() % (1 << 17);
    total_size += (*identity_lengths)[j];
  }

  *identities =
      (uint8_t **) malloc(number_of_identities_to_generate * sizeof(uint8_t *));
  for (unsigned j = 0; j < number_of_identities_to_generate; j++) {
    (*identities)[j] =
        (uint8_t *) malloc((*identity_lengths)[j] * sizeof(uint8_t));
    for (unsigned l = 0; l < (*identity_lengths)[j]; l++) {
      (*identities)[j][l] = (uint8_t) (rand() % 256);
    }
  }
}

// Free memory of identities generated with random_identities(.)
void free_identities(unsigned *identity_lengths, uint8_t **identities,
                     unsigned num_identities) {
  for(unsigned i = 0; i < num_identities; i++) {
    free(identities[i]);
  }
  free(identities);
  free(identity_lengths);
}