// Copyright [2018] PG 0-RTT

#include <stdio.h>
#include <bbg05hibe/BBG_05_HIBE.h>
#include<benchmark/benchmark.h>

#define BENCHMARK_MS(n) BENCHMARK(n)->Unit(benchmark::kMillisecond)


// only used for encapsulate, decapsulate and benchmark_hibe_key_generation_leaf_node_from_parent_bench
unsigned MAX_NUM_BENCH_ITERATIONS = 30;

unsigned MAX_DEPTH = 5;

void random_identity(bbg05_identity_t **identity, unsigned depth);
void random_child_identity(bbg05_identity_t **child, bbg05_identity_t *parent);

/**
 * Benchmarks setup of BBG 05 HIBE.
 */
static void benchmark_hibe_setup(benchmark::State& state) {
  int bench_status = 0;

  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;

  for (auto _ : state)
  {
    bench_status |= bbg05_setup(&mk, &params, MAX_DEPTH);

    bbg05_set_master_key_to_zero(mk);
    bbg05_set_public_params_to_zero(params);
    free(mk);
    free(params);
  }

  if(bench_status == STS_ERR) {
    printf("Error in benchmark_hibe_setup.\n");
  }
}

/**
 * Benchmarks key generation of leaf node from parent node of BBG 05 HIBE.
 */
static void benchmark_hibe_key_generation_leaf_node_from_parent_bench(
    benchmark::State& state) {
  int bench_status = 0;

  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;

  // identities of inner node
  bbg05_identity_t *bbg05id[MAX_NUM_BENCH_ITERATIONS];

  // corresponding secret keys
  bbg05_secret_key_inner_node_t *sk_inner[MAX_NUM_BENCH_ITERATIONS];

  // identities of leaf node
  bbg05_identity_t *leaf_bbg05id[MAX_NUM_BENCH_ITERATIONS];

  bbg05_secret_key_leaf_node_t *sk_leaf;

  bench_status |= bbg05_setup(&mk, &params, MAX_DEPTH);

  for(int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    random_identity(&bbg05id[i], MAX_DEPTH - 1);
    random_child_identity(&leaf_bbg05id[i], bbg05id[i]);
    bench_status |= bbg05_key_generation_inner_node_from_master_key(&sk_inner[i],
                                                                    mk, bbg05id[i], params);
  }

  int iteration = 0;
  for (auto _ : state)
  {
    bench_status |=  bbg05_key_generation_leaf_node_from_parent_full_identity(&sk_leaf,
                                                                sk_inner[iteration], leaf_bbg05id[iteration], params);

    bbg05_set_secret_key_leaf_node_to_zero(sk_leaf);
    free(sk_leaf);
  }

  bbg05_set_master_key_to_zero(mk);
  bbg05_set_public_params_to_zero(params);
  free(mk);
  free(params);

  for(int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    bbg05_set_secret_key_inner_node_to_zero(sk_inner[i]);
    free(sk_inner[i]);
    free(bbg05id[i]);
    free(leaf_bbg05id[i]);
  }

  if(bench_status == STS_ERR) {
    printf("Error in benchmark_hibe_key_generation_leaf_node_from_parent_bench.\n");
  }
}

/**
 * Benchmarks encapsulate of BBG 05 HIBE.
 */
static void benchmark_hibe_encapsulate(benchmark::State& state) {
  int bench_status = 0;

  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;
  bbg05_ciphertext_t* ciphertext;
  bbg05_key_t* key;

  // random identity for every loop iteration
  bbg05_identity_t *bbg05id [MAX_NUM_BENCH_ITERATIONS];
  for(int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    random_identity(&bbg05id[i], MAX_DEPTH);
  }

  bench_status |= bbg05_setup(&mk, &params, MAX_DEPTH);

  int iteration = 0;
  for (auto _ : state)
  {
    bench_status |=  bbg05_encapsulate(&ciphertext, &key, params,
                                       bbg05id[iteration]);

    bbg05_set_ciphertext_to_zero(ciphertext);
    free(ciphertext);
    bbg05_set_key_to_zero(key);
    free(key);

    iteration++;
  }

  bbg05_set_master_key_to_zero(mk);
  bbg05_set_public_params_to_zero(params);
  free(mk);
  free(params);

  for(int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    free(bbg05id[i]);
  }

  if(bench_status == STS_ERR) {
    printf("Error in benchmark_hibe_encapsulate.\n");
  }
}

/**
 * Benchmarks decapsulate of BBG 05 HIBE.
 */
static void benchmark_hibe_decapsulate(benchmark::State& state) {
  int bench_status = 0;

  bbg05_master_key_t *mk;
  bbg05_public_params_t *params;
  bbg05_key_t* key;
  bbg05_key_t* decapsulatedKey;

  // random identity for every loop iteration
  bbg05_identity_t *bbg05id [MAX_NUM_BENCH_ITERATIONS];
  bbg05_secret_key_inner_node_t *sk_leaf [MAX_NUM_BENCH_ITERATIONS];
  bbg05_ciphertext_t* ciphertext [MAX_NUM_BENCH_ITERATIONS];

  bench_status |= bbg05_setup(&mk, &params, MAX_DEPTH);

  for(int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    random_identity(&bbg05id[i], MAX_DEPTH);
    bench_status |= bbg05_key_generation_inner_node_from_master_key(&sk_leaf[i], mk, bbg05id[i], params);
    bench_status |=  bbg05_encapsulate(&ciphertext[i], &key, params, bbg05id[i]);

    // we do not need the key for the benchmark
    bbg05_set_key_to_zero(key);
    free(key);
  }

  int iteration = 0;
  for (auto _ : state)
  {
    bench_status |= bbg05_decapsulate(&decapsulatedKey, sk_leaf[iteration],
                                      ciphertext[iteration], params);

    bbg05_set_key_to_zero(decapsulatedKey);
    free(decapsulatedKey);

    iteration++;
  }

  bbg05_set_master_key_to_zero(mk);
  bbg05_set_public_params_to_zero(params);
  free(mk);
  free(params);

  for(int i = 0; i < MAX_NUM_BENCH_ITERATIONS; i++) {
    bbg05_set_secret_key_inner_node_to_zero(sk_leaf[i]);
    bbg05_set_ciphertext_to_zero(ciphertext[i]);
    free(sk_leaf[i]);
    free(ciphertext[i]);
    free(bbg05id[i]);
  }

  if(bench_status == STS_ERR) {
    printf("Error in benchmark_hibe_decapsulate.\n");
  }
}


BENCHMARK_MS(benchmark_hibe_setup);
BENCHMARK_MS(benchmark_hibe_key_generation_leaf_node_from_parent_bench);
BENCHMARK_MS(benchmark_hibe_encapsulate);
BENCHMARK_MS(benchmark_hibe_decapsulate);

int main(int argc, char** argv) {
  ::benchmark::Initialize(&argc, argv);
  if (::benchmark::ReportUnrecognizedArguments(argc, argv))
    return 1;

  if (bbg05_init() != STS_OK) {
    return 1;
  }

  ::benchmark::RunSpecifiedBenchmarks();
}

void random_identity(bbg05_identity_t **identity, unsigned depth) {
  unsigned id_depth = depth;
  *identity = (bbg05_identity_t *) malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth; i++) {
    (*identity)->id[i] = rand();
  }
  (*identity)->depth = id_depth;
}

void random_child_identity(bbg05_identity_t **child, bbg05_identity_t *parent) {
  unsigned id_depth = parent->depth + 1;
  *child = (bbg05_identity_t *) malloc(offsetof(bbg05_identity_t, id) +
      id_depth * sizeof(unsigned));
  for(unsigned i = 0; i < id_depth - 1; i++) {
    (*child)->id[i] = parent->id[i];
  }
  (*child)->id[id_depth] = rand();

  (*child)->depth = id_depth;
}