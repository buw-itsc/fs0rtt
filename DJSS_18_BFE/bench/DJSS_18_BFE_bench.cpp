// Copyright [2018] PG 0-RTT

#include<benchmark/benchmark.h>
#include "BFE_IBBE/BloomFilterEncryptionIBBE.h"
#include "BFE_IBBE/CiphertextIBBE.h"
#include "BFE_IBBE/KeyIBBE.h"

using bfe::BloomFilterEncryptionIBBE;
using bfe::Ciphertext;

#define BENCHMARK_MILLISECONDS(n) BENCHMARK(n)->Unit(benchmark::kMillisecond)
#define BENCHMARK_MICROSECONDS(n) BENCHMARK(n)->Unit(benchmark::kMicrosecond)

// Hopefully decapsulate is not benchmarked for more iterations than this
static const int MAX_ITERATIONS = 150;

static const long m = 10000;
static const int k = 10;

static void benchmark_generate10000Keys(benchmark::State& state) {
  for (auto _ : state) {
    auto bfeIBBE = new BloomFilterEncryptionIBBE(m, k);
    bfeIBBE->keyGeneration(false);
  }
}

static void benchmark_readIn10000Keys(benchmark::State& state) {
  auto bfeIBBE = new BloomFilterEncryptionIBBE(m, k);
  auto bfeIBBE2 = new BloomFilterEncryptionIBBE(m, k);
  std::string secretKeyFileName = bfeIBBE->KEY_FILE_PREFIX + "_" + std::to_string(m)
      + "_" + std::to_string(k) + "." + bfeIBBE->SECRET_KEY_FILE_EXTENSION;
  std::string publicKeyFileName = bfeIBBE->KEY_FILE_PREFIX + "_" + std::to_string(m)
      + "_" + std::to_string(k) + "." + bfeIBBE->PUBLIC_KEY_FILE_EXTENSION;
  std::remove(secretKeyFileName.c_str());
  std::remove(publicKeyFileName.c_str());
  bfeIBBE->keyGeneration(true);
  for (auto _ : state) {
    bfeIBBE2->keyGeneration(true);
  }
}

static void benchmark_encapsulate(benchmark::State& state) {
  auto bfeIBBE = new BloomFilterEncryptionIBBE(m, k);
  bfeIBBE->keyGeneration(false);

  for (auto _ : state) {
    auto tmp = bfeIBBE->encapsulate();
  }
}

static void benchmark_decapsulate(benchmark::State& state) {
  auto bfeIBBE = new BloomFilterEncryptionIBBE(m, k);
  bfeIBBE->keyGeneration(false);

  // Need different ciphertexts for each iteration since secret key is punctured
  std::vector<std::unique_ptr<Ciphertext>> ciphertexts;

  for (int i = 0; i < MAX_ITERATIONS; i++) {
    auto tmp = bfeIBBE->encapsulate();
    ciphertexts.push_back(move(tmp.second));
  }

  unsigned long iteration = 0;
  for (auto _ : state) {
    auto decapsulatedKey = bfeIBBE->decapsulate(
        ciphertexts.at(iteration).get());
    iteration++;
  }
}

BENCHMARK_MILLISECONDS(benchmark_generate10000Keys);
BENCHMARK_MILLISECONDS(benchmark_readIn10000Keys);
BENCHMARK_MILLISECONDS(benchmark_encapsulate);
BENCHMARK_MILLISECONDS(benchmark_decapsulate);

BENCHMARK_MAIN();
