# 0-RTT Forward-Secure Key Exchange Protocol for QUIC

This is a proof-of-concept implementation of a forward-secure 0-RTT key exchange integrated into Google's QUIC transport protocol. The implementation was developed by seven students during a two-semester "project group" course offered at Paderborn University. The development was supervised by Kai Gellert and Tibor Jager.

The implementation uses puncturable encryption to transfer the symmetric keys, achieving forward security, resilience against replay attacks and 0-RTT key exchange.
The Bloom Filter Encryption (BFE) scheme used is presented in the paper [Bloom Filter Encryption and Applications to Efficient Forward-Secret 0-RTT Key Exchange](https://eprint.iacr.org/2018/199).

## Folder structure
### Crypto library
- BBG_05_HIBE: Implementation of [Hierarchical Identity Based Encryption with Constant Size Ciphertext](https://eprint.iacr.org/2005/015.pdf)
- Delerablee_07_IBBE: Implementation of [Identity-Based Broadcast Encryption with Constant Size Ciphertexts and Private Keys](https://link.springer.com/content/pdf/10.1007/978-3-540-76900-2_12.pdf)
- DJSS_18_BFE: Implementation of [Bloom Filter Encryption and Applications to Efficient Forward-Secret 0-RTT Key Exchange](https://eprint.iacr.org/2018/199)
- c_crypto_wrapper: Wrapper for the BFE C++ library that exposes a C interface

### QUIC
- src: Source code of Chromium that includes a QUIC module that has been modified for FS0RTT
- src/net/third_party/quic/core: The folder containing most of the modifications

###  Building
- depot_tools: Tool collection needed for building chromium
- building: Resources for creating the custom docker image used by the CI for building

### Evaluation
- function_test: Script that tests retrieving a single file from a QUIC server, either using the original QUIC cryptography or the FS0RTT modification
- test subfolders of the crypto library: Unit tests for each component
