#!/bin/bash

if [ "$QUIC_ROOT" == "" ]; then
    echo "QUIC_ROOT is not set"
    exit 1
fi

echo "_____ running src/third_party/binutils/download.py"
$QUIC_ROOT/third_party/binutils/download.py
echo "_____ running python src/build/linux/sysroot_scripts/install-sysroot.py --running-as-hook"
python $QUIC_ROOT/build/linux/sysroot_scripts/install-sysroot.py --running-as-hook 
