This is a testharness.js-based test.
FAIL .grid 1 assert_equals: 
<div class="grid width300 justifyItemsBaseline">
  <div class="firstRowFirstColumn fixedWidth" data-offset-x="30" data-offset-y="0" data-expected-width="125" data-expected-height="100"></div>
  <div class="secondRowFirstColumn bigFont paddingLeft verticalLR" data-offset-x="0" data-offset-y="100" data-expected-width="120" data-expected-height="200">É É ÉÉ</div>
  <div class="autoRowSpanning2AutoColumn width25"></div>
</div>
offsetLeft expected 30 but got 0
FAIL .grid 2 assert_equals: 
<div class="grid width300 justifyItemsBaseline">
  <div class="firstRowFirstColumn fixedWidth" data-offset-x="60" data-offset-y="0" data-expected-width="125" data-expected-height="100"></div>
  <div class="secondRowFirstColumn bigFont paddingRight verticalRL" data-offset-x="0" data-offset-y="100" data-expected-width="120" data-expected-height="200">É É ÉÉ</div>
  <div class="autoRowSpanning2AutoColumn width25"></div>
</div>
offsetLeft expected 60 but got 120
FAIL .grid 3 assert_equals: 
<div class="grid justifyItemsBaseline verticalLR">
  <div class="firstRowFirstColumn fixedHeight" data-offset-x="0" data-offset-y="0" data-expected-width="100" data-expected-height="125"></div>
  <div class="secondRowFirstColumn bigFont horizontalTB" data-offset-x="100" data-offset-y="85" data-expected-width="200" data-expected-height="100">É É ÉÉ</div>
  <div class="autoRowSpanning2AutoColumn height25"></div>
</div>
offsetTop expected 85 but got 25
FAIL .grid 4 assert_equals: 
<div class="grid justifyItemsBaseline verticalRL">
  <div class="firstRowFirstColumn fixedHeight" data-offset-x="200" data-offset-y="0" data-expected-width="100" data-expected-height="125"></div>
  <div class="secondRowFirstColumn bigFont horizontalTB" data-offset-x="0" data-offset-y="85" data-expected-width="200" data-expected-height="100">É É ÉÉ</div>
  <div class="autoRowSpanning2AutoColumn height25"></div>
</div>
offsetTop expected 85 but got 25
Harness: the test ran to completion.

