This is a testharness.js-based test.
PASS .grid 1
FAIL .grid 2 assert_equals: 
<div class="grid alignItemsBaseline verticalLR">
  <div class="firstRowFirstColumn fixedWidth" data-offset-x="30" data-offset-y="0" data-expected-width="125" data-expected-height="100"></div>
  <div class="firstRowSecondColumn bigFont paddingLeft" data-offset-x="0" data-offset-y="100" data-expected-width="120" data-expected-height="200">É É ÉÉ</div>
  <div class="autoRowAutoColumnSpanning2 width25"></div>
</div>
offsetLeft expected 30 but got 60
PASS .grid 3
Harness: the test ran to completion.

