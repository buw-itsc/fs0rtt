layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x320
  LayoutBlockFlow {HTML} at (0,0) size 800x320
    LayoutBlockFlow {BODY} at (8,8) size 784x304
      LayoutText {#text} at (0,0) size 0x0
layer at (8,8) size 400x300
  LayoutVideo {VIDEO} at (0,0) size 400x300
layer at (8,8) size 400x300
  LayoutFlexibleBox (relative positioned) {DIV} at (0,0) size 400x300
    LayoutBlockFlow {DIV} at (200,0) size 0x0
    LayoutFlexibleBox {DIV} at (0,0) size 400x300
layer at (8,8) size 400x300
  LayoutFlexibleBox {DIV} at (0,0) size 400x300
layer at (8,8) size 400x281 clip at (9,9) size 398x279
  LayoutButton (relative positioned) {INPUT} at (0,0) size 400x281 [border: (1px solid #D8D8D8) (1px solid #D1D1D1) (1px solid #BABABA) (1px solid #D1D1D1)]
    LayoutFlexibleBox (anonymous) at (8,112) size 384x56
      LayoutBlockFlow {DIV} at (164,0) size 56x56 [bgcolor=#FFFFFFE6]
layer at (8,228) size 400x48
  LayoutFlexibleBox (relative positioned) {DIV} at (0,220) size 400x48
    LayoutBlockFlow {DIV} at (16,0) size 27.25x48 [color=#FFFFFF]
      LayoutText {#text} at (0,16) size 28x16
        text run at (0,16) width 28: "0:00"
    LayoutBlockFlow {DIV} at (47.25,0) size 35.03x48 [color=#FFFFFF]
      LayoutText {#text} at (0,16) size 36x16
        text run at (0,16) width 36: "/ 0:06"
    LayoutBlockFlow {DIV} at (82.28,48) size 211.72x0
    LayoutButton {INPUT} at (294,0) size 48x48
    LayoutButton {INPUT} at (342,0) size 48x48
layer at (8,276) size 400x32
  LayoutSlider {INPUT} at (0,268) size 400x32 [color=#909090]
    LayoutFlexibleBox {DIV} at (16,16) size 368x4
layer at (24,292) size 368x4
  LayoutBlockFlow (relative positioned) {DIV} at (0,0) size 368x4 [bgcolor=#FFFFFF4D]
layer at (24,288) size 12x12
  LayoutBlockFlow (relative positioned) {DIV} at (0,-4) size 12x12 [bgcolor=#FFFFFF]
layer at (24,292) size 368x4
  LayoutBlockFlow (positioned) {DIV} at (0,0) size 368x4
layer at (24,292) size 0x4
  LayoutBlockFlow (positioned) {DIV} at (0,0) size 0x4 [bgcolor=#FFFFFF]
layer at (24,292) size 368x4
  LayoutBlockFlow (positioned) {DIV} at (0,0) size 368x4 [bgcolor=#FFFFFF8A]
