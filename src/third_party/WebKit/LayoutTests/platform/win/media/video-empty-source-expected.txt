layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x584
      LayoutBlockFlow {P} at (0,0) size 784x20
        LayoutText {#text} at (0,0) size 408x19
          text run at (0,0) width 408: "Slider drawing with no source. The controls should render correctly."
      LayoutBlockFlow (anonymous) at (0,36) size 784x152
        LayoutText {#text} at (0,0) size 0x0
layer at (8,44) size 302x152
  LayoutVideo {VIDEO} at (0,0) size 302x152 [border: (1px solid #000000)]
layer at (9,45) size 300x150
  LayoutFlexibleBox (relative positioned) {DIV} at (1,1) size 300x150 [bgcolor=#333333]
    LayoutBlockFlow {DIV} at (150,0) size 0x0
    LayoutFlexibleBox {DIV} at (0,0) size 300x150
layer at (9,45) size 300x150
  LayoutFlexibleBox {DIV} at (0,0) size 300x150
layer at (9,45) size 300x131 clip at (11,47) size 296x127
  LayoutButton (relative positioned) {INPUT} at (0,0) size 300x131 [border: (2px outset #C0C0C0)]
    LayoutFlexibleBox (anonymous) at (8,37.50) size 284x56
layer at (131,83) size 56x56 transparent
  LayoutBlockFlow {DIV} at (114,0) size 56x56 [bgcolor=#FFFFFFE6]
layer at (9,115) size 300x48
  LayoutFlexibleBox (relative positioned) {DIV} at (0,70) size 300x48
    LayoutBlockFlow {DIV} at (16,0) size 28x48 [color=#FFFFFF]
      LayoutText {#text} at (0,16) size 28x16
        text run at (0,16) width 28: "0:00"
    LayoutBlockFlow {DIV} at (44,48) size 102x0
layer at (155,115) size 48x48 transparent
  LayoutButton {INPUT} at (146,0) size 48x48 [color=#808080]
layer at (203,115) size 48x48 transparent
  LayoutButton {INPUT} at (194,0) size 48x48 [color=#808080]
layer at (251,115) size 48x48 transparent
  LayoutButton {INPUT} at (242,0) size 48x48 [color=#808080]
layer at (9,163) size 300x32
  LayoutSlider {INPUT} at (0,118) size 300x32 [color=#C4C4C4]
    LayoutFlexibleBox {DIV} at (16,16) size 268x4
layer at (25,179) size 268x4
  LayoutBlockFlow (relative positioned) {DIV} at (0,0) size 268x4 [bgcolor=#FFFFFF4D]
layer at (25,179) size 268x4
  LayoutBlockFlow (positioned) {DIV} at (0,0) size 268x4
layer at (25,179) size 0x4
  LayoutBlockFlow (positioned) {DIV} at (0,0) size 0x4 [bgcolor=#FFFFFF]
layer at (25,179) size 0x4
  LayoutBlockFlow (positioned) {DIV} at (0,0) size 0x4 [bgcolor=#FFFFFF8A]
