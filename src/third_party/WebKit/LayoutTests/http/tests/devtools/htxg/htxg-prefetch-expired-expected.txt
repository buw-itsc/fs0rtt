Tests the signed exchange information are available when the prefetch failed.

* http://127.0.0.1:8000/loading/htxg/resources/htxg-location.sxg
  failed: true
  statusCode: 200
  resourceType: other
  SignedExchangeInfo
    Request URL: https://www.127.0.0.1/test.html
    Certificate URL: http://localhost:8000/loading/htxg/resources/127.0.0.1.pem.cbor
    Error: {"message":"Invalid timestamp. creation_time: 1522540800, expires_time: 1523145600, verification_time: 1523318460"}
    Error: {"message":"Failed to verify the signed exchange header.","signatureIndex":0,"errorField":"signatureTimestamps"}
* http://localhost:8000/loading/htxg/resources/127.0.0.1.pem.cbor
  failed: false
  statusCode: 200
  resourceType: other

