// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef NET_THIRD_PARTY_QUIC_CORE_ZERO_SERVER_HANDSHAKER_H_
#define NET_THIRD_PARTY_QUIC_CORE_ZERO_SERVER_HANDSHAKER_H_

#include "net/third_party/quic/core/quic_crypto_handshaker.h"
#include "net/third_party/quic/core/quic_crypto_server_stream.h"
#include "net/third_party/quic/core/quic_session.h"
#include "net/third_party/quic/platform/api/quic_export.h"
#include "net/third_party/quic/platform/api/quic_string.h"
#include "net/third_party/quic/core/quic_crypto_server_handshaker.h"

namespace quic {

/// This class implements the HandshakerDelegate interface of the
// QuicCryptoClientStream using a 0-RTT crypto protocol.
//
// The handshaker reads and writes bytes on the crypto stream
class QUIC_EXPORT_PRIVATE Fs0rttServerHandshaker :
    public QuicCryptoServerHandshaker {
 public:

  Fs0rttServerHandshaker(const Fs0rttCryptoServerConfig *z_crypto_config_,
                             QuicCryptoServerStream *stream,
                             QuicCompressedCertsCache *compressed_certs_cache,
                             QuicSession *session,
                             QuicCryptoServerStream::Helper *helper);

  ~Fs0rttServerHandshaker() override;

  // Cancel any outstanding callbacks, such as asynchronous validation of
  // client hello.
  void CancelOutstandingCallbacks() override;

  void SendServerConfigUpdate(
      const CachedNetworkParameters *cached_network_params) override;

  void OnHandshakeMessage(const CryptoHandshakeMessage &message) override;

  const CachedNetworkParameters *PreviousCachedNetworkParams() const override;

  bool encryption_established() const override;

  bool handshake_confirmed() const override;

  CryptoMessageParser *crypto_message_parser() override;

private:

    // Invoked by ValidateCallback::RunImpl once initial validation of
    // the client hello is complete.  Finishes processing of the client
    // hello message and handles handshake success/failure.
    void FinishProcessingHandshakeMessage(
            QuicReferenceCountedPointer<ValidateClientHelloResultCallback::Result>
            result,
            std::unique_ptr<ProofSource::Details> details);

    // Portion of FinishProcessingHandshakeMessage which executes after
    // ProcessClientHello has been called.
    void FinishProcessingHandshakeMessageAfterProcessClientHello(
            const ValidateClientHelloResultCallback::Result& result,
            QuicErrorCode error,
            const QuicString& error_details,
            std::unique_ptr<CryptoHandshakeMessage> reply,
            std::unique_ptr<DiversificationNonce> diversification_nonce,
            std::unique_ptr<ProofSource::Details> proof_source_details);

    // Returns the QuicTransportVersion of the connection.
    QuicTransportVersion transport_version() const {
        return /*session_*/session()->connection()->transport_version();
    }
};

}  // namespace quic

#endif  // NET_THIRD_PARTY_QUIC_CORE_ZERO_SERVER_HANDSHAKER_H_
