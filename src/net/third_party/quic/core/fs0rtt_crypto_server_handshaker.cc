// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/third_party/quic/core/fs0rtt_crypto_server_handshaker.h"

#include <memory>
#include "net/third_party/quic/core/crypto/fs0rtt_crypto_server_config.h"

#include "net/third_party/quic/core/quic_session.h"

namespace quic {

void Fs0rttServerHandshaker::
FinishProcessingHandshakeMessageAfterProcessClientHello(
        const ValidateClientHelloResultCallback::Result& result,
        QuicErrorCode error,
        const QuicString& error_details,
        std::unique_ptr<CryptoHandshakeMessage> reply,
        std::unique_ptr<DiversificationNonce> diversification_nonce,
        std::unique_ptr<ProofSource::Details> proof_source_details) {
    // Clear the callback that got us here.
    DCHECK(process_client_hello_cb()/*process_client_hello_cb_*/ != nullptr);
    DCHECK(validate_client_hello_cb()/*validate_client_hello_cb_*/ == nullptr);
    reset_process_client_hello_cb();// process_client_hello_cb_ = nullptr;


    const CryptoHandshakeMessage& message = result.client_hello;
    if (error != QUIC_NO_ERROR) {
        stream()->CloseConnectionWithDetails(error, error_details);
        return;
    }

    if (reply->tag() != kSHLO) {
        if (reply->tag() == kSREJ) {
            DCHECK(stream()->UseStatelessRejectsIfPeerSupported());
            DCHECK(stream()->PeerSupportsStatelessRejects());
            // Before sending the SREJ, cause the connection to save crypto packets
            // so that they can be added to the time wait list manager and
            // retransmitted.
            session()->connection()->EnableSavingCryptoPackets();
        }
        VLOG(1) << "LATENCY: Start sending server REJECTION (REJ)";
        SendHandshakeMessage(*reply);

        if (reply->tag() == kSREJ) {
            DCHECK(stream()->UseStatelessRejectsIfPeerSupported());
            DCHECK(stream()->PeerSupportsStatelessRejects());
            DCHECK(!handshake_confirmed());
            QUIC_DLOG(INFO) << "Closing connection "
                            << session()->connection()->connection_id()
                            << " because of a stateless reject.";
            session()->connection()->CloseConnection(
                    QUIC_CRYPTO_HANDSHAKE_STATELESS_REJECT, "stateless reject",
                    ConnectionCloseBehavior::SILENT_CLOSE);
        }
        return;
    }

    // If we are returning a SHLO then we accepted the handshake.  Now
    // process the negotiated configuration options as part of the
    // session config.
    QuicConfig* config = session()->config();
    OverrideQuicConfigDefaults(config);
    QuicString process_error_details;
    const QuicErrorCode process_error =
            config->ProcessPeerHello(message, CLIENT, &process_error_details);
    if (process_error != QUIC_NO_ERROR) {
        stream()->CloseConnectionWithDetails(process_error, process_error_details);
        return;
    }

    session()->OnConfigNegotiated();

    config->ToHandshakeMessage(reply.get());

    // Receiving a full CHLO implies the client is prepared to decrypt with
    // the new server write key.  We can start to encrypt with the new server
    // write key.
    //
    // NOTE: the SHLO will be encrypted with the new server write key.


    session()->connection()->SetEncrypter(
            ENCRYPTION_INITIAL,
            std::move(crypto_negotiated_params_ptr()->initial_crypters.encrypter));
    session()->connection()->SetDefaultEncryptionLevel(ENCRYPTION_INITIAL);
    // Set the decrypter immediately so that we no longer accept unencrypted
    // packets.
    session()->connection()->SetDecrypter(
            ENCRYPTION_INITIAL,
            std::move(crypto_negotiated_params_ptr()->initial_crypters.decrypter));
    session()->connection()->SetDiversificationNonce(*diversification_nonce);

    LOG(INFO) << "LATENCY: Start sending server HELLO (SHLO)";
    SendHandshakeMessage(*reply);

    session()->connection()->SetEncrypter(
            ENCRYPTION_FORWARD_SECURE,
            std::move(crypto_negotiated_params_ptr()->forward_secure_crypters.encrypter));
    session()->connection()->SetDefaultEncryptionLevel(ENCRYPTION_FORWARD_SECURE);

    session()->connection()->SetAlternativeDecrypter(
            ENCRYPTION_FORWARD_SECURE,
            std::move(crypto_negotiated_params_ptr()->forward_secure_crypters.decrypter),
            false /* don't latch */);

    set_encryption_established(true);  // encryption_established_ = true;
    set_handshake_confirmed(true);  // handshake_confirmed_ = true;
    session()->OnCryptoHandshakeEvent(QuicSession::HANDSHAKE_CONFIRMED);

    LOG(INFO) << "LATENCY: END Processing CHLO";
}

void Fs0rttServerHandshaker::SendServerConfigUpdate(
    const CachedNetworkParameters *cached_network_params) {
  QuicCryptoServerHandshaker::SendServerConfigUpdate(cached_network_params);
}
void Fs0rttServerHandshaker::OnHandshakeMessage(
    const CryptoHandshakeMessage &message) {

    DVLOG(1) << "Begin of OnHandshakeMessage";
    QuicCryptoHandshaker::OnHandshakeMessage(message);
    DVLOG(1) << "OnHandshakeMessage(message) excecuted";


    inc_num_handshake_msgs();  // ++num_handshake_messages_;
    set_chlo_packet_size(session()->connection()->GetCurrentPacket().length());



    DVLOG(1) << "if (handshake_confirmed_)";
    // Do not process handshake messages after the handshake is confirmed.
    if (handshake_confirmed()) {
        stream()->CloseConnectionWithDetails(
                QUIC_CRYPTO_MESSAGE_AFTER_HANDSHAKE_COMPLETE,
                "Unexpected handshake message from client");
        return;
    }

    if (message.tag() != kCHLO) {
        stream()->CloseConnectionWithDetails(QUIC_INVALID_CRYPTO_MESSAGE_TYPE,
                                            "Handshake packet not CHLO");
        return;
    }
    DVLOG(1) << "if (validate_client_hello_cb_ != nullptr || process_client_hello_cb_ != nullptr)";
    if (validate_client_hello_cb()/*validate_client_hello_cb_*/ != nullptr ||
        process_client_hello_cb()/*process_client_hello_cb_ */ != nullptr) {
        // Already processing some other handshake message.  The protocol
        // does not allow for clients to send multiple handshake messages
        // before the server has a chance to respond.
        stream()->CloseConnectionWithDetails(
                QUIC_CRYPTO_MESSAGE_WHILE_VALIDATING_CLIENT_HELLO,
                "Unexpected handshake message while processing CHLO");
        return;
    }

    LOG(INFO) << "LATENCY: Start processing CLIENT HELLO";

    DVLOG(1) << "Start Hashing HandshakeMessage";
    CryptoUtils::HashHandshakeMessage(message, chlo_hash_ref()/*&chlo_hash_*/,
                                      Perspective::IS_SERVER);

    DVLOG(1) << "Call Validate Client Hello";
    std::unique_ptr<ValidateCallback> cb(new ValidateCallback(this));
    DCHECK(validate_client_hello_cb()/*validate_client_hello_cb_*/ == nullptr);
    DCHECK(process_client_hello_cb()/*process_client_hello_cb_*/ == nullptr);
    set_validate_client_hello_cb(cb.get());  // validate_client_hello_cb_ = cb.get();
    crypto_config()->ValidateClientHello(
            message, GetClientAddress().host(),
            session()->connection()->self_address(), transport_version(),
            session()->connection()->clock(), signed_config()/*signed_config_*/, std::move(cb));
}
void Fs0rttServerHandshaker::FinishProcessingHandshakeMessage(
        QuicReferenceCountedPointer<ValidateClientHelloResultCallback::Result>
        result,
        std::unique_ptr<ProofSource::Details> details) {
    DVLOG(1) << "Entered method FinishProcessingHandshakeMessage";
    const CryptoHandshakeMessage& message = result->client_hello;

    // Clear the callback that got us here.
    DCHECK(validate_client_hello_cb()/*validate_client_hello_cb_*/ != nullptr);
    DCHECK(process_client_hello_cb()/*process_client_hello_cb_*/ == nullptr);
    reset_validate_client_hello_cb();  //validate_client_hello_cb_ = nullptr;

    if (stream()->UseStatelessRejectsIfPeerSupported()) {
        stream()->SetPeerSupportsStatelessRejects(
                QuicCryptoServerStreamBase::DoesPeerSupportStatelessRejects(message));
    }

    std::unique_ptr<ProcessClientHelloCallback> cb(
            new ProcessClientHelloCallback(this, result));
    set_process_client_hello_cb(cb.get());  // process_client_hello_cb_ = cb.get();
    ProcessClientHello(result, std::move(details), std::move(cb));
}
const CachedNetworkParameters *Fs0rttServerHandshaker::
    PreviousCachedNetworkParams() const {
  return QuicCryptoServerHandshaker::PreviousCachedNetworkParams();
}
bool Fs0rttServerHandshaker::encryption_established() const {
  return QuicCryptoServerHandshaker::encryption_established();
}
bool Fs0rttServerHandshaker::handshake_confirmed() const {
  return QuicCryptoServerHandshaker::handshake_confirmed();
}
CryptoMessageParser *Fs0rttServerHandshaker::crypto_message_parser() {
  return QuicCryptoServerHandshaker::crypto_message_parser();
}

Fs0rttServerHandshaker::~Fs0rttServerHandshaker() {
}
Fs0rttServerHandshaker::Fs0rttServerHandshaker(
    const Fs0rttCryptoServerConfig *z_crypto_config,
    QuicCryptoServerStream *stream,
    QuicCompressedCertsCache *compressed_certs_cache,
    QuicSession *session,
    QuicCryptoServerStream::Helper *helper) : QuicCryptoServerHandshaker(
            z_crypto_config,
            stream,
            compressed_certs_cache,
            session,
            helper)
{}

void Fs0rttServerHandshaker::CancelOutstandingCallbacks() {
  QuicCryptoServerHandshaker::CancelOutstandingCallbacks();
}

}  // namespace quic
