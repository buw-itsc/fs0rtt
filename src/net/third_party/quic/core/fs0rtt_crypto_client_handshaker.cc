// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "net/third_party/quic/core/fs0rtt_crypto_client_handshaker.h"

#include <memory>
#include <iostream>

#include "base/metrics/histogram_functions.h"
#include "base/metrics/histogram_macros.h"
#include "net/third_party/quic/core/crypto/crypto_protocol.h"
#include "net/third_party/quic/core/crypto/crypto_utils.h"
#include "net/third_party/quic/core/quic_session.h"
#include "net/third_party/quic/platform/api/quic_flags.h"
#include "net/third_party/quic/platform/api/quic_logging.h"
#include "net/third_party/quic/platform/api/quic_str_cat.h"
#include "net/third_party/quic/platform/api/quic_string.h"
#include "net/third_party/quic/platform/api/quic_text_utils.h"

namespace quic {

Fs0rttClientHandshaker::Fs0rttClientHandshaker(
    const QuicServerId &server_id,
    QuicCryptoClientStream *stream,
    QuicSession *session,
    ProofVerifyContext *verify_context,
    Fs0rttCryptoClientConfig *z_crypto_config,
    QuicCryptoClientStream::ProofHandler *proof_handler)
    : QuicCryptoClientHandshaker(server_id, stream,
                                 session, verify_context, z_crypto_config, proof_handler)
{}

Fs0rttClientHandshaker::~Fs0rttClientHandshaker() {
  // TODO(Jan Drees): implement
}

// Performs a crypto handshake with the server. Returns true if the
// connection is still connected.
bool Fs0rttClientHandshaker::CryptoConnect() {
  setNextState(STATE_INITIALIZE) ;
  DoHandshakeLoop(nullptr);
  return session()->connection()->connected();
}

//This Method gets called if the client receives a handshake message. If the connection is already established the connection gets closed.
void Fs0rttClientHandshaker::OnHandshakeMessage(const CryptoHandshakeMessage &message) {
  QuicCryptoHandshaker::OnHandshakeMessage(message);

  // Do not process handshake messages after the handshake is confirmed.
  if (handshake_confirmed()) {
    stream()->CloseConnectionWithDetails(
        QUIC_CRYPTO_MESSAGE_AFTER_HANDSHAKE_COMPLETE,
        "Unexpected handshake message");
    return;
  }

  DoHandshakeLoop(&message);
}

//Preforms the Statechart of the Client
void Fs0rttClientHandshaker::DoHandshakeLoop(
    const CryptoHandshakeMessage *in) {
  QuicCryptoClientConfig::CachedState *cached =
      crypto_config_->LookupOrCreate(server_id());

  QuicAsyncStatus rv = QUIC_SUCCESS;
  DVLOG(1)<< "DOHANDSHAKELOOP";

  do {
    CHECK_NE(STATE_NONE, getNextState());
    const State state = getNextState();
    setNextState( STATE_IDLE);
    rv = QUIC_SUCCESS;
    switch (state) {
      // Initialises the verifyer, which verify the SEPK
      case STATE_INITIALIZE:
        DVLOG(1) << "New state STATE_INITIALIZE";
        DoInitialize(cached);
        DVLOG(1) << "STATE_INITIALIZE ended";
        break;
      //Requests a Channel_ID if needed
      case STATE_GET_CHANNEL_ID:
        LOG(INFO) << "LATENCY: START STATE_GET_CHANNEL_ID";
        rv = DoGetChannelID(cached);
        LOG(INFO) << "LATENCY: END STATE_GET_CHANNEL_ID";
        break;
      //Completes the Channel_ID request
      case STATE_GET_CHANNEL_ID_COMPLETE:
        DVLOG(1) << "START STATE_GET_CHANNEL_ID_COMPLETE";
        DoGetChannelIDComplete();
        DVLOG(1) << "END STATE_GET_CHANNEL_ID_COMPLETE";
        break;
      //Decides if the Handshaker does a 1-RTT or 0-RTT connection
      case STATE_SEND_CHLO:
        DVLOG(1) << "New state STATE_SEND_CHLO";
        CheckFor0RTT(cached);
        DVLOG(1) << "EndState state STATE_SEND_CHLO";
        break;
      //Sends a complete CHLO with the CTXT
      case STATE_SEND_CHLO_0RTT:
        DVLOG(1) << "New state STATE_SEND_CHLO0RTT";
        LOG(INFO) << "LATENCY: Start building full Client Hello (chlo)";
        DoSendCHLO(cached);
        LOG(INFO) << "LATENCY: Building full chlo ended";
        DVLOG(1) << "STATE_SEND_CHLO0RTT ended";
        return;  // return waiting to hear from server.
      //Sends a inchoate CHLO
      case STATE_SEND_INCHOATE_CHLO:
        DVLOG(1) << "New state STATE_SEND_INCHOATE_CHLO";
        LOG(INFO) << "LATENCY: Start building inchoate chlo";
        DoSendInchoateCHLO(cached);
        LOG(INFO) << "LATENCY: Building building inchoate chlo ended";
        DVLOG(1) << "STATE_SEND_INCHOATE_CHLO ended";
        return;  // return waiting to hear from server.
      //verify the SEPK from the Server
      case STATE_VERIFY_PROOF:
        LOG(INFO) << "LATENCY: Start Verify";
        DVLOG(1) << "New state STATE_VERIFY_PROOF";
        rv = DoVerifyProof(cached);
        DVLOG(1) << "End state STATE_VERIFY_PROOF";
        break;
      //completes the verifying process
      case STATE_VERIFY_PROOF_COMPLETE:
        DVLOG(1) << "New state STATE_VERIFY_PROOF_COMPLETE";
        DoVerifyProofComplete(cached);
        DVLOG(1) << "End state STATE_VERIFY_PROOF_COMPLETE";
        LOG(INFO) << "LATENCY: End Verify";
        break;
      //Processes the received REJ from the Server and saves the SEPK
      case STATE_RECV_REJ:
        DVLOG(1) << "New state STATE_RECV_REJ";
        LOG(INFO) << "LATENCY: Start process server REJ";
        DoReceiveREJ(in, cached);
        LOG(INFO) << "LATENCY: Process server REJ ended";
        DVLOG(1) << "STATE_RECV_REJ ended";
        break;
      //Processes the received SHLO from the Server and completes the handshake
      case STATE_RECV_SHLO:
        DVLOG(1) << "New state STATE_RECV_SHLO";
        LOG(INFO) << "LATENCY: Start process server SHLO";
        DoReceiveSHLO(in, cached);
        LOG(INFO) << "LATENCY: End process server SHLO";
        DVLOG(1) << "STATE_RECV_REJ ended";
        break;
      //Waiting State
      case STATE_IDLE:
        // This means that the peer sent us a message that we weren't expecting.
        stream()->CloseConnectionWithDetails(QUIC_INVALID_CRYPTO_MESSAGE_TYPE,
                                             "Handshake in idle state");
        return;
      //Error state
      case STATE_NONE:QUIC_NOTREACHED();
        return;  // We are done.
    }
  } while (rv != QUIC_PENDING && getNextState() != STATE_NONE);//while (rv != QUIC_PENDING && next_state_ != STATE_NONE);
}

void Fs0rttClientHandshaker::CheckFor0RTT(QuicCryptoClientConfig::CachedState *cached) {

  Fs0rttCryptoClientConfig::Fs0rttCachedState* zcached=static_cast<Fs0rttCryptoClientConfig::Fs0rttCachedState*>(cached);
  if (zcached->getInitialized()) {
    // BFE in the cached state is already initialized. A 0-RTT connection is possible
    DVLOG(1) << "Do a 0-RTT Connection";
    setNextState(STATE_SEND_CHLO_0RTT);
  }
  else {
    setNextState(STATE_SEND_INCHOATE_CHLO);
  }
}


bool Fs0rttClientHandshaker::InitializeSendingCHLO(CryptoHandshakeMessage &out) {
  DVLOG(1) << "Entered InitializeSendingCHLO";
  if (stateless_reject_received()) {
// If we've gotten to this point, we've sent at least one hello
// and received a stateless reject in response.  We cannot
// continue to send hellos because the server has abandoned state
// for this connection.  Abandon further handshakes.
    setNextState(STATE_NONE);

    if (session()->connection()->connected()) {
      session()->connection()->CloseConnection(
          QUIC_CRYPTO_HANDSHAKE_STATELESS_REJECT, "stateless reject received",
          ConnectionCloseBehavior::SILENT_CLOSE);
    }
    return false;
  }
  DVLOG(1) << "After stateless rejection check)";

// Send the client hello in plaintext.
  session()->connection()->SetDefaultEncryptionLevel(ENCRYPTION_NONE);
  DVLOG(1) << "After encryption level is set";
  setEncryptionEstablished(false);
  if (num_sent_client_hellos() > QuicCryptoClientStream::kMaxClientHellos) {
    stream()->CloseConnectionWithDetails(
        QUIC_CRYPTO_TOO_MANY_REJECTS,
        QuicStrCat("More than ", QuicCryptoClientStream::kMaxClientHellos,
                   " rejects"));
    return false;
  }
  setNumClientHello(num_sent_client_hellos()+1);

  DCHECK(session() != nullptr);
  DCHECK(session()->config() != nullptr);
// Send all the options, regardless of whether we're sending an
// inchoate or subsequent hello.
  session()->config()->ToHandshakeMessage(&out);
  return true;
}

void Fs0rttClientHandshaker::DoSendCHLO(QuicCryptoClientConfig::CachedState *cached) {
  CryptoHandshakeMessage out;
  DVLOG(1) << "Inside of DoSendCHLO";
  if(!InitializeSendingCHLO(out)){
    return;
  }
  QuicString error_details;
  DVLOG(1) << "Entering FillClientHello";
  QuicErrorCode error = crypto_config_->FillClientHello(
      server_id(), session()->connection()->connection_id(),
      session()->connection()->supported_versions().front(), cached,
      session()->connection()->clock()->WallNow(),
      session()->connection()->random_generator(), nullptr,
      crypto_negotiated_params_ptr(), &out, &error_details);
  if (error != QUIC_NO_ERROR) {
    // Flush the cached config so that, if it's bad, the server has a
    // chance to send us another in the future.
    DVLOG(1) << "Error while filling CHLO: "<< error_details;
    cached->InvalidateServerConfig();
    stream()->CloseConnectionWithDetails(error, error_details);
    return;
  }
  DVLOG(1) << "Leaving FillClientHello";
  CryptoUtils::HashHandshakeMessage(out, chlo_hash_ptr(), Perspective::IS_CLIENT);

  setChannel_id_sent(isChannel_id_keyCreated());
  if (cached->proof_verify_details()) {
    proof_handler()->OnProofVerifyDetailsAvailable(
        *cached->proof_verify_details());
  }
  DVLOG(1)<<"a";
  setNextState(STATE_RECV_SHLO);
  SendHandshakeMessage(out);
  // Be prepared to decrypt with the new server write key.
  DVLOG(1) << "Starting setting en/-decrypters";
  session()->connection()->SetAlternativeDecrypter(
      ENCRYPTION_INITIAL,
      std::move(crypto_negotiated_params_ptr()->initial_crypters.decrypter),
      true /* latch once used */);
  // Send subsequent packets under encryption on the assumption that the
  // server will accept the handshake.
  session()->connection()->SetEncrypter(
      ENCRYPTION_INITIAL,
      std::move(crypto_negotiated_params_ptr()->initial_crypters.encrypter));
  session()->connection()->SetDefaultEncryptionLevel(ENCRYPTION_INITIAL);
  DVLOG(1) << "Ending setting en/-decrypters";
  // ENCRYPTION_FIRST_ESTABLSIHED
  setEncryptionEstablished(true);
  session()->OnCryptoHandshakeEvent(QuicSession::ENCRYPTION_REESTABLISHED);
}

void Fs0rttClientHandshaker::DoSendInchoateCHLO(QuicCryptoClientConfig::CachedState *cached) {
  CryptoHandshakeMessage out;
  DVLOG(1) << "Entering DoSendInchoateCHLO";
  if(!InitializeSendingCHLO(out)){
    return;
  }
  DVLOG(1) << "After InitializeSendingCHLO check";
  if (!cached->IsComplete(session()->connection()->clock()->WallNow())) {
    crypto_config_->FillInchoateClientHello(
        server_id(), session()->connection()->supported_versions().front(),
        cached, session()->connection()->random_generator(),
        /* demand_x509_proof= */ true, crypto_negotiated_params_ptr(), &out);
    DVLOG(1) << "preferred version: " << session()->connection()->supported_versions().front();
    // Pad the inchoate client hello to fill up a packet.
    const QuicByteCount kFramingOverhead = 50;  // A rough estimate.
    const QuicByteCount max_packet_size =
        session()->connection()->max_packet_length();
    if (max_packet_size <= kFramingOverhead) {
      QUIC_DLOG(DFATAL) << "max_packet_length (" << max_packet_size
                        << ") has no room for framing overhead.";
      RecordInternalErrorLocation(QUIC_CRYPTO_CLIENT_HANDSHAKER_MAX_PACKET);
      stream()->CloseConnectionWithDetails(QUIC_INTERNAL_ERROR,
                                           "max_packet_size too smalll");
      return;
    }
    if (kClientHelloMinimumSize > max_packet_size - kFramingOverhead) {
      QUIC_DLOG(DFATAL) << "Client hello won't fit in a single packet.";
      RecordInternalErrorLocation(QUIC_CRYPTO_CLIENT_HANDSHAKER_CHLO);
      stream()->CloseConnectionWithDetails(QUIC_INTERNAL_ERROR,
                                           "CHLO too large");
      return;
    }
    // FLAGS_quic_reloadable_flag_quic_use_chlo_packet_size
    out.set_minimum_size(
        static_cast<size_t>(max_packet_size - kFramingOverhead));
    setNextState(STATE_RECV_REJ);
    CryptoUtils::HashHandshakeMessage(out, chlo_hash_ptr(), Perspective::IS_CLIENT);
    SendHandshakeMessage(out);
    DVLOG(1) << "Leaving DoSendInchoateCHLO";
    return;
  }
  DVLOG(1) << "Leaving DoSendInchoateCHLO and sending a normal CHLO";
  DoSendCHLO(cached);
}
void Fs0rttClientHandshaker::DoReceiveREJ(const CryptoHandshakeMessage *in, QuicCryptoClientConfig::CachedState *cached) {
  // We sent a dummy CHLO because we didn't have enough information to
  // perform a handshake, or we sent a full hello that the server
  // rejected. Here we hope to have a REJ that contains the information
  // that we need.
  if ((in->tag() != kREJ) && (in->tag() != kSREJ)) {
    DVLOG(1)<< "Waited for a rejection. Received: "<< QuicTagToString(in->tag());
    setNextState(STATE_NONE);
    stream()->CloseConnectionWithDetails(QUIC_INVALID_CRYPTO_MESSAGE_TYPE,
                                         "Expected REJ");
    return;
  }

  QuicTagVector reject_reasons;
  static_assert(sizeof(QuicTag) == sizeof(uint32_t), "header out of sync");
  if (in->GetTaglist(kRREJ, &reject_reasons) == QUIC_NO_ERROR) {
    uint32_t packed_error = 0;
    for (size_t i = 0; i < reject_reasons.size(); ++i) {
      // HANDSHAKE_OK is 0 and don't report that as error.
      if (reject_reasons[i] == HANDSHAKE_OK || reject_reasons[i] >= 32) {
        continue;
      }
      HandshakeFailureReason reason =
          static_cast<HandshakeFailureReason>(reject_reasons[i]);
      packed_error |= 1 << (reason - 1);
    }
    DVLOG(1) << "Reasons for rejection: " << packed_error;
    if (num_sent_client_hellos() == QuicCryptoClientStream::kMaxClientHellos) {
      base::UmaHistogramSparse("Net.QuicClientHelloRejectReasons.TooMany",
                               packed_error);
    }
    base::UmaHistogramSparse("Net.QuicClientHelloRejectReasons.Secure",
                             packed_error);
  }

  // Receipt of a REJ message means that the server received the CHLO
  // so we can cancel and retransmissions.
  session()->NeuterUnencryptedData();
  setStateless_Reject_Received(in->tag() == kSREJ);
  QuicString error_details;
  QuicErrorCode error = crypto_config_->ProcessRejection(
      *in, session()->connection()->clock()->WallNow(),
      session()->connection()->transport_version(), chlo_hash(), cached,
      crypto_negotiated_params_ptr(), &error_details);

  DVLOG(1) << "transport version is: " << session()->connection()->transport_version();

  if (error != QUIC_NO_ERROR) {
    DVLOG(1)<< "Error by processing REJ. Error: " << error_details;
    if(error==ZERO_BFE_CREATING_FAILED){
      //A error happened while caching the received server public key.
      //The Client now ask again for the server public key
      DVLOG(1)<<"Going back to send INCHOATE_CHLO";
      setNextState(STATE_SEND_INCHOATE_CHLO);
      return;
    }
    setNextState(STATE_NONE);
    stream()->CloseConnectionWithDetails(error, error_details);
    return;
  }


   if (!cached->proof_valid()) {
     if (!cached->signature().empty()) {
     // Note that we only verify the proof if the cached proof is not
     // valid. If the cached proof is valid here, someone else must have
     // just added the server config to the cache and verified the proof,
     // so we can assume no CA trust changes or certificate expiration
     // has happened since then.
     setNextState(STATE_VERIFY_PROOF);
     return;
     }
   }
   setNextState(STATE_GET_CHANNEL_ID);
}

void Fs0rttClientHandshaker::DoReceiveSHLO(const CryptoHandshakeMessage *in,
                                         QuicCryptoClientConfig::CachedState *cached) {
  setNextState(STATE_NONE);
  // We sent a CHLO that we expected to be accepted and now we're
  // hoping for a SHLO from the server to confirm that.  First check
  // to see whether the response was a reject, and if so, move on to
  // the reject-processing state.
  if ((in->tag() == kREJ) || (in->tag() == kSREJ)) {
    DVLOG(1)<< "Waited for SHLO got an Rejection";
    // alternative_decrypter will be nullptr if the original alternative
    // decrypter latched and became the primary decrypter. That happens
    // if we received a message encrypted with the INITIAL key.
    if (session()->connection()->alternative_decrypter() == nullptr) {
      // The rejection was sent encrypted!
      stream()->CloseConnectionWithDetails(
          QUIC_CRYPTO_ENCRYPTION_LEVEL_INCORRECT, "encrypted REJ message");
      return;
    }
    setNextState(STATE_RECV_REJ);
    return;
  }

  if (in->tag() != kSHLO) {
    DVLOG(1)<< "Didn't receive a SHLO or a REJ. Abort Connection";
    stream()->CloseConnectionWithDetails(QUIC_INVALID_CRYPTO_MESSAGE_TYPE,
                                         "Expected SHLO or REJ");
    return;
  }

  // alternative_decrypter will be nullptr if the original alternative
  // decrypter latched and became the primary decrypter. That happens
  // if we received a message encrypted with the INITIAL key.
  if (session()->connection()->alternative_decrypter() != nullptr) {
    // The server hello was sent without encryption.
    DVLOG(1)<< "alternative decrypter is nullptr. Abort Connection";
    stream()->CloseConnectionWithDetails(QUIC_CRYPTO_ENCRYPTION_LEVEL_INCORRECT,
                                         "unencrypted SHLO message");
    return;
  }

  QuicString error_details;
  DVLOG(1)<< "start SHLO processing";
  QuicErrorCode error = crypto_config_->ProcessServerHello(
      *in, session()->connection()->connection_id(),
      session()->connection()->version(),
      session()->connection()->server_supported_versions(), cached,
      crypto_negotiated_params_ptr(), &error_details);

  if (error != QUIC_NO_ERROR) {
    DVLOG(1)<< "SHLO processing with an error: "<< error_details;
    stream()->CloseConnectionWithDetails(
        error, "Server hello invalid: " + error_details);
    return;
  }

  error = session()->config()->ProcessPeerHello(*in, SERVER, &error_details);
  if (error != QUIC_NO_ERROR) {
    DVLOG(1)<< "ProcessPeerHello ended with an Error: "<<error_details;
    stream()->CloseConnectionWithDetails(
        error, "Server hello invalid: " + error_details);
    return;
  }

  session()->OnConfigNegotiated();

  CrypterPair* crypters = &crypto_negotiated_params().forward_secure_crypters;
  // has been floated that the server shouldn't send packets encrypted
  // with the FORWARD_SECURE key until it receives a FORWARD_SECURE
  // packet from the client.
  session()->connection()->SetAlternativeDecrypter(
      ENCRYPTION_FORWARD_SECURE, std::move(crypters->decrypter),
      false /* don't latch */);
  session()->connection()->SetEncrypter(ENCRYPTION_FORWARD_SECURE,
                                        std::move(crypters->encrypter));
  session()->connection()->SetDefaultEncryptionLevel(ENCRYPTION_FORWARD_SECURE);

  setHandshake_confirmed(true);
  DVLOG(1)<< "Handshake confirmed";
  session()->OnCryptoHandshakeEvent(QuicSession::HANDSHAKE_CONFIRMED);
  session()->connection()->OnHandshakeComplete();
}

}  // namespace quic