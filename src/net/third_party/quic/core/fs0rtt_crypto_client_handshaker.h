// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef NET_THIRD_PARTY_QUIC_CORE_ZERO_CLIENT_HANDSHAKER_H_
#define NET_THIRD_PARTY_QUIC_CORE_ZERO_CLIENT_HANDSHAKER_H_

#include "net/third_party/quic/core/crypto/channel_id.h"
#include "net/third_party/quic/core/crypto/proof_verifier.h"
#include "net/third_party/quic/core/crypto/quic_crypto_client_config.h"
#include "net/third_party/quic/core/crypto/fs0rtt_crypto_client_config.h"
#include "net/third_party/quic/core/quic_crypto_client_stream.h"
#include "net/third_party/quic/core/quic_server_id.h"
#include "net/third_party/quic/platform/api/quic_export.h"
#include "net/third_party/quic/platform/api/quic_string.h"
#include "net/third_party/quic/core/quic_crypto_client_handshaker.h"

namespace quic {

class QUIC_EXPORT_PRIVATE Fs0rttClientHandshaker :
    public QuicCryptoClientHandshaker {
 public:

  Fs0rttClientHandshaker(
        const QuicServerId &server_id,
        QuicCryptoClientStream *stream,
        QuicSession *session,
        ProofVerifyContext *verify_context,
        Fs0rttCryptoClientConfig *z_crypto_config,
        QuicCryptoClientStream::ProofHandler *proof_handler);

  ~Fs0rttClientHandshaker() override;

  // From QuicCryptoHandshaker
  void OnHandshakeMessage(const CryptoHandshakeMessage &message) override;

  // From QuicCryptoClientStream::HandshakerDelegate
  bool CryptoConnect() override;
  
  
 protected: 
  
  // DoHandshakeLoop performs a step of the handshake state machine. Note that
  // |in| may be nullptr if the call did not result from a received message.
  void DoHandshakeLoop(const CryptoHandshakeMessage *in) override;

 private:

  //creates a rough chlo which is needed in a inchoate CHLO and a CHLO
  bool InitializeSendingCHLO(CryptoHandshakeMessage &out);

  // Send a ClientHello message to the server.
  void DoSendCHLO(QuicCryptoClientConfig::CachedState *cached);

  // Send a InchoateClientHello message to the server.
  void DoSendInchoateCHLO(QuicCryptoClientConfig::CachedState *cached);

  // Process REJ message from the server.
  void DoReceiveREJ(const CryptoHandshakeMessage *in,
                    QuicCryptoClientConfig::CachedState *cached);

  // Process SHLO message from the server.
  void DoReceiveSHLO(const CryptoHandshakeMessage *in,
                     QuicCryptoClientConfig::CachedState *cached);
  //Checks if a 0RTT connection would be possible
  void CheckFor0RTT(QuicCryptoClientConfig::CachedState *cached);



};
}  // namespace quic

#endif  // NET_THIRD_PARTY_QUIC_CORE_ZERO_CLIENT_HANDSHAKER_H_
