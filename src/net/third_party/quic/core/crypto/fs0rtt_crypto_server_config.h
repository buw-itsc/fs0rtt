//
// Created by pg2018 on 19.08.18.
//


#ifndef  NET_THIRD_PARTY_QUIC_CORE_CRYPTO_ZERO_CRYPTO_SERVER_CONFIG_H
#define  NET_THIRD_PARTY_QUIC_CORE_CRYPTO_ZERO_CRYPTO_SERVER_CONFIG_H

#include <memory>
#include <stddef.h>
#include "net/third_party/quic/core/crypto/quic_crypto_server_config.h"

#include <crypto_wrapper/wrapper.h>

namespace quic {

class QUIC_EXPORT_PRIVATE Fs0rttCryptoServerConfig :
        public QuicCryptoServerConfig {
 public:

  // similar to QuicCryptoServerConfig, bfe_m and bfe_k are parameters for the BFE crypto
  Fs0rttCryptoServerConfig(QuicStringPiece source_address_token_secret,
                      QuicRandom* server_nonce_entropy,
                      std::unique_ptr<ProofSource> proof_source,
                      bssl::UniquePtr<SSL_CTX> ssl_ctx,
                      unsigned int bfe_m,
                      unsigned int bfe_k);
  // default constructor
  Fs0rttCryptoServerConfig(QuicStringPiece source_address_token_secret,
                         QuicRandom* server_nonce_entropy,
                         std::unique_ptr<ProofSource> proof_source,
                         bssl::UniquePtr<SSL_CTX> ssl_ctx);

  ~Fs0rttCryptoServerConfig();

 protected:
    // used to distinguish QUIC server config and FS0RTT server config
    void who_am_i() const override;
    bool isZeroServerConfig()const override{return true;};

    // instead of the SCFG, the server's public key is signed
    const QuicString& getDataToSign()const  override {return sepk_;}



 private:
    // Portion of ProcessClientHello which executes after GetProof.
    void ProcessClientHelloAfterGetProof(
        bool found_error,
        std::unique_ptr<ProofSource::Details> proof_source_details,
        const ValidateClientHelloResultCallback::Result& validate_chlo_result,
        bool reject_only,
        QuicConnectionId connection_id,
        const QuicSocketAddress& client_address,
        ParsedQuicVersion version,
        const ParsedQuicVersionVector& supported_versions,
        bool use_stateless_rejects,
        QuicConnectionId server_designated_connection_id,
        const QuicClock* clock,
        QuicRandom* rand,
        QuicCompressedCertsCache* compressed_certs_cache,
        QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> params,
        QuicReferenceCountedPointer<QuicSignedServerConfig> crypto_proof,
        QuicByteCount total_framing_overhead,
        QuicByteCount chlo_packet_size,
        const QuicReferenceCountedPointer<Config>& requested_config,
        const QuicReferenceCountedPointer<Config>& primary_config,
        std::unique_ptr<ProcessClientHelloResultCallback> done_cb) const override;

    // BuildRejection sets |out| to be a REJ message in reply to |client_hello|.
    void BuildRejection(
            QuicTransportVersion version,
            QuicWallTime now,
            const Config& config,
            const CryptoHandshakeMessage& client_hello,
            const ClientHelloInfo& info,
            const CachedNetworkParameters& cached_network_params,
            bool use_stateless_rejects,
            QuicConnectionId server_designated_connection_id,
            QuicRandom* rand,
            QuicCompressedCertsCache* compressed_certs_cache,
            QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> params,
            const QuicSignedServerConfig& crypto_proof,
            QuicByteCount total_framing_overhead,
            QuicByteCount chlo_packet_size,
            CryptoHandshakeMessage* out) const override;

    QuicString NewSourceAddressToken(
            const Config& config,
            const SourceAddressTokens& previous_tokens,
            const QuicIpAddress& ip,
            QuicRandom* rand,
            QuicWallTime now,
            const CachedNetworkParameters* cached_network_params) const override;

    // NewServerNonce generates and encrypts a random nonce.
    QuicString NewServerNonce(QuicRandom* rand, QuicWallTime now) const override;

    // Returns HANDSHAKE_OK if the source address token in |token| is a timely
    // token given that the current time is |now|. Otherwise it returns the
    // reason for failure.
    HandshakeFailureReason ValidateSourceAddressTokenTimestamp(
            const SourceAddressToken& token,
            QuicWallTime now) const override;

    // CompressChain compresses the certificates in |chain->certs| and returns a
    // compressed representation. |common_sets| contains the common certificate
    // sets known locally and |client_common_set_hashes| contains the hashes of
    // the common sets known to the peer. |client_cached_cert_hashes| contains
    // 64-bit, FNV-1a hashes of certificates that the peer already possesses.
    static QuicString CompressChain(
            QuicCompressedCertsCache* compressed_certs_cache,
            const QuicReferenceCountedPointer<ProofSource::Chain>& chain,
            const QuicString& client_common_set_hashes,
            const QuicString& client_cached_cert_hashes,
            const CommonCertSets* common_sets);

    // Returns true if the PDMD field from the client hello demands an X509
    // certificate.
    bool ClientDemandsX509Proof(const CryptoHandshakeMessage& client_hello) const override;

    // public key of server is initialized
    void initSEPK();

    void* bfeIBBE_ = nullptr; // server BFE object used for crypto operations
    QuicString sepk_; // server public key for encapsulation

    DISALLOW_COPY_AND_ASSIGN(Fs0rttCryptoServerConfig);

};

}  // namespace quic

#endif  // NET_THIRD_PARTY_QUIC_CORE_CRYPTO_ZERO_CRYPTO_SERVER_CONFIG_H
