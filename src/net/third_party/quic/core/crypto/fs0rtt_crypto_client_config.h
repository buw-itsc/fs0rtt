//
// Created by pg2018 on 09.08.18.
//

#ifndef NET_THIRD_PARTY_QUIC_CORE_CRYPTO_ZERO_CRYPTO_CLIENT_CONFIG_H_
#define NET_THIRD_PARTY_QUIC_CORE_CRYPTO_ZERO_CRYPTO_CLIENT_CONFIG_H_

#include "net/third_party/quic/core/crypto/quic_crypto_client_config.h"

#include <crypto_wrapper/wrapper.h>
#include <stddef.h>

namespace quic {

// Fs0rttCryptoClientConfig contains crypto-related configuration settings for a
// client. Note that this object isn't thread-safe. It's designed to be used on
// a single thread at a time.
class QUIC_EXPORT_PRIVATE Fs0rttCryptoClientConfig : public QuicCryptoClientConfig {
 public:

  class QUIC_EXPORT_PRIVATE Fs0rttCachedState: public CachedState {
   public:

    Fs0rttCachedState();
    ~Fs0rttCachedState() override;

    // IsComplete returns true if this object contains enough information to
    // perform a handshake with the server. |now| is used to judge whether any
    // cached server config has expired.
    bool IsComplete(QuicWallTime now) const override;

    // IsEmpty returns true if |sepk_| is empty.
    bool IsEmpty() const override;

    // Clears all the data.
    void Clear()override;

    virtual const QuicString& getSignedData()const override;


   private:

    void* bfeClient_;
    QuicString sepk_;
    bool isInitialized_;

   public:
    //Returns the cached BFE object
    void* bfeClient();
    //Creates with the given server public key a BFE object and caches it
    QuicErrorCode InitializeBfe(QuicStringPiece sepk, QuicString version);
    //Checks if the BFE object is initialized
    bool getInitialized(){return isInitialized_;};

    DISALLOW_COPY_AND_ASSIGN(Fs0rttCachedState);
  };

  Fs0rttCryptoClientConfig(std::unique_ptr<ProofVerifier> proof_verifier,
  bssl::UniquePtr<SSL_CTX> ssl_ctx);

  ~Fs0rttCryptoClientConfig();

  //Fills a given Handshakemessage with all important data for a first connection
  void FillInchoateClientHello(const QuicServerId &server_id,
                               const ParsedQuicVersion preferred_version,
                               const CachedState *cached,
                               QuicRandom *rand,
                               bool demand_x509_proof,
                               QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                               CryptoHandshakeMessage *out) const override;
  //Fills a given Handshakemessage with all important data for a 0-RTT connection
  QuicErrorCode FillClientHello(const QuicServerId &server_id,
                                QuicConnectionId connection_id,
                                const ParsedQuicVersion preferred_version,
                                CachedState *cached,
                                QuicWallTime now,
                                QuicRandom *rand,
                                const ChannelIDKey *channel_id_key,
                                QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                                CryptoHandshakeMessage *out,
                                QuicString *error_details) override;

  //Handles a received REJ message from the server
  QuicErrorCode ProcessRejection(const CryptoHandshakeMessage &rej,
                                 QuicWallTime now,
                                 QuicTransportVersion version,
                                 QuicStringPiece chlo_hash,
                                 CachedState *cached,
                                 QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                                 QuicString *error_details) override;

  // CacheBloomFilterEncryption checks for SEPK, STK, PROF, and CRT tags in |message|,
  // verifies them, and stores them in the cached state if they validate.
  // This is used on receipt of a REJ from a server
  QuicErrorCode CacheNewBloomFilterEncryption(
      const CryptoHandshakeMessage& message,
      const QuicTransportVersion version,
      QuicStringPiece chlo_hash,
      const std::vector<QuicString>& cached_certs,
      Fs0rttCachedState* cached,
      QuicString* error_details);

  //Handles a received SHLO message from the server
  QuicErrorCode ProcessServerHello(const CryptoHandshakeMessage &server_hello,
                                   QuicConnectionId connection_id,
                                   ParsedQuicVersion version,
                                   const ParsedQuicVersionVector &negotiated_versions,
                                   CachedState *cached,
                                   QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                                   QuicString *error_details) override;

  // creates a CachedState
  QuicCryptoClientConfig::CachedState* createCachedState()override;


private:

};
}
#endif //NET_THIRD_PARTY_QUIC_CORE_CRYPTO_ZERO_CRYPTO_CLIENT_CONFIG_H_
