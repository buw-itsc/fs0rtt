//
// Created by pg2018 on 19.08.18.
//
#include "net/third_party/quic/core/crypto/fs0rtt_crypto_server_config.h"

#include <stdlib.h>

#include <utility>
#include <algorithm>
#include <memory>
#include <iostream>

#include "base/macros.h"
#include "crypto/hkdf.h"
#include "net/third_party/quic/core/crypto/aes_128_gcm_12_decrypter.h"
#include "net/third_party/quic/core/crypto/aes_128_gcm_12_encrypter.h"
#include "net/third_party/quic/core/crypto/cert_compressor.h"
#include "net/third_party/quic/core/crypto/chacha20_poly1305_encrypter.h"
#include "net/third_party/quic/core/crypto/channel_id.h"
#include "net/third_party/quic/core/crypto/crypto_framer.h"
#include "net/third_party/quic/core/crypto/crypto_handshake_message.h"
#include "net/third_party/quic/core/crypto/crypto_server_config_protobuf.h"
#include "net/third_party/quic/core/crypto/crypto_utils.h"
#include "net/third_party/quic/core/crypto/curve25519_key_exchange.h"
#include "net/third_party/quic/core/crypto/ephemeral_key_source.h"
#include "net/third_party/quic/core/crypto/key_exchange.h"
#include "net/third_party/quic/core/crypto/p256_key_exchange.h"
#include "net/third_party/quic/core/crypto/proof_source.h"
#include "net/third_party/quic/core/crypto/quic_decrypter.h"
#include "net/third_party/quic/core/crypto/quic_encrypter.h"
#include "net/third_party/quic/core/crypto/quic_random.h"
#include "net/third_party/quic/core/proto/source_address_token.pb.h"
#include "net/third_party/quic/core/quic_packets.h"
#include "net/third_party/quic/core/quic_socket_address_coder.h"
#include "net/third_party/quic/core/quic_utils.h"
#include "net/third_party/quic/platform/api/quic_bug_tracker.h"
#include "net/third_party/quic/platform/api/quic_clock.h"
#include "net/third_party/quic/platform/api/quic_endian.h"
#include "net/third_party/quic/platform/api/quic_fallthrough.h"
#include "net/third_party/quic/platform/api/quic_flags.h"
#include "net/third_party/quic/platform/api/quic_hostname_utils.h"
#include "net/third_party/quic/platform/api/quic_logging.h"
#include "net/third_party/quic/platform/api/quic_reference_counted.h"
#include "net/third_party/quic/platform/api/quic_string.h"
#include "net/third_party/quic/platform/api/quic_text_utils.h"
#include "third_party/boringssl/src/include/openssl/sha.h"
#include "third_party/boringssl/src/include/openssl/ssl.h"

#include <crypto_wrapper/wrapper.h>

using std::string;

namespace quic {

// kMultiplier is the multiple of the CHLO message size that a REJ message
// must stay under when the client doesn't present a valid source-address
// token. This is used to protect QUIC from amplification attacks.
// TODO(rch): Reduce this to 2 again once b/25933682 is fixed.
const size_t kMultiplier = 3;

const int kMaxTokenAddresses = 4;

// kServerNoncePlaintextSize is the number of bytes in an unencrypted server
// nonce.
static const size_t kServerNoncePlaintextSize =
    4 /* timestamp */ + 20 /* random bytes */;

Fs0rttCryptoServerConfig::Fs0rttCryptoServerConfig(
    QuicStringPiece source_address_token_secret,
    QuicRandom *server_nonce_entropy,
    std::unique_ptr<ProofSource> proof_source,
    bssl::UniquePtr<SSL_CTX> ssl_ctx,
    unsigned int bfe_m,
    unsigned int bfe_k) : QuicCryptoServerConfig(
    source_address_token_secret,
    server_nonce_entropy,
    std::move(proof_source),
    std::move(ssl_ctx)),
    bfeIBBE_(nullptr) {
    //TODO(Jonas) Implement HIBE
    DVLOG(1) << "m and k are: " << bfe_m << " and " << bfe_k;
  // Third parameter indicates that keys will be newly generated.
  // If the should be read in from file, set it to 1. Then, if the key files
  // exist, they will be read in. If they don't exist a new key pair will be
  // generated and written to the files.
    if(newBFEIBBE(&bfeIBBE_, bfe_m, bfe_k, 1)){
        DVLOG(1) << "Error while creating bfe ibbe";
    }
    DVLOG(1)<<"Keygen complete";
    initSEPK();
}

// used in some end to end tests
Fs0rttCryptoServerConfig::Fs0rttCryptoServerConfig(
    QuicStringPiece source_address_token_secret,
    QuicRandom *server_nonce_entropy,
    std::unique_ptr<ProofSource> proof_source,
    bssl::UniquePtr<SSL_CTX> ssl_ctx) : QuicCryptoServerConfig(
    source_address_token_secret,
    server_nonce_entropy,
    std::move(proof_source),
    std::move(ssl_ctx)),
    bfeIBBE_(nullptr) {
  DVLOG(1) << "m and k are: " << 100 << " and " << 3;
  // Third parameter indicates that keys will be newly generated.
  // If the should be read in from file, set it to 1. Then, if the key files
  // exist, they will be read in. If they don't exist a new key pair will be
  // generated and written to the files.
  if(newBFEIBBE(&bfeIBBE_, 100, 3, 0)){
    DVLOG(1) << "Error while creating bfe ibbe";
  }
  DVLOG(1)<<"Keygen complete";
  initSEPK();
}

Fs0rttCryptoServerConfig::~Fs0rttCryptoServerConfig() {
  if(bfeIBBE_!=nullptr)
  {
    PE_destroy(bfeIBBE_);
  }
}

class ProcessClientHelloHelper {
 public:
  explicit ProcessClientHelloHelper(
      std::unique_ptr<ProcessClientHelloResultCallback>* done_cb)
      : done_cb_(done_cb) {}

  ~ProcessClientHelloHelper() {
          QUIC_BUG_IF(done_cb_ != nullptr)
          << "Deleting ProcessClientHelloHelper with a pending callback.";
  }

  void Fail(QuicErrorCode error, const QuicString& error_details) {
      (*done_cb_)->Run(error, error_details, nullptr, nullptr, nullptr);
      DetachCallback();
  }

  void Succeed(std::unique_ptr<CryptoHandshakeMessage> message,
               std::unique_ptr<DiversificationNonce> diversification_nonce,
               std::unique_ptr<ProofSource::Details> proof_source_details) {
      (*done_cb_)->Run(QUIC_NO_ERROR, QuicString(), std::move(message),
                       std::move(diversification_nonce),
                       std::move(proof_source_details));
      DetachCallback();
  }

  void DetachCallback() {
          QUIC_BUG_IF(done_cb_ == nullptr) << "Callback already detached.";
      done_cb_ = nullptr;
  }

 private:
  std::unique_ptr<ProcessClientHelloResultCallback>* done_cb_;
};

void Fs0rttCryptoServerConfig::ProcessClientHelloAfterGetProof(
    bool found_error,
    std::unique_ptr<ProofSource::Details> proof_source_details,
    const ValidateClientHelloResultCallback::Result& validate_chlo_result,
    bool reject_only,
    QuicConnectionId connection_id,
    const QuicSocketAddress& client_address,
    ParsedQuicVersion version,
    const ParsedQuicVersionVector& supported_versions,
    bool use_stateless_rejects,
    QuicConnectionId server_designated_connection_id,
    const QuicClock* clock,
    QuicRandom* rand,
    QuicCompressedCertsCache* compressed_certs_cache,
    QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> params,
    QuicReferenceCountedPointer<QuicSignedServerConfig> signed_config,
    QuicByteCount total_framing_overhead,
    QuicByteCount chlo_packet_size,
    const QuicReferenceCountedPointer<Config>& requested_config,
    const QuicReferenceCountedPointer<Config>& primary_config,
    std::unique_ptr<ProcessClientHelloResultCallback> done_cb) const {
    connection_id = QuicEndian::HostToNet64(connection_id);

    ProcessClientHelloHelper helper(&done_cb);

    DVLOG(1) << "Currently in ProcessClientHelloAfterGetProof";

    if (found_error) {
        DVLOG(1) << "Inside found_error if clause";
        helper.Fail(QUIC_HANDSHAKE_FAILED, "Failed to get proof");
        return;
    }

    const CryptoHandshakeMessage& client_hello =
        validate_chlo_result.client_hello;
  ClientHelloInfo& info = const_cast<ClientHelloInfo&>( validate_chlo_result.info);
    std::unique_ptr<DiversificationNonce> out_diversification_nonce(
        new DiversificationNonce);
    DVLOG(1) << "Set client_hello and out_diversification_nonce";

    QuicStringPiece cert_sct;
    if (client_hello.GetStringPiece(kCertificateSCTTag, &cert_sct) &&
        cert_sct.empty()) {
        DVLOG(1) << "Inside of if clause for certificate tag";
        params->sct_supported_by_client = true;
    }

    DVLOG(1) << "Before creating out";
    std::unique_ptr<CryptoHandshakeMessage> out(new CryptoHandshakeMessage);
    if (!info.reject_reasons.empty() /*|| !requested_config.get()*/) {
        DVLOG(1) << "Reject reasons were not empty OR requested_config.get() didn't work";
        BuildRejection(version.transport_version, clock->WallNow(), *primary_config,
                       client_hello, info,
                       validate_chlo_result.cached_network_params,
                       use_stateless_rejects, server_designated_connection_id, rand,
                       compressed_certs_cache, params, *signed_config,
                       total_framing_overhead, chlo_packet_size, out.get());
        LOG(INFO) << "LATENCY: End building REJECTION";
      DVLOG(1) << "After BuildRejection";
        if (get_rejection_observer() != nullptr) {
            get_rejection_observer()->OnRejectionBuilt(info.reject_reasons, out.get());
        }
        helper.Succeed(std::move(out), std::move(out_diversification_nonce),
                       std::move(proof_source_details));
        return;
    }

  LOG(INFO) << "LATENCY: Start building server hello";

  DVLOG(1) << "Before Reject only";
    if (reject_only) {
        DVLOG(1) << "After Reject only  1";
        helper.Succeed(std::move(out), std::move(out_diversification_nonce),
                       std::move(proof_source_details));
        DVLOG(1) << "After Reject only  2";
        return;
    }

    DVLOG(1) << "Before SNI";
    if (!info.sni.empty()) {
        DVLOG(1) << "AFTER SNI 1";
        std::unique_ptr<char[]> sni_tmp(new char[info.sni.length() + 1]);
        memcpy(sni_tmp.get(), info.sni.data(), info.sni.length());
        sni_tmp[info.sni.length()] = 0;
        params->sni = QuicHostnameUtils::NormalizeHostname(sni_tmp.get());
        DVLOG(1) << "AFTER SNI 2";
    }

  // get tags from CHLO relevant for crypto
  DVLOG(1)<< "starting our crypto part in CHLO handling";
  QuicStringPiece ctxt;
  if (!client_hello.GetStringPiece(kCTXT, &ctxt)) {
      DVLOG(1)<< "Error: missing CTXT";
    helper.Fail(QUIC_INVALID_CRYPTO_MESSAGE_PARAMETER,
                "Missing CTXT");
      return;
  }
  QuicStringPiece bfes;
  if (!client_hello.GetStringPiece(kBFES, &bfes)) {
      DVLOG(1)<< "Error: Missing BFES";
    helper.Fail(QUIC_INVALID_CRYPTO_MESSAGE_PARAMETER,
                "Missing BFES");
      return;
  }
  if(!(bfes.as_string()=="IBBE")){
    DVLOG(1)<< "Error: unknown BFES: "<< bfes.as_string();
    helper.Fail(QUIC_INVALID_CRYPTO_MESSAGE_PARAMETER,
                "unknown BFES");
    return;
  }

  // preparing for decapsulation
  char * cCTXT = (char*)malloc(ctxt.as_string().length() * sizeof(char));
  DVLOG(1)<< "CTXT length: "<<ctxt.as_string().length() * sizeof(char);
  memcpy(cCTXT, ctxt.as_string().c_str(), ctxt.as_string().size());
  uint8_t* cipherText= reinterpret_cast<uint8_t*>(cCTXT);
  // decapsulate ciphertext
  DVLOG(1) << "starting decapsulate";
  uint8_t *sessionKeyServer = (uint8_t *) malloc(getSessionKeySize(bfeIBBE_) * sizeof(uint8_t));
  // if decapsulate fails, we send the client a rejection, because we could not retrieve a key
  if(decapsulate(bfeIBBE_, cipherText, sessionKeyServer)){
    DVLOG(1) << "Reject reasons: Decapsulate failed";
    info.reject_reasons.push_back(ZERO_SERVER_DECAPSULATE_FAILED);
    BuildRejection(version.transport_version, clock->WallNow(), *primary_config,
                   client_hello, info,
                   validate_chlo_result.cached_network_params,
                   use_stateless_rejects, server_designated_connection_id, rand,
                   compressed_certs_cache, params, *signed_config,
                   total_framing_overhead, chlo_packet_size, out.get());
    if (get_rejection_observer() != nullptr) {
      get_rejection_observer()->OnRejectionBuilt(info.reject_reasons, out.get());
    }
    helper.Succeed(std::move(out), std::move(out_diversification_nonce),
                   std::move(proof_source_details));
    DVLOG(1) << "Leaving CHLO handling with error: decapsulate failed";
    return;
  }
  DVLOG(1)<< "SessionkeyServer length: "<<getSessionKeySize(bfeIBBE_) * sizeof(uint8_t);
  DVLOG(1)<<"SessionKey: 0x"<< QuicTextUtils::HexEncode( (char*)sessionKeyServer /*sessionKey*/);
  DVLOG(1)<< "ending decapsulate";
  DVLOG(1)<< "ending our crypto part in CHLO handling";
  // operations from the crypto library are done now

  params->aead = kCC20;

  // used for the key derivation as an input, premaster secret
  QuicString session_key;
  session_key.assign((char*)sessionKeyServer, getSessionKeySize(bfeIBBE_));
  CryptoUtils::Diversification diversification = CryptoUtils::Diversification::Never();

  DVLOG(1)<<"Client nonce: 0x"<<QuicTextUtils::HexEncode(info.client_nonce)<<" Server nonce: 0x"<<QuicTextUtils::HexEncode(info.server_nonce);
  if (!CryptoUtils::DeriveKeys(
            session_key, params->aead, info.client_nonce,
            ""/*info.server_nonce*/, "", "",
            Perspective::IS_SERVER, diversification, &params->initial_crypters,
            &params->initial_subkey_secret)) {
    helper.Fail(QUIC_CRYPTO_SYMMETRIC_KEY_SETUP_FAILED,
            "Symmetric key setup failed");
    return;
  }
  if (!CryptoUtils::DeriveKeys(
            session_key, params->aead,
            info.client_nonce,
            ""/*shlo_nonce.empty() ? info.server_nonce : shlo_nonce*/, "",
            "", Perspective::IS_SERVER,
            CryptoUtils::Diversification::Never(),
            &params->forward_secure_crypters, &params->subkey_secret)) {
    helper.Fail(QUIC_CRYPTO_SYMMETRIC_KEY_SETUP_FAILED,
            "Symmetric key setup failed");
    return;
  }

  // freeing memmory and deleting sensitive data
  CryptoUtils::memzero(sessionKeyServer, getSessionKeySize(bfeIBBE_));
  // this may be a bit dirty, because we're ignoring the const qualifier of session_key.data(),
  // but otherwise we would have to rewrite lots of functions to get rid of the std::string
  CryptoUtils::memzero((void*) session_key.data(), session_key.size());
  free(sessionKeyServer);
  free(cipherText);

  out->set_tag(kSHLO);
  out->SetVersionVector(kVER, supported_versions);

  LOG(INFO) << "LATENCY: End building SHLO";
  helper.Succeed(std::move(out), std::move(out_diversification_nonce),
                 std::move(proof_source_details));
}

QuicString Fs0rttCryptoServerConfig::NewServerNonce(QuicRandom* rand,
                                                  QuicWallTime now) const {
    const uint32_t timestamp = static_cast<uint32_t>(now.ToUNIXSeconds());

    uint8_t server_nonce[kServerNoncePlaintextSize];
    static_assert(sizeof(server_nonce) > sizeof(timestamp), "nonce too small");
    server_nonce[0] = static_cast<uint8_t>(timestamp >> 24);
    server_nonce[1] = static_cast<uint8_t>(timestamp >> 16);
    server_nonce[2] = static_cast<uint8_t>(timestamp >> 8);
    server_nonce[3] = static_cast<uint8_t>(timestamp);
    rand->RandBytes(&server_nonce[sizeof(timestamp)],
                    sizeof(server_nonce) - sizeof(timestamp));

    return server_nonce_boxer_.Box(rand, QuicStringPiece(reinterpret_cast<char*>(server_nonce),
                                                         sizeof(server_nonce)));

}

void Fs0rttCryptoServerConfig::who_am_i()const
{
    DVLOG(1) << "I AM A ZEROSERVERCONFIG";
}


QuicString Fs0rttCryptoServerConfig::NewSourceAddressToken(
    const Config& config,
    const SourceAddressTokens& previous_tokens,
    const QuicIpAddress& ip,
    QuicRandom* rand,
    QuicWallTime now,
    const CachedNetworkParameters* cached_network_params) const {
    SourceAddressTokens source_address_tokens;
    SourceAddressToken* source_address_token = source_address_tokens.add_tokens();
    source_address_token->set_ip(ip.DualStacked().ToPackedString());
    source_address_token->set_timestamp(now.ToUNIXSeconds());
    if (cached_network_params != nullptr) {
        *(source_address_token->mutable_cached_network_parameters()) =
            *cached_network_params;
    }

    // Append previous tokens.
    for (const SourceAddressToken& token : previous_tokens.tokens()) {
        if (source_address_tokens.tokens_size() > kMaxTokenAddresses) {
            break;
        }

        if (token.ip() == source_address_token->ip()) {
            // It's for the same IP address.
            continue;
        }

        if (ValidateSourceAddressTokenTimestamp(token, now) != HANDSHAKE_OK) {
            continue;
        }

        *(source_address_tokens.add_tokens()) = token;
    }

    return config.source_address_token_boxer->Box(
        rand, source_address_tokens.SerializeAsString());
}

HandshakeFailureReason
Fs0rttCryptoServerConfig::ValidateSourceAddressTokenTimestamp(
    const SourceAddressToken& source_address_token,
    QuicWallTime now) const {
    const QuicWallTime timestamp(
        QuicWallTime::FromUNIXSeconds(source_address_token.timestamp()));
    const QuicTime::Delta delta(now.AbsoluteDifference(timestamp));

    if (now.IsBefore(timestamp) &&
        delta.ToSeconds() > get_source_address_token_future_secs()) {
        return SOURCE_ADDRESS_TOKEN_CLOCK_SKEW_FAILURE;
    }

    if (now.IsAfter(timestamp) &&
        delta.ToSeconds() > get_source_address_token_lifetime_secs()) {
        return SOURCE_ADDRESS_TOKEN_EXPIRED_FAILURE;
    }

    return HANDSHAKE_OK;
}

QuicString Fs0rttCryptoServerConfig::CompressChain(
    QuicCompressedCertsCache* compressed_certs_cache,
    const QuicReferenceCountedPointer<ProofSource::Chain>& chain,
    const QuicString& client_common_set_hashes,
    const QuicString& client_cached_cert_hashes,
    const CommonCertSets* common_sets) {
    // Check whether the compressed certs is available in the cache.
    DCHECK(compressed_certs_cache);
    const QuicString* cached_value = compressed_certs_cache->GetCompressedCert(
        chain, client_common_set_hashes, client_cached_cert_hashes);
    if (cached_value) {
        return *cached_value;
    }
    QuicString compressed =
        CertCompressor::CompressChain(chain->certs, client_common_set_hashes,
                                      client_cached_cert_hashes, common_sets);
    // Insert the newly compressed cert to cache.
    compressed_certs_cache->Insert(chain, client_common_set_hashes,
                                   client_cached_cert_hashes, compressed);
    return compressed;
}



void Fs0rttCryptoServerConfig::initSEPK()
{
  uint8_t *publicKey;
  publicKey = (uint8_t *) malloc(getPublicKeySize(bfeIBBE_) * sizeof(uint8_t));
  DVLOG(1)<< "SEPK lenght: "<<getPublicKeySize(bfeIBBE_) * sizeof(uint8_t);
  if(getPublicKey(bfeIBBE_, publicKey)){
    DVLOG(1)<<"Error in getting SEPK";
  }
  sepk_.assign((char *) publicKey, getPublicKeySize(bfeIBBE_));
  free(publicKey);
}

void Fs0rttCryptoServerConfig::BuildRejection(
    QuicTransportVersion version,
    QuicWallTime now,
    const Config& config,
    const CryptoHandshakeMessage& client_hello,
    const ClientHelloInfo& info,
    const CachedNetworkParameters& cached_network_params,
    bool use_stateless_rejects,
    QuicConnectionId server_designated_connection_id,
    QuicRandom* rand,
    QuicCompressedCertsCache* compressed_certs_cache,
    QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> params,
    const QuicSignedServerConfig& signed_config,
    QuicByteCount total_framing_overhead,
    QuicByteCount chlo_packet_size,
    CryptoHandshakeMessage* out) const {
  LOG(INFO) << "LATENCY: Start building REJECTION";

  DVLOG(1) << "Inside of BuildRejection";
    if (GetQuicReloadableFlag(enable_quic_stateless_reject_support) &&
        use_stateless_rejects) {
        QUIC_DVLOG(1) << "QUIC Crypto server config returning stateless reject "
                      << "with server-designated connection ID "
                      << server_designated_connection_id;
        out->set_tag(kSREJ);
        out->SetValue(kRCID,
                      QuicEndian::HostToNet64(server_designated_connection_id));
    } else {
        out->set_tag(kREJ);
    }
    out->SetStringPiece(
        kSourceAddressTokenTag,
        NewSourceAddressToken(config, info.source_address_tokens, info.client_ip,
                              rand, info.now, &cached_network_params));
    out->SetValue(kSTTL, config.expiry_time.AbsoluteDifference(now).ToSeconds());

  // setting tags for the BFE scheme to be used and the server's public key
  DVLOG(1)<< "starting our crypto part in REJ building";
  out->SetStringPiece(kBFES, "IBBE"); //Setting BFE mode to IBBE
  out->SetStringPiece(kSEPK, sepk_);  //Adding server public key to the message
  DVLOG(1)<< "ending our crypto part in REJ building";

    // Send client the reject reason for debugging purposes.
    DCHECK_LT(0u, info.reject_reasons.size());
    out->SetVector(kRREJ, info.reject_reasons);

    // The client may have requested a certificate chain.
    if (!ClientDemandsX509Proof(client_hello)) {
        QUIC_BUG << "x509 certificates not supported in proof demand";
        return;
    }

    DVLOG(1) << "Before checking for hashes";
    QuicStringPiece client_common_set_hashes;
    if (client_hello.GetStringPiece(kCCS, &client_common_set_hashes)) {
        params->client_common_set_hashes = string(client_common_set_hashes);
    }

    QuicStringPiece client_cached_cert_hashes;
    if (client_hello.GetStringPiece(kCCRT, &client_cached_cert_hashes)) {
        params->client_cached_cert_hashes = string(client_cached_cert_hashes);
    } else {
        params->client_cached_cert_hashes.clear();
    }

    DVLOG(1) << "Before cert chain compressing";
    const QuicString compressed =
        CompressChain(compressed_certs_cache, signed_config.chain,
                      params->client_common_set_hashes,
                      params->client_cached_cert_hashes, config.common_cert_sets);

    DCHECK_GT(chlo_packet_size, client_hello.size());
    // outdated for our implementation
    // kREJOverheadBytes is a very rough estimate of how much of a REJ
    // message is taken up by things other than the certificates.
    // STK: 56 bytes
    // SNO: 56 bytes
    // SCFG
    //   SCID: 16 bytes
    //   PUBS: 38 bytes
    const size_t kREJOverheadBytes = 166;
    // max_unverified_size is the number of bytes that the certificate chain,
    // signature, and (optionally) signed certificate timestamp can consume before
    // we will demand a valid source-address token.
    const size_t max_unverified_size =
        get_chlo_multiplier() * (chlo_packet_size - total_framing_overhead) -
            kREJOverheadBytes;
    static_assert(kClientHelloMinimumSize * kMultiplier >= kREJOverheadBytes,
                  "overhead calculation may underflow");
    bool should_return_sct =
        params->sct_supported_by_client && get_enable_serving_sct();
    const QuicString& cert_sct = signed_config.proof.leaf_cert_scts;
    const size_t sct_size = should_return_sct ? cert_sct.size() : 0;
    const size_t total_size =
        signed_config.proof.signature.size() + compressed.size() + sct_size;
    if (info.valid_source_address_token || total_size < max_unverified_size) {
        DVLOG(1) << "We have a valid source adress token";
        out->SetStringPiece(kCertificateTag, compressed);
        out->SetStringPiece(kPROF, signed_config.proof.signature);
        if (should_return_sct) {
            if (cert_sct.empty()) {
                QUIC_LOG_EVERY_N_SEC(WARNING, 60) << "SCT is expected but it is empty.";
            } else {
                out->SetStringPiece(kCertificateSCTTag, cert_sct);
            }
        }
    } else {
        QUIC_LOG_EVERY_N_SEC(WARNING, 60)
        << "Sending inchoate REJ for hostname: " << info.sni
        << " signature: " << signed_config.proof.signature.size()
        << " cert: " << compressed.size() << " sct:" << sct_size
        << " total: " << total_size << " max: " << max_unverified_size;
    }
}

bool Fs0rttCryptoServerConfig::ClientDemandsX509Proof(
    const CryptoHandshakeMessage& client_hello) const {
    QuicTagVector their_proof_demands;

    if (client_hello.GetTaglist(kPDMD, &their_proof_demands) != QUIC_NO_ERROR) {
        return false;
    }

    for (const QuicTag tag : their_proof_demands) {
        if (tag == kX509) {
            return true;
        }
    }
    return false;
}
}  // namespace quic