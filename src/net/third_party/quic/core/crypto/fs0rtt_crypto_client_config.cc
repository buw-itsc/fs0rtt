//
// Created by pg2018 on 09.08.18.
//

#include "net/third_party/quic/core/crypto/fs0rtt_crypto_client_config.h"

#include <algorithm>
#include <memory>

#include "base/metrics/histogram_macros.h"
#include "net/third_party/quic/core/crypto/cert_compressor.h"
#include "net/third_party/quic/core/crypto/chacha20_poly1305_encrypter.h"
#include "net/third_party/quic/core/crypto/channel_id.h"
#include "net/third_party/quic/core/crypto/common_cert_set.h"
#include "net/third_party/quic/core/crypto/crypto_framer.h"
#include "net/third_party/quic/core/crypto/crypto_utils.h"
#include "net/third_party/quic/core/crypto/curve25519_key_exchange.h"
#include "net/third_party/quic/core/crypto/key_exchange.h"
#include "net/third_party/quic/core/crypto/p256_key_exchange.h"
#include "net/third_party/quic/core/crypto/proof_verifier.h"
#include "net/third_party/quic/core/crypto/quic_encrypter.h"
#include "net/third_party/quic/core/crypto/quic_random.h"
#include "net/third_party/quic/core/quic_utils.h"
#include "net/third_party/quic/platform/api/quic_arraysize.h"
#include "net/third_party/quic/platform/api/quic_bug_tracker.h"
#include "net/third_party/quic/platform/api/quic_endian.h"
#include "net/third_party/quic/platform/api/quic_hostname_utils.h"
#include "net/third_party/quic/platform/api/quic_logging.h"
#include "net/third_party/quic/platform/api/quic_map_util.h"
#include "net/third_party/quic/platform/api/quic_ptr_util.h"
#include "net/third_party/quic/platform/api/quic_string.h"
#include "net/third_party/quic/platform/api/quic_text_utils.h"
#include "third_party/boringssl/src/include/openssl/ssl.h"

using std::string;

namespace quic {

Fs0rttCryptoClientConfig::Fs0rttCachedState::Fs0rttCachedState(): CachedState(), isInitialized_(false), bfeClient_(nullptr) {}

Fs0rttCryptoClientConfig::Fs0rttCachedState::~Fs0rttCachedState() {}

bool Fs0rttCryptoClientConfig::Fs0rttCachedState::IsComplete(QuicWallTime now) const {
  return isInitialized_ && bfeClient_!= nullptr;
}

bool Fs0rttCryptoClientConfig::Fs0rttCachedState::IsEmpty() const {
  return !isInitialized_;
}

//Clears all cached data needed for a 0-RTT connection
void Fs0rttCryptoClientConfig::Fs0rttCachedState::Clear() {
  DVLOG(1) << "Clearing Fs0rttCachedState";
  sepk_.clear();
  if(bfeClient_ != nullptr){
    PE_destroy(bfeClient_); //destroys the BFE object
    bfeClient_= nullptr;
  }
  isInitialized_=false;
  CachedState::Clear();
}

const QuicString& Fs0rttCryptoClientConfig::Fs0rttCachedState::getSignedData()const
{
  return sepk_;
}

Fs0rttCryptoClientConfig::Fs0rttCryptoClientConfig(
    std::unique_ptr<ProofVerifier> proof_verifier,
    bssl::UniquePtr<SSL_CTX> ssl_ctx)
    : QuicCryptoClientConfig(std::move(proof_verifier), std::move(ssl_ctx)) {
  DVLOG(1) << "Created a Zero Config";
}

Fs0rttCryptoClientConfig::~Fs0rttCryptoClientConfig() {}


void Fs0rttCryptoClientConfig::FillInchoateClientHello(const QuicServerId &server_id,
                                                     const ParsedQuicVersion preferred_version,
                                                     const QuicCryptoClientConfig::CachedState *cached,
                                                     QuicRandom *rand,
                                                     bool demand_x509_proof,
                                                     QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                                                     CryptoHandshakeMessage *out) const {
  DVLOG(1) << "Entering FillInchoateClientHello";
  out->set_tag(kCHLO);
  out->set_minimum_size(kClientHelloMinimumSize);

  // Server name indication. We only send SNI if it's a valid domain name, as
  // per the spec.
  if (QuicHostnameUtils::IsValidSNI(server_id.host())) {
    out->SetStringPiece(kSNI, server_id.host());
  }
  out->SetVersion(kVER, preferred_version);

  if (!get_user_agent_id().empty()) {
    out->SetStringPiece(kUAID, get_user_agent_id());
  }

  if (!get_alpn().empty()) {
    out->SetStringPiece(kALPN, get_alpn());
  }

  if (!cached->source_address_token().empty()) {
    out->SetStringPiece(kSourceAddressTokenTag, cached->source_address_token());
  }

  if (!demand_x509_proof) {
    DVLOG(1) << "leaving FillInchoateClientHello because x509 proof not needed";
    return;
  }
  DVLOG(1) << "x509 proof needed";
  char proof_nonce[32];
  rand->RandBytes(proof_nonce, QUIC_ARRAYSIZE(proof_nonce));
  out->SetStringPiece(
      kNONP, QuicStringPiece(proof_nonce, QUIC_ARRAYSIZE(proof_nonce)));

  out->SetVector(kPDMD, QuicTagVector{kX509});

  if (common_cert_sets) {
    out->SetStringPiece(kCCS, common_cert_sets->GetCommonHashes());
  }

  out->SetStringPiece(kCertificateSCTTag, "");

  const std::vector<QuicString>& certs = cached->certs();
  // We save |certs| in the QuicCryptoNegotiatedParameters so that, if the
  // client config is being used for multiple connections, another connection
  // doesn't update the cached certificates and cause us to be unable to
  // process the server's compressed certificate chain.
  out_params->cached_certs = certs;
  if (!certs.empty()) {
    std::vector<uint64_t> hashes;
    hashes.reserve(certs.size());
    for (std::vector<QuicString>::const_iterator i = certs.begin();
         i != certs.end(); ++i) {
      hashes.push_back(QuicUtils::FNV1a_64_Hash(*i));
    }
    out->SetVector(kCCRT, hashes);
  }
  DVLOG(1) << "leaving FillInchoateClientHello";
}
QuicErrorCode Fs0rttCryptoClientConfig::FillClientHello(const QuicServerId &server_id,
                                                      QuicConnectionId connection_id,
                                                      const ParsedQuicVersion preferred_version,
                                                      QuicCryptoClientConfig::CachedState *cached,
                                                      QuicWallTime now,
                                                      QuicRandom *rand,
                                                      const ChannelIDKey *channel_id_key,
                                                      QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                                                      CryptoHandshakeMessage *out,
                                                      QuicString *error_details) {
  DVLOG(1) << "Entering FillClientHello";
  DCHECK(error_details != nullptr);
  connection_id = QuicEndian::HostToNet64(connection_id);

  FillInchoateClientHello(server_id, preferred_version, cached, rand,
      /* demand_x509_proof= */ true, out_params, out);


  out->SetStringPiece(kCertificateSCTTag, "");
  out_params->aead=kCC20;

  //set key exchange method to Curve25519
  out_params->key_exchange=kC255;
  out->SetVector(kAEAD, QuicTagVector{out_params->aead});
  out->SetVector(kKEXS, QuicTagVector{out_params->key_exchange});

  //creates Client nonce with a random generated orbit
  char orbit_bytes[kOrbitSize];
  rand->RandBytes(orbit_bytes, sizeof(orbit_bytes));
  CryptoUtils::GenerateNonce(now, rand, QuicStringPiece(orbit_bytes, sizeof(orbit_bytes)), &out_params->client_nonce);
  out->SetStringPiece(kNONC, out_params->client_nonce);

  const std::vector<QuicString>& certs = cached->certs();
  if (certs.empty()) {
    *error_details = "No certs to calculate XLCT";
    DVLOG(1) << "leaving FillClientHello with an error: " << *error_details;
    return QUIC_CRYPTO_INTERNAL_ERROR;
  }
  out->SetValue(kXLCT, CryptoUtils::ComputeLeafCertHash(certs[0]));

  // preparing for encapsulation
  DVLOG(1)<< "Starting our crypto part in FillCHLO";

  Fs0rttCachedState* zcached=static_cast<Fs0rttCachedState*>(cached); //transforming to Fs0rttCachedState to get access to the BFE object
  void* client=zcached->bfeClient();
  out->SetStringPiece(kBFES, "IBBE");
  DVLOG(1)<< "sessionKeyClient lenght: "<<getSessionKeySize(client) * sizeof(uint8_t);
  uint8_t* sessionKeyClient = (uint8_t*)malloc(getSessionKeySize(client) * sizeof(uint8_t));
  uint8_t* cipherText = (uint8_t*)malloc(getCipherTextSize(client) * sizeof(uint8_t));
  DVLOG(1)<< "CTXT lenght: "<<getCipherTextSize(client) * sizeof(uint8_t);
  if(encapsulate(client, sessionKeyClient, cipherText)){ // encapsulate to generate a session key and the ciphertext which contains the session key
    DVLOG(1)<< "Error in encapsulate";
    zcached->Clear();
    *error_details="Error while encapsulating";
    return ZERO_ENCAPSULATE_FAILED;
  }

  DVLOG(1)<<"SessionKey: 0x"<< QuicTextUtils::HexEncode( (char*)sessionKeyClient /*sessionKey*/);
  QuicString ctxt;
  ctxt.assign((char *) cipherText, getCipherTextSize(client)); //transforms the uint8_t* pointer to a QuicString to send it
  out->SetStringPiece(kCTXT, ctxt);
  DVLOG(1)<< "Ending our crypto part in FillCHLO";
  // we're done with operations from the crypto library

  //TODO(Jonas): check if channel_id is needed
  /*
  if (channel_id_key) {
    // In order to calculate the encryption key for the CETV block we need to
    // serialise the client hello as it currently is (i.e. without the CETV
    // block). For this, the client hello is serialized without padding.
    const size_t orig_min_size = out->minimum_size();
    out->set_minimum_size(0);

    CryptoHandshakeMessage cetv;
    cetv.set_tag(kCETV);

    QuicString hkdf_input;
    const QuicData& client_hello_serialized =
        out->GetSerialized(Perspective::IS_CLIENT);
    hkdf_input.append(QuicCryptoConfig::kCETVLabel,
                      strlen(QuicCryptoConfig::kCETVLabel) + 1);
    hkdf_input.append(reinterpret_cast<char*>(&connection_id),
                      sizeof(connection_id));
    hkdf_input.append(client_hello_serialized.data(),
                      client_hello_serialized.length());
    hkdf_input.append(cached->server_config());

    QuicString key = channel_id_key->SerializeKey();
    QuicString signature;
    if (!channel_id_key->Sign(hkdf_input, &signature)) {
      *error_details = "Channel ID signature failed";
      return QUIC_INVALID_CHANNEL_ID_SIGNATURE;
    }

    cetv.SetStringPiece(kCIDK, key);
    cetv.SetStringPiece(kCIDS, signature);

    CrypterPair crypters;

    if (!CryptoUtils::DeriveKeys(out_params->initial_premaster_secret,
                                 out_params->aead, out_params->client_nonce,
                                 out_params->server_nonce, pre_shared_key_,
                                 hkdf_input, Perspective::IS_CLIENT,
                                 CryptoUtils::Diversification::Never(),
                                 &crypters, nullptr )) {
      *error_details = "Symmetric key setup failed";
      return QUIC_CRYPTO_SYMMETRIC_KEY_SETUP_FAILED;
    }

    const QuicData& cetv_plaintext = cetv.GetSerialized(Perspective::IS_CLIENT);
    const size_t encrypted_len =
        crypters.encrypter->GetCiphertextSize(cetv_plaintext.length());
    std::unique_ptr<char[]> output(new char[encrypted_len]);
    size_t output_size = 0;
    if (!crypters.encrypter->EncryptPacket(
        preferred_version.transport_version, 0 ,
        QuicStringPiece() ,
        cetv_plaintext.AsStringPiece(), output.get(), &output_size,
        encrypted_len)) {
      *error_details = "Packet encryption failed";
      return QUIC_ENCRYPTION_FAILURE;
    }

    out->SetStringPiece(kCETV, QuicStringPiece(output.get(), output_size));
    out->MarkDirty();

    out->set_minimum_size(orig_min_size);
  }
  */

  QuicString* subkey_secret = &out_params->initial_subkey_secret;

  DVLOG(1) << "Derive Keys";
  QuicString session_key;
  session_key.assign((char*)sessionKeyClient, getSessionKeySize(client));
  if(!CryptoUtils::DeriveKeys(session_key,
                              out_params->aead, out_params->client_nonce,
                              "", "", "",
                              Perspective::IS_CLIENT,
                              CryptoUtils::Diversification::Never(),
                              &out_params->initial_crypters, subkey_secret))
  {
    *error_details = "Symmetric key setup failed";
    return QUIC_CRYPTO_SYMMETRIC_KEY_SETUP_FAILED;
  }

  if(!CryptoUtils::DeriveKeys(session_key,
                              out_params->aead, out_params->client_nonce,
                              "", "", "",
                              Perspective::IS_CLIENT,
                              CryptoUtils::Diversification::Never(),
                              &out_params->forward_secure_crypters, subkey_secret))
  {
    *error_details = "Symmetric key setup failed";
    return QUIC_CRYPTO_SYMMETRIC_KEY_SETUP_FAILED;
  }

  //free memory
  CryptoUtils::memzero(sessionKeyClient, getSessionKeySize(client));
  // this may be a bit dirty, because we're ignoring the const qualifier of session_key.data(),
  // but otherwise we would have to rewrite lots of functions to get rid of the std::string
  CryptoUtils::memzero((void*) session_key.data(), session_key.size());
  free(sessionKeyClient);
  free(cipherText);

  DVLOG(1) << "leaving FillClientHello";
  return QUIC_NO_ERROR;
}

QuicErrorCode Fs0rttCryptoClientConfig::ProcessRejection(const CryptoHandshakeMessage &rej,
                                                       QuicWallTime now,
                                                       QuicTransportVersion version,
                                                       QuicStringPiece chlo_hash,
                                                       QuicCryptoClientConfig::CachedState *cached,
                                                       QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                                                       QuicString *error_details) {


  DVLOG(1) << "Entering ProcessRejection";
  DCHECK(error_details != nullptr);

  if ((rej.tag() != kREJ) && (rej.tag() != kSREJ)) {
    *error_details = "Message is not REJ or SREJ";
    DVLOG(1) << "Leaving ProcessRejection with an error: " << *error_details;
    return QUIC_CRYPTO_INTERNAL_ERROR;
  }

  QuicErrorCode error =
      CacheNewBloomFilterEncryption(rej, version, chlo_hash,
                                    out_params->cached_certs, static_cast<Fs0rttCachedState*>(cached), error_details);
  if (error != QUIC_NO_ERROR) {
    DVLOG(1) << "Leaving ProcessRejection with an error: " << *error_details;
    return error;
  }

  if (rej.tag() == kSREJ) {
    QuicConnectionId connection_id;
    if (rej.GetUint64(kRCID, &connection_id) != QUIC_NO_ERROR) {
      *error_details = "Missing kRCID";
      DVLOG(1) << "leaving ProcessRejection with an error: " << *error_details;
      return QUIC_CRYPTO_MESSAGE_PARAMETER_NOT_FOUND;
    }
    connection_id = QuicEndian::NetToHost64(connection_id);
    cached->add_server_designated_connection_id(connection_id);
    DVLOG(1) << "leaving ProcessRejection";
    return QUIC_NO_ERROR;
  }
  DVLOG(1) << "leaving ProcessRejection";
  return QUIC_NO_ERROR;
}

QuicErrorCode Fs0rttCryptoClientConfig::CacheNewBloomFilterEncryption(
    const CryptoHandshakeMessage& message,
    QuicTransportVersion version,
    QuicStringPiece chlo_hash,
    const std::vector<QuicString>& cached_certs,
    Fs0rttCachedState* cached,
    QuicString* error_details) {
  DCHECK(error_details != nullptr);

  // retrieving tags related to our crypto operations
  DVLOG(1)<< "Entering our crypto part in REJ";
  QuicStringPiece sepk;
  if (!message.GetStringPiece(kSEPK, &sepk)) {
    *error_details = "Missing SEPK";
    DVLOG(1)<< "Error: "<< *error_details;
    return ZERO_MISSING_SEPK;
  }

  QuicStringPiece bfes;
  if (!message.GetStringPiece(kBFES, &bfes)) {
    *error_details = "Missing BFES";
    DVLOG(1)<< "Error: "<< *error_details;
    return QUIC_CRYPTO_MESSAGE_PARAMETER_NOT_FOUND;
  }
  QuicErrorCode error =cached->InitializeBfe(sepk,bfes.as_string());
  if(error!=QUIC_NO_ERROR){
    return error;
  }
  DVLOG(1)<< "Leaving our crypto part in REJ";

  QuicStringPiece token;
  if (message.GetStringPiece(kSourceAddressTokenTag, &token)) {
    cached->set_source_address_token(token);
  }

  QuicStringPiece proof, cert_bytes, cert_sct;
  bool has_proof = message.GetStringPiece(kPROF, &proof);
  bool has_cert = message.GetStringPiece(kCertificateTag, &cert_bytes);
  if (has_proof && has_cert) {
    std::vector<QuicString> certs;
    if (!CertCompressor::DecompressChain(cert_bytes, cached_certs,
                                         common_cert_sets, &certs)) {
      *error_details = "Certificate data invalid";
      return QUIC_INVALID_CRYPTO_MESSAGE_PARAMETER;
    }

    message.GetStringPiece(kCertificateSCTTag, &cert_sct);
    cached->SetProof(certs, cert_sct, chlo_hash, proof);
  } else {
    // Secure QUIC: clear existing proof as we have been sent a new SCFG
    // without matching proof/certs.
    cached->ClearProof();

    if (has_proof && !has_cert) {
      *error_details = "Certificate missing";
      return QUIC_INVALID_CRYPTO_MESSAGE_PARAMETER;
    }

    if (!has_proof && has_cert) {
      *error_details = "Proof missing";
      return QUIC_INVALID_CRYPTO_MESSAGE_PARAMETER;
    }
  }

  return QUIC_NO_ERROR;
}

QuicErrorCode Fs0rttCryptoClientConfig::ProcessServerHello(const CryptoHandshakeMessage &server_hello,
                                                         QuicConnectionId connection_id,
                                                         ParsedQuicVersion version,
                                                         const ParsedQuicVersionVector &negotiated_versions,
                                                         QuicCryptoClientConfig::CachedState *cached,
                                                         QuicReferenceCountedPointer<QuicCryptoNegotiatedParameters> out_params,
                                                         QuicString *error_details) {
  DCHECK(error_details != nullptr);

  QuicErrorCode valid = CryptoUtils::ValidateServerHello(
      server_hello, negotiated_versions, error_details);
  if (valid != QUIC_NO_ERROR) {
    return valid;
  }

  // Learn about updated source address tokens.
  QuicStringPiece token;
  if (server_hello.GetStringPiece(kSourceAddressTokenTag, &token)) {
    cached->set_source_address_token(token);
  }
  return QUIC_NO_ERROR;
}

QuicCryptoClientConfig::CachedState* Fs0rttCryptoClientConfig::createCachedState(){
  return new Fs0rttCachedState;
}

void* Fs0rttCryptoClientConfig::Fs0rttCachedState::bfeClient(){
  return bfeClient_;
}

QuicErrorCode Fs0rttCryptoClientConfig::Fs0rttCachedState::InitializeBfe(QuicStringPiece sepk, QuicString version) {
  if(!(version=="IBBE")){
    DVLOG(1)<<"Wrong BFES: " << version;
    isInitialized_ = false;
    return QUIC_INVALID_CRYPTO_MESSAGE_PARAMETER;
  }

  if(sepk==sepk_ && bfeClient_!= nullptr){
    // the SEPK is the same as the cached one and a BloomFilterEncryption object is already created
    return QUIC_NO_ERROR;
  }
  DVLOG(1) << "starting transforming sepk to unit8_t*";
  char * csepk = (char*)malloc(sepk.size() * sizeof(char));
  memcpy(csepk, sepk.data(), sepk.size());
  uint8_t* usepk=reinterpret_cast<uint8_t *>(csepk);
  DVLOG(1) << "ending transforming sepk to unit8_t*";
  if(bfeClient_!= nullptr){
    PE_destroy(bfeClient_);
  }
  if(BFEIBBEFromPublicKey(&bfeClient_,usepk)){
    DVLOG(1) << "Error while generating BFE-IBBE with Sepk: "<<sepk.as_string();
    isInitialized_ = false;
    free(csepk);
    return ZERO_BFE_CREATING_FAILED;
  }
  DVLOG(1) << "Initialize IBBE-Sepk with length "<<sepk.as_string().length();
  SetProofInvalid();
  sepk_=sepk.as_string();

  isInitialized_ = true;
  free(csepk);
  return QUIC_NO_ERROR;
}

}
